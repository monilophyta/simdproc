#!/bin/sh

TEST_BINARIES="native nohw sse2 avx2 fastc"

for tc in ${TEST_BINARIES}
do
    BIN="./test_build/tests/CMakeTargets/${tc}/simd_test_${tc}"
    if [ -f "${BIN}" ]
    then
        echo "${BIN}"
        ${BIN}
        ERR=${?}
        if [ ${ERR} -ne 0 ]
        then
            FAILED_TEST="${FAILED_TEST} ${tc}"
        fi
    else
        echo "Not test for ${tc}"
    fi
done


if [ -n "${FAILED_TEST}" ]
then
    echo "Failed tests: ${FAILED_TEST}"
    exit -1
fi

exit 0


