/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <numeric>
#include <simd.hpp>
#include "gtest/gtest.h"


using namespace smd;



template <typename vect_t, typename elem_t>
static void generic_sum_test();


TEST( ualigned_iter, sum_f32 ) 
{
    generic_sum_test<vf32_t,float32_t>();
}


TEST( ualigned_iter, sum_i32 ) 
{
    generic_sum_test<vi32_t,int32_t>();
}


namespace
{
    constexpr size_t NUM_VECTS = 732u;
    constexpr size_t ARRAY_SIZE = (vi32_t::num_items() * (NUM_VECTS+1)) - 1u;
}


template <typename vect_t, typename elem_t>
static void generic_sum_test()
{ 
    static const unsigned long l_check_sum = ((NUM_VECTS * vect_t::num_items() + 1) * ((NUM_VECTS * vect_t::num_items()))) >> 1u;
    
    elem_t l_dataArray[ARRAY_SIZE];

    elem_t* l_start = std::begin(l_dataArray);
    l_start += size_t( 0u == (reinterpret_cast<size_t>(l_start) & (sizeof(vect_t) - 1u)) );

    // check that array is not aligned
    ASSERT_NE( 0u, (reinterpret_cast<size_t>(l_start) & (sizeof(vect_t) - 1u)) );
    
    static const elem_t SHIFT(17);

    // fill array 
    std::iota( l_start, std::end(l_dataArray), SHIFT );

    // test iterator
    const smd::ualigned_iterator<vect_t> l_vstart(l_start);
    const smd::ualigned_iterator<vect_t> l_vend = l_vstart + NUM_VECTS;
    
    // shift back
    auto kernel = []( const vect_t& x ) -> vect_t { return (x+1u-SHIFT); };
    std::transform( l_vstart, l_vend, l_vstart, kernel );

    // calculate sum
    const vect_t l_vsum = std::accumulate( l_vstart, l_vstart + NUM_VECTS, vect_t(0) );

    // verify result
    EXPECT_EQ( l_check_sum, static_cast<unsigned long>( sum(l_vsum)) );
}


