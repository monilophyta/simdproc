/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "simd.hpp"
#include "gtest/gtest.h"
#include <limits>


using namespace smd;


static void check_equalness( const vf32_t& a, const vf32_t& b )
{
    for ( auto ait = a.cbegin(), bit = b.cbegin(); 
                ait != a.cend(); 
                ++ait, ++bit )
    {
        EXPECT_EQ( *ait, *bit );
    }

    EXPECT_TRUE( smd::all(a == b) );
}

//--------------------------------------------------------------------


TEST( CSimdLimitsTest, test_vf32_epsilon ) 
{ 
    vf32_t a = std::numeric_limits<vf32_t>::epsilon();
    vf32_t b( std::numeric_limits<float32_t>::epsilon() );

    auto c = std::numeric_limits<vf32_t>::epsilon();

    check_equalness( a, b );
    check_equalness( a, c );
}

TEST( CSimdLimitsTest, test_vf32_min ) 
{ 
    vf32_t a = std::numeric_limits<vf32_t>::min();
    vf32_t b( std::numeric_limits<float32_t>::min() );

    auto c = std::numeric_limits<vf32_t>::min();

    check_equalness( a, b );
    check_equalness( a, c );
}


TEST( CSimdLimitsTest, test_vf32_lowest ) 
{ 
    vf32_t a = std::numeric_limits<vf32_t>::lowest();
    vf32_t b( std::numeric_limits<float32_t>::lowest() );

    auto c = std::numeric_limits<vf32_t>::lowest();

    check_equalness( a, b );
    check_equalness( a, c );
}


TEST( CSimdLimitsTest, test_vf32_max ) 
{ 
    vf32_t a = std::numeric_limits<vf32_t>::max();
    vf32_t b( std::numeric_limits<float32_t>::max() );

    auto c = std::numeric_limits<vf32_t>::max();

    check_equalness( a, b );
    check_equalness( a, c );
}

//--------------------------------------------------------------------

TEST( CSimdLimitsTest, test_vi32_min ) 
{ 
    vi32_t a = std::numeric_limits<vi32_t>::min();
    vi32_t b( std::numeric_limits<int32_t>::min() );

    auto c = std::numeric_limits<vi32_t>::min();

    check_equalness( a, b );
    check_equalness( a, c );
}


TEST( CSimdLimitsTest, test_vi32_lowest ) 
{ 
    vi32_t a = std::numeric_limits<vi32_t>::lowest();
    vi32_t b( std::numeric_limits<int32_t>::lowest() );

    auto c = std::numeric_limits<vi32_t>::lowest();

    check_equalness( a, b );
    check_equalness( a, c );
}


TEST( CSimdLimitsTest, test_vi32_max ) 
{ 
    vi32_t a = std::numeric_limits<vi32_t>::max();
    vi32_t b( std::numeric_limits<int32_t>::max() );

    auto c = std::numeric_limits<vi32_t>::max();

    check_equalness( a, b );
    check_equalness( a, c );
}



