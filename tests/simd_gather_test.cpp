/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "simd.hpp"
#include "gtest/gtest.h"
#include <random>
#include <limits>


/*
 * Google Test: 
 * https://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 * http://cheezyworld.com/wp-content/uploads/2010/12/PlainGoogleQuickTestReferenceGuide1.pdf
 */


//#define NOEXCEPT_TEST noexcept
#define NOEXCEPT_TEST


using namespace smd;


class CSimdGatherTest : public ::testing::Test
{
protected:

    const size_t m_numElemInSimd     = vi32_t::num_items();
    
    const int m_randomSeed_u         = 669;
    const int m_repeats_u            = 300;

    const size_t m_testArraySizes[12]        = { 1, 2, 3, 4, 7, 8, 13, 43, 2033, 4095, 4096, 4097 };

    const size_t m_testMatrixRowColSizes[13] = { 1, 2, 3, 4, 7, 8, 13, 43, 126, 127, 128, 129, 130 };


    std::default_random_engine       m_rand;


    // -----------------------------------------------------------------------


    virtual void SetUp()
    {}

    virtual void TearDown()
    {}

    // -----------------------------------------------------------------------


    template <typename array_T, typename Sampler_T>
    void fillArray( array_T& f_array, Sampler_T& sampler ) NOEXCEPT_TEST;

    template <typename array_T>
    void verify_array_gather( 
            const array_T& f_array,
            const typename array_T::simd_elem_t& f_gather_vals, 
            const vi32_t& f_Idx_vi32 ) NOEXCEPT_TEST;

    template <typename array_T, typename Sampler_T>
    void testArrays( size_t f_size, Sampler_T& sampler ) NOEXCEPT_TEST;



    template <typename matrix_T, typename Sampler_T>
    void fillMatrix( matrix_T& f_matrix, Sampler_T& sampler ) NOEXCEPT_TEST;

    template <typename matrix_T>
    void verify_matrix_gather( 
            const matrix_T& f_matrix,
            const typename matrix_T::simd_elem_t& f_gather_vals, 
            const vi32_t& f_rowIdx_vi32,
            const vi32_t& f_colIdx_vi32 ) NOEXCEPT_TEST;

    template <typename matrix_T, typename Sampler_T>
    void testMatrices( size_t f_nRows, size_t f_nCols, Sampler_T& sampler ) NOEXCEPT_TEST;
    
    // -----------------------------------------------------------------------

public:

    CSimdGatherTest()
        : m_rand( m_randomSeed_u )
    {}

};


//---------------------------------------------------------------------------------------------------

TEST_F( CSimdGatherTest, testBoolArray ) 
{ 
    int64_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> bool32_t { return (3 > ((++sampleVal) % 4)); };
    
    for ( auto arraySizeIt = std::cbegin( m_testArraySizes ); 
               arraySizeIt != std::cend( m_testArraySizes );
               ++arraySizeIt )
    {
        testArrays<array_vb32_t>( *arraySizeIt, sampler );
    }

    ASSERT_GT( sampleVal, 0 );
}


TEST_F( CSimdGatherTest, testIntArray ) 
{ 
    int32_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> int32_t { return (++sampleVal); };
    
    for ( auto arraySizeIt = std::cbegin( m_testArraySizes ); 
               arraySizeIt != std::cend( m_testArraySizes );
               ++arraySizeIt )
    {
        testArrays<array_vi32_t>( *arraySizeIt, sampler );
    }

    ASSERT_GT( sampleVal, 0 );
}


TEST_F( CSimdGatherTest, testFloatArray ) 
{ 
    int64_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> float32_t { return static_cast<float32_t>( ++sampleVal ); };
    
    for ( auto arraySizeIt = std::cbegin( m_testArraySizes ); 
               arraySizeIt != std::cend( m_testArraySizes );
               ++arraySizeIt )
    {
        testArrays<array_vf32_t>( *arraySizeIt, sampler );
    }

    ASSERT_GT( sampleVal, 0 );
}

//---------------------------------------------------------------------------------------------------

TEST_F( CSimdGatherTest, testBoolMatrix ) 
{ 
    int64_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> bool32_t { return (3 > ((++sampleVal) % 4)); };
    
    for ( auto rowSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
               rowSizeIt != std::cend( m_testMatrixRowColSizes );
               ++rowSizeIt )
    {
        for ( auto colSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
                colSizeIt != std::cend( m_testMatrixRowColSizes );
                ++colSizeIt )
        {
            testMatrices<matrix_vb32_t>( *rowSizeIt, *colSizeIt, sampler );
        }
    }

    ASSERT_GT( sampleVal, 0 );
}


TEST_F( CSimdGatherTest, testIntMatrix ) 
{ 
    int32_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> int32_t { return (++sampleVal); };
    
    for ( auto rowSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
               rowSizeIt != std::cend( m_testMatrixRowColSizes );
               ++rowSizeIt )
    {
        for ( auto colSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
                colSizeIt != std::cend( m_testMatrixRowColSizes );
                ++colSizeIt )
        {
            testMatrices<matrix_vi32_t>( *rowSizeIt, *colSizeIt, sampler );
        }
    }

    ASSERT_GT( sampleVal, 0 );
}


TEST_F( CSimdGatherTest, testFloatMatrix ) 
{ 
    int64_t sampleVal = 0;
    auto sampler = [&sampleVal]() -> float32_t { return static_cast<float32_t>( ++sampleVal ); };
    
    for ( auto rowSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
               rowSizeIt != std::cend( m_testMatrixRowColSizes );
               ++rowSizeIt )
    {
        for ( auto colSizeIt = std::cbegin( m_testMatrixRowColSizes ); 
                colSizeIt != std::cend( m_testMatrixRowColSizes );
                ++colSizeIt )
        {
            testMatrices<matrix_vf32_t>( *rowSizeIt, *colSizeIt, sampler );
        }
    }

    ASSERT_GT( sampleVal, 0 );
}


//---------------------------------------------------------------------------------------------------


template <typename array_T, typename Sampler_T>
void CSimdGatherTest::fillArray( array_T& f_array, Sampler_T& sampler ) NOEXCEPT_TEST
{
    // TODO: also handle overlap!

    const auto it_end = f_array.elem().end();
    for ( auto it = f_array.elem().begin();
               it != it_end;
            ++it )
    {
        auto val = sampler();
        *it = val;
    }
}



template <typename array_T>
void CSimdGatherTest::verify_array_gather( 
        const array_T& f_array,
        const typename array_T::simd_elem_t& f_gather_vals, 
        const vi32_t& f_Idx_vi32 ) NOEXCEPT_TEST
{
    auto vit = f_gather_vals.cbegin();
    auto  it = f_Idx_vi32.cbegin();

    for (; vit != f_gather_vals.cend();
           ++vit, ++it )
    {
        ASSERT_EQ( f_array.elem( *it ), *vit );
    }
}



template <typename array_T, typename Sampler_T>
void CSimdGatherTest::testArrays( size_t f_size, Sampler_T& sampler ) NOEXCEPT_TEST
{
    //typedef typename array_T::elem_t      elem_t;
    typedef typename array_T::simd_elem_t simd_elem_t;
    
    array_T gen_array( f_size );
    fillArray( gen_array, sampler );

    const array_T& test_array = gen_array;

    std::uniform_int_distribution<int32_t> idxSampler( 0, f_size - 1 );

    /// TC
    for ( int n=0; n < m_repeats_u; ++n )
    {
        vi32_t idx;
        for ( auto it = idx.begin(); it != idx.end(); ++it )
        {
            *it = idxSampler( m_rand );
        }

        // Test call
        const simd_elem_t test_vals = gather( test_array.elem(), idx );

        // Verfiy Results
        verify_array_gather( test_array, test_vals, idx );
    }
}


//---------------------------------------------------------------------------------------------------


template <typename matrix_T, typename Sampler_T>
void CSimdGatherTest::fillMatrix( matrix_T& f_matrix, Sampler_T& sampler ) NOEXCEPT_TEST
{
    auto l_matrix_view = f_matrix.elem();
    
    for ( size_t rIdx = 0; rIdx < l_matrix_view.n_rows(); ++rIdx )
    {
        for ( auto it = l_matrix_view.begin(rIdx);
                   it != l_matrix_view.end(rIdx);
              ++it )
        {
            *it = sampler();
        }
    }
}


template <typename matrix_T>
void CSimdGatherTest::verify_matrix_gather( 
        const matrix_T& f_matrix,
        const typename matrix_T::simd_elem_t& f_gather_vals, 
        const vi32_t& f_rowIdx_vi32,
        const vi32_t& f_colIdx_vi32 ) NOEXCEPT_TEST
{
    auto vit = f_gather_vals.cbegin();
    auto rit = f_rowIdx_vi32.cbegin();
    auto cit = f_colIdx_vi32.cbegin();

    for (; vit != f_gather_vals.cend();
           ++vit, ++rit, ++cit )
    {
        ASSERT_EQ( f_matrix.elem( *rit, *cit ), *vit );
    }
}


template <typename matrix_T, typename Sampler_T>
void CSimdGatherTest::testMatrices( size_t f_nRows, size_t f_nCols, Sampler_T& sampler ) NOEXCEPT_TEST
{
    //typedef typename matrix_T::elem_t      elem_t;
    typedef typename matrix_T::simd_elem_t simd_elem_t;
    
    matrix_T gen_matrix( f_nRows, f_nCols );
    fillMatrix( gen_matrix, sampler );

    const matrix_T& test_matrix = gen_matrix;


    std::uniform_int_distribution<int32_t> rowSampler( 0, f_nRows - 1 );
    std::uniform_int_distribution<int32_t> colSampler( 0, f_nCols - 1 );

    /// TC1- single rowIdx - many colIdx
    for ( int n=0; n < m_repeats_u; ++n )
    {
        
        const int32_t rowIdx = rowSampler( m_rand );

        vi32_t colIdx;
        for ( auto it = colIdx.begin(); it != colIdx.end(); ++it )
        {
            *it = colSampler( m_rand );
        }

        // Test call
        const simd_elem_t testvals = gather( test_matrix.elem(), rowIdx, colIdx );

        // Verfiy Results
        verify_matrix_gather( test_matrix, testvals, vi32_t( rowIdx ), colIdx );
    }

    /// TC2- many rowIdx - single colIdx
    for ( int n=0; n < m_repeats_u; ++n )
    {
        
        const int32_t colIdx = colSampler( m_rand );

        vi32_t rowIdx;
        for ( auto it = rowIdx.begin(); it != rowIdx.end(); ++it )
        {
            *it = rowSampler( m_rand );
        }

        // Test call
        const simd_elem_t testvals = gather( test_matrix.elem(), rowIdx, colIdx );

        // Verfiy Results
        verify_matrix_gather( test_matrix, testvals, rowIdx, vi32_t(colIdx) );
    }

    /// TC3- many rowIdx - many colIdx
    for ( int n=0; n < m_repeats_u; ++n )
    {
        vi32_t rowIdx;
        for ( auto it = rowIdx.begin(); it != rowIdx.end(); ++it )
        {
            *it = rowSampler( m_rand );
        }

        vi32_t colIdx;
        for ( auto it = colIdx.begin(); it != colIdx.end(); ++it )
        {
            *it = colSampler( m_rand );
        }

        // Test call
        const simd_elem_t testvals = gather( test_matrix.elem(), rowIdx, colIdx );

        // Verfiy Results
        verify_matrix_gather( test_matrix, testvals, rowIdx, colIdx );
    }
}



