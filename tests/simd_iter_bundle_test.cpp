/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "simd.hpp"
#include "gtest/gtest.h"
#include <numeric>
#include <limits>
#include <algorithm>


using namespace smd;


template <class MATRIX_T>
class CIterBundleTest : public ::testing::Test
{
protected:

    const int m_randomSeed_u         = 69;

    const size_t n_rows              = 51;
    const size_t n_cols              = 37;

    // -----------------------------------------------------------------------

    typedef MATRIX_T matrix_t;

    matrix_t  m_src_matrix;
    matrix_t  m_trg_matrix;

    typedef typename matrix_t::value_type  value_type;


    virtual void SetUp()
    {}

    virtual void TearDown()
    {}


    template <typename T>
    static void iteration_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );

    template <typename T>
    static void algorithm_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );

    template <typename T>
    static void random_access_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );

    template <typename T>
    static void random_access_copy2_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );


    template <typename T>
    static void forward_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );

    template <typename T>
    static void backward_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );



    template <typename TITER_T, typename SITER_T>
    static void loop_copy( TITER_T& trg, SITER_T& src );

    template <typename TITER_T, typename SITER_T>
    static void iter_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter );

    template <typename TITER_T, typename SITER_T>
    static void algorithm_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter );

    template <typename TITER_T, typename SITER_T>
    static void random_access_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter );

    template <typename T>
    static void check_equalness( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view );

protected:

    CIterBundleTest()
        : m_src_matrix( n_rows, n_cols )
        , m_trg_matrix( n_rows, n_cols )
    {}
};



class CIterBundleTest_fundamental : public CIterBundleTest<matrix_i32_t>
{
public:

    CIterBundleTest_fundamental()
        : CIterBundleTest()
    {}

    typedef matrix_i32_t::elem_t elem_t;

    virtual void SetUp()
    {
        /// initialize src matrix
        std::iota( m_src_matrix.begin(), m_src_matrix.end(), 69 );
        
        /// initialize target matrix -> fill overlap as well
        std::fill( m_trg_matrix.begin(), 
                   m_trg_matrix.end(),
                   std::numeric_limits<elem_t>::max() );

        /// ensure both matrices are unequal
        for (size_t rIdx = 0; rIdx < n_rows; ++rIdx)
        {
            ASSERT_FALSE( std::equal( m_src_matrix.cbegin(rIdx), m_src_matrix.cend(rIdx),
                                      m_trg_matrix.cbegin(rIdx) ) );
        }
    }
};


class CIterBundleTest_simd : public CIterBundleTest<matrix_vi32_t>
{

    typedef matrix_vi32_t::elem_t           elem_t;
    typedef matrix_vi32_t::simd_elem_t simd_elem_t;

 
public:

    CIterBundleTest_simd()
        : CIterBundleTest()
    {}


    virtual void SetUp()
    {
        /// initialize src matrix including overlap 
        std::iota( m_src_matrix.elem().begin(), 
                   m_src_matrix.overlap().end(), 69 );

        
        /// initialize target matrix -> fill overlap as well
        std::fill( m_trg_matrix.begin(),
                   m_trg_matrix.end(),
                   std::numeric_limits<simd_elem_t>::max() );

        /// ensure both matrices are unequal
        for (size_t rIdx = 0; rIdx < n_rows; ++rIdx)
        {
            ASSERT_FALSE( std::equal( m_src_matrix.elem().cbegin(rIdx), m_src_matrix.elem().cend(rIdx),
                                      m_trg_matrix.elem().cbegin(rIdx) ) );
        }
    }
};



// -------------------------------------------------------------------------------------------------------------------------


TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_iteration_copy )
{
    iteration_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_algorithm_copy )
{
    algorithm_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_random_access_copy )
{
    random_access_copy_test( m_trg_matrix, m_src_matrix );
}

TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_random_access_copy2 )
{
    random_access_copy2_test( m_trg_matrix, m_src_matrix );
}

TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_forward_copy )
{
    forward_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_fundamental, test_matrix_raligned_backward_copy )
{
    backward_copy_test( m_trg_matrix, m_src_matrix );
}

// -----------------------------------------------------------------------------------------


TEST_F( CIterBundleTest_simd, test_matrix_raligned_iteration_copy )
{
    auto l_trg_view = m_trg_matrix.elem();
    iteration_copy_test( l_trg_view, m_src_matrix.elem() );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_algorithm_copy )
{
    auto l_trg_view = m_trg_matrix.elem();
    algorithm_copy_test( l_trg_view, m_src_matrix.elem() );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_random_access_copy )
{
    auto l_trg_view = m_trg_matrix.elem();
    random_access_copy_test( l_trg_view, m_src_matrix.elem() );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_random_access_copy2 )
{
    auto l_trg_view = m_trg_matrix.elem();
    random_access_copy2_test( l_trg_view, m_src_matrix.elem() );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_forward_copy )
{
    auto l_trg_view = m_trg_matrix.elem();
    forward_copy_test( l_trg_view, m_src_matrix.elem() );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_backward_copy )
{
    auto l_trg_view = m_trg_matrix.elem();
    backward_copy_test( l_trg_view, m_src_matrix.elem() );
}


// --------------------------------------------------------------------


TEST_F( CIterBundleTest_simd, test_matrix_raligned_iteration_simd_copy )
{
    iteration_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_algorithm_simd_copy )
{
    algorithm_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_random_access_simd_copy )
{
    random_access_copy_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_random_access_simd_copy2 )
{
    random_access_copy2_test( m_trg_matrix, m_src_matrix );
}


TEST_F( CIterBundleTest_simd, test_matrix_raligned_forward_simd_copy )
{
    forward_copy_test( m_trg_matrix, m_src_matrix );
}

TEST_F( CIterBundleTest_simd, test_matrix_raligned_backward_simd_copy )
{
    backward_copy_test( m_trg_matrix, m_src_matrix );
}

// -------------------------------------------------------------------------------------------------------------------------


template <class MATRIX_T>
template <typename TITER_T, typename SITER_T>
void CIterBundleTest<MATRIX_T>::loop_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter )
{
    for ( size_t idx = 0; idx < f_src_iter->size(); ++idx )
    {
        ASSERT_FALSE( smd::any( f_trg_iter->value(idx) == f_src_iter->value(idx) ) );

        if ( 0 == (idx % 3) )
        {
            f_trg_iter->value(idx) = f_src_iter->value(idx);
        }
        else
        {
            (*f_trg_iter)[idx] = (*f_src_iter)[idx];
        }

        EXPECT_TRUE( smd::all( f_trg_iter->value(idx) == f_src_iter->value(idx) ) );
    }
}


template <class MATRIX_T>
template <typename TITER_T, typename SITER_T>
void CIterBundleTest<MATRIX_T>::iter_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter )
{
    auto tit     = f_trg_iter->begin();
    auto sit_orig     = f_src_iter->begin();
    auto sit_orig_end = f_src_iter->end();

    auto sit = sit_orig;
    auto sit_end = sit_orig_end;

    while ( sit != sit_end ) 
    {
        ASSERT_FALSE( smd::any( *tit == *sit ) );
        
        *tit = *sit;

        EXPECT_TRUE( smd::all( *tit == *sit ) );

        ++tit; ++sit;
    }

    EXPECT_EQ( tit, f_trg_iter->end() );
}


template <class MATRIX_T>
template <typename TITER_T, typename SITER_T>
void CIterBundleTest<MATRIX_T>::algorithm_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter )
{
    auto tit     = f_trg_iter->begin();
    auto sit     = f_src_iter->begin();
    auto sit_end = f_src_iter->end();
    
    auto end4check = std::copy( sit, sit_end, tit );
    EXPECT_EQ( end4check, f_trg_iter->end() );
}



template <class MATRIX_T>
template <typename TITER_T, typename SITER_T>
void CIterBundleTest<MATRIX_T>::random_access_copy( TITER_T& f_trg_iter, SITER_T& f_src_iter )
{
    auto tit     = f_trg_iter->begin();
    auto tit_end = f_trg_iter->end();
    auto sit     = f_src_iter->begin();
    auto sit_end = f_src_iter->end();

    ASSERT_EQ( std::distance( tit, tit_end ), std::distance( sit, sit_end) );
    std::ptrdiff_t N = std::distance( sit, sit_end);
    //std::ptrdiff_t N = sit_end - sit;
    
    ASSERT_GE( N, 0 );
    ASSERT_EQ( N, static_cast<std::ptrdiff_t>( f_trg_iter->size() ) );

    for (std::ptrdiff_t idx = 0; idx < N; ++idx )
    {
        ASSERT_FALSE( smd::any( sit[idx] == tit[idx] ) );

        tit[idx] = sit[idx];

        EXPECT_TRUE( smd::all( sit[idx] == tit[idx] ) );
    }
}

// -------------------------------------------------------------------------------------------------------------------------


template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::check_equalness( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{
    /// ensure both matrices are equal now
    for (size_t rIdx = 0; rIdx < src_view.n_rows(); ++rIdx)
    {
        // count number of unequal items
        size_t num_equal = 0;

        for (auto its  = src_view.cbegin(rIdx), itt = trg_view.cbegin(rIdx);
                  its != src_view.cend(rIdx);
            ++its, ++itt  )
        {
            num_equal += (smd::all(*itt == *its)) ? 1 : 0;
        }
        
        EXPECT_EQ( src_view.n_cols(), num_equal );
    }
}

// -------------------------------------------------------------------------------------------------------------------------

template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::iteration_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    {
        /// initialize iterators
        auto           l_src_iter = src_view.cbegin_row_bundle();
        const auto l_src_iter_end = src_view.cend_row_bundle();

        auto           l_trg_iter = trg_view.begin_row_bundle();
        const auto l_trg_iter_end = trg_view.end_row_bundle();

        ASSERT_EQ( l_src_iter->size(), src_view.n_rows() );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_end->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_end->size() );

        /// copy content
        for (; (l_src_iter != l_src_iter_end) || (l_trg_iter != l_trg_iter_end);
            ++l_src_iter, ++l_trg_iter )
        {
            iter_copy( l_trg_iter, l_src_iter );
        }

        ASSERT_EQ( l_src_iter, l_src_iter_end );
        ASSERT_EQ( l_trg_iter, l_trg_iter_end );
    }

    check_equalness( trg_view, src_view );
}

// -------------------------------------------------------------------------------------------------------------------------

template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::random_access_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    {
        /// initialize iterators
        auto           l_src_iter = src_view.cbegin_row_bundle();
        const auto l_src_iter_end = src_view.cend_row_bundle();

        auto           l_trg_iter = trg_view.begin_row_bundle();
        const auto l_trg_iter_end = trg_view.end_row_bundle();

        ASSERT_EQ( l_src_iter->size(), src_view.n_rows() );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_end->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_end->size() );

        /// copy content
        for (; (l_src_iter != l_src_iter_end) || (l_trg_iter != l_trg_iter_end);
            ++l_src_iter, ++l_trg_iter )
        {
            random_access_copy( l_trg_iter, l_src_iter );
        }

        ASSERT_EQ( l_src_iter, l_src_iter_end );
        ASSERT_EQ( l_trg_iter, l_trg_iter_end );
    }

    /// ensure both matrices are equal now
    check_equalness( trg_view, src_view );
}


template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::random_access_copy2_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    {
        /// initialize iterators
        auto           l_src_iter = src_view.cbegin_row_bundle();
        const auto l_src_iter_end = src_view.cend_row_bundle();

        auto           l_trg_iter = trg_view.begin_row_bundle();
        const auto l_trg_iter_end = trg_view.end_row_bundle();

        const size_t l_nRows = src_view.n_rows();
        const size_t l_nCols = src_view.n_cols();

        ASSERT_EQ( l_src_iter->size(), l_nRows );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_end->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_end->size() );

        ASSERT_EQ( static_cast<std::ptrdiff_t>( l_nCols ), l_trg_iter_end -  l_trg_iter );
        ASSERT_EQ( static_cast<std::ptrdiff_t>( l_nCols ), l_src_iter_end -  l_src_iter );

        /// copy content
        for ( size_t c = 0; c < l_nCols; ++c )
        {
            for ( size_t r = 0; r < l_nRows; ++r )
            {
                l_trg_iter[c][r] = l_src_iter[c][r];
            }
        }
    }

    /// ensure both matrices are equal now
    check_equalness( trg_view, src_view );
}


// -------------------------------------------------------------------------------------------------------------------------

template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::algorithm_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    {
        /// initialize iterators
        auto           l_src_iter = src_view.cbegin_row_bundle();
        const auto l_src_iter_end = src_view.cend_row_bundle();

        auto           l_trg_iter = trg_view.begin_row_bundle();
        const auto l_trg_iter_end = trg_view.end_row_bundle();

        ASSERT_EQ( l_src_iter->size(), src_view.n_rows() );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_end->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_end->size() );

        /// copy content
        for (; (l_src_iter != l_src_iter_end) || (l_trg_iter != l_trg_iter_end);
            ++l_src_iter, ++l_trg_iter )
        {
            algorithm_copy( l_trg_iter, l_src_iter );
        }

        ASSERT_EQ( l_src_iter, l_src_iter_end );
        ASSERT_EQ( l_trg_iter, l_trg_iter_end );
    }

    /// ensure both matrices are equal now
    check_equalness( trg_view, src_view );
}


// -------------------------------------------------------------------------------------------------------------------------

template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::forward_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    {
        /// initialize iterators
        auto           l_src_iter = src_view.cbegin_row_bundle();
        const auto l_src_iter_end = src_view.cend_row_bundle();

        auto           l_trg_iter = trg_view.begin_row_bundle();
        const auto l_trg_iter_end = trg_view.end_row_bundle();

        ASSERT_EQ( l_src_iter->size(), src_view.n_rows() );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_end->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_end->size() );

        /// copy content
        for (; (l_src_iter != l_src_iter_end) || (l_trg_iter != l_trg_iter_end);
            ++l_src_iter, ++l_trg_iter )
        {
            loop_copy( l_trg_iter, l_src_iter );
        }

        ASSERT_EQ( l_src_iter, l_src_iter_end );
        ASSERT_EQ( l_trg_iter, l_trg_iter_end );
    }

    /// ensure both matrices are equal now
    check_equalness( trg_view, src_view );
}


// -------------------------------------------------------------------------------------------------------------------------


template <class MATRIX_T>
template <typename T>
void CIterBundleTest<MATRIX_T>::backward_copy_test( val_matrix_view<T>& trg_view, const val_matrix_view<T>& src_view )
{   
    /// TC:- reverse copy of bundled elementwise iterators - bundled iterator as end
    
    {
        /// initialize iterators
        const auto l_src_iter_begin = src_view.cbegin_row_bundle();
              auto       l_src_iter = src_view.cend_row_bundle();

        const auto l_trg_iter_begin = trg_view.begin_row_bundle();
              auto       l_trg_iter = trg_view.end_row_bundle();

        ASSERT_EQ( l_src_iter->size(), src_view.n_rows() );
        ASSERT_EQ( l_src_iter->size(), l_src_iter_begin->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter->size() );
        ASSERT_EQ( l_src_iter->size(), l_trg_iter_begin->size() );

        /// copy content
        if ( (l_src_iter != l_src_iter_begin) && (l_trg_iter != l_trg_iter_begin) )
        {
            do
            {
                --l_src_iter;
                --l_trg_iter;
                
                loop_copy( l_trg_iter, l_src_iter );

            } while ( (l_src_iter > l_src_iter_begin) || (l_trg_iter > l_trg_iter_begin) );
        }

        ASSERT_EQ( l_src_iter, l_src_iter_begin );
        ASSERT_EQ( l_trg_iter, l_trg_iter_begin );
    }

    /// ensure both matrices are equal now
    check_equalness( trg_view, src_view );
}

// -------------------------------------------------------------------------------------------------------------------------


#if 0  // might not work with stashing iterator
TEST_F( CIterBundleTest_raligned, test_matrix_raligned_reverse_copy ) 
{   
    /// TC:- forward copy of bundled elementwise iterators - bundled iterator as end
    
    {
        /// initialize iterators
        auto l_src_iter_begin = m_src_matrix.elem.cbegin_row_bundle();
        auto   l_src_iter_end = m_src_matrix.elem.cend_row_bundle();

        auto l_trg_iter_begin = m_trg_matrix.elem.begin_row_bundle();
        auto   l_trg_iter_end = m_trg_matrix.elem.end_row_bundle();

        typedef decltype(l_src_iter_begin)  const_iter_t;
        typedef decltype(l_trg_iter_begin)        iter_t;

        auto l_src_riter     = std::reverse_iterator<const_iter_t>( l_src_iter_end );
        auto l_src_riter_end = std::reverse_iterator<const_iter_t>( l_src_iter_begin );
        
        auto l_trg_riter     = std::reverse_iterator<iter_t>( l_trg_iter_end );
        auto l_trg_riter_end = std::reverse_iterator<iter_t>( l_trg_iter_begin );

        ASSERT_EQ( l_src_riter->size(), n_rows );
        ASSERT_EQ( l_src_riter->size(), l_src_riter_end->size() );
        ASSERT_EQ( l_src_riter->size(), l_trg_riter->size() );
        ASSERT_EQ( l_src_riter->size(), l_trg_riter_end->size() );


        /// copy content
        for (; (l_src_riter != l_src_riter_end) || (l_trg_riter != l_trg_riter_end);
               ++l_src_riter, ++l_trg_riter )
        {
            for ( size_t idx = 0; idx < l_src_riter->size(); ++idx )
            {
                ASSERT_NE( l_trg_riter->value(idx), l_src_riter->value(idx) );

                if ( 0 == (idx % 3) )
                {
                    l_trg_riter->value(idx) = l_src_riter->value(idx);
                }
                else
                {
                    (*l_trg_riter)[idx] = (*l_src_riter)[idx];
                }

                EXPECT_EQ( l_trg_riter->value(idx), l_src_riter->value(idx) );
            }
        }

        ASSERT_EQ( l_src_riter, l_src_riter_end );
        ASSERT_EQ( l_trg_riter, l_trg_riter_end );
    }

    /// ensure both matrices are equal now
    for (size_t rIdx = 0; rIdx < n_rows; ++rIdx)
    {
        // count number of unequal items
        size_t num_equal = 0;

        for (auto its  = m_src_matrix.elem.cbegin(rIdx), itt = m_trg_matrix.elem.cbegin(rIdx);
                  its != m_src_matrix.elem.cend(rIdx);
             ++its, ++itt  )
        {
            num_equal += (*itt == *its) ? 1 : 0;
        }
        
        EXPECT_EQ( n_cols, num_equal );
    }
}
#endif


//---------------------------------------------------------------------------------------------------
