/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */



#include "simd.hpp"
#include "simd_cfg.inl"
#include "gtest/gtest.h"
#include <random>
#include <limits>
#include <functional>


/*
 * Google Test: 
 * https://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 * http://cheezyworld.com/wp-content/uploads/2010/12/PlainGoogleQuickTestReferenceGuide1.pdf
 */

//#define NOEXCEPT_TEST noexcept
#define NOEXCEPT_TEST


using namespace smd;

template <typename ET>
struct Error : public std::conditional<
                            std::is_floating_point<ET>::value,
                            ET,
                            int32_t>
{};

namespace
{
#if defined(__SIMD_USE_NEON__) // or defined(__SIMD_USE_ARM64__)

    static const float32_t FLOAT_DEVISION_REL_TOL = 1e-6;
#else
    static const float32_t FLOAT_DEVISION_REL_TOL = 0.f;
#endif

}

class CSimdNumericTest : public ::testing::Test
{
protected:

    const size_t m_numElemInSimd     = vi32_t::num_items();
    
    const int m_randomSeed_u         = 69;
    const int m_repeats_u            = 10000;

    const float32_t m_floatSumToleranceFactor = 2e-3f;
    const float32_t m_floatProdToleranceFactor = 1e-6f;
    const float32_t m_floatFMAToleranceFactor = 5e-5f;

    const int32_t   m_maxIntVal_i32;  // = 43;
    const float32_t m_maxFloatVal_f32;// = 43.f;

    std::default_random_engine                m_rand;
    std::uniform_int_distribution<int32_t>    m_intSampler;
    std::uniform_real_distribution<float32_t> m_floatSampler;

    // -----------------------------------------------------------------------


    virtual void SetUp()
    {}

    virtual void TearDown()
    {}

    template <typename VT, template<class> class VOP, template<class> class EOP=VOP>
    void test_unary_v_operation( const VT a_v32, const char* msg="" ) const;

    template <typename VT, template<class...> class VOP, template<class...> class EOP = VOP>
    void test_binary_vv_operation( const VT a_v32, const VT b_v32, const char* msg="" ) const;

    template <typename VT, typename ET, typename VOP, typename EOP, typename ERT=typename Error<ET>::type >
    void test_binary_ve_operation( const VT a_v32, ET b, VOP vop, EOP eop, const char* msg="", ERT abs_error = ERT(0), ERT rel_error = ERT(0) ) const;

    template <typename ET, typename VT, typename VOP, typename EOP>
    void test_binary_ev_operation( ET a, const VT b_v32, VOP vop, EOP eop, const char* msg="" ) const;

    template <typename VT, typename VOP, typename EOP>
    void test_binary_assign_vv_operation( const VT a_v32, const VT b_v32, VOP vop, EOP eop, const char* msg="" ) const;

    template <typename VT, typename ET, typename VOP, typename EOP>
    void test_binary_assign_ve_operation( const VT a_v32, ET b, VOP vop, EOP eop, const char* msg="", ET abs_error = ET(0), ET rel_error = ET(0) ) const;


    template <typename VT, typename VFUNC, typename EFUNC, typename ET = typename VT::elem_t>
    void test_unary_v_function( const VT a_v32, VFUNC vfunc, EFUNC efunc, 
                                 ET abs_error = ET(0), ET rel_error = ET(0), const char* msg="" ) const;

    template <typename VT, typename VFUNC, typename EFUNC>
    void test_binary_vv_function( const VT a_v32, const VT b_v32, VFUNC vfunc, EFUNC efunc, const char* msg="" ) const;


    void test_fma_operators( const vf32_t& a_vf32, const vf32_t& b_vf32, const vf32_t& c_vf32 ) const NOEXCEPT_TEST;


    template <typename VT>
    void test_blend( const vb32_t f_mask, const VT a_v32, const VT b_v32, VT& y ) const;


public:

    CSimdNumericTest()
        : m_maxIntVal_i32( std::ceil( std::pow( std::numeric_limits<int32_t>::max(),
                                                1.f / static_cast<float32_t>( m_numElemInSimd ) ) ) )
        , m_maxFloatVal_f32( std::ceil( std::pow( std::numeric_limits<float32_t>::max(),
                                                  1.f / static_cast<float32_t>( m_numElemInSimd  ) ) ) )
        , m_rand( m_randomSeed_u )
        , m_intSampler( -m_maxIntVal_i32, m_maxIntVal_i32 )
        , m_floatSampler( -m_maxFloatVal_f32, m_maxFloatVal_f32 )
    {}

};

// -----------------------------------------------------------------------------------------------------
// Base numeric operations test


template <typename VT, template<class> class VOP, template<class> class EOP>
void CSimdNumericTest::test_unary_v_operation( const VT a_v32, const char* msg ) const
{
    typedef VOP<VT>                             vect_op_t;
    typedef decltype(vect_op_t()(VT()))                RT;

    typedef typename VT::elem_t                    elem_t;
    typedef typename RT::elem_t                   relem_t;
    typedef EOP<elem_t>                         elem_op_t;
    
    // instaniate operations
    vect_op_t vop;
    elem_op_t eop;

    RT x;
    x = vop( a_v32 );

    auto aIter = a_v32.cbegin();
    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter )
    {
        relem_t rx = eop( *aIter );
        EXPECT_EQ( rx, *xIter ) << msg;
    }
}


template <typename VT, template<class...> class VOP, template<class...> class EOP>
void CSimdNumericTest::test_binary_vv_operation( const VT a_v32, const VT b_v32, const char* msg ) const
{
    typedef VOP<VT>                             vect_op_t;
    typedef decltype(vect_op_t()(VT(),VT()))           RT;

    typedef typename VT::elem_t                    elem_t;
    typedef typename RT::elem_t                   relem_t;
    typedef EOP<elem_t>                         elem_op_t;
    
    // instaniate operations
    vect_op_t vop;
    elem_op_t eop;

    RT x;
    x = vop( a_v32, b_v32 );

    auto aIter = a_v32.cbegin();
    auto bIter = b_v32.cbegin();
    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter, ++bIter )
    {
        relem_t rx = eop( *aIter, *bIter );
        EXPECT_EQ( rx, *xIter ) << msg;
    }
}


template <typename VT, typename ET, typename VOP, typename EOP, typename ERT>
void CSimdNumericTest::test_binary_ve_operation( const VT a_v32, ET b, VOP vop, EOP eop, const char* msg, ERT abs_error, ERT rel_error ) const
{
    typedef decltype(eop( a_v32[0], b )) RES_T;
    typedef typename select_simd<RES_T>::type RT;
    static_assert( is_simd<RT>::value == true );
    RT x = vop( a_v32, b );

    auto aIter = a_v32.cbegin();
    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter )
    {
        RES_T rx = eop( *aIter, b );

        if ((abs_error > 0) || (rel_error > 0))
        {
            abs_error += rel_error * std::abs( static_cast<ERT>(*xIter) );
            ASSERT_NEAR( rx, *xIter, abs_error ) << msg;
        }
        else
        {
            ASSERT_EQ( rx, *xIter ) << msg;
        }
    }
}


template <typename ET, typename VT, typename VOP, typename EOP>
void CSimdNumericTest::test_binary_ev_operation( ET a, const VT b_v32, VOP vop, EOP eop, const char* msg ) const
{
    typedef decltype(eop( a, b_v32[0] )) ERT;
    typedef typename select_simd<ERT>::type RT;
    static_assert( is_simd<RT>::value == true );
    RT x = vop( a, b_v32 );

    auto bIter = b_v32.cbegin();
    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++bIter )
    {
        ET rx = eop( a, *bIter );
        EXPECT_EQ( rx, *xIter ) << msg;
    }
}

template <typename VT, typename VOP, typename EOP>
void CSimdNumericTest::test_binary_assign_vv_operation( const VT a_v32, const VT b_v32, VOP vop, EOP eop, const char* msg ) const
{
    typedef typename VT::elem_t                    elem_t;

    VT x = a_v32;

    vop( x, b_v32 );

    auto aIter = a_v32.cbegin();
    auto bIter = b_v32.cbegin();
    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter, ++bIter )
    {
        elem_t rx = *aIter;
        eop( rx, *bIter );
        
        EXPECT_EQ( rx, *xIter ) << msg;
    }
}


template <typename VT, typename ET, typename VOP, typename EOP>
void CSimdNumericTest::test_binary_assign_ve_operation( const VT a_v32, ET b, VOP vop, EOP eop, const char* msg, ET abs_error, ET rel_error ) const
{
    VT x = a_v32;
    vop( x, b );

    auto aIter = a_v32.cbegin();

    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter )
    {
        ET rx = *aIter;
        eop( rx, b );
        
        if ((abs_error > 0) || (rel_error > 0))
        {
            abs_error += rel_error * std::abs( *xIter );
            ASSERT_NEAR( rx, *xIter, abs_error ) << msg;
        }
        else
        {
            ASSERT_EQ( rx, *xIter ) << msg;
        }
    }
}


template <typename VT, typename VFUNC, typename EFUNC, typename ET>
void CSimdNumericTest::test_unary_v_function( const VT a_v32, VFUNC vfunc, EFUNC efunc, ET abs_error, ET rel_error, const char* msg ) const
{
    VT x = vfunc( a_v32 );

    auto aIter = a_v32.cbegin();

    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter )
    {
        ET rx = efunc( *aIter );

        if ((abs_error > 0) || (rel_error > 0))
        {
            abs_error += rel_error * std::abs( *xIter );
            EXPECT_NEAR( rx, *xIter, abs_error ) << msg;
        }
        else
        {
            EXPECT_EQ( rx, *xIter ) << msg;
        }
    }
}


template <typename VT, typename VFUNC, typename EFUNC>
void CSimdNumericTest::test_binary_vv_function( const VT a_v32, const VT b_v32, VFUNC vfunc, EFUNC efunc, const char* msg ) const

{
    typedef typename VT::elem_t ET;

    VT x = vfunc( a_v32, b_v32 );

    auto aIter = a_v32.cbegin();
    auto bIter = b_v32.cbegin();

    for ( auto xIter = x.cbegin(); xIter != x.cend();
           ++xIter, ++aIter, ++bIter )
    {
        ET rx = efunc( *aIter, *bIter );
        EXPECT_EQ( rx, *xIter ) << msg;
    }
}


//--------------------------------------------------------------------------------------------------------------


TEST_F( CSimdNumericTest, int32_base_op_test )
{
    vi32_t a_v32, b_v32; 
    vi32_t s_v32;   // for bit shifts
    
    for ( int n = 0; n < m_repeats_u; ++n )
    {
        for ( size_t i = 0; i < vi32_t::size(); ++i )
        {
            a_v32[i] = m_intSampler( m_rand );
            b_v32[i] = m_intSampler( m_rand );

            // prevent division by zero
            a_v32[i] += (a_v32[i] == 0) ? 1 : 0;
            b_v32[i] += (b_v32[i] == 0) ? 1 : 0;

            // for bit shifts
            s_v32[i] = std::abs( m_intSampler( m_rand ) ) % 4;
            EXPECT_LT( s_v32[i], 4 );
            EXPECT_GE( s_v32[i], 0 );
        }

        // ----------------------------------------------------------------------------------------
        // --- Unary Operations -------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        test_unary_v_operation<vi32_t,std::negate>      ( a_v32, "std::negate<vi32_t> failed" );
        test_unary_v_operation<vi32_t,smd::bit_invert>  ( a_v32, "std::bit_invert<vi32_t> failed" );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // integer math
        test_binary_vv_operation<vi32_t,std::plus>      ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::minus>     ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::multiplies>( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::divides>   ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::modulus>   ( a_v32, b_v32 );
        
        // bitwise operators
        test_binary_vv_operation<vi32_t,std::bit_and>   ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::bit_or>    ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,std::bit_xor>   ( a_v32, b_v32 );
        
        // comparisons on unequal values
        test_binary_vv_operation<vi32_t,smd::equal_to,std::equal_to>          ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,smd::greater,std::greater>            ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,smd::less,std::less>                  ( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,smd::greater_equal,std::greater_equal>( a_v32, b_v32 );
        test_binary_vv_operation<vi32_t,smd::less_equal,std::less_equal>      ( a_v32, b_v32 );

        // comparisons on equal values
        test_binary_vv_operation<vi32_t,smd::equal_to,std::equal_to>          ( a_v32, a_v32 );
        test_binary_vv_operation<vi32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, a_v32 );
        test_binary_vv_operation<vi32_t,smd::greater,std::greater>            ( a_v32, a_v32 );
        test_binary_vv_operation<vi32_t,smd::less,std::less>                  ( a_v32, a_v32 );
        test_binary_vv_operation<vi32_t,smd::greater_equal,std::greater_equal>( a_v32, a_v32 );
        test_binary_vv_operation<vi32_t,smd::less_equal,std::less_equal>      ( a_v32, a_v32 );

        // bit shift operations
        test_binary_vv_operation<vi32_t,smd::shift_left>( std::abs(a_v32), s_v32 );
        test_binary_vv_operation<vi32_t,smd::shift_right>( a_v32, s_v32 );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Vect Assignment Operations ----------------------------------------------------
        // ----------------------------------------------------------------------------------------

        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x+=y);}, 
                                        []( int32_t& x, int32_t y) { return (x+=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x-=y);}, 
                                        []( int32_t& x, int32_t y) { return (x-=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x*=y);}, 
                                        []( int32_t& x, int32_t y) { return (x*=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x/=y);}, 
                                        []( int32_t& x, int32_t y) { return (x/=y);} );

        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x%=y);}, 
                                        []( int32_t& x, int32_t y) { return (x%=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x&=y);}, 
                                        []( int32_t& x, int32_t y) { return (x&=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x|=y);}, 
                                        []( int32_t& x, int32_t y) { return (x|=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x^=y);}, 
                                        []( int32_t& x, int32_t y) { return (x^=y);} );
    
        // bit shift operations
        test_binary_assign_vv_operation( std::abs( a_v32 ), s_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x<<=y);}, 
                                        []( int32_t& x, int32_t y) { return (x<<=y);} );
        test_binary_assign_vv_operation( a_v32, s_v32, 
                                        []( vi32_t& x, const vi32_t& y) { return (x>>=y);}, 
                                        []( int32_t& x, int32_t y) { return (x>>=y);} );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Elem Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // integer math
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x+y);},
                                  std::plus<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x-y);},
                                  std::minus<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x*y);},
                                  std::multiplies<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x/y);},
                                  std::divides<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x%y);},
                                  std::modulus<int32_t>() );

        // bitwise operators
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x & y);},
                                  std::bit_and<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x | y);},
                                  std::bit_or<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vi32_t& x, int32_t y ) { return (x ^ y);},
                                  std::bit_xor<int32_t>() );

        
        // comparisons on unequal values
        test_binary_ve_operation( a_v32, b_v32[1],      smd::equal_to<vi32_t,int32_t>(),      std::equal_to<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],  smd::not_equal_to<vi32_t,int32_t>(),  std::not_equal_to<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],       smd::greater<vi32_t,int32_t>(),       std::greater<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],          smd::less<vi32_t,int32_t>(),          std::less<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], smd::greater_equal<vi32_t,int32_t>(), std::greater_equal<int32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],    smd::less_equal<vi32_t,int32_t>(),    std::less_equal<int32_t>() );

        // comparisons on equal values
        test_binary_ve_operation( a_v32, a_v32[0],      smd::equal_to<vi32_t,int32_t>(),      std::equal_to<int32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],  smd::not_equal_to<vi32_t,int32_t>(),  std::not_equal_to<int32_t>() );
        test_binary_ve_operation( a_v32, a_v32[0],       smd::greater<vi32_t,int32_t>(),       std::greater<int32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],          smd::less<vi32_t,int32_t>(),          std::less<int32_t>() );
        test_binary_ve_operation( a_v32, a_v32[0], smd::greater_equal<vi32_t,int32_t>(), std::greater_equal<int32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],    smd::less_equal<vi32_t,int32_t>(),    std::less_equal<int32_t>() );

        // bit shift operations
        test_binary_ve_operation( std::abs(a_v32), s_v32[0], smd::shift_left<vi32_t,int32_t>(),    smd::shift_left<int32_t>() );
        test_binary_ve_operation( a_v32, s_v32[1],   smd::shift_right<vi32_t,int32_t>(),   smd::shift_right<int32_t>() );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Elem  Assignment Operations ---------------------------------------------------
        // ----------------------------------------------------------------------------------------

        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x+=y);}, 
                                        []( int32_t& x, int32_t y) { return (x+=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x-=y);}, 
                                        []( int32_t& x, int32_t y) { return (x-=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x*=y);}, 
                                        []( int32_t& x, int32_t y) { return (x*=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x%=y);}, 
                                        []( int32_t& x, int32_t y) { return (x%=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x/=y);}, 
                                        []( int32_t& x, int32_t y) { return (x/=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x&=y);}, 
                                        []( int32_t& x, int32_t y) { return (x&=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x|=y);}, 
                                        []( int32_t& x, int32_t y) { return (x|=y);} );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x^=y);}, 
                                        []( int32_t& x, int32_t y) { return (x^=y);} );
        
        test_binary_assign_ve_operation( std::abs(a_v32), s_v32[0], 
                                        [](  vi32_t& x, int32_t y) { return (x<<=y);}, 
                                        []( int32_t& x, int32_t y) { return (x<<=y);} );
        test_binary_assign_ve_operation( a_v32, s_v32[1], 
                                        [](  vi32_t& x, int32_t y) { return (x>>=y);}, 
                                        []( int32_t& x, int32_t y) { return (x>>=y);} );
    

        // ----------------------------------------------------------------------------------------
        // --- Elem-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // integer math
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x+y);},
                                  std::plus<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x-y);},
                                  std::minus<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x*y);},
                                  std::multiplies<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x/y);},
                                  std::divides<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x%y);},
                                  std::modulus<int32_t>() );

        // bitwise operators
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x & y);},
                                  std::bit_and<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x | y);},
                                  std::bit_or<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( int32_t x, const vi32_t& y ) { return (x ^ y);},
                                  std::bit_xor<int32_t>() );
        
        
        // // comparisons on unequal values
        test_binary_ev_operation( a_v32[1], b_v32,      smd::equal_to<int32_t,vi32_t>(),      std::equal_to<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,  smd::not_equal_to<int32_t,vi32_t>(),  std::not_equal_to<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,       smd::greater<int32_t,vi32_t>(),       std::greater<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,          smd::less<int32_t,vi32_t>(),          std::less<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, smd::greater_equal<int32_t,vi32_t>(), std::greater_equal<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,    smd::less_equal<int32_t,vi32_t>(),    std::less_equal<int32_t>() );

        // comparisons on equal values
        test_binary_ev_operation( a_v32[0], b_v32,      smd::equal_to<int32_t,vi32_t>(),      std::equal_to<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,  smd::not_equal_to<int32_t,vi32_t>(),  std::not_equal_to<int32_t>() );
        test_binary_ev_operation( a_v32[0], b_v32,       smd::greater<int32_t,vi32_t>(),       std::greater<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,          smd::less<int32_t,vi32_t>(),          std::less<int32_t>() );
        test_binary_ev_operation( a_v32[0], b_v32, smd::greater_equal<int32_t,vi32_t>(), std::greater_equal<int32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,    smd::less_equal<int32_t,vi32_t>(),    std::less_equal<int32_t>() );

        // bit shift operations
        test_binary_ev_operation( std::abs(a_v32[0]), s_v32,    smd::shift_left<int32_t,vi32_t>(),    smd::shift_left<int32_t>() );
        test_binary_ev_operation( a_v32[1], s_v32,   smd::shift_right<int32_t,vi32_t>(),   smd::shift_right<int32_t>() );


        // ----------------------------------------------------------------------------------------
        // --- test type specific functions -------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        test_binary_vv_function( a_v32, b_v32, smd::maximum<vi32_t>(), smd::maximum<int32_t>() );
        test_binary_vv_function( a_v32, b_v32, smd::minimum<vi32_t>(), smd::minimum<int32_t>() );
        
        test_unary_v_function( a_v32, smd::absolute<vi32_t>(), smd::absolute<int32_t>() );
    }
}


TEST_F( CSimdNumericTest, float32_base_op_test )
{
    vf32_t a_v32, b_v32;

    const float32_t l_minFloat_f32 = 1. / m_maxFloatVal_f32;
    
    for ( int n = 0; n < m_repeats_u; ++n )
    {
        for ( size_t i = 0; i < vi32_t::size(); ++i )
        {
            a_v32[i] = m_floatSampler( m_rand );
            b_v32[i] = m_floatSampler( m_rand );

            // prevent division by zero
            a_v32[i] += std::copysignf( (std::abs( a_v32[i] ) < l_minFloat_f32) ? l_minFloat_f32 : 0.f, a_v32[i] );
            b_v32[i] += std::copysignf( (std::abs( b_v32[i] ) < l_minFloat_f32) ? l_minFloat_f32 : 0.f, b_v32[i] );
        }
        
        // ----------------------------------------------------------------------------------------
        // --- Unary Operations -------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        test_unary_v_operation<vf32_t,std::negate>      ( a_v32, "std::negative<vf32> failed" );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // float math
        test_binary_vv_operation<vf32_t,std::plus>      ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,std::minus>     ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,std::multiplies>( a_v32, b_v32 );
        
        if (smd::all( std::abs(b_v32) > 1e-12f ))
        {
            test_binary_vv_operation<vf32_t,std::divides>   ( a_v32, b_v32 );
        }
        
        // comparisons on unequal values
        test_binary_vv_operation<vf32_t,smd::equal_to,std::equal_to>          ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,smd::greater,std::greater>            ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,smd::less,std::less>                  ( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,smd::greater_equal,std::greater_equal>( a_v32, b_v32 );
        test_binary_vv_operation<vf32_t,smd::less_equal,std::less_equal>      ( a_v32, b_v32 );

        // comparisons on equal values
        test_binary_vv_operation<vf32_t,smd::equal_to,std::equal_to>          ( a_v32, a_v32 );
        test_binary_vv_operation<vf32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, a_v32 );
        test_binary_vv_operation<vf32_t,smd::greater,std::greater>            ( a_v32, a_v32 );
        test_binary_vv_operation<vf32_t,smd::less,std::less>                  ( a_v32, a_v32 );
        test_binary_vv_operation<vf32_t,smd::greater_equal,std::greater_equal>( a_v32, a_v32 );
        test_binary_vv_operation<vf32_t,smd::less_equal,std::less_equal>      ( a_v32, a_v32 );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Vect Assignment Operations ----------------------------------------------------
        // ----------------------------------------------------------------------------------------

        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vf32_t& x, const vf32_t& y) { return (x+=y);}, 
                                        []( float32_t& x, float32_t y) { return (x+=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vf32_t& x, const vf32_t& y) { return (x-=y);}, 
                                        []( float32_t& x, float32_t y) { return (x-=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vf32_t& x, const vf32_t& y) { return (x*=y);}, 
                                        []( float32_t& x, float32_t y) { return (x*=y);} );
        test_binary_assign_vv_operation( a_v32, b_v32, 
                                        []( vf32_t& x, const vf32_t& y) { return (x/=y);}, 
                                        []( float32_t& x, float32_t y) { return (x/=y);} );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Elem Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // float math
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vf32_t& x, float32_t y ) { return (x+y);},
                                  std::plus<float32_t>(), "float plus failed" );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vf32_t& x, float32_t y ) { return (x-y);},
                                  std::minus<float32_t>(), "float minus failed" );
        test_binary_ve_operation( a_v32, b_v32[1], 
                                  []( const vf32_t& x, float32_t y ) { return (x*y);},
                                  std::multiplies<float32_t>(), "float multiplies failed" );
        
        if (std::abs(b_v32[1]) > 1e-12f)
        {
            test_binary_ve_operation( a_v32, b_v32[1], 
                                      []( const vf32_t& x, float32_t y ) { return (x/y);},
                                      std::divides<float32_t>(), "float divides failed", 0.f, FLOAT_DEVISION_REL_TOL );
        }

        // comparisons on unequal values
        test_binary_ve_operation( a_v32, b_v32[1],      smd::equal_to<vf32_t,float32_t>(),      std::equal_to<float32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],  smd::not_equal_to<vf32_t,float32_t>(),  std::not_equal_to<float32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],       smd::greater<vf32_t,float32_t>(),       std::greater<float32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],          smd::less<vf32_t,float32_t>(),          std::less<float32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1], smd::greater_equal<vf32_t,float32_t>(), std::greater_equal<float32_t>() );
        test_binary_ve_operation( a_v32, b_v32[1],    smd::less_equal<vf32_t,float32_t>(),    std::less_equal<float32_t>() );

        // comparisons on equal values
        test_binary_ve_operation( a_v32, a_v32[0],      smd::equal_to<vf32_t,float32_t>(),      std::equal_to<float32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],  smd::not_equal_to<vf32_t,float32_t>(),  std::not_equal_to<float32_t>() );
        test_binary_ve_operation( a_v32, a_v32[0],       smd::greater<vf32_t,float32_t>(),       std::greater<float32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],          smd::less<vf32_t,float32_t>(),          std::less<float32_t>() );
        test_binary_ve_operation( a_v32, a_v32[0], smd::greater_equal<vf32_t,float32_t>(), std::greater_equal<float32_t>() );
        test_binary_ve_operation( a_v32, a_v32[1],    smd::less_equal<vf32_t,float32_t>(),    std::less_equal<float32_t>() );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Elem  Assignment Operations ---------------------------------------------------
        // ----------------------------------------------------------------------------------------

        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vf32_t& x, float32_t y) { return (x+=y);}, 
                                        []( float32_t& x, float32_t y) { return (x+=y);},
                                        "float += failed" );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vf32_t& x, float32_t y) { return (x-=y);}, 
                                        []( float32_t& x, float32_t y) { return (x-=y);},
                                        "float -= failed" );
        test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                        [](  vf32_t& x, float32_t y) { return (x*=y);}, 
                                        []( float32_t& x, float32_t y) { return (x*=y);},
                                        "float *= failed" );
        
        if ( std::abs(b_v32[0]) > 1e-12f )
        {
           test_binary_assign_ve_operation( a_v32, b_v32[0], 
                                           [](  vf32_t& x, float32_t y) { return (x/=y);}, 
                                           []( float32_t& x, float32_t y) { return (x/=y);},
                                           "float /= failed", 0.f, FLOAT_DEVISION_REL_TOL );
        }

        // ----------------------------------------------------------------------------------------
        // --- Elem-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // float math
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( float32_t x, const vf32_t& y ) { return (x+y);},
                                  std::plus<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( float32_t x, const vf32_t& y ) { return (x-y);},
                                  std::minus<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, 
                                  []( float32_t x, const vf32_t& y ) { return (x*y);},
                                  std::multiplies<float32_t>() );
        
        if (smd::all( std::abs(b_v32) > 1e-12f ))
        {
            test_binary_ev_operation( a_v32[1], b_v32, 
                                      []( float32_t x, const vf32_t& y ) { return (x/y);},
                                      std::divides<float32_t>() );
        }
        
        
        // // comparisons on unequal values
        test_binary_ev_operation( a_v32[1], b_v32,      smd::equal_to<float32_t,vf32_t>(),      std::equal_to<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,  smd::not_equal_to<float32_t,vf32_t>(),  std::not_equal_to<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,       smd::greater<float32_t,vf32_t>(),       std::greater<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,          smd::less<float32_t,vf32_t>(),          std::less<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32, smd::greater_equal<float32_t,vf32_t>(), std::greater_equal<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,    smd::less_equal<float32_t,vf32_t>(),    std::less_equal<float32_t>() );

        // comparisons on equal values
        test_binary_ev_operation( a_v32[0], b_v32,      smd::equal_to<float32_t,vf32_t>(),      std::equal_to<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,  smd::not_equal_to<float32_t,vf32_t>(),  std::not_equal_to<float32_t>() );
        test_binary_ev_operation( a_v32[0], b_v32,       smd::greater<float32_t,vf32_t>(),       std::greater<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,          smd::less<float32_t,vf32_t>(),          std::less<float32_t>() );
        test_binary_ev_operation( a_v32[0], b_v32, smd::greater_equal<float32_t,vf32_t>(), std::greater_equal<float32_t>() );
        test_binary_ev_operation( a_v32[1], b_v32,    smd::less_equal<float32_t,vf32_t>(),    std::less_equal<float32_t>() );


        // ----------------------------------------------------------------------------------------
        // --- test type specific functions -------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        test_binary_vv_function( a_v32, b_v32, smd::maximum<vf32_t>(), smd::maximum<float32_t>(), "minimum failed" );
        test_binary_vv_function( a_v32, b_v32, smd::minimum<vf32_t>(), smd::minimum<float32_t>(), "minimum failed" );
        
        test_unary_v_function( a_v32, smd::absolute<vf32_t>(), smd::absolute<float32_t>(), 0.f, 0.f, "absolute failed" );
        test_unary_v_function( std::abs(a_v32), smd::logarithm<vf32_t>(), smd::logarithm<float32_t>(), 1e-8f, 2e-7f, "logarithm failed" );

        test_unary_v_function( std::abs(a_v32), smd::square_root<vf32_t>(), smd::square_root<float32_t>(), 1e-6f, 6e-3f, "square_root failed" );
        test_unary_v_function( std::abs(a_v32), smd::reciprocal_square_root<vf32_t>(), smd::reciprocal_square_root<float32_t>(), 1e-6f, 5e-3f, "reciprocal_square_root failed" );

        test_unary_v_function( std::abs(a_v32), smd::reciprocal<vf32_t>(), smd::reciprocal<float32_t>(), 1e-6f, 1e-3f, "reciprocal failed" );

        test_unary_v_function( a_v32, smd::rounding_up<vf32_t>(), smd::rounding_up<float32_t>(), 0.f, 0.f, "rounding_up failed" );
        test_unary_v_function( a_v32, smd::rounding_down<vf32_t>(), smd::rounding_down<float32_t>(), 0.f, 0.f, "rounding_down failed" );
        test_unary_v_function( a_v32, smd::rounding<vf32_t>(), smd::rounding<float32_t>(), 0.f, 0.f, "rounding failed" );
        test_unary_v_function( a_v32, smd::truncation<vf32_t>(), smd::truncation<float32_t>(), 0.f, 0.f, "truncation failed" );
    }
}


TEST_F( CSimdNumericTest, bool_base_op_test )
{
    vb32_t a_v32, b_v32;

    for ( int n = 0; n < m_repeats_u; ++n )
    {
        for ( size_t i = 0; i < vf32_t::size(); ++i )
        {
            a_v32[i] = bool32_t( 0 < m_intSampler( m_rand ) );
            b_v32[i] = bool32_t( 0 < m_intSampler( m_rand ) );
        }

        // ----------------------------------------------------------------------------------------
        // --- Vect-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------

        // logical operations
        test_unary_v_operation<vb32_t,smd::logical_not,std::logical_not>      ( a_v32, "smd::logical_not failed" );
        test_binary_vv_operation<vb32_t,smd::logical_and,std::logical_and>    ( a_v32, b_v32 );
        test_binary_vv_operation<vb32_t,smd::logical_or,std::logical_or>      ( a_v32, b_v32 );

        // comparisons on unequal values
        test_binary_vv_operation<vb32_t,smd::equal_to,std::equal_to>          ( a_v32, b_v32 );
        test_binary_vv_operation<vb32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, b_v32 );

        // comparisons on equal values
        test_binary_vv_operation<vb32_t,smd::equal_to,std::equal_to>          ( a_v32, a_v32 );
        test_binary_vv_operation<vb32_t,smd::not_equal_to,std::not_equal_to>  ( a_v32, a_v32 );

        // ----------------------------------------------------------------------------------------
        // --- Vect-Elem Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // logical operations
        test_binary_ve_operation( a_v32, bool(b_v32[1]), smd::logical_and<vb32_t,bool>(), std::logical_and<bool>() );
        test_binary_ve_operation( a_v32, bool(b_v32[1]), smd::logical_or<vb32_t,bool>(), std::logical_or<bool>() );

        // comparisons on unequal values
        test_binary_ve_operation( a_v32, bool(b_v32[1]), smd::equal_to<vb32_t,bool>(), std::equal_to<bool>() );
        test_binary_ve_operation( a_v32, bool(b_v32[1]), smd::not_equal_to<vb32_t,bool>(), std::not_equal_to<bool>() );

        // comparisons on equal values
        test_binary_ve_operation( a_v32, bool(a_v32[1]), smd::equal_to<vb32_t,bool>(), std::equal_to<bool>() );
        test_binary_ve_operation( a_v32, bool(a_v32[1]), smd::not_equal_to<vb32_t,bool>(), std::not_equal_to<bool>() );

        // ----------------------------------------------------------------------------------------
        // --- Elem-Vect Operations ---------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        
        // logical operations
        test_binary_ev_operation( bool(b_v32[1]), a_v32, smd::logical_and<bool,vb32_t>(), std::logical_and<bool>() );
        test_binary_ev_operation( bool(b_v32[1]), a_v32, smd::logical_or<bool,vb32_t>(), std::logical_or<bool>() );

        // comparisons on unequal values
        test_binary_ev_operation( bool(b_v32[1]), a_v32, smd::equal_to<bool,vb32_t>(), std::equal_to<bool>() );
        test_binary_ev_operation( bool(b_v32[1]), a_v32, smd::not_equal_to<bool,vb32_t>(), std::not_equal_to<bool>() );

        // comparisons on equal values
        test_binary_ev_operation( bool(a_v32[1]), a_v32, smd::equal_to<bool,vb32_t>(), std::equal_to<bool>() );
        test_binary_ev_operation( bool(a_v32[1]), a_v32, smd::not_equal_to<bool,vb32_t>(), std::not_equal_to<bool>() );
    }
}


// -----------------------------------------------------------------------------------------------------
// lateral sum/prod tests

TEST_F( CSimdNumericTest, int32_vt_sum_prod ) 
{ 
    for ( int n = 0; n < m_repeats_u; ++n )
    {
        int32_t l_checkSum_i32 = 0;
        int32_t l_checkProd_i32 = 1;
        
        vi32_t x_vi32;

        for ( auto xIter = x_vi32.begin();
                xIter != x_vi32.end();
                ++xIter )
        {
            int32_t x = m_intSampler( m_rand );

            *xIter = x;
            l_checkSum_i32 += x;
            l_checkProd_i32 *= x;
        }

        // The following to cast should always result in a compilation error!
        #if 0
        {
            int32_t z_i32 = x_vi32;
            int32_t x_i32 = int32_t(x_vi32);
        }
        #endif

        EXPECT_EQ( l_checkSum_i32, sum( x_vi32 ) );
        EXPECT_EQ( l_checkProd_i32, prod( x_vi32 ) );
    }
}


TEST_F( CSimdNumericTest, float32_vt_sum_prod ) 
{ 
    for ( int n = 0; n < m_repeats_u; ++n )
    {
        float32_t l_checkSum_f32 = 0.f;
        float32_t l_checkProd_f32 = 1.f;
        float32_t l_checkSumXlogX_f32 = 0.f;
        
        vf32_t x_vf32;

        for ( auto xIter = x_vf32.begin();
                xIter != x_vf32.end();
                ++xIter )
        {
            float32_t x = m_floatSampler( m_rand );

            *xIter = x;
            l_checkSum_f32 += x;
            l_checkProd_f32 *= x;
            l_checkSumXlogX_f32 += nuk::safe_XlogX( x );
        }

        // The following to cast should always result in a compilation error!
        #if 0
        {
            float32_t z_f32 = x_vf32;
            float32_t x_f32 = float32_t(x_vf32);
        }
        #endif

        float32_t l_sumfloatTolerance = std::abs(l_checkSum_f32) 
                                        * m_floatSumToleranceFactor;
        
        float32_t l_prodfloatTolerance = std::abs(l_checkProd_f32)
                                         * m_floatProdToleranceFactor;
        
        EXPECT_NEAR( l_checkSum_f32, sum( x_vf32 ), l_sumfloatTolerance );
        EXPECT_NEAR( l_checkProd_f32, prod( x_vf32 ), l_prodfloatTolerance );

        EXPECT_NEAR( l_checkSumXlogX_f32, safe_sumXlogX( x_vf32 ), l_sumfloatTolerance );
    }
}


// -----------------------------------------------------------------------------------------------------
// lateral fma-tests tests

void
CSimdNumericTest::test_fma_operators( const vf32_t& a,
                                      const vf32_t& b,
                                      const vf32_t& c ) const
{
    // TC1: (a*b) + c
    {
        // test statement
        const vf32_t z = a*b + c;

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * b[i]) + c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z[i], z_check, abs_tol );
        }
    }

    // TC2: c + (a*b)    and   c += a*b
    {
        // test statement
        const vf32_t z1 = a*b + c;
        const vf32_t z2 = c + (a*b);
        EXPECT_TRUE( all( z1 == z2 ) );

        vf32_t z3 = c;
        z3 += a*b;
        EXPECT_TRUE( all( z1 == z3 ) );
    }

    // TC3: (a*b) - c   and   c - (a*b)    and   c -= a*b
    {
        // test statement
        const vf32_t z1 = a*b - c;
        const vf32_t z2 = c - a*b;
        EXPECT_TRUE( all( z1 == (-z2) ) );

        // c -= a*b
        vf32_t z3 = c;
        z3 -= a*b;
        EXPECT_TRUE( all( z2 == z3 ) );

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * b[i]) - c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z1[i], z_check, abs_tol );
        }
    }

    // TC5: (a*b) + e
    {
        const float32_t e = c[0];
        // test statement
        const vf32_t z = a*b + e;

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * b[i]) + e;
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z[i], z_check, abs_tol );
        }
    }

    // TC6: (a*e) + c
    {
        const float32_t e = b[0];
        // test statement
        const vf32_t z = a*e + c;

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * e) + c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z[i], z_check, abs_tol );
        }
    }

    // TC7: (e*b) + c
    {
        const float32_t e = a[0];
        // test statement
        const vf32_t z = e*b + c;

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (e * b[i]) + c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z[i], z_check, abs_tol );
        }
    }

    // TC8: e + (a*b)
    {
        const float32_t e = c[0];

        // test statement
        const vf32_t z1 = a*b + e;
        const vf32_t z2 = e + a*b;

        EXPECT_TRUE( all( z1 == z2 ) );
    }

    // TC9: (a*e) + c
    {
        const float32_t e = b[0];

        // test statement
        const vf32_t z1 = a*e + c;
        const vf32_t z2 = c + a*e;

        EXPECT_TRUE( all( z1 == z2 ) );
    }

    // TC10: (e*b) + c
    {
        const float32_t e = a[0];

        // test statement
        const vf32_t z1 = e*b + c;
        const vf32_t z2 = c + e*b;

        EXPECT_TRUE( all( z1 == z2 ) );
    }

    // TC11: (a*b) - e   and    e - (a*b)
    {
        const float32_t e = c[0];
        
        // test statement
        const vf32_t z1 = a*b - e;
        const vf32_t z2 = e - a*b;

        EXPECT_TRUE( all( z1 == (-z2) ) );

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * b[i]) - e;
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z1[i], z_check, abs_tol );
        }
    }

    // TC12: (a*e) - c    and     c - (a*e)
    {
        const float32_t e = b[0];
        
        // test statement
        const vf32_t z1 = a*e - c;
        const vf32_t z2 = c - a*e;

        EXPECT_TRUE( all( z1 == (-z2) ) );

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (a[i] * e) - c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z1[i], z_check, abs_tol );
        }
    }

    // TC13: (e*b) - c    and     c - (e*b)
    {
        const float32_t e = a[0];
        
        // test statement
        const vf32_t z1 = e*b - c;
        const vf32_t z2 = c - e*b;

        EXPECT_TRUE( all( z1 == (-z2) ) );

        for (size_t i = 0; i < vf32_t::size(); ++i )
        {
            const float32_t z_check = (e * b[i]) - c[i];
            const float32_t abs_tol = std::abs(z_check) * m_floatFMAToleranceFactor;
            
            EXPECT_NEAR( z1[i], z_check, abs_tol );
        }
    }
}


TEST_F( CSimdNumericTest, fma ) 
{ 
    vf32_t a_vf32, b_vf32, c_vf32;  

    for ( int n = 0; n < m_repeats_u; ++n )
    {
        
        // sample data
        for ( size_t i = 0; i < vf32_t::size(); ++i )
        {
            a_vf32[i] = m_floatSampler( m_rand );
            b_vf32[i] = m_floatSampler( m_rand );
            c_vf32[i] = m_floatSampler( m_rand );
        }

        test_fma_operators( a_vf32, b_vf32, c_vf32 );
    }

}


// -----------------------------------------------------------------------------------------------------
//  test blend



template <typename VT>
void CSimdNumericTest::test_blend( const vb32_t f_mask, const VT a_v32, const VT b_v32, VT& y ) const
{
    //typedef typename VT::elem_t ET;
    const VT x = smd::blend( f_mask, a_v32, b_v32 );

    for (size_t i = 0; i < VT::size(); ++i )
    {
        y.val.si[i] = (f_mask.val.si[i] != 0) ? a_v32.val.si[i] : b_v32.val.si[i];
        
        // check for bit-identity
        EXPECT_EQ( y.val.si[i], x.val.si[i] );
        EXPECT_EQ( std::isfinite(y.val.sf[i]), std::isfinite(y.val.sf[i]) );
        EXPECT_EQ( std::isnan(y.val.sf[i]), std::isnan(y.val.sf[i]) );
    }
}



TEST_F( CSimdNumericTest, blend ) 
{ 
    vf32_t a_vf32, b_vf32, y_vf32;
    vi32_t a_vi32, b_vi32, y_vi32;
    vi32_t a_vb32, b_vb32, y_vb32;
    

    vb32_t mask_vb32;

    const float32_t spec_vals[] = { -0.f, 
                                    std::numeric_limits<float32_t>::min(),
                                    std::numeric_limits<float32_t>::infinity(),
                                    std::numeric_limits<float32_t>::quiet_NaN(),
                                    std::numeric_limits<float32_t>::signaling_NaN() };

    EXPECT_EQ( spec_vals[0], spec_vals[0] );
    EXPECT_EQ( spec_vals[1], spec_vals[1] );
    EXPECT_EQ( spec_vals[2], spec_vals[2] );
    EXPECT_NE( spec_vals[3], spec_vals[3] );
    EXPECT_NE( spec_vals[4], spec_vals[4] );
    EXPECT_NE( spec_vals[3], spec_vals[4] );


    for ( int n = 0; n < m_repeats_u; ++n )
    {
        
        // sample data
        for ( size_t i = 0; i < vf32_t::size(); ++i )
        {
            a_vf32[i] = m_floatSampler( m_rand );
            b_vf32[i] = m_floatSampler( m_rand );

            a_vi32[i] = m_intSampler( m_rand );
            b_vi32[i] = m_intSampler( m_rand );

            a_vb32[i] = bool32_t( 0 < m_intSampler( m_rand ) );
            b_vb32[i] = bool32_t( 0 < m_intSampler( m_rand ) );

            mask_vb32[i] = bool32_t( 0 < m_intSampler( m_rand ) );
        }

        // set one value to a special value
        a_vf32.val.sf[0] = spec_vals[n%5];
        EXPECT_NE( 0, a_vf32.val.si[0] );

        a_vi32.val.sf[1] = spec_vals[n%5];
        EXPECT_NE( 0, a_vi32.val.si[1] );

        a_vb32.val.sf[2] = spec_vals[n%5];
        EXPECT_NE( 0, a_vb32.val.si[2] );

        /// TEST Calls
        test_blend( mask_vb32, a_vi32, b_vi32, y_vi32 );
        EXPECT_TRUE( all( y_vi32 == smd::blend( mask_vb32, a_vi32, b_vi32 ) ) );

        test_blend( mask_vb32, a_vb32, b_vb32, y_vb32 );
        EXPECT_TRUE( all( y_vb32 == smd::blend( mask_vb32, a_vb32, b_vb32 ) ) );
        
        // check additionally handling of non finite values
        {
            test_blend( mask_vb32, a_vf32, b_vf32, y_vf32 );
            
            
            if ( (false == std::isnan( spec_vals[n%5] )) || (mask_vb32[0] == false) )
            {
                EXPECT_TRUE( all( y_vf32 == smd::blend( mask_vb32, a_vf32, b_vf32 ) ) );
            }
            else
            {
                EXPECT_FALSE( all( y_vf32 == smd::blend( mask_vb32, a_vf32, b_vf32 ) ) );
            }
        }
    }
}
