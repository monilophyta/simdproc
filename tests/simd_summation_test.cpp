/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>
#include "simd.hpp"
#include "gtest/gtest.h"


using namespace smd;



template <typename FT, typename DerivedT>
class CSimdSummationTest : public ::testing::Test
{
private:

    typedef CSimdSummationTest<FT,DerivedT>  This;

protected:

    DerivedT& derived() { return static_cast<DerivedT&>(*this); }
    const DerivedT& derived() const { return static_cast<const DerivedT&>(*this); }

    // naive summation
    FT        m_naiveAccu_f32;
    float64_t m_naiveAccu_f64;

    // kahan summation
    FT m_kahanAccu_f32;
    FT m_kahanComp_f32;

    // neumaier summation
    FT m_neumaierAccu_f32;
    FT m_neumaierComp_f32;

    // klein summation
    FT m_kleinAccu_f32;
    FT m_kleinComp1_f32;
    FT m_kleinComp2_f32;

    // Summation Errors evaluate after last update call
    float64_t m_naive32Error_f64;
    float64_t m_naive64Error_f64;
    float64_t m_kahanError_f64;
    float64_t m_neumaierError_f64;
    float64_t m_kleinError_f64;


    void update_step( const float32_t f_xval_f32 );
    void update_step( const float32_t f_wval_f32, const float32_t f_xval_f32 );
    void dump_errors() const;


    // tests
    void large_small_neglarge_test();
    void one_eps_negeps_test();
    void large_small_small_test();
    void large_small_small_neglarge_test();
    void square_sum_test();
    void square_alternating_sum_test();

    void simple_weighted_test();

public:

    CSimdSummationTest()
        : m_naiveAccu_f32(0)
        , m_naiveAccu_f64(0)
        , m_kahanAccu_f32(0)
        , m_kahanComp_f32(0)
        , m_neumaierAccu_f32(0)
        , m_neumaierComp_f32(0)
        , m_kleinAccu_f32(0)
        , m_kleinComp1_f32(0)
        , m_kleinComp2_f32(0)
        , m_naive32Error_f64(0)
        , m_naive64Error_f64(0)
        , m_kahanError_f64(0)
        , m_neumaierError_f64(0)
        , m_kleinError_f64(0)
    {}
};



// ------------------------------------------------------------------------------------------------

template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::update_step( const float32_t f_xval_f32 )
{
    const FT l_xval( f_xval_f32 );
    
    // update naive accumulators
    m_naiveAccu_f32 += l_xval;
    m_naiveAccu_f64 += static_cast<float64_t>(f_xval_f32);

    // update compensated accumulators
    kahan_sum_step( m_kahanAccu_f32, l_xval, m_kahanComp_f32 );
    neumaier_sum_step( m_neumaierAccu_f32, l_xval, m_neumaierComp_f32 );
    klein_sum_step( m_kleinAccu_f32, l_xval, m_kleinComp1_f32, m_kleinComp2_f32 );
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::update_step( const float32_t f_wval_f32, const float32_t f_xval_f32 )
{
    const FT l_xval( f_xval_f32 );
    const FT l_wval( f_wval_f32 );
    
    // update naive accumulators
    m_naiveAccu_f32 += l_wval * l_xval;
    m_naiveAccu_f64 += static_cast<float64_t>(f_wval_f32 * f_xval_f32);

    // update compensated accumulators
    kahan_sum_step( m_kahanAccu_f32, l_wval, l_xval, m_kahanComp_f32 );
    neumaier_sum_step( m_neumaierAccu_f32, l_wval, l_xval, m_neumaierComp_f32 );
    klein_sum_step( m_kleinAccu_f32, l_wval, l_xval, m_kleinComp1_f32, m_kleinComp2_f32 );
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::dump_errors() const
{
    std::cout << "l_naive32Error_f64 = " << m_naive32Error_f64 << std::endl
              << "l_naive64Error_f64 = " << m_naive64Error_f64 << std::endl
              << "l_kahanError_f64 = " << m_kahanError_f64 << std::endl
              << "l_neumaierError_f64 = " << m_neumaierError_f64 << std::endl
              << "l_kleinError_f64 = " << m_kleinError_f64 << std::endl;
}

// ------------------------------------------------------------------------------------------------

template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::large_small_neglarge_test()
{
    const float32_t l_maxValue_f32 = 0.75f * std::numeric_limits<float32_t>::max();
    const float32_t l_seq_af32[3] = {l_maxValue_f32,1.f,-l_maxValue_f32};

    for (int i = 0; i < 3; ++i)
    {
        update_step( l_seq_af32[i] );
    }

    derived().evaluate_update( 1.0 );

    ASSERT_LE( m_naive64Error_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

    EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 );
    EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );

    //dump_errors();
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::one_eps_negeps_test()
{
    float32_t l_eps_f32 = 1.0f;
    while ( (1.0f + l_eps_f32) != 1.0f) 
    {
        l_eps_f32 *= 0.5f;
    }
    const float32_t l_seq_af32[3] = { 1.f, l_eps_f32, -l_eps_f32 };

    for (int i = 0; i < 3; ++i)
    {
        update_step( l_seq_af32[i] );
    }

    derived().evaluate_update( 1.0 );

    ASSERT_LE( m_naive64Error_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

    EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );
    //EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 ); // seems not to be true for all cases
    
    //dump_errors();
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::large_small_small_test()
{
    const float32_t l_largeFloat32_f32 = static_cast<float32_t>(1u << 25u);
    const float32_t l_seq_af32[3] = { l_largeFloat32_f32, 3.14159f, 2.71828f};

    for (int i = 0; i < 3; ++i)
    {
        update_step( l_seq_af32[i] );
    }

    //float32_t l_bestOrder_f32 = l_seq_af32[1] + l_seq_af32[2];
    //l_bestOrder_f32 += l_seq_af32[0];
    //evaluate_update( static_cast<float64_t>(l_bestOrder_f32) );

    float64_t l_bestOrder_f64 = static_cast<float64_t>(l_seq_af32[1]) + 
                                static_cast<float64_t>(l_seq_af32[2]);
    l_bestOrder_f64 += static_cast<float64_t>(l_seq_af32[0]);

    derived().evaluate_update( l_bestOrder_f64 );

    ASSERT_LE( m_naive64Error_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

    EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );
    //EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 ); // seems not to be true for all cases
    
    //dump_errors();
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::large_small_small_neglarge_test()
{
    const float32_t l_largeFloat32_f32 = static_cast<float32_t>(1u << 25u);
    const float32_t l_seq_af32[4] = { l_largeFloat32_f32, 3.14159f, 2.71828f, -l_largeFloat32_f32};

    for (int i = 0; i < 4; ++i)
    {
        update_step( l_seq_af32[i] );
    }

    float64_t l_targetValue_f64 = static_cast<float64_t>(l_seq_af32[1]) + 
                                  static_cast<float64_t>(l_seq_af32[2]);
    derived().evaluate_update( l_targetValue_f64 );

    ASSERT_LE( m_naive64Error_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
    ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

    EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );
    // EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 );  // seems not to be true for all cases

    //dump_errors();
}


template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::square_sum_test()
{
    //const int32_t l_targetN_i32 = 65536;
    const int32_t l_targetN_i32 = 1u << 17u;

    for (int64_t n = 0; n <= l_targetN_i32; ++n )
    {
        const int64_t l_val_i64 = n*n;

        update_step( static_cast<float32_t>(l_val_i64) );

        derived().evaluate_update( m_naiveAccu_f64 );

        ASSERT_EQ( m_naive64Error_f64, 0.0 );
        
        ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

        EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 );
        EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );
    }

    //dump_errors();
}

template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::square_alternating_sum_test()
{
    //const int32_t l_targetN_i32 = 65536;
    const int32_t l_targetN_i32 = 1u << 17u;

    for (int64_t n = 0; n <= l_targetN_i32; ++n )
    {
        const int64_t l_val_i64 = (n & 1u) ? (n*n)
                                           : ((n - l_targetN_i32) * (n - l_targetN_i32));

        update_step( static_cast<float32_t>(l_val_i64) );

        derived().evaluate_update( m_naiveAccu_f64 );

        ASSERT_EQ( m_naive64Error_f64, 0.0 );
        
        ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

        ASSERT_LE( m_kleinError_f64, m_neumaierError_f64 );
        //EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 );  // seems not to be true for all cases
        
    }

    //dump_errors();
}

template <typename FT, typename DerivedT>
void CSimdSummationTest<FT,DerivedT>::simple_weighted_test()
{
    //const int32_t l_targetN_i32 = 65536;
    const int32_t l_targetN_i32 = 1u << 18u;

    for (int64_t n = 1; n <= l_targetN_i32; ++n )
    {
        const float32_t l_xval_f32 = static_cast<float32_t>(n);
        const float32_t l_wval_f32 = 1.f / l_xval_f32;

        update_step( l_wval_f32, l_xval_f32 );

        derived().evaluate_update( static_cast<float64_t>(n) );

        //ASSERT_LE( m_naive64Error_f64, m_naive32Error_f64 ); // seems not to be true for all cases
        
        ASSERT_LE( m_kahanError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_neumaierError_f64, m_naive32Error_f64 );
        ASSERT_LE( m_kleinError_f64, m_naive32Error_f64 );

        EXPECT_LE( m_neumaierError_f64, m_kahanError_f64 );  // seems not to be true for all cases
        EXPECT_LE( m_kleinError_f64, m_neumaierError_f64 );
    }

    //dump_errors();
}

// ------------------------------------------------------------------------------------------------

class CSimdSummationTestF32 : public CSimdSummationTest<float32_t,CSimdSummationTestF32>
{
public:

    void evaluate_update( const double f_targetVal_f64 );
    void dump_errors() const;
};


void CSimdSummationTestF32::evaluate_update( const double f_targetVal_f64 )
{
    // calculate sums
    const float32_t l_neumaierSum_f32 = m_neumaierAccu_f32 + m_neumaierComp_f32;
    const float32_t l_kleinSum_f32 = m_kleinAccu_f32 + m_kleinComp1_f32 + m_kleinComp2_f32;

    // calculate errors
    m_naive32Error_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(m_naiveAccu_f32) );
    m_naive64Error_f64 = std::abs( f_targetVal_f64 - m_naiveAccu_f64 );
    m_kahanError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(m_kahanAccu_f32) );
    m_neumaierError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(l_neumaierSum_f32) );
    m_kleinError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(l_kleinSum_f32) );
}

// ------------------------------------------------------------------------------------------------

TEST_F( CSimdSummationTestF32, large_small_neglarge_test )
{
    large_small_neglarge_test();
}
TEST_F( CSimdSummationTestF32, one_eps_negeps_test )
{
    one_eps_negeps_test();
}
TEST_F( CSimdSummationTestF32, large_small_small_test )
{
    large_small_small_test();
}
TEST_F( CSimdSummationTestF32, large_small_small_neglarge_test )
{
    large_small_small_neglarge_test();
}
TEST_F( CSimdSummationTestF32, square_sum_test )
{
    square_sum_test();
}
TEST_F( CSimdSummationTestF32, square_alternating_sum_test )
{
    square_alternating_sum_test();
}
TEST_F( CSimdSummationTestF32, simple_weighted_test )
{
    simple_weighted_test();
}

// ------------------------------------------------------------------------------------------------

class CSimdSummationTestVF32 : public CSimdSummationTest<vf32_t,CSimdSummationTestVF32>
{
public:

    void evaluate_update( const double f_targetVal_f64 );
};


void CSimdSummationTestVF32::evaluate_update( const double f_targetVal_f64 )
{
    // calculate sums
    const vf32_t l_neumaierSum_f32 = m_neumaierAccu_f32 + m_neumaierComp_f32;
    const vf32_t l_kleinSum_f32 = m_kleinAccu_f32 + m_kleinComp1_f32 + m_kleinComp2_f32;

    // ensure that all values in vectors are the same
    ASSERT_TRUE( all( m_naiveAccu_f32 == m_naiveAccu_f32[0] ) );
    ASSERT_TRUE( all( m_kahanAccu_f32 == m_kahanAccu_f32[0] ) );
    ASSERT_TRUE( all( l_neumaierSum_f32 == l_neumaierSum_f32[0] ) );
    ASSERT_TRUE( all( l_kleinSum_f32 == l_kleinSum_f32[0] ) );

    // calculate errors
    m_naive32Error_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(m_naiveAccu_f32[0]) );
    m_naive64Error_f64 = std::abs( f_targetVal_f64 - m_naiveAccu_f64 );
    m_kahanError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(m_kahanAccu_f32[0]) );
    m_neumaierError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(l_neumaierSum_f32[0]) );
    m_kleinError_f64 = std::abs( f_targetVal_f64 - static_cast<float64_t>(l_kleinSum_f32[0]) );
}

// ------------------------------------------------------------------------------------------------

TEST_F( CSimdSummationTestVF32, large_small_neglarge_test )
{
    large_small_neglarge_test();
}
TEST_F( CSimdSummationTestVF32, one_eps_negeps_test )
{
    one_eps_negeps_test();
}
TEST_F( CSimdSummationTestVF32, large_small_small_test )
{
    large_small_small_test();
}
TEST_F( CSimdSummationTestVF32, large_small_small_neglarge_test )
{
    large_small_small_neglarge_test();
}
TEST_F( CSimdSummationTestVF32, square_sum_test )
{
    square_sum_test();
}
TEST_F( CSimdSummationTestVF32, square_alternating_sum_test )
{
    square_alternating_sum_test();
}
TEST_F( CSimdSummationTestVF32, simple_weighted_test )
{
    simple_weighted_test();
}
