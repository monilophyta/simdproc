/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <simd.hpp>
#include "gtest/gtest.h"


/*
 * Google Test: 
 * https://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 * http://cheezyworld.com/wp-content/uploads/2010/12/PlainGoogleQuickTestReferenceGuide1.pdf
 */


using namespace smd;


TEST( simd_array, sum_simd_array ) 
{ 
    const int32_t N = 1123;

    for (int32_t n = 1; n <= N; ++n )
    {
        array_vi32_t l_testArray1( n );
        l_testArray1.overlap().fill(std::numeric_limits<int32_t>::max());
        
        array_vi32_t l_testArray2( n );
        l_testArray2.overlap().fill(std::numeric_limits<int32_t>::max());

        int32_t idx = 0;
        for (auto it = l_testArray2.elem().begin(); 
                  it != l_testArray2.elem().end();
                ++it, ++idx )
        {
            l_testArray1.elem(idx) = idx+1;
            *it = idx+1;
        }

        const int32_t l_comparison_sum_i32 = ( n * (n+1) ) / 2;

        const int32_t l_arraySum1_i32 = sum( l_testArray1 );
        const int32_t l_arraySum2_i32 = sum( l_testArray2 );
        
        ASSERT_EQ( l_arraySum1_i32, l_arraySum2_i32 );
        ASSERT_EQ( l_comparison_sum_i32, l_arraySum1_i32 );
        ASSERT_EQ( l_comparison_sum_i32, l_arraySum2_i32 );
    }
}


TEST( simd_array, safe_sumXlogX_simd_array ) 
{ 
    const int32_t N = 1123;

    for (int32_t n = 1; n <= N; ++n )
    {
        const float32_t p = 1.f / static_cast<float32_t>(n);
        const float32_t l_comparison_val = std::log( p );

        array_vf32_t l_testArray( n );
        l_testArray.fill( vf32_t(p) );
        //Sl_testArray.fill_overlap(0);

        const float32_t l_arraySumXlogX_f32 = safe_sumXlogX( l_testArray );
        const float32_t l_tolerance = std::abs(l_comparison_val) * 1e-5f;
        
        ASSERT_NEAR( l_comparison_val, l_arraySumXlogX_f32, l_tolerance );
    }
}
