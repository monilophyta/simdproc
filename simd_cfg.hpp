/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <stdint.h>



// MAKRO overloading gcc vector builin implementation by some hardware specific implementaitons
#define ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION
//#undef ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION


namespace smd
{


static const bool USE_I32_MATRIX_INDICES = true;


static_assert( 4 == sizeof(float), "float32_t can not be defined");
typedef float    float32_t;
//typedef int32_t  bool32_t;

static_assert( 8 == sizeof(double), "float64_t can not be defined");
typedef double   float64_t;

//#define SIMD_NOEXCEPT noexcept
#define SIMD_NOEXCEPT

} // namespace smd


// ------------------------------------------------------------------------------------------------------
// -------------- Decision about used x86intrinsics -----------------------------------------------------
// ------------------------------------------------------------------------------------------------------


#ifdef __AVX2__

    // AVX2 can - if available - also be used in conjunction with SSE
    #define __SIMD_USE_AVX2__

#endif


#ifdef __AVX__

    #define __SIMD_USE_AVX__

    #ifndef NO_SIMD_256
        #define __USE_SIMD_256__
    #endif
#endif


#if defined(__SSE2__) && not defined(NO_SSE)  // No implementation for SSE alone

    // https://stackoverrun.com/de/q/7951459

    #define __SIMD_USE_SSE2__
    
    #ifndef __USE_SIMD_256__
        #define __USE_SIMD_128__
    #endif

    #ifdef __SSE3__
        #define __SIMD_USE_SSE3__
    #endif

    #ifdef __SSSE3__
        #define __SIMD_USE_SSSE3__
    #endif

    #ifdef __SSE4_1__
        #define __SIMD_USE_SSE41__
    #endif

    //#ifdef __SSE4_2__
    //    #define __SIMD_USE_SSE42__
    //#endif
#endif

// ABOUT USING FMA
#if defined(__SIMD_USE_SSE2__) || defined(__SIMD_USE_AVX__)

    #ifdef __FMA__
        #define __SIMD_USE_FMA__
    #endif

#endif


// Verify configuration

#if defined(__USE_SIMD_128__) && defined(__USE_SIMD_256__)
    #error inconsistent simd vector configuration
#endif



// ------------------------------------------------------------------------------------------------------
// -------------- Decision about used arm intrinsics ----------------------------------------------------
// ------------------------------------------------------------------------------------------------------



#if defined(__ARM_NEON__ ) && not defined(NO_NEON)
    #define __USE_SIMD_128__
    #define __SIMD_USE_NEON__

    #if defined(__ARM_FEATURE_FMA) || defined(__ARM_FEATURE_FMA__)
        #define __SIMD_USE_FMA__
    #endif
#endif

#if defined(__aarch64__) && not defined(NO_NEON)
    #define __SIMD_USE_ARM64__
    
    #if defined(__ARM_FEATURE_FMA) || defined(__ARM_FEATURE_FMA__)
        #define __SIMD_USE_FMA__
    #endif
#endif
