#!/bin/bash

# https://stackoverflow.com/questions/45933732/how-to-specify-a-compiler-in-cmake

#export CXX=/usr/bin/g++-8
#export CXX=/usr/bin/clang++-7

## used build type
#BUILD_TYPE=Debug
BUILD_TYPE=Release
#BUILD_TYPE=RelWithDebInfo   # Obsolet, debugging symbols anyway added


mkdir -p test_build
cd test_build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ../
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ../
cmake --build . -- -j4
cd ../

