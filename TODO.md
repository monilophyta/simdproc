
### TODO

* facotorize array_view and martrix_view for simple (non-simd) array
   * restructure source folder in: 
        0. foreign_code
        1. bundle_iterator, 
        2. valarray+valmatrix,
        3. simd base types,
        4. simd array+matrix
        5. avx

* bring const on the level of array/matrix elements
