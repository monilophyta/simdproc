/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_cfg.hpp"






#ifdef __USE_SIMD_128__
    #define MSG10 "using SIMD vectors of 128bit with instructions:"
#else
    #define MSG10 ""
#endif

#ifdef __USE_SIMD_256__
    #define MSG20 MSG10 "using SIMD vectors of 256bit with instructions:"
#else
    #define MSG20 MSG10
#endif

#ifdef __SIMD_USE_SSE2__
    #define MSG30 MSG20 " SSE2"
#else
    #define MSG30 MSG20
#endif

#ifdef __SIMD_USE_SSE3__
    #define MSG35 MSG30 " SSE3"
#else
    #define MSG35 MSG30
#endif


#ifdef __SIMD_USE_SSSE3__
    #define MSG40 MSG35 " SSSE3"
#else
    #define MSG40 MSG35
#endif

#ifdef __SIMD_USE_SSE41__
    #define MSG50 MSG40 " SSE41"
#else
    #define MSG50 MSG40
#endif


#ifdef __SIMD_USE_AVX__
    #define MSG60 MSG50 " AVX"
#else
    #define MSG60 MSG50
#endif


#ifdef __SIMD_USE_AVX2__
    #define MSG70 MSG60 " AVX2"
#else
    #define MSG70 MSG60
#endif


#ifdef __SIMD_USE_FMA__
    #define MSG80 MSG70 " FMA"
#else
    #define MSG80 MSG70
#endif


#ifdef __SIMD_USE_NEON__
    #define MSG90 MSG80 " NEON"
#else
    #define MSG90 MSG80
#endif

#ifdef __SIMD_USE_ARM64__
    #define MSG100 MSG90 " A64"
#else
    #define MSG100 MSG90
#endif


// Print message
#pragma message( MSG100 )


#undef MSG10
#undef MSG20
#undef MSG30
#undef MSG35
#undef MSG40
#undef MSG50
#undef MSG70
#undef MSG80
#undef MSG90
#undef MSG100

// ------------------------------------------------------------------------------------------------------
// -------------- Analysing ARM environment -------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------


// https://www.keil.com/support/man/docs/armclang_ref/armclang_ref_chr1383660321827.htm
// http://tessy.org/wiki/index.php?Arm%A4%CEFPU

#define XSTR(s) STR(s)
#define STR(s) #s

#define DUMP(s) #s "=" XSTR(s) "  "

#if defined(__ARM_ARCH)
    #pragma message( DUMP(__ARM_ARCH) DUMP(__ARM_FP) DUMP(__ARM_PCS_VFP) DUMP(__ARM_NEON_FP) DUMP(__ARM_NEON)  DUMP(__ARM_NEON__) DUMP(__ARM_FEATURE_FMA) DUMP(__ARM_FEATURE_DIRECTED_ROUNDING) DUMP(__ARM_FEATURE_IDIV) DUMP(__ARM_FEATURE_NUMERIC_MAXMIN) DUMP(__ARM_FEATURE_SIMD32) )
    //#pragma message( DUMP(__ARM_ARCH) DUMP(__ARMCC_VERSION) DUMP(__ARM_FEATURE_CRYPTO) DUMP(__ARM_FP) DUMP(__ARM_NEON_FP) DUMP(__ARM_NEON)  DUMP(__ARM_NEON__) DUMP(__ARM_FEATURE_FMA)  DUMP(__FP_FAST_FMAF) DUMP(__FP_FAST_FMA)  DUMP(__FP_FAST_FMAL) DUMP(__ARM_FEATURE_SIMD32) )
#endif

#undef XSTR
#undef STR
#undef DUMP
