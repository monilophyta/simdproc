
[//]: # (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


## Overview
* [SIMD Types](#SIMD-Types)
* [SIMD Member functions](#SIMD-Member-functions)
* [Functions of SIMD Types](#Functions-of-SIMD-Types)
* [Array and Matrix](#Array-and-Matrix)
* [Array View and Matrix View](#Array-View-and-Matrix-View)
* [Array and Matrix Member Functions](#Array-and-Matrix-Member-Functions)
* [Array View and Matrix View Member Functions](#Array-View-and-Matrix-View-Member-Functions)
* [Functions of Arrays](#Functions-of-Arrays)
* [Functions of Matrices](#Functions-of-Matrices)

### SIMD Types

#### SIMD Member functions

##### Boolean


##### Integer


| Member Function                |  OUT   |  IN  |
| ------------------------------ | ------ | ---- |
| ST .**gather** ( const T* memptr )  | ST ∈ {vi32_t, vf32_t} |  T ∈ {int32_t, float32_t}  |


#### Functions of SIMD Types

##### Boolean

| Function                       |  OUT   |  IN  |
| ------------------------------ | ------ | ---- |
| bool **all** ( vb32_t )  |    |    |
| bool **any** ( vb32_t )  |    |    |
| ST **blend** ( vb32_t Mask, ST ifTrue, ST ifFalse )  | ST ∈ {vb32_t, vi32_t, vf32_t}  |  ST  |

##### Numerical Types

| Function                       |  OUT   |  IN  |
| ------------------------------ | ------ | ---- |
| T **sum** ( ST value ) <br> T **sum** ( T value ) |  T ∈ {int32_t, float32_t} |   ST ∈ {vi32_t, vf32_t}  |
| T **prod** ( ST value ) <br> T **prod** ( T value ) |  T ∈ {int32_t, float32_t} |   ST ∈ {vi32_t, vf32_t}  |
|||
| ST **abs** ( ST value )  | ST ∈ {vi32_t, vf32_t}  |  ST  |
| ST **min** ( ST value1, ST value2 )  | ST ∈ {vi32_t, vf32_t}  |  ST  |
| ST **max** ( ST value1, ST value2 )  | ST ∈ {vi32_t, vf32_t}  |  ST  |

##### Floating Point

| Function                       |  OUT   |  IN  |
| ------------------------------ | ------ | ---- |
| vf32_t **log** ( vf32_t value )  | vf32_t   | vf32_t   |
| vf32_t **log2** ( vf32_t value )  | vf32_t   | vf32_t   |
| vf32_t **log10** ( vf32_t value )  | vf32_t   | vf32_t   |
| ST **copyminus** ( ST to, ST from ) <br>  T **copyminus** ( T to, T from )  | ST ∈ {vi32_t, vf32_t} <br>  T ∈ {int32_t, float32_t}  | ST <br> T   |
|||
| float32_t **safe_sumXlogX** ( vf32_t value )  | float32_t   | vf32_t   |


### Array and Matrix

#### Array View and Matrix View

#### Array and Matrix Member Functions

#### Array View and Matrix View Member Functions

#### Functions of Arrays

#### Functions of Matrices

