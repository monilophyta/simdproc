
# Make sure new operator aligns correctly
AVX2_MANDATORY_FLAGS += -faligned-new=32

# Warn about a new-expression of a type that requires greater alignment, also warn about class member allocation functions
AVX2_MANDATORY_FLAGS += -Waligned-new=all
