
FFAST_MATH_FLAGS = -fno-math-errno  # Do not set "errno" after calling math functions that are executed with a single instruction, e.g., "sqrt"
FFAST_MATH_FLAGS += -funsafe-math-optimizations # Allow optimizations for floating-point arithmetic that (a) assume that
                                                # arguments and results are valid and (b) may violate IEEE or ANSI standards.
#FFAST_MATH_FLAGS += -ffinite-math-only  # Allow optimizations for floating-point arithmetic that assume that arguments and results are 
                                        # not NaNs or +-Infs.
FFAST_MATH_FLAGS += -fno-rounding-math   # default anyway
FFAST_MATH_FLAGS += -fno-signaling-nans   # default anyway
FFAST_MATH_FLAGS += -fcx-limited-range    # When enabled, this option states that a range reduction step is not needed when performing complex 
                                          # division. Also, there is no checking whether the result of a complex multiplication or division
										  # is "NaN + I*NaN", with an attempt to rescue the situation in that case.
FFAST_MATH_FLAGS += -fexcess-precision=fast

