/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iterator>



namespace smd
{

template <typename ST, typename ELEM_T = const typename ST::elem_t>
class ualigned_const_reference;

template <typename ST>
class ualigned_reference;


template <typename ST, typename ELEM_T, typename REF_T>
class ualigned_base_iterator : public std::iterator< std::random_access_iterator_tag,
                                                     ST,
                                                     std::ptrdiff_t,
                                                     ualigned_base_iterator<ST,ELEM_T,REF_T>,
                                                     REF_T >
{
    typedef ELEM_T                                              elem_t;
    typedef ualigned_base_iterator<ST,ELEM_T,REF_T>             this_t;

    typedef std::iterator< std::random_access_iterator_tag,
                           ST,
                           std::ptrdiff_t,
                           this_t,
                           REF_T >                              base_t;


public:

    using typename base_t::reference;
    using typename base_t::value_type;
    using typename base_t::difference_type;


    inline ualigned_base_iterator() : m_ptr(NULL) {}
    inline ualigned_base_iterator( elem_t* f_ptr ) : m_ptr(f_ptr) {}

    /// Iterator comparison
    inline bool operator==(const this_t& f_other) const;
    inline bool operator!=(const this_t& f_other) const;
    inline bool operator> (const this_t& f_other) const;
    inline bool operator>=(const this_t& f_other) const;
    inline bool operator< (const this_t& f_other) const;
    inline bool operator<=(const this_t& f_other) const;


    /// forward iterator progress
    inline this_t& operator++()      {                     std::advance(m_ptr,STEP); return (*this); }
    inline this_t& operator++(int)   { this_t aux = *this; std::advance(m_ptr,STEP); return (  aux); }

    /// backward iterator progress
    inline this_t& operator--()      {                     std::advance(m_ptr,-STEP); return (*this); }
    inline this_t& operator--(int)   { this_t aux = *this; std::advance(m_ptr,-STEP); return (  aux); }


    /// forward/backward iterator progress
    inline this_t operator+( difference_type n ) const { return this_t( std::move( std::next( m_ptr, n * STEP ) ) ); }
    inline this_t operator-( difference_type n ) const { return this_t( std::move( std::prev( m_ptr, n * STEP ) ) ); }

    inline this_t& operator+=( difference_type n ) { std::advance( m_ptr, n *  STEP ); return (*this); }
    inline this_t& operator-=( difference_type n ) { std::advance( m_ptr, n * -STEP ); return (*this); }

    /// Differences
    inline difference_type operator-( const this_t& f_other ) const { return (std::distance(f_other.m_ptr, m_ptr) / STEP); }

    // Dereferencing
    inline reference     operator*() const { return reference(m_ptr); }
    inline this_t&       operator->() { return *this; }
    inline const this_t& operator->() const { return *this; }

    inline reference operator[]( difference_type n ) const { return reference( std::next(m_ptr, n * STEP) ); }

private:

    static const std::ptrdiff_t STEP = ST::num_items();

    elem_t*    m_ptr;
};

// -----------------------------------------------------------------------------------------------------------------


template <typename ST>
using const_ualigned_iterator = ualigned_base_iterator<ST,
                                                       const typename ST::elem_t,
                                                       ualigned_const_reference<ST>>;

template <typename ST>
using ualigned_iterator = ualigned_base_iterator<ST,
                                                 typename ST::elem_t,
                                                 ualigned_reference<ST>>;


// -----------------------------------------------------------------------------------------------------------------


template <typename ST, typename ELEM_T>
class ualigned_const_reference
{
protected:

    typedef ST                             val_t;
    typedef ualigned_const_reference<ST>  this_t;
    typedef ELEM_T                        elem_t;

public:

    inline ualigned_const_reference()
        : m_ptr(NULL)
    {}

    inline ualigned_const_reference( elem_t* f_ptr )
        : m_ptr(f_ptr)
    {}

    inline this_t& operator=( const val_t& other ) = delete;
    inline this_t& operator=( const val_t&& other ) = delete;

    inline operator const val_t() const { return val_t::from_ptr(m_ptr); }

protected:

    elem_t* m_ptr;
};



// -----------------------------------------------------------------------------------------------------------------


template <typename ST>
class ualigned_reference : public ualigned_const_reference<ST,typename ST::elem_t>
{
protected:

    typedef ualigned_const_reference<ST,typename ST::elem_t>  base_t;
    typedef ualigned_reference<ST>                            this_t;

    using typename base_t::elem_t;
    using typename base_t::val_t;

public:

    using base_t::base_t;

    inline this_t& operator=( const val_t& other );
};


} // namespace smd
