/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_iterator_bundle.hpp"


namespace smd
{

template <class ST>
inline CIteratorBundleIterator<ST> make_iterator_bundle( size_t f_numIters, 
                                                         std::function<ST*(size_t)> iter_gen,
                                                         std::ptrdiff_t l_offset,
                                                         std::ptrdiff_t l_stepSize )
{
    static_assert( true == std::is_move_constructible< CIteratorBundleIterator<ST> >::value );
    static_assert( true == std::is_copy_constructible< CIteratorBundleIterator<ST> >::value );

    static_assert( true == std::is_move_constructible< CIteratorBundleValueIterator<ST> >::value );
    static_assert( true == std::is_copy_constructible< CIteratorBundleValueIterator<ST> >::value );

    static_assert( true == std::is_move_constructible< CIteratorBundleValue<ST> >::value );
    static_assert( true == std::is_copy_constructible< CIteratorBundleValue<ST> >::value );

    return cppt::make_iterator_bundle( f_numIters, iter_gen, l_offset, l_stepSize );
}


// -------------------------------------------------------------------------------------------



template <typename T, typename ...iTs, class>
inline typename select_noconst_simd<T>::type
gather( const CIteratorBundleValueIterator<T>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 )
{
    typedef typename select_noconst_simd<T>::type  simd_elem_t;
    static_assert( vt_i32<iTs...>::size() == simd_elem_t::size() );

    simd_elem_t l_simdval;
    {
        auto valIt = l_simdval.begin();

        for (auto idxIt  = f_idx_vi32.cbegin(); 
                  idxIt != f_idx_vi32.cend();
            ++idxIt, ++valIt )
        {
            *valIt = f_from_a[ *idxIt ];
        }
    }

    return l_simdval;
}



template <typename VT, typename ...iTs, class>
inline typename std::remove_const<VT>::type
gather( const CIteratorBundleValueIterator<VT>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 )
{
    typedef typename std::remove_const<VT>::type  simd_elem_t;
    static_assert( vt_i32<iTs...>::size() == simd_elem_t::size() );

    simd_elem_t l_simdval;
    {
        int32_t vidx = 0;
        auto valIt = l_simdval.begin();

        for (auto idxIt  = f_idx_vi32.cbegin(); 
                  idxIt != f_idx_vi32.cend();
            ++idxIt, ++vidx, ++valIt )
        {
            *valIt = f_from_a[ *idxIt ][ vidx ];
        }
    }

    return l_simdval;
}



template <typename T, typename ...iTs, class>
inline typename select_noconst_simd<T>::type
gather( const CIteratorBundleIterator<T>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 )
{
    typedef typename select_noconst_simd<T>::type  simd_elem_t;
    static_assert( vt_i32<iTs...>::size() == simd_elem_t::size() );

    simd_elem_t l_simdval;
    {
        int32_t vidx = 0;
        auto valIt = l_simdval.begin();

        for (auto idxIt  = f_idx_vi32.cbegin(); 
                  idxIt != f_idx_vi32.cend();
            ++idxIt, ++vidx, ++valIt )
        {
            *valIt = f_from_a[vidx][ *idxIt ];
        }
    }

    return l_simdval;
}


} // namespace smd

