/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_iterator.hpp"


namespace smd
{


#define ITER_BINARY_COMPARISON_OP( OP ) \
template <typename ST, typename ELEM_T, typename REF_T> \
inline bool ualigned_base_iterator<ST,ELEM_T,REF_T>::operator OP ( const this_t &other ) const \
{ \
    return (m_ptr OP other.m_ptr); \
}

ITER_BINARY_COMPARISON_OP( == )
ITER_BINARY_COMPARISON_OP( != )
ITER_BINARY_COMPARISON_OP( < )
ITER_BINARY_COMPARISON_OP( <= )
ITER_BINARY_COMPARISON_OP( > )
ITER_BINARY_COMPARISON_OP( >= )

#undef ITER_BINARY_COMPARISON_OP



template <typename ST>
inline auto ualigned_reference<ST>::operator=( const val_t& other ) -> this_t&
{
    // store assignment
    other.store(this->m_ptr);
    
    return (*this);
}




} // namespace smd

