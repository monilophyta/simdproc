/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 */

#pragma once

#include <type_traits>
#include <cpp_tools.hpp>


namespace smd
{


//--------------------------------------------------------------------------------------------

template <typename ST>
struct CIteratorBundleIterator;

template <typename ST>
struct CIteratorBundleValue;

template <typename ST>
struct CIteratorBundleValueIterator;


// -------------------------------------------------------------------------------------------

template <class ST>
inline CIteratorBundleIterator<ST> make_iterator_bundle( size_t f_numIters, 
                                                         std::function<ST*(size_t)> iter_gen,
                                                         std::ptrdiff_t l_offset=0,
                                                         std::ptrdiff_t l_stepSize=1 );

// -------------------------------------------------------------------------------------------


/*!
    \brief gathers f_idx_vi32.size() elements from iterator bundle

    \param f_from_a random access iterator where elements are gathered from
    \param f_idx_vi32 indices of items in f_from_a to be gathered

    \return retv of type select_simd<T>::type with retv.size() == f_idx_vi32.size and 
    retv[i] = f_from_a[ f_idx_vi32[i] ]
*/
template <typename T, typename ...iTs, 
          class = typename std::enable_if< std::is_fundamental<T>::value >::type>
inline typename select_noconst_simd<T>::type
gather( const CIteratorBundleValueIterator<T>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 );


/*!
    \brief gathers f_idx_vi32.size() elements from vector iterator bundle

    \param f_from_a random access iterator where elements are gathered from
    \param f_idx_vi32 indices of items in f_from_a to be gathered

    \return retv of type std::remove_const<VT>::type with retv.size() == f_idx_vi32.size and 
    retv[i] = f_from_a[ f_idx_vi32[i] ][ i ]
*/
template <typename VT, typename ...iTs,
          class = typename std::enable_if< is_simd<VT>::value >::type>
inline typename std::remove_const<VT>::type
gather( const CIteratorBundleValueIterator<VT>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 );


/*!
    \brief gathers f_idx_vi32.size() elements from vector iterator bundle

    \param f_from_a random access iterator where elements are gathered from
    \param f_idx_vi32 indices of items in f_from_a to be gathered

    \return retv of type select_noconst_simd<T>::type with retv.size() == f_idx_vi32.size and 
    retv[i] = f_from_a[i][ f_idx_vi32[i] ]
*/
template <typename T, typename ...iTs,
          class = typename std::enable_if< std::is_fundamental<T>::value >::type>
inline typename select_noconst_simd<T>::type
gather( const CIteratorBundleIterator<T>& f_from_a, const vt_i32<iTs...>& f_idx_vi32 );


// -------------------------------------------------------------------------------------------


template <typename ST>
struct CIteratorBundleValueIterator : public cppt::CIteratorBundleValueIterator<ST*>
{
private:

    typedef cppt::CIteratorBundleValueIterator<ST*> base_t;
    typedef CIteratorBundleValueIterator<ST*>       this_t;

    typedef std::remove_const<ST>                   elem_t;

public:

    using base_t::base_t;

    inline CIteratorBundleValueIterator( const base_t& f_other )
        : base_t( f_other )
    {}

    inline CIteratorBundleValueIterator( base_t&& f_other )
        : base_t( std::move(f_other) )
    {}

    // conversion from non const to const pointer
    inline CIteratorBundleValueIterator( const CIteratorBundleValueIterator<elem_t>& f_other )
        //: base_t( reinterpret_cast<const this_t&>(f_other) )  - maybe this cast is required?!
        : base_t( static_cast<const this_t&>(f_other) )
    {}
};

//--------------------------------------------------------------------------------------------


template <typename ST>
struct CIteratorBundleValue : public cppt::CIteratorBundleValue<ST*>
{
private:

    typedef cppt::CIteratorBundleValue<ST*>         base_t;
    typedef CIteratorBundleValue<ST*>               this_t;

    typedef std::remove_const<ST>                   elem_t;

public:

    using base_t::base_t;

    inline CIteratorBundleValue( const base_t& f_other )
        : base_t( f_other )
    {}

    inline CIteratorBundleValue( base_t&& f_other )
        : base_t( std::move(f_other) )
    {}

    // conversion from non const to const pointer
    inline CIteratorBundleValue( const CIteratorBundleValue<elem_t>& f_other )
        //: base_t( reinterpret_cast<const this_t&>(f_other) )  - maybe this cast is required?!
        : base_t( static_cast<const this_t&>(f_other) )
    {}
};

//--------------------------------------------------------------------------------------------

template <typename ST>
struct CIteratorBundleIterator : public cppt::CIteratorBundleIterator<ST*>
{
private:

    typedef cppt::CIteratorBundleIterator<ST*> base_t;
    typedef CIteratorBundleIterator<ST*>       this_t;

    typedef std::remove_const<ST>              elem_t;

public:

    using base_t::base_t;

    inline CIteratorBundleIterator( const base_t& f_other )
        : base_t( f_other )
    {}

    inline CIteratorBundleIterator( base_t&& f_other )
        : base_t( std::move(f_other) )
    {}

    // conversion from non const to const pointer
    inline CIteratorBundleIterator( const CIteratorBundleIterator<elem_t>& f_other )
        //: base_t( reinterpret_cast<const this_t&>(f_other) )  - maybe this cast is required?!
        : base_t( static_cast<const this_t&>(f_other) )
    {}
};


//--------------------------------------------------------------------------------------------





} // namespace smd
