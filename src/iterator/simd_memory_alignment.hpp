/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cpp_tools.hpp>


namespace smd
{

struct v32_array_aligner
    : public cppt::array_aligner< sizeof(float32_t), sizeof(smd::vf32_t)>
{
private:

    typedef cppt::array_aligner< sizeof(float32_t), sizeof(smd::vf32_t)>  base_t;

public:

    // inherit constructor
    using base_t::base_t;

    template <typename T,
              typename = typename std::enable_if< is_simd_elem<T>::value >::type>
    inline auto
    aligned_begin_cast( T* f_array_p ) const -> typename select_simd<T>::type*
    {
        typedef typename smd::select_simd<T>::type vect_t;

        vect_t* l_begin_p = reinterpret_cast<vect_t*>( base_t::aligned_begin( f_array_p ) );
        simd_assert( is_memory_aligned( l_begin_p ), "casting to unaligned memory" );

        return l_begin_p;
    }
    

    template <typename T,
              typename = typename std::enable_if< is_simd_elem<T>::value >::type>
    inline auto
    aligned_end_cast( T* f_array_p ) const -> typename select_simd<T>::type*
    {
        typedef typename smd::select_simd<T>::type vect_t;

        vect_t* l_end_p = reinterpret_cast<vect_t*>( base_t::aligned_end( f_array_p ) );
        simd_assert( is_memory_aligned( l_end_p ), "casting to unaligned memory" );

        return l_end_p;
    }
};



} // namespace smd
