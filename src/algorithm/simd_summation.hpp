/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{

/** @name KahanSummationAlgorithm
 * @brief Compensates errors in summations using Kahan summation algorithm
 * @details This function does one addition like
 * f_accu += f_x  or  f_accu += f_w * f_x
 * in an error compensated way. The parameter reference f_accu = f_sum always contain the current
 * valid result of summation
 * @param f_accu Summation accumulator. Has to be reused on every sum update
 * @param f_w (Optional) weight for f_x
 * @param f_x summand in called update
 * @param f_comp keeper for for error compensation. Has to be reused on every sum update
 */
///@{
inline void kahan_sum_step( float32_t& f_accu_f32,     float32_t  f_x_f32, float32_t& f_comp_f32 );
inline void kahan_sum_step(    vf32_t& f_accu_vf32, const vf32_t& f_x_vf32,   vf32_t& f_comp_vf32 );

inline void kahan_sum_step( float32_t& f_accu_f32,     float32_t  f_w_f32,     float32_t  f_x_f32, float32_t& f_comp_f32 );
inline void kahan_sum_step(    vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32,   vf32_t& f_comp_vf32 );
///@}



/** @name NeumaierSummationAlgorithm
 * @brief Compensates errors in summations using improved Kahan–Babuška algorithm (introduced bei Neimaier)
 * @details This function does one addition like
 * f_accu += f_x  or  f_accu += f_w * f_x
 * in an error compensated way. The valid sum result has to be optained by
 * f_sum = f_accu + f_comp
 * @param f_accu Summation accumulator. Has to be reused on every sum update
 * @param f_w (Optional) weight for f_x
 * @param f_x summand in called update
 * @param f_comp keeper for for error compensation. Has to be reused on every sum update
 */
///@{
inline void neumaier_sum_step( float32_t& f_accu_f32,     float32_t  f_x_f32, float32_t& f_comp_f32 );
inline void neumaier_sum_step(    vf32_t& f_accu_vf32, const vf32_t& f_x_vf32,   vf32_t& f_comp_vf32 );

inline void neumaier_sum_step( float32_t& f_accu_f32,     float32_t  f_w_f32,     float32_t  f_x_f32, float32_t& f_comp_f32 );
inline void neumaier_sum_step(    vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32,   vf32_t& f_comp_vf32 );
///@}




/** @name KleinSummationAlgorithm
 * @brief Compensates errors in summations using second-order iterative Kahan–Babuška algorithm (introduced by Klein)
 * @details This function does one addition like
 * f_accu += f_x  or  f_accu += f_w * f_x
 * in an error compensated way. The valid sum result has to be optained by
 * f_sum = f_accu + f_comp1 + f_comp2
 * @param f_accu Summation accumulator. Has to be reused on every sum update
 * @param f_w (Optional) weight for f_x
 * @param f_x summand in called update
 * @param f_comp1 first-order keeper for for error compensation. Has to be reused on every sum update
 * @param f_comp2 second-order keeper for for error compensation. Has to be reused on every sum update
 */
///@{
inline void klein_sum_step( float32_t& f_accu_f32,  float32_t  f_x_f32, 
                            float32_t& f_comp1_f32, float32_t& f_comp2_f32 );
inline void klein_sum_step( float32_t& f_accu_f32,  float32_t f_w_f32, float32_t f_x_f32, 
                            float32_t& f_comp1_f32, float32_t& f_comp2_f32 );

inline void klein_sum_step( vf32_t& f_accu_vf32,  const vf32_t& f_x_vf32, 
                            vf32_t& f_comp1_vf32,       vf32_t& f_comp2_vf32 );
inline void klein_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32, 
                            vf32_t& f_comp1_vf32,      vf32_t& f_comp2_vf32 );
///@}


} // namespace smd
