/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_summation.hpp"


namespace smd
{

namespace
{
    template<typename RT, typename XT>
    inline 
    void generic_kahan_sum_step( RT& f_accu, XT f_x, RT& f_comp )
    {
        /// compensate for lost low-order bits
        const XT l_y = f_x - f_comp;
        
        /// calculate sum
        const XT l_accu = f_accu + l_y;

        /// update compensator
        f_comp = (l_accu - f_accu) - l_y;

        /// update accumulator
        f_accu = l_accu;
    }
} // anonymous namespace


inline void
kahan_sum_step( float32_t& f_accu_f32, float32_t f_x_f32, float32_t& f_comp_f32 )
{
    generic_kahan_sum_step<float32_t,float32_t>( f_accu_f32, f_x_f32, f_comp_f32 );
}

inline void
kahan_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_x_vf32, vf32_t& f_comp_vf32 )
{
    generic_kahan_sum_step<vf32_t,const vf32_t>( f_accu_vf32, f_x_vf32, f_comp_vf32 );
}

inline void
kahan_sum_step( float32_t& f_accu_f32, float32_t f_w_f32, float32_t f_x_f32, float32_t& f_comp_f32 )
{
    generic_kahan_sum_step<float32_t,float32_t>( f_accu_f32, (f_w_f32 * f_x_f32), f_comp_f32 );
}


inline void
kahan_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32, vf32_t& f_comp_vf32 )
{
    /// compensate for lost low-order bits
    const vf32_t l_y_vf32 = (f_w_vf32 * f_x_vf32) - f_comp_vf32;  // might use fma instructions if available
    
    /// calculate sum
    const vf32_t l_accu_vf32 = f_accu_vf32 + l_y_vf32;

    /// update compensator
    f_comp_vf32 = (l_accu_vf32 - f_accu_vf32) - l_y_vf32;

    /// update accumulator
    f_accu_vf32 = l_accu_vf32;
}

// ---------------------------------------------------------------------------------------------------------------

namespace
{
    static inline
    float32_t generic_neumaier_sum_step( float32_t& f_accu_f32, float32_t f_x_f32 )
    {
        /// calculate sum
        const float32_t l_accu_f32 = f_accu_f32 + f_x_f32;

        /// update compensator
        float32_t l_comp_f32 = (std::abs(f_accu_f32) >= std::abs(f_x_f32))
                                ? ((f_accu_f32 - l_accu_f32) + f_x_f32)   // f_accu_f32 is bigger, low-order digits of f_x_32 are lost
                                : (f_x_f32 - l_accu_f32) + f_accu_f32;    // l_x_32 is bigger, low-order digits of f_accu_f32 are lost.
        
        f_accu_f32 = l_accu_f32;
        return l_comp_f32;
    }

    static inline
    vf32_t generic_neumaier_sum_step( vf32_t& f_accu_vf32, vf32_t f_x_vf32 )
    {
        /// calculate sum
        const vf32_t l_accu_vf32 = f_accu_vf32 + f_x_vf32;

        /// update compensator
        vf32_t l_comp_vf32 = smd::blend(  std::abs(f_accu_vf32) >= std::abs(f_x_vf32),
                                         (f_accu_vf32 - l_accu_vf32) + f_x_vf32,      // f_accu_f32 is bigger, low-order digits of f_x_32 are lost
                                         (f_x_vf32 - l_accu_vf32) + f_accu_vf32 );    // l_x_32 is bigger, low-order digits of f_accu_f32 are lost.
        
        f_accu_vf32 = l_accu_vf32;
        return l_comp_vf32;
    }

    static inline
    vf32_t generic_neumaier_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32 )
    {
        /// calculate sum
        const vf32_t l_accu_vf32 = f_accu_vf32 + (f_w_vf32 * f_x_vf32);  // might use fma instructions if available

        /// update compensator
        vf32_t l_comp_vf32 = smd::blend(  std::abs(f_accu_vf32) >= std::abs(f_x_vf32),
                                         (f_accu_vf32 - l_accu_vf32) + (f_w_vf32 * f_x_vf32),      // f_accu_f32 is bigger, low-order digits of f_x_32 are lost
                                         ((f_w_vf32 * f_x_vf32) - l_accu_vf32) + f_accu_vf32 );    // l_x_32 is bigger, low-order digits of f_accu_f32 are lost.
        
        f_accu_vf32 = l_accu_vf32;
        return l_comp_vf32;
    }

} // anonymous namespace


inline void
neumaier_sum_step( float32_t& f_accu_f32, float32_t f_x_f32, float32_t& f_comp_f32 )
{
    f_comp_f32 += generic_neumaier_sum_step( f_accu_f32, f_x_f32 );
}


inline void
neumaier_sum_step( float32_t& f_accu_f32, float32_t f_w_f32, float32_t f_x_f32, float32_t& f_comp_f32 )
{
    f_comp_f32 += generic_neumaier_sum_step( f_accu_f32, f_w_f32 * f_x_f32 );
}


inline void
neumaier_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_x_vf32, vf32_t& f_comp_vf32 )
{
    f_comp_vf32 += generic_neumaier_sum_step( f_accu_vf32, f_x_vf32 );
}



inline void
neumaier_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32, vf32_t& f_comp_vf32 )
{
    f_comp_vf32 += generic_neumaier_sum_step( f_accu_vf32, f_w_vf32, f_x_vf32 );
}

// ---------------------------------------------------------------------------------------------------------------


inline void
klein_sum_step( float32_t& f_accu_f32,  float32_t  f_x_f32, 
                float32_t& f_comp1_f32, float32_t& f_comp2_f32 )
{
    // first order
    const float32_t l_comp1_f32 = generic_neumaier_sum_step( f_accu_f32, f_x_f32 );

    // second order
    f_comp2_f32 += generic_neumaier_sum_step( f_comp1_f32, l_comp1_f32 );
}


inline void
klein_sum_step( float32_t& f_accu_f32,  float32_t  f_w_f32, float32_t f_x_f32, 
                float32_t& f_comp1_f32, float32_t& f_comp2_f32 )
{
    // first order
    const float32_t l_comp1_f32 = generic_neumaier_sum_step( f_accu_f32, f_w_f32 * f_x_f32 );

    // second order
    f_comp2_f32 += generic_neumaier_sum_step( f_comp1_f32, l_comp1_f32 );
}


inline void
klein_sum_step( vf32_t& f_accu_vf32,  const vf32_t& f_x_vf32, 
                vf32_t& f_comp1_vf32,       vf32_t& f_comp2_vf32 )
{
    // first order
    const vf32_t l_comp1_vf32 = generic_neumaier_sum_step( f_accu_vf32, f_x_vf32 );

    // second order
    f_comp2_vf32 += generic_neumaier_sum_step( f_comp1_vf32, l_comp1_vf32 );
}


inline void
klein_sum_step( vf32_t& f_accu_vf32, const vf32_t& f_w_vf32, const vf32_t& f_x_vf32, 
                vf32_t& f_comp1_vf32,      vf32_t& f_comp2_vf32 )
{
    // first order
    const vf32_t l_comp1_vf32 = generic_neumaier_sum_step( f_accu_vf32, f_w_vf32, f_x_vf32 );

    // second order
    f_comp2_vf32 += generic_neumaier_sum_step( f_comp1_vf32, l_comp1_vf32 );
}

} // namespace smd

