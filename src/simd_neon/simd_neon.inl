/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_neon.hpp"



namespace smd
{


namespace
{

constexpr bool is_neon_aligned( const void* f_ptr )
{
    return ( 0u == (reinterpret_cast<size_t>(f_ptr) & (sizeof(v128)-1u)) );
}

} // anonymous namespace


inline neon_b32_vt neon_b32_vt::from_ptr( const bool32_t* f_ptr )
{
    neon_b32_vt v;
    v.val.m128i = is_neon_aligned(f_ptr) ? _*reinterpret_cast<const m128i_t*>(f_ptr)
                                         : vld1q_s32( static_cast<const int32_t*>(f_ptr) );
    return v;
}



/// store content of vector in memory
inline void neon_b32_vt::store( bool32_t* f_stream_p ) const
{
    if ( is_neon_aligned(f_stream_p) )
    {
        *reinterpret_cast<m128i_t*>(f_stream_p) = this->val.m128i;
    }
    else
    {
        vst1q_s32( static_cast<int32_t*>(f_stream_p), this->val.m128i );
    }
}


// --------------------------------------------------------------------------------


template <class... oTs>
inline typename vt_base<oTs...>::derived_t 
blend( const neon_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right )
{
    typedef typename vt_base<oTs...>::derived_t arg_derived_t;

    uint32x4_t bitmask = vtstq_s32( f_mask.val.m128i, f_mask.val.m128i );

    arg_derived_t res;
    res.val.m128i = vbslq_s32( bitmask,              // if (mask)
                               f_left.val.m128i,    // then
                               f_right.val.m128i ); // else
    return res;
}

// --------------------------------------------------------------------------------


inline bool
all( const neon_b32_vt &x )
{
    uint32x4_t logical = vtstq_s32( x.val.m128i, x.val.m128i );
    uint32x2_t l_or = vand_u32( vget_high_u32(logical), vget_low_u32(logical) );  // [0 1] || [2 3]

    return (vget_lane_u32(l_or, 0) && vget_lane_u32(l_or, 1));
}


inline bool
any( const neon_b32_vt &x )
{
    uint32x4_t logical = vtstq_s32( x.val.m128i, x.val.m128i );
    uint32x2_t l_or = vorr_u32( vget_high_u32(logical), vget_low_u32(logical) );  // [0 1] || [2 3]

    return (vget_lane_u32(l_or, 0) || vget_lane_u32(l_or, 1));
}

// --------------------------------------------------------------------------------

inline neon_f32_vt neon_i32_vt::cast_to_vf32() const 
{
    neon_f32_vt res;

    res.val.m128f = vcvtq_f32_s32( val.m128i );
    return res;
}

// --------------------------------------------------------------------------------


inline neon_i32_vt neon_i32_vt::from_ptr( const int32_t* f_ptr )
{
    neon_i32_vt v;
    v.val.m128i = is_neon_aligned(f_ptr) ? *reinterpret_cast<const m128i_t*>(f_ptr)
                                         : vld1q_s32( f_ptr );
    return v;
}


inline neon_i32_vt neon_i32_vt::zero()
{
    return neon_i32_vt( v128( int32_t(0) ) );
}


/// store content of vector in memory
inline void neon_i32_vt::store( int32_t* f_stream_p ) const
{
    if ( is_neon_aligned(f_stream_p) )
    {
        *reinterpret_cast<m128i_t*>(f_stream_p) = this->val.m128i;
    }
    else
    {
        vst1q_s32( f_stream_p, this->val.m128i );
    }
}


#if 0


    inline neon_i32_vt neon_i32_vt::gather( const int32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx int32 pointer gather implementation" );
        
        __neon_helper::neon_i32gather_i32 gfunc;

        return neon_i32_vt( v128( gfunc( f_from_a,
                                        derived().val.m128i ) ) );
    }


    inline neon_f32_vt neon_i32_vt::gather( const float32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx float32 pointer gather implementation" );
        
        __neon_helper::neon_i32gather_f32 gfunc;

        return neon_f32_vt( v128( gfunc( f_from_a,
                                        derived().val.m128i ) ) );
    }

#endif



// --------------------------------------------------------------------------------


inline neon_f32_vt neon_f32_vt::from_ptr( const float32_t* f_ptr )
{
    neon_f32_vt v;
    v.val.m128f = is_neon_aligned(f_ptr) ? *reinterpret_cast<const m128f_t*>(f_ptr)
                                         : vld1q_f32( f_ptr );
    return v;
}


inline neon_f32_vt neon_f32_vt::zero()
{
    return neon_f32_vt( v128( 0.f )  );
}


/// store content of vector in memory
inline void neon_f32_vt::store( float32_t* f_stream_p ) const
{
    if ( is_neon_aligned(f_stream_p) )
    {
        *reinterpret_cast<m128f_t*>(f_stream_p) = this->val.m128f;
    }
    else
    {
        vst1q_f32( f_stream_p, this->val.m128f );
    }
}


// --------------------------------------------------------------------------------

inline int32_t sum( const neon_i32_vt &x )
{
    int32x2_t aux = vget_high_s32( x.val.m128i ) + vget_low_s32( x.val.m128i ); 
    return (vget_lane_s32(aux, 0) + vget_lane_s32(aux, 1));
}



inline int32_t prod( const neon_i32_vt &x )
{
    int32x2_t aux = vget_high_s32( x.val.m128i ) * vget_low_s32( x.val.m128i ); 
    return (vget_lane_s32(aux, 0) * vget_lane_s32(aux, 1));
}

// --------------------------------------------------------------------------------
// sum product for floating

inline float32_t sum( const neon_f32_vt &x )
{
    float32x2_t aux = vget_high_f32( x.val.m128f ) + vget_low_f32( x.val.m128f ); 
    return (vget_lane_f32(aux, 0) + vget_lane_f32(aux, 1));
}


inline float32_t prod( const neon_f32_vt &x )
{
    float32x2_t aux = vget_high_f32( x.val.m128f ) * vget_low_f32( x.val.m128f ); 
    return (vget_lane_f32(aux, 0) * vget_lane_f32(aux, 1));
}


} // namespace smd

// --------------------------------------------------------------------------------



