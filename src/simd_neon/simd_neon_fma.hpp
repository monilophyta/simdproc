/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_neon_multiplication.hpp"



namespace smd
{


// ---------------------------------------------------------------------------------------------------------------------------
// operations of type fmadd: "(a*b) + c"


template <typename MT>
inline neon_f32_vt operator+( const simd_multiply_base<neon_f32_vt,MT>& f_a, const neon_f32_vt& f_b );


template <typename MT>
inline neon_f32_vt operator+( const simd_multiply_base<neon_f32_vt,MT>& f_a, float32_t f_b );


template <typename MT>
inline neon_f32_vt operator+( const neon_f32_vt& f_b, const simd_multiply_base<neon_f32_vt,MT>& f_a ) { return (f_a + f_b); }


template <typename MT>
inline neon_f32_vt operator+( float32_t f_b, const simd_multiply_base<neon_f32_vt,MT>& f_a ) { return (f_a + f_b); }


// ---------------------------------------------------------------------------------------------------------------------------
// operations of type fmsub: "(a*b) - c"

template <typename MT>
inline neon_f32_vt operator-( const simd_multiply_base<neon_f32_vt,MT>& f_a, const neon_f32_vt& f_b );


template <typename MT>
inline neon_f32_vt operator-( const simd_multiply_base<neon_f32_vt,MT>& f_a, float32_t f_b );


// ---------------------------------------------------------------------------------------------------------------------------
// operations of type fnmadd: "c - (a*b)"

template <typename MT>
inline neon_f32_vt operator-( const neon_f32_vt& f_a, const simd_multiply_base<neon_f32_vt,MT>& f_b );


template <typename MT>
inline neon_f32_vt operator-( float32_t f_a, const simd_multiply_base<neon_f32_vt,MT>& f_b );


// ---------------------------------------------------------------------------------------------------------------------------
// operations of type fnmsub: "-(a*b) - c"
//
//TODO: this requires unary operation type


} // namespace smd
