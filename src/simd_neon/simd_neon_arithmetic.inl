/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_neon_arithmetic.hpp"
#include <arm_neon.h>

namespace smd
{


inline smd::neon_f32_vt rcp( const smd::neon_f32_vt &x )
{
    smd::neon_f32_vt res;
    res.val.m128f = vrecpeq_f32( x.val.m128f );
    return res;
}


inline smd::neon_f32_vt rsqrt( const smd::neon_f32_vt &x )
{
    smd::neon_f32_vt res;
    res.val.m128f = vrsqrteq_f32( x.val.m128f );
    return res;
}

#if defined(__ARM_FEATURE_DIRECTED_ROUNDING) or defined(__ARM_FEATURE_DIRECTED_ROUNDING__) 

    inline smd::neon_f32_vt ceil( const smd::neon_f32_vt &x )
    {
        smd::neon_f32_vt res;
        res.val.m128f = vrndpq_f32( x.val.m128f ); // Round to integral (towards +Inf)
        return res;
    }

    inline smd::neon_f32_vt floor( const smd::neon_f32_vt &x )
    {
        smd::neon_f32_vt res;
        res.val.m128f = vrndmq_f32( x.val.m128f );  // Round to integral (towards -Inf)
        return res;
    }


    inline smd::neon_f32_vt round( const smd::neon_f32_vt &x )
    {
        return trunc(x + 0.5f);
    }

    inline smd::neon_f32_vt trunc( const smd::neon_f32_vt &x )
    {
        smd::neon_f32_vt res;
        res.val.m128f = vrndq_f32( x.val.m128f ); // Round to integral (towards 0)
        return res;
    }

#endif

} // namespace smd


// --------------------------------------------------------------------------------------------
// ---STD NAMESPACE----------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------



namespace std
{

// --------------------------------------------------------------------------------

inline smd::neon_i32_vt max( const smd::neon_i32_vt &x, const smd::neon_i32_vt &y )
{
    smd::neon_i32_vt z;
    z.val.m128i = vmaxq_s32( x.val.m128i, y.val.m128i );

    return z;
}

inline smd::neon_i32_vt min( const smd::neon_i32_vt &x, const smd::neon_i32_vt &y )
{
    smd::neon_i32_vt z;
    z.val.m128i = vminq_s32( x.val.m128i, y.val.m128i );

    return z;
}

// --------------------------------------------------------------------------------



inline smd::neon_f32_vt max( const smd::neon_f32_vt &x, const smd::neon_f32_vt &y )
{
    smd::neon_f32_vt z;
    z.val.m128f = vmaxq_f32( x.val.m128f, y.val.m128f );

    return z;
}

inline smd::neon_f32_vt min( const smd::neon_f32_vt &x, const smd::neon_f32_vt &y )
{
    smd::neon_f32_vt z;
    z.val.m128f = vminq_f32( x.val.m128f, y.val.m128f );

    return z;
}

// --------------------------------------------------------------------------------

inline smd::neon_f32_vt log( const smd::neon_f32_vt &x )
{
    smd::foreign::CGarberoglioLog<smd::neon_f32_vt,smd::neon_i32_vt> logfunctor;
    
    smd::neon_f32_vt res = logfunctor(x);
    return res;
}




inline smd::neon_f32_vt sqrt( const smd::neon_f32_vt &x )
{
    smd::neon_f32_vt res;
    
    #ifdef __SIMD_USE_ARM64__
        res.val.m128f = vsqrtq_f32( x.val.m128f );
    #else
        res.val.m128f = vrecpeq_f32( vrsqrteq_f32( x.val.m128f ) );
    #endif
    
    return res;
}



inline smd::neon_i32_vt abs( const smd::neon_i32_vt &x )
{
    return smd::neon_i32_vt( smd::v128( vabsq_s32( x.val.m128i ) ) );
}

// strange error in testing
// inline smd::neon_f32_vt abs( const smd::neon_f32_vt &x )
// {
//     smd::neon_i32_vt res;
//     res.val.m128f = vabsq_f32( x.val.m128f );
//     return res;
// }


}  // namespace std


