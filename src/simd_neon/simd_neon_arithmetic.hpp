/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{

inline smd::neon_f32_vt rcp( const smd::neon_f32_vt &x );
inline smd::neon_f32_vt rsqrt( const smd::neon_f32_vt &x );


#if defined(__ARM_FEATURE_DIRECTED_ROUNDING) or defined(__ARM_FEATURE_DIRECTED_ROUNDING__) 

    inline smd::neon_f32_vt ceil( const smd::neon_f32_vt &x );
    inline smd::neon_f32_vt floor( const smd::neon_f32_vt &x );
    inline smd::neon_f32_vt round( const smd::neon_f32_vt &x );
    inline smd::neon_f32_vt trunc( const smd::neon_f32_vt &x );

#endif

} // namespace smd





namespace std
{

inline smd::neon_i32_vt max( const smd::neon_i32_vt &x, const smd::neon_i32_vt &y );
inline smd::neon_i32_vt min( const smd::neon_i32_vt &x, const smd::neon_i32_vt &y );

inline smd::neon_f32_vt max( const smd::neon_f32_vt &x, const smd::neon_f32_vt &y );
inline smd::neon_f32_vt min( const smd::neon_f32_vt &x, const smd::neon_f32_vt &y );

inline smd::neon_f32_vt log( const smd::neon_f32_vt &x );
inline smd::neon_f32_vt sqrt( const smd::neon_f32_vt &x );

inline smd::neon_i32_vt abs( const smd::neon_i32_vt &x );
//inline smd::neon_f32_vt abs( const smd::neon_f32_vt &x );

} // namespace std
