/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_neon_fma.hpp"
#include <arm_neon.h>


namespace smd
{

// -------------------------------------------------------------------------------------------------------------
// Inside class operators
// -------------------------------------------------------------------------------------------------------------


template <typename MT>
inline const neon_f32_vt& neon_f32_vt::operator+=( const simd_multiply_base<neon_f32_vt,MT> &other )
{
    // vfmaq_f32(a,b,c) => a + b*c
    this->val.m128f = vfmaq_f32( this->val.m128f, other.vx().val.m128f, other.vy().val.m128f );
    return *this;
}


template <typename MT>
inline const neon_f32_vt& neon_f32_vt::operator-=( const simd_multiply_base<neon_f32_vt,MT> &other )
{
    // vfmsq_f32(a,b,c) => a - b*c
    this->val.m128f = vfmsq_f32( this->val.m128f, other.vx().val.m128f, other.vy().val.m128f );
    return *this;
}

// -------------------------------------------------------------------------------------------------------------
// Outside class operators
// -------------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------
// operations of type fmadd: "(a*b) + c"


template <typename MT>
inline neon_f32_vt
operator+( const simd_multiply_base<neon_f32_vt,MT>& f_a, const neon_f32_vt& f_b )
{ 
    neon_f32_vt res;
    // vfmaq_f32(a,b,c) => a + b*c
    res.val.m128f = vfmaq_f32( f_b.val.m128f, f_a.vx().val.m128f, f_a.vy().val.m128f );
    return res;
}


template <typename MT>
inline neon_f32_vt
operator+( const simd_multiply_base<neon_f32_vt,MT>& f_a, float32_t f_b )
{ 
    neon_f32_vt res;
    // vfmaq_f32(a,b,c) => a + b*c
    res.val.m128f = vfmaq_f32( neon_f32_vt( f_b ).val.m128f, f_a.vx().val.m128f, f_a.vy().val.m128f );
    return res;
}

// -------------------------------------------------------------------------------------------------
// operations of type fmsub: "(a*b) - c"

template <typename MT>
inline neon_f32_vt
operator-( const simd_multiply_base<neon_f32_vt,MT>& f_a, const neon_f32_vt& f_b )
{
    neon_f32_vt res;
    // vfmsq_f32(a,b,c) => a - b*c
    res.val.m128f = vfmsq_f32( f_b.val.m128f, f_a.vx().val.m128f, f_a.vy().val.m128f );  // c - a*b
    return (-res);   // -(c - a*b) = a*b - c
}


template <typename MT>
inline neon_f32_vt
operator-( const simd_multiply_base<neon_f32_vt,MT>& f_a, float32_t f_b )
{
    neon_f32_vt res;
    // vfmsq_f32(a,b,c) => a - b*c
    res.val.m128f = vfmsq_f32( neon_f32_vt( f_b ).val.m128f, f_a.vx().val.m128f, f_a.vy().val.m128f );  // c - a*b
    return (-res);   // -(c - a*b) = a*b - c
}

// -------------------------------------------------------------------------------------------------
// operations of type fnmadd: "c - (a*b)"

template <typename MT>
inline neon_f32_vt
operator-( const neon_f32_vt& f_a, const simd_multiply_base<neon_f32_vt,MT>& f_b )
{
    neon_f32_vt res;
    // vfmsq_f32(a,b,c) => a - b*c
    res.val.m128f = vfmsq_f32( f_a.val.m128f,  f_b.vx().val.m128f, f_b.vy().val.m128f );
    return res;
}


template <typename MT>
inline neon_f32_vt
operator-( float32_t f_a, const simd_multiply_base<neon_f32_vt,MT>& f_b )
{
    neon_f32_vt res;
    // vfmsq_f32(a,b,c) => a - b*c
    res.val.m128f = vfmsq_f32( neon_f32_vt( f_a ).val.m128f, f_b.vx().val.m128f, f_b.vy().val.m128f );
    return res;
}

// -------------------------------------------------------------------------------------------------------------

} // namespace smd
