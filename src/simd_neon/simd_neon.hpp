/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cfloat>
#include <type_traits>
#include <arm_neon.h>


namespace smd
{

//  According to:
// https://gcc.gnu.org/onlinedocs/gcc-3.3/gcc/Type-Attributes.html
// https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
// https://godbolt.org/z/eqYbaq


typedef int32_t    v4si __attribute__ ((vector_size (16), aligned(16)));
typedef float32_t  v4sf __attribute__ ((vector_size (16), aligned(16)));

typedef int64_t    v2di __attribute__ ((vector_size (16), aligned(16)));


typedef int32x4_t   m128i_t;
typedef float32x4_t m128f_t;


#define neon_alignment_check( PTR ) simd_alignment_check( PTR, 0x0f )

//static_assert( std::is_same< m128i_t, v4si >::value );
//static_assert( std::is_same< m128f_t, v4sf >::value );

//----------------------------------------------------------------------------------


union alignas(sizeof(v4si)) v128
{
    
    m128i_t m128i;
    m128f_t m128f;

    v4si    si;
    v4sf    sf;
    
    v2di    di;
    

    inline v128() = default;
    inline v128( const v128& ) = default;
    inline v128( v128&& ) = default;
    inline v128& operator=( const v128& ) = default;
    inline v128& operator=( v128&& ) = default;


    inline v128( const v4si &init )
        : si( init )
    {
        neon_alignment_check( this );
    }

    inline v128( const v4sf &init )
        : sf( init )
    {
        neon_alignment_check( this );
    }

    inline v128( const v2di &init )
        : di( init )
    {
        neon_alignment_check( this );
    }

    inline v128( int32_t si1, int32_t si2, int32_t si3, int32_t si4 )
        : si{ si1, si2, si3, si4 }
    {
        neon_alignment_check( this );
    }

    inline v128( int32_t f_initv_i32 )
        : si{ f_initv_i32, f_initv_i32, f_initv_i32, f_initv_i32 }
    {
        neon_alignment_check( this );
    }

    inline v128( float32_t sf1, float32_t sf2, float32_t sf3, float32_t sf4 )
        : sf{ sf1, sf2, sf3, sf4 }
    {
        neon_alignment_check( this );
    }

    inline v128( float32_t f_initv_f32 )
        : sf{ f_initv_f32, f_initv_f32, f_initv_f32, f_initv_f32 }
    {
        neon_alignment_check( this );
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"

    // for some g++ m128i_t and v4si are the same types
    template < typename U = m128i_t,
               typename = typename std::enable_if< false == std::is_same<U,v4si>::value >::type>
    inline v128( const m128i_t &init )
        : m128i( init )
    {
        neon_alignment_check( this );
    }

    //probably not required since __m128 and v4sf seem to be the same types
    template < typename U = m128f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline v128( const m128f_t &init )
        : m128f( init )
    {
        neon_alignment_check( this );
    }
#pragma GCC diagnostic pop

};

static_assert( sizeof(v128) == 16 );
static_assert( true == std::is_trivially_default_constructible<v128>::value );

//----------------------------------------------------------------------------------


struct neon_b32_vt;
struct neon_i32_vt;
struct neon_f32_vt;


//----------------------------------------------------------------------------------


/*
 * SSE bool type
 */ 

struct alignas(sizeof(v128)) neon_b32_vt 
    : public vt_bool_base< v128, neon_b32_vt >
{
private:

    typedef vt_bool_base< v128, neon_b32_vt > base_t;

public:

    using typename base_t::elem_t;

    static constexpr int32_t bool_to_i32( bool f_b ) { return ((f_b) ? int32_t(-1) : int32_t(0)); }

    inline neon_b32_vt() = default;
    inline neon_b32_vt( const neon_b32_vt& ) = default;
    inline neon_b32_vt( neon_b32_vt&& ) = default;
    inline neon_b32_vt& operator=( const neon_b32_vt& ) = default;
    inline neon_b32_vt& operator=( neon_b32_vt&& ) = default;


    explicit inline neon_b32_vt( const v128 &f_initv )
        : vt_bool_base( f_initv )
    {}

    explicit inline neon_b32_vt( bool f_initv_b )
        : vt_bool_base( v128( bool_to_i32( f_initv_b ) ) )
    {}

    /// factory function from memory pointer
    static inline neon_b32_vt from_ptr( const bool32_t* f_elemPtr );

    // Type reinterpretation
    inline       neon_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      neon_i32_vt&>(*this); }
    inline const neon_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const neon_i32_vt&>(*this); }

    inline       neon_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      neon_f32_vt&>(*this); }
    inline const neon_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const neon_f32_vt&>(*this); }

    /// store content of vector in memory
    inline void store( bool32_t* f_stream_p ) const;
};


template <class... oTs>
inline typename vt_base<oTs...>::derived_t 
blend( const neon_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right );


inline bool all( const neon_b32_vt &x );
inline bool any( const neon_b32_vt &x );



//----------------------------------------------------------------------------------


/*
 * SSE int32 type
 */ 


struct alignas(sizeof(v128)) neon_i32_vt
    : public vt_i32< v128,
                     neon_b32_vt,
                     neon_i32_vt >
{

private:

    typedef vt_i32< v128,neon_b32_vt,neon_i32_vt > base_t;

public:

    using typename base_t::elem_t;
    
    inline neon_i32_vt() = default;
    inline neon_i32_vt( const neon_i32_vt& ) = default;
    inline neon_i32_vt( neon_i32_vt&& ) = default;
    inline neon_i32_vt& operator=( const neon_i32_vt& ) = default;
    inline neon_i32_vt& operator=( neon_i32_vt&& ) = default;


    explicit inline neon_i32_vt( const v128 &f_initv )
        : vt_i32( f_initv )
    {}

    explicit inline neon_i32_vt( int32_t f_initv_i32 )
        : vt_i32( v128( f_initv_i32 ) )
    {}

    /// factory function from memory pointer
    static inline neon_i32_vt from_ptr( const int32_t* f_elemPtr );

    inline neon_f32_vt cast_to_vf32() const;

    inline explicit operator const v4si() const { return this->val.si; }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    template < typename U = m128i_t,
               typename = typename std::enable_if< false == std::is_same<U,v4si>::value >::type>
    inline explicit operator const m128i_t() const { return this->val.m128i; }
#pragma GCC diagnostic pop
    
    static inline neon_i32_vt zero();

    // type reinterpretation
    inline       neon_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      neon_f32_vt&>(*this); }
    inline const neon_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const neon_f32_vt&>(*this); }

#if 0

    inline neon_i32_vt gather( const int32_t* f_from_a ) const;
    inline neon_f32_vt gather( const float32_t* f_from_a ) const;

#endif

    /// store content of vector in memory
    inline void store( int32_t* f_stream_p ) const;
};


inline int32_t sum( const neon_i32_vt &x );
inline int32_t prod( const neon_i32_vt &x );

//----------------------------------------------------------------------------------


/*
 * SSE float32 type
 */ 

struct alignas(sizeof(v128)) neon_f32_vt
    : public vt_f32< v128, 
                     neon_b32_vt,
                     neon_f32_vt >
{

public:

    typedef vt_f32< v128,neon_b32_vt,neon_f32_vt > base_t;

    using typename base_t::elem_t;
    
    
    inline neon_f32_vt() = default;
    inline neon_f32_vt( const neon_f32_vt& ) = default;
    inline neon_f32_vt( neon_f32_vt&& ) = default;
    inline neon_f32_vt& operator=( const neon_f32_vt& ) = default;
    inline neon_f32_vt& operator=( neon_f32_vt&& ) = default;


    explicit inline neon_f32_vt( const v128 &f_initv )
        : vt_f32( f_initv )
    {}

    explicit inline neon_f32_vt( float32_t f_initv_f32 )
        : vt_f32( v128( f_initv_f32 ) )
    {}


    inline neon_f32_vt( const neon_i32_vt& neonInt )
        : vt_f32( neonInt.cast_to_vf32().val )
    {}

    /// factory function from memory pointer
    static inline neon_f32_vt from_ptr( const float32_t* f_elemPtr );

    inline operator const v4sf() const { return val.sf; }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    template < typename U = m128f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline operator const m128f_t() const { return val.m128f; }
#pragma GCC diagnostic pop

    static inline neon_f32_vt zero();

    // type reinterpretation
    inline       neon_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      neon_i32_vt&>(*this); }
    inline const neon_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const neon_i32_vt&>(*this); }


    // FMA instruction based operators
    #ifdef __SIMD_USE_FMA__

        // Don't forget that we have already some operator we like to continue using
        using base_t::operator+=;
        using base_t::operator-=;

        template <typename MT>
        inline const neon_f32_vt& operator+=( const simd_multiply_base<neon_f32_vt,MT> &other );

        template <typename MT>
        inline const neon_f32_vt& operator-=( const simd_multiply_base<neon_f32_vt,MT> &other );

    #endif // __SIMD_USE_FMA__

    /// store content of vector in memory
    inline void store( float32_t* f_stream_p ) const;
};


inline float32_t sum( const neon_f32_vt &x );
inline float32_t prod( const neon_f32_vt &x );

}  // namespace smd


//----------------------------------------------------------------------------------

