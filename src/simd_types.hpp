/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_types/bool32.hpp"

namespace smd
{
    typedef bool32  bool32_t;
} // namespace smd

#include "simd_types/simd_base.hpp"
#include "simd_types/simd_bool.hpp"
#include "simd_types/simd_numeric.hpp"
#include "simd_types/simd_integer.hpp"
#include "simd_types/simd_float.hpp"

#include "simd_types/simd_base.inl"
#include "simd_types/simd_bool.inl"
#include "simd_types/simd_numeric.inl"
#include "simd_types/simd_integer.inl"
#include "src/simd_types/simd_float.inl"

#include "simd_types/simd_type_traits2.hpp"


#include "simd_types/simd_arithmetic.hpp"

#if defined(__SIMD_USE_SSE2__) || defined(__SIMD_USE_AVX__) || defined(__SIMD_USE_NEON__) 
    // log implementation template for AVX2 and SSE2
    #include "foreign_code/simd_log.hpp"
#endif



#if defined(__SIMD_USE_AVX__) && defined(__USE_SIMD_256__)

    #define SIMD_MESSAGE "Using AVX instruction set"

    #ifdef __SIMD_USE_FMA__
        #include "simd_operations/simd_multiplication.hpp"
    #endif

    #include "simd_avx/simd_avx.hpp"

    #ifdef __SIMD_USE_FMA__
        #include "simd_avx/simd_avx_multiplication.hpp"
        #include "simd_avx/simd_avx_fma.hpp"
        #include "simd_avx/simd_avx_fma.inl"
    #endif

    #include "simd_avx/simd_avx.inl"

    #include "simd_avx/simd_avx_arithmetic.hpp"
    #include "simd_avx/simd_avx_arithmetic.inl"

    #include "foreign_code/simd_log.inl"

    namespace smd
    {
        typedef  avx_b32_vt  vb32_t;
        typedef  avx_i32_vt  vi32_t;
        typedef  avx_f32_vt  vf32_t;
    }

#elif defined(__SIMD_USE_SSE2__) && defined(__USE_SIMD_128__)
    
    #define SIMD_MESSAGE "Using SSE instruction set"

    #ifdef __SIMD_USE_FMA__
        #include "simd_operations/simd_multiplication.hpp"
    #endif

    #include "simd_sse/simd_sse.hpp"

    #ifdef __SIMD_USE_FMA__
        #include "simd_sse/simd_sse_multiplication.hpp"
        #include "simd_sse/simd_sse_fma.hpp"
        #include "simd_sse/simd_sse_fma.inl"
    #endif

    #include "simd_sse/simd_sse.inl"

    #include "simd_sse/simd_sse_arithmetic.hpp"
    #include "simd_sse/simd_sse_arithmetic.inl"

    #include "foreign_code/simd_log.inl"

    namespace smd
    {
        typedef  sse_b32_vt  vb32_t;
        typedef  sse_i32_vt  vi32_t;
        typedef  sse_f32_vt  vf32_t;
    }
#elif defined(__USE_SIMD_128__) && defined(__SIMD_USE_NEON__)

    #define SIMD_MESSAGE "Using NEON instruction set"

    #ifdef __SIMD_USE_FMA__
        #include "simd_operations/simd_multiplication.hpp"
    #endif

    #include "simd_neon/simd_neon.hpp"

    #ifdef __SIMD_USE_FMA__
        #include "simd_neon/simd_neon_multiplication.hpp"
        #include "simd_neon/simd_neon_fma.hpp"
        #include "simd_neon/simd_neon_fma.inl"
    #endif

    #include "simd_neon/simd_neon.inl"

    #include "simd_neon/simd_neon_arithmetic.hpp"
    #include "simd_neon/simd_neon_arithmetic.inl"

    #include "foreign_code/simd_log.inl"

    namespace smd
    {
        typedef  neon_b32_vt  vb32_t;
        typedef  neon_i32_vt  vi32_t;
        typedef  neon_f32_vt  vf32_t;
    }
#else
    #define SIMD_MESSAGE "Not using any special instruction set"

    #include "simd_types/simd_nohw.hpp"
    #include "simd_types/simd_nohw.inl"

    namespace smd
    {
        typedef  nhw::nhw_b32_vt  vb32_t;
        typedef  nhw::nhw_i32_vt  vi32_t;
        typedef  nhw::nhw_f32_vt  vf32_t;
    }

#endif // __SIMD_USE_AVX__


namespace smd
{


    static_assert( vb32_t::num_items() == ( sizeof( vb32_t ) / sizeof( bool32_t ) ) );
    static_assert( vi32_t::num_items() == ( sizeof( vi32_t ) / sizeof( int32_t ) ) );
    static_assert( vf32_t::num_items() == ( sizeof( vf32_t ) / sizeof( float32_t ) ) );

    static_assert( vb32_t::num_items() == vi32_t::num_items() );
    static_assert( vi32_t::num_items() == vf32_t::num_items() );

    static_assert( 0 == ( sizeof( vb32_t ) % sizeof( bool32_t ) ) );
    static_assert( 0 == ( sizeof( vi32_t ) % sizeof( int32_t ) ) );
    static_assert( 0 == ( sizeof( vf32_t ) % sizeof( float32_t ) ) );


    static_assert( true == std::is_standard_layout< vb32_t >::value );
    static_assert( true == std::is_standard_layout< vi32_t >::value );
    static_assert( true == std::is_standard_layout< vf32_t >::value );


    static_assert( true == std::is_trivially_constructible< vb32_t >::value );
    static_assert( true == std::is_trivially_constructible< vi32_t >::value );
    static_assert( true == std::is_trivially_constructible< vf32_t >::value );

    static_assert( true == std::is_trivially_default_constructible< vb32_t >::value );
    static_assert( true == std::is_trivially_default_constructible< vi32_t >::value );
    static_assert( true == std::is_trivially_default_constructible< vf32_t >::value );

    static_assert( true == std::is_trivially_copy_constructible< vb32_t >::value );
    static_assert( true == std::is_trivially_copy_constructible< vi32_t >::value );
    static_assert( true == std::is_trivially_copy_constructible< vf32_t >::value );

    static_assert( true == std::is_trivially_move_constructible< vb32_t >::value );
    static_assert( true == std::is_trivially_move_constructible< vi32_t >::value );
    static_assert( true == std::is_trivially_move_constructible< vf32_t >::value );


    static_assert( true == std::is_trivially_assignable< vb32_t, const vb32_t >::value );
    static_assert( true == std::is_trivially_assignable< vi32_t, const vi32_t >::value );
    static_assert( true == std::is_trivially_assignable< vf32_t, const vf32_t >::value );

    static_assert( true == std::is_trivially_copyable< vb32_t >::value );
    static_assert( true == std::is_trivially_copyable< vi32_t >::value );
    static_assert( true == std::is_trivially_copyable< vf32_t >::value );

    static_assert( true == std::is_trivially_copy_assignable< vb32_t >::value );
    static_assert( true == std::is_trivially_copy_assignable< vi32_t >::value );
    static_assert( true == std::is_trivially_copy_assignable< vf32_t >::value );

    static_assert( true == std::is_trivially_move_assignable< vb32_t >::value );
    static_assert( true == std::is_trivially_move_assignable< vi32_t >::value );
    static_assert( true == std::is_trivially_move_assignable< vf32_t >::value );


    static_assert( true == std::is_nothrow_assignable< vb32_t, const vb32_t >::value );
    static_assert( true == std::is_nothrow_assignable< vi32_t, const vi32_t >::value );
    static_assert( true == std::is_nothrow_assignable< vf32_t, const vf32_t >::value );

    static_assert( true == std::is_nothrow_copy_assignable< vb32_t >::value );
    static_assert( true == std::is_nothrow_copy_assignable< vi32_t >::value );
    static_assert( true == std::is_nothrow_copy_assignable< vf32_t >::value );

    static_assert( true == std::is_nothrow_move_assignable< vb32_t >::value );
    static_assert( true == std::is_nothrow_move_assignable< vi32_t >::value );
    static_assert( true == std::is_nothrow_move_assignable< vf32_t >::value );

    static_assert( true == std::is_nothrow_copy_constructible< vb32_t >::value );
    static_assert( true == std::is_nothrow_copy_constructible< vi32_t >::value );
    static_assert( true == std::is_nothrow_copy_constructible< vf32_t >::value );

    static_assert( true == std::is_nothrow_move_constructible< vb32_t >::value );
    static_assert( true == std::is_nothrow_move_constructible< vi32_t >::value );
    static_assert( true == std::is_nothrow_move_constructible< vf32_t >::value );
}


#include "simd_types/simd_numeric_limits.hpp"
#include "simd_types/simd_numeric_limits.inl"
#include "simd_types/simd_type_traits3.hpp"

#include "simd_types/simd_functional.hpp"
#include "simd_types/simd_functional.inl"

#include "simd_types/numeric_kernels.hpp"
#include "simd_types/numeric_kernels.inl"

#include "simd_types/simd_arithmetic.inl"

#include "simd_types/simd_compatibility.hpp"
