/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_matrix_view.hpp"


namespace smd
{


template <typename ST>
struct vt_matrix : public vt_matrix_view<ST>
{
private:

    typedef vt_matrix_view<ST>        base_t;
    typedef vt_matrix<ST>             this_t;

    cnt_memory<ST>   m_memory;

public:

    using typename base_t::simd_elem_t;

    inline vt_matrix() SIMD_NOEXCEPT : base_t(), m_memory()  {}

    inline vt_matrix( size_t f_nElemRows, size_t f_nElemCols ) SIMD_NOEXCEPT;

private:

    inline void initialize( size_t f_nRows, size_t f_nCols, size_t f_overlapSize=0 ) SIMD_NOEXCEPT;
};



} // namespace smd
