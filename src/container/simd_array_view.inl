/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_array_view.hpp"

namespace smd
{


template <typename ST>
inline void
vt_array_view<ST>::assert_consistency() const SIMD_NOEXCEPT
{
    base_t::assert_consistency();
    simd_assert( simd_elem_t::num_items() > m_overlapSize, "Inconsistency" );

    // -----------------------------------------------------------------
    
    static_assert( std::is_nothrow_copy_constructible<this_t>::value );
    static_assert( std::is_nothrow_copy_assignable<this_t>::value );

    static_assert( std::is_nothrow_move_constructible<this_t>::value );
    static_assert( std::is_nothrow_move_assignable<this_t>::value );

    // -----------------------------------------------------------------
    
    typedef vt_array_view< const_simd_elem_t >   const_this_t;
    static_assert( true == std::is_convertible< this_t, const_this_t >::value );

    typedef vt_array_view< noconst_simd_elem_t >  noconst_this_t;
    static_assert( true == std::is_convertible< noconst_this_t, const_this_t >::value );
    static_assert( false == std::is_convertible< const_this_t, noconst_this_t >::value );

    // -----------------------------------------------------------------

    static_assert( std::is_convertible< this_t,       val_array_view<simd_elem_t>       >::value );
    static_assert( std::is_convertible< this_t,       val_array_view<const_simd_elem_t> >::value );
    static_assert( std::is_convertible< const_this_t, val_array_view<const_simd_elem_t> >::value );

    static_assert( false == std::is_convertible< val_array_view<simd_elem_t>,       this_t >::value );
    static_assert( false == std::is_convertible< val_array_view<const_simd_elem_t>, this_t >::value );
    static_assert( false == std::is_convertible< val_array_view<simd_elem_t>,       const_this_t >::value );
    static_assert( false == std::is_convertible< val_array_view<const_simd_elem_t>, const_this_t >::value );

    // -----------------------------------------------------------------
}



// ------------------------------------------------------------------------------------------------------------------



template <typename ST>
inline
vt_array_view<ST>::vt_array_view() SIMD_NOEXCEPT
    : base_t()
    , m_overlapSize(0)
{
    assert_consistency();
}


template <typename ST>
inline
vt_array_view<ST>::vt_array_view( simd_elem_t* f_array_p, simd_elem_t* f_array_end_p, size_t f_overlapSize ) SIMD_NOEXCEPT
    : base_t( f_array_p, f_array_end_p )
    , m_overlapSize( f_overlapSize )
{
    assert_consistency();
}

// ------------------------------------------------------------------------------------------------------------------



template <typename ST>
inline auto
vt_array_view<ST>::elem() SIMD_NOEXCEPT -> elem_view_t
{
    return elem_view_t( static_cast<elem_t*>(this->begin()[0]),
                        static_cast<elem_t*>( this->end()[0] ) - m_overlapSize );
}



template <typename ST>
inline auto
vt_array_view<ST>::elem() const SIMD_NOEXCEPT -> const_elem_view_t
{
    return const_elem_view_t( static_cast<const_elem_t*>(this->cbegin()[0]),
                              static_cast<const_elem_t*>( this->cend()[0] ) - m_overlapSize );
}


template <typename ST>
inline auto
vt_array_view<ST>::elem( size_t f_idx ) SIMD_NOEXCEPT -> elem_t&
{
    simd_bounds_check( f_idx < ( (this->size() * simd_elem_t::num_items()) - m_overlapSize ) );
    return static_cast<elem_t*>( this->begin()[0] )[f_idx];
}


template <typename ST>
inline auto
vt_array_view<ST>::elem( size_t f_idx ) const SIMD_NOEXCEPT -> elem_t
{
    simd_bounds_check( f_idx < ( (this->size() * const_simd_elem_t::num_items()) - m_overlapSize ) );
    return static_cast<const_elem_t*>( this->cbegin()[0] )[f_idx];
}

// ------------------------------------------------------------------------------------------------------------------



template <typename ST>
inline void
vt_array_view<ST>::initialize_overlap( size_t f_overlapSize ) SIMD_NOEXCEPT
{
    m_overlapSize = f_overlapSize;
}


// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_array_view<ST>::create_overhang_view( CNT_T& f_array ) SIMD_NOEXCEPT
{
    typedef typename VIEW_T::elem_t             elem_t;
    
    typedef VIEW_T                              view_t;
    
    if (0 == f_array.overlapSize())
        return view_t();

    elem_t* l_begin = static_cast<elem_t*>( (f_array.end() - 1)[0] );
    elem_t*   l_end = static_cast<elem_t*>( f_array.end()[0] ) - f_array.overlapSize();
    
    simd_assert( l_end == static_cast<elem_t*>( f_array.elem().end() ), "Inconsistent pointer" );
    simd_assert( l_begin < l_end,                                       "Inconsistent pointer" );
    
    view_t l_view( l_begin, l_end );

    simd_assert( l_view.size() == ( CNT_T::simd_elem_t::num_items() - f_array.overlapSize() ),
                 "Inconsistent number of overlap items" );
    simd_assert( l_view.end() == f_array.elem().end(), "Inconsistent pointers" );

    return l_view;
}

// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_array_view<ST>::create_overlap_view( CNT_T& f_array ) SIMD_NOEXCEPT
{
    typedef typename VIEW_T::elem_t             elem_t;
    typedef VIEW_T                              view_t;
    
    if (0 == f_array.overlapSize())
        return view_t();

    elem_t* l_begin = static_cast<elem_t*>( f_array.end()[0] ) - f_array.overlapSize();
    elem_t*   l_end = static_cast<elem_t*>( f_array.end()[0] );

    simd_assert( l_begin == static_cast<elem_t*>( f_array.elem().end() ), "Inconsistent pointer" );
    simd_assert( l_begin < l_end,                                         "Inconsistent pointer" );
    
    view_t l_view( l_begin, l_end );

    simd_assert( l_view.size() == f_array.overlapSize(), "Inconsistent number of overlap items" );
    simd_assert( l_view.begin() == f_array.elem().end(), "Inconsistent pointers" );
    simd_assert( l_view.end() > f_array.elem().end(), "Inconsistent pointers" );

    return l_view;
}


// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_array_view<ST>::create_inside_view( CNT_T& f_array ) SIMD_NOEXCEPT
{
    typedef VIEW_T   view_t;
    
    if (0 == f_array.overlapSize())
        return f_array;
    
    view_t l_view( f_array.begin(), f_array.end() - 1, 0 );
    
    simd_assert( l_view.size() == (f_array.size() - 1), "Wrong number of items" );
    simd_assert( l_view.begin() == f_array.begin(), "Wrong number of items" );

    simd_assert( l_view.elem().begin() == f_array.elem().begin(), "Inconsistent pointers" );
    simd_assert( l_view.elem().end() < f_array.elem().end(), "Inconsistent pointers" );
    simd_assert( (f_array.elem().end() - l_view.elem().end())
                  == static_cast<std::ptrdiff_t>( CNT_T::simd_elem_t::num_items() - f_array.overlapSize() ), 
                  "Inconsistent pointers" );

    return l_view;
}



}  // namespace smd

