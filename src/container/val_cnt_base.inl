/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_cnt_base.hpp"


namespace smd
{


template <typename T>
val_cnt_base<T>::~val_cnt_base() SIMD_NOEXCEPT
{
    m_array_p     = NULL;
    m_array_end_p = NULL;
}



/// for debugging
template <typename T>
inline void val_cnt_base<T>::assert_consistency() const SIMD_NOEXCEPT
{
    simd_assert( cend() >= cbegin(), "Array dimension consistency check failed" );
    simd_assert( 0 == ( ( reinterpret_cast<std::ptrdiff_t>( cend() ) 
                           -  reinterpret_cast<std::ptrdiff_t>( cbegin() ) ) % sizeof(elem_t) ),
                 "Array dimension consistency check failed" );

    static_assert( std::is_nothrow_copy_constructible<this_t>::value );
    static_assert( std::is_nothrow_copy_assignable<this_t>::value );

    static_assert( std::is_nothrow_move_constructible<this_t>::value );
    static_assert( std::is_nothrow_move_assignable<this_t>::value );

    static_assert( std::is_nothrow_copy_constructible<const_this_t>::value );
    static_assert( std::is_nothrow_copy_assignable<const_this_t>::value );

    static_assert( std::is_nothrow_move_constructible<const_this_t>::value );
    static_assert( std::is_nothrow_move_assignable<const_this_t>::value );
}




template <typename T>
inline 
void val_cnt_base<T>::initialize_array( elem_t* f_array_p, elem_t* f_array_end_p ) SIMD_NOEXCEPT
{
    m_array_p     = f_array_p;
    m_array_end_p = f_array_end_p;
}



} // namespace smd

