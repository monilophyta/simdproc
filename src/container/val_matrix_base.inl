/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "val_matrix_base.hpp"
#include <cstring>



namespace smd
{

//----------------------------------------------------------------------------------

template <typename T>
inline void val_matrix_base<T>::assert_consistency() const SIMD_NOEXCEPT
{
    simd_expensive_assert( (n_rows() == 0) || ( this->cend(n_rows() - 1) == this->cend() ),
                           "Inconsistent end pointer" );
    
    simd_expensive_assert(              n_cols() <= m_stride,       "Matrix dimension consistency check failed" );
    simd_expensive_assert( (n_rows() * n_cols()) <= base_t::size(), "Matrix dimension consistency check failed" );
    simd_expensive_assert( ( n_cols() + ( (n_rows() - 1) * m_stride ) ) == base_t::size(), 
                           "Matrix dimension consistency check failed" );

    simd_expensive_assert( this->cend() == ( &( (*this)( n_rows()-1, n_cols()-1 ) ) + 1 ),
                           "Inconsistent end pointer" );

    static_assert( std::is_copy_constructible<this_t>::value );
    static_assert( std::is_copy_assignable<this_t>::value );

    static_assert( std::is_move_constructible<this_t>::value );
    static_assert( std::is_move_assignable<this_t>::value );

    static_assert( std::is_copy_constructible<const_this_t>::value );
    static_assert( std::is_copy_assignable<const_this_t>::value );

    static_assert( std::is_move_constructible<const_this_t>::value );
    static_assert( std::is_move_assignable<const_this_t>::value );
}



template <typename T>
inline 
auto val_matrix_base<T>::row_ptr( size_t f_rowidx ) const SIMD_NOEXCEPT -> elem_t*
{
    simd_bounds_check( f_rowidx <= n_rows() );

    return ( this->m_array_p + (f_rowidx * m_stride));
}


template <typename T>
inline
size_t val_matrix_base<T>::size() const SIMD_NOEXCEPT
{
    return (n_rows() * n_cols());
}


template <typename T>
inline
size_t val_matrix_base<T>::n_rows() const SIMD_NOEXCEPT
{
    simd_assert( (base_t::size() > 0) == (m_stride > 0), "Matrix dimension consistency check failed" );

    return ( (base_t::size() == 0) ? size_t(0)
                                   : ( (base_t::size() + m_stride - 1) / m_stride) );
}


template <typename T>
inline 
size_t val_matrix_base<T>::n_cols() const SIMD_NOEXCEPT
{
    return m_nCols;
}


template <typename T>
inline 
size_t val_matrix_base<T>::stride() const SIMD_NOEXCEPT
{
    return m_stride;
}



template <typename T>
inline 
auto val_matrix_base<T>::operator[] (size_t f_rowidx) const SIMD_NOEXCEPT -> elem_t*
{
    simd_bounds_check( f_rowidx < n_rows() );
    
    return row_ptr(f_rowidx);
}


template <typename T>
inline 
auto val_matrix_base<T>::operator() (size_t f_rowidx, size_t f_colidx) const SIMD_NOEXCEPT -> elem_t&
{
    simd_bounds_check( f_rowidx < n_rows() );
    simd_bounds_check( f_colidx < n_cols() );

    return row_ptr(f_rowidx)[ f_colidx ];
}





template <typename T>
inline
auto  val_matrix_base<T>::row( size_t f_rowidx ) const SIMD_NOEXCEPT -> row_view_t
{
    elem_t* l_row_ptr = row_ptr(f_rowidx);
    
    return row_view_t( l_row_ptr, m_nCols );
}




template <typename T>
inline auto val_matrix_base<T>::row_bundle( size_t f_colIdx ) const SIMD_NOEXCEPT -> iter_bundle_t
{
    std::function<elem_t*(size_t)> row_gen_kernel = [this] ( size_t rIdx ) -> elem_t*
    {
        return this->begin( rIdx );
    };

    return smd::make_iterator_bundle( n_rows(), row_gen_kernel, f_colIdx );
}


template <typename T>
inline auto val_matrix_base<T>::const_row_bundle( size_t f_colIdx ) const SIMD_NOEXCEPT -> const_iter_bundle_t
{
    std::function<const_elem_t*(size_t)> row_gen_kernel = [this] ( size_t rIdx ) -> const_elem_t*
    {
        return this->cbegin( rIdx );
    };

    return smd::make_iterator_bundle( n_rows(), row_gen_kernel, f_colIdx );
}



template <typename T>
inline void val_matrix_base<T>::fill( const noconst_elem_t& f_val ) SIMD_NOEXCEPT
{
    if ( true == this->isContiguous() )
    {
        std::fill( this->begin(), this->end(), f_val );
    }
    else
    {
        const size_t l_nRows = n_rows();

        for (size_t i = 0; i < l_nRows; ++i)
        {
            std::fill( this->begin(i), this->end(i), f_val );
        }
    }
}


template <typename T>
inline void val_matrix_base<T>::set_zero() SIMD_NOEXCEPT
{
    if ( true == this->isContiguous() )
    {
        simd_assert( this->mem_size() == this->size(), "Non contiguous memory shape" );

        std::memset( reinterpret_cast<void*>(this->begin()), 
                     int(0), this->mem_size() * sizeof( elem_t ) );
    }
    else
    {
        const size_t l_nRows = n_rows();
        const size_t l_nCols = n_cols();

        for (size_t i = 0; i < l_nRows; ++i)
        {
            std::memset( reinterpret_cast<void*>(this->begin(i)),
                         int(0), l_nCols * sizeof( elem_t ) );
        }
    }
}



template <typename T>
inline 
void val_matrix_base<T>::initialize_matrix_dims( size_t f_stride, size_t f_nCols ) SIMD_NOEXCEPT
{
    m_stride = f_stride;
    m_nCols  = f_nCols;

    assert_consistency();
}

} // namespace smd
