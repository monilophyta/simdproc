/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_matrix_view.hpp"



namespace smd
{


template <typename ST>
struct vt_matrix_view;


template <typename ST>
vt_matrix_view< ST > wrap_vt_matrix( ST*, size_t, size_t, size_t );



template <typename ST>
struct vt_matrix_view : public val_matrix_view< ST >
{
    static_assert( is_simd<ST>::value );

    friend vt_matrix_view<ST> wrap_vt_matrix<ST>( ST*, size_t, size_t, size_t );

public:

    typedef typename vt_matrix_traits<ST>::simd_elem_t                   simd_elem_t;
    typedef typename vt_matrix_traits<ST>::const_simd_elem_t       const_simd_elem_t;
    typedef typename vt_matrix_traits<ST>::noconst_simd_elem_t   noconst_simd_elem_t;

    typedef typename vt_matrix_traits<ST>::elem_t                             elem_t;
    typedef typename vt_matrix_traits<ST>::const_elem_t                 const_elem_t;

    typedef typename vt_matrix_traits<ST>::elem_view_t                   elem_view_t;
    typedef typename vt_matrix_traits<ST>::const_elem_view_t       const_elem_view_t;

    typedef vt_matrix_view<ST>                                      simd_elem_view_t;

    typedef typename vt_matrix_traits<ST>::row_view_t                     row_view_t;

private:

    typedef val_matrix_view< simd_elem_t >                                    base_t;
    typedef vt_matrix_view< simd_elem_t >                                     this_t;
    typedef vt_matrix_view< const_simd_elem_t >                         const_this_t;

    size_t m_overlapSize;

public:

    // ----------------------------------------------------------------------


    inline vt_matrix_view() SIMD_NOEXCEPT;

    // ----------------------------------------------------------------------
    // Const conversion

    template< typename U = const_simd_elem_t,
              typename = typename std::enable_if< std::is_same<U,simd_elem_t>::value >::type >
    inline vt_matrix_view( const vt_matrix_view<noconst_simd_elem_t>& f_other ) SIMD_NOEXCEPT
        : vt_matrix_view( f_other.cbegin(), f_other.cend(), f_other.m_overlapSize )
    {}

    // ----------------------------------------------------------------------

    inline size_t overlapSize() const SIMD_NOEXCEPT { return m_overlapSize; }
    inline bool is_overlapping() const SIMD_NOEXCEPT { return ( 0 < overlapSize() ); }

    // ----------------------------------------------------------------------

    inline elem_t& elem( size_t f_rowidx, size_t f_colidx ) SIMD_NOEXCEPT;
    inline elem_t  elem( size_t f_rowidx, size_t f_colidx ) const SIMD_NOEXCEPT;

    inline elem_view_t elem() SIMD_NOEXCEPT;
    inline const_elem_view_t elem() const SIMD_NOEXCEPT;

    /// conversion operators to val_matrix_view
    inline explicit operator elem_view_t() SIMD_NOEXCEPT { return elem(); }
    inline explicit operator const_elem_view_t() const SIMD_NOEXCEPT { return elem(); }

    // ----------------------------------------------------------------------
 
    inline elem_view_t overhang() SIMD_NOEXCEPT { return create_overhang_view<elem_view_t,this_t>(*this); }
    inline const_elem_view_t overhang() const SIMD_NOEXCEPT { return create_overhang_view<const_elem_view_t,const this_t>(*this); }
    
    // ----------------------------------------------------------------------
    
    inline elem_view_t overlap() SIMD_NOEXCEPT { return create_overlap_view<elem_view_t,this_t>(*this); }
    inline const_elem_view_t overlap() const SIMD_NOEXCEPT { return create_overlap_view<const_elem_view_t,const this_t>(*this); }

    // ----------------------------------------------------------------------

    inline this_t inside() SIMD_NOEXCEPT { return create_inside_view<this_t,this_t>(*this); }
    inline const_this_t inside() const SIMD_NOEXCEPT { return create_inside_view<const_this_t,const this_t>(*this); }

    // ----------------------------------------------------------------------

    inline row_view_t row( size_t f_rowidx ) const SIMD_NOEXCEPT;

    // ----------------------------------------------------------------------

    /// for debugging
    //inline void assert_consistency() const SIMD_NOEXCEPT;

protected:

    inline vt_matrix_view( simd_elem_t* f_mem_p,
                           simd_elem_t* f_mem_end_p,
                                size_t  f_nCols,
                                size_t  f_rowStride,
                                size_t  f_overlapSize ) SIMD_NOEXCEPT;

    inline void initialize_overlap( size_t f_overlapSize ) SIMD_NOEXCEPT;

private:

    template <typename VIEW_T, typename CNT_T>
    static inline VIEW_T create_overhang_view( CNT_T& f_matrix ) SIMD_NOEXCEPT;

    template <typename VIEW_T, typename CNT_T>
    static inline VIEW_T create_overlap_view( CNT_T& f_matrix ) SIMD_NOEXCEPT;

    template <typename VIEW_T, typename CNT_T>
    static inline VIEW_T create_inside_view( CNT_T& f_matrix ) SIMD_NOEXCEPT;
};


} // namespace smd
