/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_array_functions.hpp"



namespace smd
{


template <typename T>
inline typename select_noconst_simd<T>::type
gather( const val_array_view<T>& f_from_a, const vi32_t& f_idx_vi32 ) SIMD_NOEXCEPT
{
    static_assert( false == is_simd<T>::value );

    typename select_noconst_simd<T>::type res;

    std::transform( f_idx_vi32.cbegin(), f_idx_vi32.cend(),
                    res.begin(),
                    std::cref( f_from_a ) );
    
    return res;
}


} // namespace smd
