/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>


namespace smd
{


template <typename T>
inline typename select_noconst_simd<T>::type
gather( const val_matrix_view<T>& f_from_a, const  vi32_t& f_rowidx_vi32, const  vi32_t& f_colidx_vi32 ) SIMD_NOEXCEPT;



template <typename T>
inline typename select_noconst_simd<T>::type
gather( const val_matrix_view<T>& f_from_a, const  vi32_t& f_rowidx_vi32, const int32_t f_colidx_i32  ) SIMD_NOEXCEPT;



template <typename T>
inline typename select_noconst_simd<T>::type
gather( const val_matrix_view<T>& f_from_a, const int32_t f_rowidx_i32,  const  vi32_t& f_colidx_vi32 ) SIMD_NOEXCEPT;


} // namespace smd
