/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cpp_tools.hpp>
#include <type_traits>


namespace smd
{


template <typename T>
struct val_common_traits
{
    typedef T                                                            elem_t;
    typedef typename std::add_const<elem_t>::type                  const_elem_t;

    typedef typename std::remove_const<const_elem_t>::type       noconst_elem_t;
};


// ---------------------------------------------------------------------------------------------------


template <typename T>
struct val_array_traits : public val_common_traits<T>
{};

// ---------------------------------------------------------------------------------------------------


template <typename T>
struct val_array_view;


template <typename T>
struct val_matrix_traits : public val_common_traits<T>
{
    using typename val_common_traits<T>::elem_t;
    using typename val_common_traits<T>::const_elem_t;
    using typename val_common_traits<T>::noconst_elem_t;

    typedef CIteratorBundleIterator<elem_t>                      iter_bundle_t;
    typedef CIteratorBundleIterator<const_elem_t>          const_iter_bundle_t;

    typedef val_array_view<elem_t>                                  row_view_t;
};

// ---------------------------------------------------------------------------------------------------



} // namespace smd
