/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_array_base.hpp"
#include <algorithm>
#include <cstring>


namespace smd
{


template <typename T>
inline void val_array_base<T>::fill( const noconst_elem_t& f_val )
{
    std::fill( this->begin(), this->end(), f_val );
}

template <typename T>
inline void val_array_base<T>::set_zero() SIMD_NOEXCEPT
{
    std::memset( reinterpret_cast<void*>(this->begin()),
                 int(0), this->mem_size() * sizeof( elem_t ) );
}


template <typename T>
inline auto val_array_base<T>::operator[] (size_t f_idx) -> elem_t&
{
    simd_bounds_check( f_idx < this->size() );
    return this->begin()[ f_idx ];
}



template <typename T>
inline auto val_array_base<T>::operator[] (size_t f_idx) const -> const_elem_t&
{
    simd_bounds_check( f_idx < this->size() );
    return this->cbegin()[ f_idx ];
}




} // namespace smd

