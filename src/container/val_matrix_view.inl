/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_matrix_view.hpp"
#include <algorithm>


namespace smd
{


template <typename T>
inline void
val_matrix_view<T>::assert_consistency() const SIMD_NOEXCEPT
{
    base_t::assert_consistency();
    
    static_assert( std::is_nothrow_copy_constructible<this_t>::value );
    static_assert( std::is_nothrow_copy_assignable<this_t>::value );

    static_assert( std::is_nothrow_move_constructible<this_t>::value );
    static_assert( std::is_nothrow_move_assignable<this_t>::value );

    //static_assert( false == std::is_move_constructible<this_t>::value );
    //static_assert( false == std::is_move_assignable<this_t>::value );

    typedef val_matrix_view< typename std::add_const<T>::type >   const_this_t;
    static_assert( true == std::is_convertible< this_t, const_this_t >::value );

    typedef val_matrix_view< typename std::remove_const<T>::type >  noconst_this_t;
    static_assert( true == std::is_convertible< noconst_this_t, const_this_t >::value );

    static_assert( false == std::is_convertible< const_this_t, noconst_this_t >::value );
}


// ------------------------------------------------------------------------------------------------------------------



template <typename T>
inline void val_matrix_view<T>::deep_copy_from( const const_this_t& f_other ) SIMD_NOEXCEPT
{
    simd_check( f_other.n_rows() == this->n_rows(), "copy between matrices of unequal size" );
    simd_check( f_other.n_cols() == this->n_cols(), "copy between matrices of unequal size" );
    

    if ( ( true == this->isContiguous() ) && ( true == f_other.isContiguous() ) )
    {
        simd_assert( this->mem_size() == this->size(), "Inconsistent pointers" );
        simd_assert( f_other.mem_size() == f_other.size(), "Inconsistent pointers" );

        const size_t l_numCopy = std::min( this->mem_size(), f_other.mem_size() );
        
        std::copy_n( f_other.cbegin(), l_numCopy, this->begin() );
    }
    else
    {
        const size_t l_nRows = std::min( this->n_rows(), f_other.n_rows() );
        const size_t l_nCols = std::min( this->n_cols(), f_other.n_cols() );

        for (size_t i = 0; i < l_nRows; ++i)
        {
            std::copy_n( f_other.cbegin(i), l_nCols, this->begin(i) );
        }
    }
}



// ------------------------------------------------------------------------------------------------------------------


template <typename T>
inline
val_matrix_view<T>::val_matrix_view( elem_t* f_mem_p, elem_t* f_mem_end_p, size_t f_nCols, size_t f_rowStride ) SIMD_NOEXCEPT
{
    this->initialize_array( f_mem_p, f_mem_end_p );
    this->initialize_matrix_dims( f_rowStride, f_nCols );
    assert_consistency();
}



// ------------------------------------------------------------------------------------------------------------------


template <typename T>
template <typename CNT_T>
inline CNT_T val_matrix_view<T>::create_outside_view( CNT_T& f_matrix ) SIMD_NOEXCEPT
{
    typedef typename CNT_T::elem_t             elem_t;
    
    if ( f_matrix.n_cols() == f_matrix.stride() )
        return CNT_T();
    
    elem_t* l_begin = f_matrix.begin() + f_matrix.n_cols();
    elem_t* l_end   = f_matrix.end()   - f_matrix.n_cols();

    CNT_T l_view( l_begin, l_end,
                  f_matrix.stride() - f_matrix.n_cols(),
                  f_matrix.stride() );

    simd_assert( (l_view.n_rows() + 1) == f_matrix.n_rows(), "Wrong number of rows" );

    return l_view;
}



} // namespace smd
