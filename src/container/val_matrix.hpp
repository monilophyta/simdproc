/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_matrix_view.hpp"


namespace smd
{



template <typename T>
struct val_matrix : public val_matrix_view<T>
{
private:

    typedef val_matrix_view<T>        base_t;
    typedef val_matrix<T>             this_t;

    cnt_memory<T>   m_memory;

public:

    inline val_matrix() SIMD_NOEXCEPT : base_t(), m_memory() {}

    inline val_matrix( size_t f_nRows, size_t f_nCols ) SIMD_NOEXCEPT;

private:

    inline void initialize( size_t f_nRows, size_t f_nCols ) SIMD_NOEXCEPT;
};



} // namespace smd
