/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_matrix.hpp"


namespace smd
{


template <typename T>
inline val_matrix<T>::val_matrix( size_t f_nRows, size_t f_nCols ) SIMD_NOEXCEPT
    : base_t()
    , m_memory()
{
    initialize( f_nRows, f_nCols );
}


template <typename T>
inline void
val_matrix<T>::initialize( size_t f_nRows, size_t f_nCols ) SIMD_NOEXCEPT
{
    const size_t f_Size = f_nRows * f_nCols;
    m_memory.initialize_memory( f_Size );
    
    this->initialize_array( m_memory.getPtr(), f_Size );
    this->initialize_matrix_dims( f_nCols, f_nCols );

    this->assert_consistency();
}


} // namespace smd
