/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_matrix_view.hpp"


namespace smd
{



template <typename ST>
inline
vt_matrix_view<ST>::vt_matrix_view() SIMD_NOEXCEPT
    : base_t()
    , m_overlapSize( 0 )
{}



template <typename ST>
inline
vt_matrix_view<ST>::vt_matrix_view( simd_elem_t* f_mem_p,
                                    simd_elem_t* f_mem_end_p,
                                         size_t  f_nCols,
                                         size_t  f_rowStride,
                                         size_t  f_overlapSize ) SIMD_NOEXCEPT
    : base_t( f_mem_p, f_mem_end_p, f_nCols, f_rowStride )
    , m_overlapSize( f_overlapSize )
{
    this->assert_consistency();
}


// ------------------------------------------------------------------------------------------------------------------



template <typename ST>
inline auto
vt_matrix_view<ST>::elem() SIMD_NOEXCEPT -> elem_view_t
{
    elem_t*  l_memBegin_p = static_cast<elem_t*>( this->begin()[0] );
    elem_t*  l_memEnd_p   = static_cast<elem_t*>( this->end()[0] )     - m_overlapSize;
    size_t   l_numCols    = (simd_elem_t::num_items() * this->n_cols()) - m_overlapSize;
    size_t   l_rowStride  = simd_elem_t::num_items() * this->stride();
    
    return elem_view_t( l_memBegin_p, l_memEnd_p, l_numCols, l_rowStride );
}



template <typename ST>
inline auto
vt_matrix_view<ST>::elem() const SIMD_NOEXCEPT -> const_elem_view_t
{
    const_elem_t*  l_memBegin_p = static_cast<const_elem_t*>( this->cbegin()[0] );
    const_elem_t*  l_memEnd_p   = static_cast<const_elem_t*>( this->cend()[0] )  - m_overlapSize;
    size_t         l_numCols    = (simd_elem_t::num_items() * this->n_cols())    - m_overlapSize;
    size_t         l_rowStride  = simd_elem_t::num_items() * this->stride();
    
    return const_elem_view_t( l_memBegin_p, l_memEnd_p, l_numCols, l_rowStride );
}


template <typename ST>
inline auto
vt_matrix_view<ST>::elem( size_t f_rowidx, size_t f_colidx ) SIMD_NOEXCEPT -> elem_t&
{
    elem_view_t elem_view = this->elem();
    return elem_view( f_rowidx, f_colidx );
}


template <typename ST>
inline auto
vt_matrix_view<ST>::elem( size_t f_rowidx, size_t f_colidx ) const SIMD_NOEXCEPT -> elem_t
{
    const_elem_view_t elem_view = this->elem();
    return elem_view( f_rowidx, f_colidx );
}

// ------------------------------------------------------------------------------------------------------------------


template <typename ST>
inline auto 
vt_matrix_view<ST>::row( size_t f_rowidx ) const SIMD_NOEXCEPT -> row_view_t
{
    simd_bounds_check( f_rowidx < this->n_rows() );
    return row_view_t( this->row_ptr( f_rowidx ), this->n_cols(), m_overlapSize );
}


// ------------------------------------------------------------------------------------------------------------------


template <typename ST>
inline void
vt_matrix_view<ST>::initialize_overlap( size_t f_overlapSize ) SIMD_NOEXCEPT
{
    m_overlapSize = f_overlapSize;
}


// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_matrix_view<ST>::create_overhang_view( CNT_T& f_matrix ) SIMD_NOEXCEPT
{
    typedef typename VIEW_T::elem_t              elem_t;
    typedef typename CNT_T::simd_elem_t     simd_elem_t;
    typedef VIEW_T                               view_t;
    
    if (0 == f_matrix.overlapSize())
        return view_t();

    elem_t* l_begin = static_cast<elem_t*>( (f_matrix.end(0) - 1)[0] );
    elem_t*   l_end = static_cast<elem_t*>( f_matrix.end()[0] ) - f_matrix.overlapSize();

    simd_assert( l_begin < l_end, "Inconsistent pointer" );
    
    view_t l_view( l_begin,                                              // elem_t* f_mem_p
                   l_end,                                                // elem_t* f_mem_end_p
                   simd_elem_t::num_items() - f_matrix.overlapSize(),    // size_t f_nCols
                   simd_elem_t::num_items() * f_matrix.stride() );       // size_t f_rowStride

    simd_assert( l_view.n_rows() == f_matrix.n_rows(), "Insonsitent number of rows" );
    simd_assert( l_view.n_cols() == (simd_elem_t::num_items() - f_matrix.overlapSize()), "Inconsistent number of cols" );

    simd_assert( l_view.end() == f_matrix.elem().end(), "Inconsistent pointers" );
    simd_assert( l_view.end(0) == f_matrix.elem().end(0), "Inconsistent pointers" );

    return l_view;
}


// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_matrix_view<ST>::create_overlap_view( CNT_T& f_matrix ) SIMD_NOEXCEPT
{
    typedef typename VIEW_T::elem_t              elem_t;
    typedef typename CNT_T::simd_elem_t     simd_elem_t;
    typedef VIEW_T                               view_t;
    
    if (0 == f_matrix.overlapSize())
        return view_t();

    elem_t* l_begin = static_cast<elem_t*>( f_matrix.end(0)[0] ) - f_matrix.overlapSize();
    elem_t*   l_end = static_cast<elem_t*>( f_matrix.end()[0] );

    simd_assert( l_begin < l_end, "Inconsistent pointer" );
    
    view_t l_view( l_begin,                                              // elem_t* f_mem_p
                   l_end,                                                // elem_t* f_mem_end_p
                   f_matrix.overlapSize(),                               // size_t f_nCols
                   simd_elem_t::num_items() * f_matrix.stride() );       // size_t f_rowStride

    simd_assert( l_view.n_rows() == f_matrix.n_rows(), "Insonsitent number of rows" );
    simd_assert( l_view.n_cols() == f_matrix.overlapSize(), "Inconsistent number of cols" );

    simd_assert( l_view.begin(0) == f_matrix.elem().end(0), "Inconsistent pointers" );
    simd_assert( l_view.end(0)   >  f_matrix.elem().end(0), "Inconsistent pointers" );
    simd_assert( l_view.end()    >  f_matrix.elem().end(),  "Inconsistent pointers" );

    return l_view;
}


// ------------------------------------------------------------------------------------------------------------------

template <typename ST>
template <typename VIEW_T, typename CNT_T>
inline VIEW_T vt_matrix_view<ST>::create_inside_view( CNT_T& f_matrix ) SIMD_NOEXCEPT
{
    typedef VIEW_T   view_t;
    
    if (0 == f_matrix.overlapSize())
        return f_matrix;
    
    view_t l_view( f_matrix.begin(),
                   f_matrix.end() - 1,
                   f_matrix.n_cols() - 1,
                   f_matrix.stride(),
                   0 );
    
    simd_assert( l_view.n_rows() == f_matrix.n_rows(), "Wrong number of rows" );
    simd_assert( l_view.n_cols() == (f_matrix.n_cols() - 1), "Wrong number of columns" ); 

    simd_assert( l_view.elem().begin() == f_matrix.elem().begin(), "Inconsistent pointers" );
    simd_assert( l_view.elem().end(0) < f_matrix.elem().end(0), "Inconsistent pointers" );
    simd_assert( l_view.elem().end() < f_matrix.elem().end(), "Inconsistent pointers" );
    simd_assert( (f_matrix.elem().end() - l_view.elem().end())
                  == static_cast<std::ptrdiff_t>( CNT_T::simd_elem_t::num_items() - f_matrix.overlapSize() ), 
                  "Inconsistent pointers" );

    simd_assert( (f_matrix.elem().end(0) - l_view.elem().end(0))
                  == static_cast<std::ptrdiff_t>( CNT_T::simd_elem_t::num_items() - f_matrix.overlapSize() ), 
                  "Inconsistent pointers" );

    return l_view;
}


// ------------------------------------------------------------------------------------------------------------------


} // namespace smd
