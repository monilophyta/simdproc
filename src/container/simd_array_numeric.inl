/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "simd_array_numeric.hpp"
#include <functional>
#include <numeric>
#include "simd_cnt_traits.hpp"



namespace smd 
{

namespace
{


template <typename T, typename OP_T>
inline auto generic_sum( const val_array_view<T> &f_array, OP_T op ) SIMD_NOEXCEPT 
-> typename val_array_traits<T>::elem_t
{
    typedef typename val_array_traits<T>::elem_t elem_t;
    
    simd_assert( 0 <= ( f_array.cend() - f_array.cbegin() ), "Inconsistent pointer" );
    simd_assert( static_cast<std::ptrdiff_t>( f_array.size() ) == ( f_array.cend() - f_array.cbegin() ), "Inconsistent pointer" );

    elem_t l_sum = std::accumulate( f_array.cbegin(),
                                    f_array.cend(),
                                    elem_t(0), op );
    
    return l_sum;
}



template <typename ST, typename OP_T, typename SIMD_OP_T>
inline auto generic_sum( const vt_array_view<ST> &f_array, OP_T op, SIMD_OP_T simd_op ) SIMD_NOEXCEPT 
-> typename vt_array_traits<ST>::elem_t
{
    typedef typename vt_array_traits<ST>::const_simd_elem_t const_simd_elem_t;
    typedef typename vt_array_traits<ST>::elem_t                       elem_t;
    typedef typename vt_array_traits<ST>::const_elem_t           const_elem_t;

    const vt_array_view<const_simd_elem_t> l_inside_view = f_array.inside();
    simd_assert( false == l_inside_view.is_overlapping(), "defective inside member function" );

    elem_t l_sum = sum( generic_sum( l_inside_view, simd_op ) );

    if ( true == f_array.is_overlapping() )
    {
        const val_array_view<const_elem_t> l_overhang_view = f_array.overhang();
        l_sum += generic_sum( l_overhang_view, op );
    }

    return l_sum;
}



template <typename T, typename OP_T>
inline auto generic_sum( const val_matrix_view<T> &f_matrix, OP_T op ) SIMD_NOEXCEPT 
-> typename val_matrix_traits<T>::elem_t
{
    typedef typename val_matrix_traits<T>::elem_t  elem_t;
    
    if (true == f_matrix.isContiguous())
    {
        simd_assert( 0 <= ( f_matrix.cend() - f_matrix.cbegin() ), "Inconsistent pointers" );
        simd_assert( static_cast<std::ptrdiff_t>( f_matrix.size() ) == ( f_matrix.cend() - f_matrix.cbegin() ),
                     "Inconsistent pointers" );
        simd_assert( f_matrix.size() == ( f_matrix.n_rows() * f_matrix.n_cols() ), "Inconsistent sizes" );

        elem_t l_sum = std::accumulate( f_matrix.cbegin(),
                                        f_matrix.cend(),
                                        elem_t(0), op );
        return l_sum;
    }
    else // (false == f_matrix.is_contiguous())
    {
        const size_t l_nRows = f_matrix.n_rows();

        simd_assert( 0 <= ( f_matrix.cend() - f_matrix.cbegin() ), "Inconsistent pointer" );
        simd_assert( static_cast<std::ptrdiff_t>( f_matrix.size() ) == ( f_matrix.cend() - f_matrix.cbegin() ),
                     "Inconsistent pointers" );
        simd_assert( f_matrix.size() > ( f_matrix.n_rows() * f_matrix.n_cols() ), "Inconsistent sizes" );

        elem_t l_sum(0);
        for ( size_t rIdx = 0; rIdx < l_nRows; ++rIdx )
        {
            simd_assert( static_cast<std::ptrdiff_t>( f_matrix.n_cols() ) == 
                              ( f_matrix.cend( rIdx ) - f_matrix.cbegin( rIdx ) ), "Inconsistent pointers" );
            
            l_sum = std::accumulate( f_matrix.cbegin( rIdx ),
                                     f_matrix.cend( rIdx ),
                                     l_sum, op );
        }
        return l_sum;
    }
}



template <typename ST, typename OP_T, typename SIMD_OP_T>
inline auto generic_sum( const vt_matrix_view<ST> &f_matrix, OP_T op, SIMD_OP_T simd_op ) SIMD_NOEXCEPT 
-> typename vt_matrix_traits<ST>::elem_t
{
    typedef typename vt_matrix_traits<ST>::elem_t                        elem_t;
    typedef typename vt_matrix_traits<ST>::const_simd_elem_t  const_simd_elem_t;
    typedef typename vt_matrix_traits<ST>::const_elem_t            const_elem_t;

    if ( false == f_matrix.is_overlapping() )
    {
        const val_matrix_view<const_simd_elem_t>& l_matrix_view = f_matrix;

        return sum( generic_sum( l_matrix_view, simd_op ) );
    }
    else 
    {
        const val_matrix_view<const_simd_elem_t> l_inside_view = f_matrix.inside();
        simd_assert( false == l_inside_view.is_overlapping(), "defective inside member function" );

        elem_t l_sum = sum( generic_sum( l_inside_view, simd_op ) );

        if ( true == f_matrix.is_overlapping() )
        {
            const val_matrix_view<const_elem_t> l_overhang_view = f_matrix.overhang();
            
            l_sum += generic_sum_accumulation( l_overhang_view, op );
        }

        return l_sum;
    }
}


} // anonymous namespace


// ---------------------------------------------------------------------------------------


template <typename T>
inline auto sum( const val_array_view<T> &f_array ) SIMD_NOEXCEPT 
-> typename val_array_traits<T>::elem_t
{
    return generic_sum( f_array, std::plus<T>() );
}


template <typename ST>
inline auto sum( const vt_array_view<ST> &f_array ) SIMD_NOEXCEPT 
-> typename vt_array_traits<ST>::elem_t
{
    typedef typename vt_array_traits<ST>::simd_elem_t  simd_elem_t;
    typedef typename vt_array_traits<ST>::elem_t            elem_t;

    return generic_sum( f_array, std::plus<elem_t>(), std::plus<simd_elem_t>() );
}


template <typename T>
inline auto sum( const val_matrix_view<T> &f_matrix ) SIMD_NOEXCEPT 
-> typename val_matrix_traits<T>::elem_t
{
    return generic_sum( f_matrix, std::plus<T>() );
}


template <typename ST>
inline auto sum( const vt_matrix_view<ST> &f_matrix ) SIMD_NOEXCEPT 
-> typename vt_matrix_traits<ST>::elem_t
{
    typedef typename vt_matrix_traits<ST>::simd_elem_t  simd_elem_t;
    typedef typename vt_matrix_traits<ST>::elem_t            elem_t;

    return generic_sum( f_matrix, std::plus<elem_t>(), std::plus<simd_elem_t>() );
}


// ---------------------------------------------------------------------------------------


template <typename T>
inline float32_t safe_sumXlogX( const val_array_view<T> &f_array ) SIMD_NOEXCEPT
{
    return generic_sum( f_array, nuk::safe_sumXlogXkernel<float32_t>() );
}


template <typename ST>
inline float32_t safe_sumXlogX( const vt_array_view<ST> &f_array ) SIMD_NOEXCEPT
{
    return generic_sum( f_array, nuk::safe_sumXlogXkernel<float32_t>(),
                                 nuk::safe_sumXlogXkernel<vf32_t>() );
}


template <typename T>
inline float32_t safe_sumXlogX( const val_matrix_view<T> &f_matrix ) SIMD_NOEXCEPT
{
    return generic_sum( f_matrix, nuk::safe_sumXlogXkernel<vf32_t>() );
}


template <typename ST>
inline float32_t safe_sumXlogX( const vt_matrix_view<ST> &f_matrix ) SIMD_NOEXCEPT
{
    return generic_sum( f_matrix, nuk::safe_sumXlogXkernel<float32_t>(),
                                  nuk::safe_sumXlogXkernel<vf32_t>() );
}


} // namespace smd
