/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "val_array_view.hpp"
#include "val_matrix_view.hpp"
#include "simd_array_view.hpp"
#include "simd_matrix_view.hpp"


namespace smd
{


template <typename T>
inline val_array_view<T> wrap_array( T* f_mem_p, size_t n_items ) SIMD_NOEXCEPT;


template <typename T>
inline vt_array_view< typename select_simd<T>::type >
wrap_vt_array( T* f_mem_p, size_t n_items ) SIMD_NOEXCEPT;



template <typename T>
inline val_matrix_view<T> wrap_matrix( T* f_mem_p, size_t f_nRows, size_t f_nCols, size_t f_rowStride ) SIMD_NOEXCEPT;


template <typename T>
inline vt_matrix_view< typename select_simd<T>::type >
wrap_vt_matrix( T* f_mem_p, size_t f_nElemRows, size_t f_nElemCols, size_t f_rowElemStride ) SIMD_NOEXCEPT;


} // namespace smd
