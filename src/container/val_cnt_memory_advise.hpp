/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once


#include <cpp_tools.hpp>
#include "val_cnt_base.hpp"


namespace smd
{


template <typename T, typename ...Args>
inline
void memory_advise( const val_cnt_base<T>& f_cnt_rs, Args... flags )
{
    cppt::memory_advise( f_cnt_rs.begin(), sizeof(T) * f_cnt_rs.mem_size(), flags... );
}


} // namespace smd

