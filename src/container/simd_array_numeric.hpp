/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_array_view.hpp"
#include "simd_matrix_view.hpp"


namespace smd
{


template <typename T>
inline auto sum( const val_array_view<T> &f_array ) SIMD_NOEXCEPT 
-> typename val_array_traits<T>::elem_t;


template <typename ST>
inline auto sum( const vt_array_view<ST> &f_array ) SIMD_NOEXCEPT 
-> typename vt_array_traits<ST>::elem_t;



template <typename T>
inline auto sum( const val_matrix_view<T> &f_matrix ) SIMD_NOEXCEPT 
-> typename val_matrix_traits<T>::elem_t;


template <typename ST>
inline auto sum( const vt_matrix_view<ST> &f_matrix ) SIMD_NOEXCEPT 
-> typename vt_matrix_traits<ST>::elem_t;


// -----------------------------------------------------------------------------------------


template <typename T>
inline float32_t safe_sumXlogX( const val_array_view<T> &f_array ) SIMD_NOEXCEPT;


template <typename ST>
inline float32_t safe_sumXlogX( const vt_array_view<ST> &f_array ) SIMD_NOEXCEPT;


template <typename T>
inline float32_t safe_sumXlogX( const val_matrix_view<T> &f_matrix ) SIMD_NOEXCEPT;


template <typename ST>
inline float32_t safe_sumXlogX( const vt_matrix_view<ST> &f_matrix ) SIMD_NOEXCEPT;



} // namespace smd
