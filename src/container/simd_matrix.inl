/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_matrix.hpp"


namespace smd
{


template <typename ST>
inline vt_matrix<ST>::vt_matrix( size_t f_nElemRows, size_t f_nElemCols ) SIMD_NOEXCEPT
    : base_t()
    , m_memory()
{
    const size_t l_simdItems   = simd_elem_t::num_items();
    const size_t l_simdCols    = (f_nElemCols + l_simdItems - 1) / l_simdItems;
    const size_t l_overlapSize = (l_simdCols * l_simdItems) - f_nElemCols;

    simd_assert( l_overlapSize < l_simdItems, "Not plausible overlap size" );
    
    this->initialize( f_nElemRows, l_simdCols, l_overlapSize );

    simd_assert( this->elem().n_cols() == f_nElemCols, "Wrong number of columns" );
    simd_assert( this->elem().n_rows() == f_nElemRows, "Wrong number of rows" );
}


template <typename ST>
inline void
vt_matrix<ST>::initialize( size_t f_nRows, size_t f_nCols, size_t f_overlapSize ) SIMD_NOEXCEPT
{
    const size_t f_Size = f_nRows * f_nCols;
    m_memory.initialize_memory( f_Size );
    
    this->initialize_array( m_memory.getPtr(), f_Size );
    this->initialize_matrix_dims( f_nCols, f_nCols );
    this->initialize_overlap( f_overlapSize );

    this->assert_consistency();
}


} // namespace smd
