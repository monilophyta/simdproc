/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once



#include "val_cnt_base.hpp"
#include "val_array_view.hpp"


namespace smd
{


template <typename T>
struct val_matrix_base : public val_cnt_base< T >
{
public:

    typedef typename val_matrix_traits<T>::elem_t                              elem_t;
    typedef typename val_matrix_traits<T>::const_elem_t                  const_elem_t;
    typedef typename std::remove_const<elem_t>::type                   noconst_elem_t;

    typedef typename val_matrix_traits<T>::iter_bundle_t                iter_bundle_t;
    typedef typename val_matrix_traits<T>::const_iter_bundle_t    const_iter_bundle_t;

    typedef typename val_matrix_traits<T>::row_view_t                      row_view_t;

private:
    
    typedef val_cnt_base< elem_t >                           base_t;
    typedef val_matrix_base< elem_t >                        this_t;
    typedef val_matrix_base< const_elem_t >            const_this_t;


private:

    size_t m_stride;
    size_t m_nCols;

public:

    inline val_matrix_base()
        : base_t()
        , m_stride(0)
        , m_nCols(0)
    {}

    static constexpr  bool isContiguousByDesign() SIMD_NOEXCEPT { return false; }
    inline bool isContiguous() const SIMD_NOEXCEPT { return (m_stride == m_nCols); }

    inline size_t size() const SIMD_NOEXCEPT;
    inline size_t n_rows() const SIMD_NOEXCEPT;
    inline size_t n_cols() const SIMD_NOEXCEPT;
    inline size_t stride() const SIMD_NOEXCEPT;


    inline elem_t& operator() (size_t f_rowidx, size_t f_colidx) const SIMD_NOEXCEPT;
    inline elem_t* operator[] (size_t f_rowidx) const SIMD_NOEXCEPT;

    inline row_view_t row( size_t f_rowidx ) const SIMD_NOEXCEPT;

    using base_t::begin;
    using base_t::end;
    inline elem_t* begin( size_t f_rowidx ) const SIMD_NOEXCEPT { return row_ptr( f_rowidx ); }; 
    inline elem_t* end( size_t f_rowidx ) const SIMD_NOEXCEPT { return row_ptr( f_rowidx ) + n_cols(); }; 
    inline elem_t* last( size_t f_rowidx ) const SIMD_NOEXCEPT;

    using base_t::cbegin;
    using base_t::cend;
    inline const_elem_t* cbegin( size_t f_rowidx ) const SIMD_NOEXCEPT { return row_ptr( f_rowidx ); }; 
    inline const_elem_t* cend( size_t f_rowidx ) const SIMD_NOEXCEPT   { return row_ptr( f_rowidx ) + n_cols(); }; 
    inline const_elem_t* clast( size_t f_rowidx ) const SIMD_NOEXCEPT;


    inline iter_bundle_t row_bundle( size_t f_colIdx ) const SIMD_NOEXCEPT;
    inline const_iter_bundle_t const_row_bundle( size_t f_colIdx ) const SIMD_NOEXCEPT;

    inline iter_bundle_t begin_row_bundle() const SIMD_NOEXCEPT { return row_bundle( 0 ); }
    inline iter_bundle_t end_row_bundle() const SIMD_NOEXCEPT   { return row_bundle( n_cols() ); }

    inline const_iter_bundle_t cbegin_row_bundle() const SIMD_NOEXCEPT { return const_row_bundle( 0 ); }
    inline const_iter_bundle_t cend_row_bundle() const SIMD_NOEXCEPT   { return const_row_bundle( n_cols() ); }


    inline void fill( const noconst_elem_t& f_val ) SIMD_NOEXCEPT;
    inline void set_zero() SIMD_NOEXCEPT;

    /// for debugging
    inline void assert_consistency() const SIMD_NOEXCEPT;

protected:

    inline void initialize_matrix_dims( size_t f_stride, size_t f_nCols ) SIMD_NOEXCEPT;

    inline elem_t* row_ptr( size_t f_rowidx ) const SIMD_NOEXCEPT;
};


} // namespace smd
