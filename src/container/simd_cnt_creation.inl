/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_cnt_creation.hpp"



namespace smd
{


template <typename T>
inline val_array_view<T> wrap_array( T* f_mem_p, size_t n_items ) SIMD_NOEXCEPT
{
    typedef val_array_view<T> array_view_t;

    return array_view_t( f_mem_p, f_mem_p + n_items );
}


template <typename T>
inline vt_array_view< typename select_simd<T>::type >
wrap_vt_array( T* f_mem_p, size_t n_items, size_t f_memSize ) SIMD_NOEXCEPT
{
    typedef typename select_simd<T>::type     simd_elem_t;
    typedef vt_array_view< simd_elem_t >     array_view_t;
    
    constexpr size_t l_simdItems   = simd_elem_t::num_items();
    const size_t l_simdSize        = (n_items + l_simdItems - 1) / l_simdItems;
    const size_t l_overlapSize     = (l_simdSize * l_simdItems) - n_items;

    simd_assert( l_overlapSize < l_simdItems, "Not plausible overlap size" );
    simd_check( f_memSize >= (l_simdSize * l_simdItems), "Memory block of insufficient size" );
    
    array_view_t l_array( f_mem_p, l_simdSize, l_overlapSize );

    simd_assert( l_array.elem().size() == n_items, "Wrong number of items" );

    return l_array;
}




template <typename T>
inline val_matrix_view<T> wrap_matrix( T* f_mem_p, size_t f_nRows, size_t f_nCols, size_t f_rowStride ) SIMD_NOEXCEPT
{
    typedef val_matrix_view<T>  matrix_view_t;

    simd_check( f_nCols <= f_rowStride, "row stride must be larger than number of columns" );

    return matrix_view_t( f_mem_p, f_nRows, f_nCols, f_rowStride );
}



template <typename T>
inline vt_matrix_view< typename select_simd<T>::type >
wrap_vt_matrix( T* f_mem_p, size_t f_nElemRows, size_t f_nElemCols, size_t f_rowElemStride ) SIMD_NOEXCEPT
{
    typedef typename select_simd<T>::type      simd_elem_t;
    typedef vt_matrix_view< simd_elem_t >    matrix_view_t;

    if ( (0 == f_nElemRows) || ( 0 == f_nElemCols) )
        return matrix_view_t();
    

    constexpr size_t l_simdItems = simd_elem_t::num_items();

    simd_check( f_nElemCols <= f_rowElemStride, "row stride must be larger than number of columns" );
    simd_check( 0 == (f_rowElemStride % l_simdItems), "row stride must be larger than number of columns" );

    const size_t l_rowStride = f_rowElemStride / l_simdItems;

    const size_t l_simdCols    = (f_nElemCols + l_simdItems - 1) / l_simdItems;
    const size_t l_overlapSize = (l_simdCols * l_simdItems) - f_nElemCols;

    simd_assert( l_simdCols <= l_rowStride, "Unplausible number of colums" );
    simd_assert( l_overlapSize < l_simdItems, "Not plausible overlap size" );
    
    matrix_view_t l_matrix( f_mem_p,
                            f_mem_p + ( (f_nElemRows - 1) * l_rowStride ) + l_simdCols,
                            l_simdCols,
                            l_rowStride,
                            l_overlapSize );

    simd_assert( l_matrix.elem().n_cols() == f_nElemCols, "Wrong number of columns" );
    simd_assert( l_matrix.elem().n_rows() == f_nElemRows, "Wrong number of rows" );
    simd_assert( l_matrix.elem().stride() == f_rowElemStride, "Wrong row stride" );

    return l_matrix;
}


} // namespace smd
