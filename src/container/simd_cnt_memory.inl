/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_cnt_memory.hpp"


namespace smd
{

template <typename T>
inline cnt_memory<T>::cnt_memory() SIMD_NOEXCEPT
    : m_memPointer()
{}


template <typename T>
inline void cnt_memory<T>::initialize_memory( size_t f_nItems ) SIMD_NOEXCEPT
{
    allocator_t l_allocator;
    m_memPointer.reset( l_allocator.allocate( f_nItems ) );
}


template <typename T>
inline auto cnt_memory<T>::getPtr() const SIMD_NOEXCEPT -> elem_t*
{
    return m_memPointer.get();
}


template <typename T>
inline void cnt_memory<T>::free_memory() SIMD_NOEXCEPT
{
    return m_memPointer.release();
}



} // namespace smd