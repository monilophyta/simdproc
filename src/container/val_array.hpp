/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_array_view.hpp"


namespace smd
{


template <typename T>
struct val_array : public val_array_view<T>
{
private:

    typedef val_array_view<T>        base_t;
    typedef val_array<T>             this_t;

    cnt_memory<T>                    m_memory;

public:

    inline val_array() SIMD_NOEXCEPT;

    inline val_array( size_t f_Size ) SIMD_NOEXCEPT;

private:

    inline void initialize( size_t f_Size ) SIMD_NOEXCEPT;
};



} // namespace smd

