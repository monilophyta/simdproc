/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_array_base.hpp"


namespace smd
{

template <typename T>
struct val_array_view;


template <typename ST>
struct vt_array_view;


template <typename T>
struct val_matrix_base;


template <typename T>
val_array_view<T> wrap_array( T*, size_t );





template <typename T>
struct val_array_view : public val_array_base< T >
{
public:

    template <typename ST>
    friend struct vt_array_view;

    friend struct val_matrix_base<T>;

    friend val_array_view<T> wrap_array<T>( T*, size_t );


    typedef typename val_array_traits<T>::elem_t                    elem_t;
    typedef typename val_array_traits<T>::const_elem_t        const_elem_t;
    typedef typename val_array_traits<T>::noconst_elem_t    noconst_elem_t;

private:

    typedef val_array_base< elem_t >                                base_t;
    typedef val_array_view< elem_t >                                this_t;
    typedef val_array_view< const_elem_t >                    const_this_t;


public:

    inline val_array_view() {}

    inline val_array_view( elem_t* f_array_p, size_t f_size ) SIMD_NOEXCEPT
        : val_array_view( f_array_p, f_array_p + f_size )
    {}

    //inline val_array_view( this_t&& f_other ) SIMD_NOEXCEPT = delete;
    //inline derived_t& operator=( this_t&& f_other ) SIMD_NOEXCEPT = delete;

    // ----------------------------------------------------------------------------------------------------
    // Const Conversions

    template< typename U = elem_t,
              typename = typename std::enable_if< std::is_same<U,const_elem_t>::value >::type >
    inline val_array_view( const val_array_base<noconst_elem_t>& f_other ) SIMD_NOEXCEPT
        : val_array_view( f_other.cbegin(), f_other.cend() )
    {}

    template< typename U = elem_t,
              typename = typename std::enable_if< std::is_same<U,const_elem_t>::value >::type >
    inline val_array_view( const val_array_base<const_elem_t>& f_other ) SIMD_NOEXCEPT
        : val_array_view( f_other.cbegin(), f_other.cend() )
    {}

    // ----------------------------------------------------------------------------------------------------

    inline void deep_copy_from( const const_this_t& f_other ) SIMD_NOEXCEPT;

    // ----------------------------------------------------------------------------------------------------

    /// for debugging
    inline void assert_consistency() const SIMD_NOEXCEPT;


protected:

    inline val_array_view( elem_t* f_array_p, elem_t* f_array_end_p ) SIMD_NOEXCEPT;

};


} // namespace smd
