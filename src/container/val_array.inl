/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_array.hpp"


namespace smd
{


template <typename T>
inline val_array<T>::val_array() SIMD_NOEXCEPT
    : base_t()
    , m_memory()
{}


template <typename T>
inline val_array<T>::val_array( size_t f_Size ) SIMD_NOEXCEPT
    : base_t()
    , m_memory()
{
    initialize( f_Size );
}


template <typename T>
inline void
val_array<T>::initialize( size_t f_Size ) SIMD_NOEXCEPT
{
    m_memory.initialize_memory( f_Size );
    this->initialize_array( m_memory.getPtr(), f_Size );

    this->assert_consistency();
}


} // namespace smd
