/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_array_view.hpp"
#include <algorithm>


namespace smd
{



template <typename T>
inline void
val_array_view<T>::assert_consistency() const SIMD_NOEXCEPT
{
    static_assert( std::is_nothrow_copy_constructible<this_t>::value );
    static_assert( std::is_nothrow_copy_assignable<this_t>::value );

    static_assert( std::is_nothrow_move_constructible<this_t>::value );
    static_assert( std::is_nothrow_move_assignable<this_t>::value );

    //static_assert( false == std::is_move_constructible<this_t>::value );
    //static_assert( false == std::is_move_assignable<this_t>::value );

    typedef val_array_view< const_elem_t >   const_this_t;
    static_assert( true == std::is_convertible< this_t, const_this_t >::value );

    typedef val_array_view< noconst_elem_t >  noconst_this_t;
    static_assert( true == std::is_convertible< noconst_this_t, const_this_t >::value );

    static_assert( false == std::is_convertible< const_this_t, noconst_this_t >::value );

    base_t::assert_consistency();
}


// ------------------------------------------------------------------------------------------------------------------



template <typename T>
inline void val_array_view<T>::deep_copy_from( const const_this_t& f_other ) SIMD_NOEXCEPT
{
    simd_check( f_other.size() == this->size(), "copy between arrays of unequal size" );
    
    const size_t l_numCopy = std::min( this->size(), f_other.size() );

    std::copy_n( f_other.cbegin(), l_numCopy, this->begin() );
}


// -------------------------------------------------------------------------------------



template <typename T>
inline
val_array_view<T>::val_array_view( elem_t* f_array_p, elem_t* f_array_end_p ) SIMD_NOEXCEPT
{
    this->initialize_array( f_array_p, f_array_end_p );
    assert_consistency();
}



} // namespace smd
