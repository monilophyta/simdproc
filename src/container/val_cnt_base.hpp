/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once


#include <type_traits>
#include "simd_cnt_traits.hpp"


namespace smd
{


template <typename T>
struct val_cnt_base
{

public:
    
    typedef T                                         elem_t;
    typedef typename std::add_const<T>::type    const_elem_t;

    typedef elem_t                                value_type;
    
private:

    typedef val_cnt_base<elem_t>                      this_t;
    typedef val_cnt_base<const_elem_t>          const_this_t;


protected:

    elem_t* m_array_p;
    elem_t* m_array_end_p;

public:

    ~val_cnt_base() SIMD_NOEXCEPT;

    // --------------------------------------------------------------------------------------------------
    // Constructors + assignment

    inline val_cnt_base() SIMD_NOEXCEPT
        : m_array_p(NULL)
        , m_array_end_p(NULL)
    {}

    // ------------------------------------------------------------------------------------------------------
    // Members
    
    inline size_t mem_size() const SIMD_NOEXCEPT { return (m_array_end_p - m_array_p); }

    inline size_t size() const SIMD_NOEXCEPT { return this->mem_size(); }

    inline elem_t* begin() SIMD_NOEXCEPT
        { return m_array_p; }
    
    inline elem_t* end() SIMD_NOEXCEPT
        { return m_array_end_p; }


    inline const_elem_t* cbegin() const SIMD_NOEXCEPT
        { return m_array_p; }
    
    inline const_elem_t* cend() const SIMD_NOEXCEPT
        { return m_array_end_p; }
    

    inline const_elem_t* begin() const SIMD_NOEXCEPT
        { return cbegin(); }
    
    inline const_elem_t* end() const SIMD_NOEXCEPT
        { return cend(); }
    
    /// for debugging
    inline void assert_consistency() const SIMD_NOEXCEPT;

protected:

    inline void initialize_array( elem_t* f_array_p, elem_t* f_array_end_p ) SIMD_NOEXCEPT;
    inline void initialize_array( elem_t* f_array_p, size_t f_size ) SIMD_NOEXCEPT { initialize_array( f_array_p, f_array_p + f_size ); }

};



} // namespace smd
