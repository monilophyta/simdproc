/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once


#include "val_cnt_base.hpp"


namespace smd
{


template <typename T>
struct val_array_base : public val_cnt_base< T >
{
public:

    typedef typename val_array_traits<T>::elem_t              elem_t;
    typedef typename val_array_traits<T>::const_elem_t  const_elem_t;
    typedef typename std::remove_const<elem_t>::type  noconst_elem_t;

private:

    typedef val_array_base< elem_t >                          this_t;
    typedef val_cnt_base< elem_t >                            base_t;


public:

    using base_t::base_t;

    static constexpr bool isContiguousByDesign() SIMD_NOEXCEPT { return true; }
    static constexpr bool isContiguous() SIMD_NOEXCEPT { return true; }

    
    inline elem_t* last();
    inline const_elem_t* clast() const;

    inline void fill( const noconst_elem_t& f_val ) SIMD_NOEXCEPT;
    inline void set_zero() SIMD_NOEXCEPT;

    inline elem_t& operator[] (size_t f_idx) SIMD_NOEXCEPT;
    inline elem_t& operator() (size_t f_idx)  SIMD_NOEXCEPT { return (*this)[f_idx]; }

    inline const_elem_t& operator[] (size_t f_idx) const SIMD_NOEXCEPT;
    inline const_elem_t& operator() (size_t f_idx) const  SIMD_NOEXCEPT { return (*this)[f_idx]; }
};



} // namespace smd
