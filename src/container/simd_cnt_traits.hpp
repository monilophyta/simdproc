/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_cnt_traits.hpp"



namespace smd
{



template <typename ST>
struct vt_common_traits
{
    static_assert( is_simd<ST>::value );

    typedef ST                                                                  simd_elem_t;
    typedef typename std::add_const<simd_elem_t>::type                    const_simd_elem_t;
    typedef typename std::remove_const<simd_elem_t>::type               noconst_simd_elem_t;

    static_assert( false == std::is_same< noconst_simd_elem_t, const_simd_elem_t >::value  );

    typedef typename transfer_const<ST,typename simd_elem_t::elem_t>::type           elem_t;
    typedef typename std::add_const<elem_t>::type                              const_elem_t;
    typedef typename std::remove_const<const_elem_t>::type                   noconst_elem_t;

    static_assert( false == std::is_same< noconst_elem_t, const_elem_t >::value  );
};

// ---------------------------------------------------------------------------------------------------


template <typename T>
struct val_array_view;


template <typename ST>
struct vt_array_traits : public vt_common_traits<ST>
{
    using typename vt_common_traits<ST>::elem_t;
    using typename vt_common_traits<ST>::const_elem_t;

    typedef val_array_view<elem_t>                                  elem_view_t;
    typedef val_array_view<const_elem_t>                      const_elem_view_t;
};


// ---------------------------------------------------------------------------------------------------
 

#if 0

template <typename T>
struct array_traits
    : public std::conditional<
                                is_simd<T>::value,
                                vt_array_traits<T>,
                                val_array_traits<T>
                             >::type
{};

#endif

// ---------------------------------------------------------------------------------------------------

template <typename T>
struct val_matrix_view;


template <typename ST>
struct vt_array_view;


template <typename ST>
struct vt_matrix_traits : public vt_common_traits<ST>
{
    using typename vt_common_traits<ST>::elem_t;
    using typename vt_common_traits<ST>::const_elem_t;
    using typename vt_common_traits<ST>::simd_elem_t;


    typedef val_matrix_view<elem_t>                                elem_view_t;
    typedef val_matrix_view<const_elem_t>                    const_elem_view_t;

    typedef vt_array_view<simd_elem_t>                              row_view_t;
};

// ---------------------------------------------------------------------------------------------------

#if 0

template <typename T>
struct matrix_traits
    : public std::conditional<
                                is_simd<T>::value,
                                vt_matrix_traits<T>,
                                val_matrix_traits<T>
                             >::type
{};

#endif

// ---------------------------------------------------------------------------------------------------



} // namespace smd
