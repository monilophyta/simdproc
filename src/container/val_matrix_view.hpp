/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "val_matrix_base.hpp"
#include "val_cnt_traits.hpp"


namespace smd
{

template <typename T>
struct val_matrix_view;


template <typename ST>
struct vt_matrix_view;



template <typename T>
val_matrix_view<T> wrap_matrix( T*, size_t, size_t, size_t );



template <typename T>
struct val_matrix_view : public val_matrix_base< T >
{
public:

    template <typename ST>
    friend struct vt_matrix_view;

    friend val_matrix_view<T> wrap_matrix<T>( T*, size_t, size_t, size_t );


    typedef typename val_matrix_traits<T>::elem_t                      elem_t;
    typedef typename val_matrix_traits<T>::const_elem_t          const_elem_t;
    typedef typename val_matrix_traits<T>::noconst_elem_t      noconst_elem_t;

private:

    typedef val_matrix_base< elem_t >                                  base_t;
    typedef val_matrix_view< elem_t >                                  this_t;
    typedef val_matrix_view< const elem_t >                      const_this_t;


public:

    inline val_matrix_view() {}


    //inline val_matrix_view( this_t&& f_other ) SIMD_NOEXCEPT = delete;
    //inline derived_t& operator=( this_t&& f_other ) SIMD_NOEXCEPT = delete;


    // ----------------------------------------------------------------------------------------------------
    // Const Conversion


    template< typename U = elem_t,
              typename = typename std::enable_if< std::is_same<U,const_elem_t>::value >::type >
    inline val_matrix_view( const val_matrix_base<noconst_elem_t>& f_other ) SIMD_NOEXCEPT
    {
        this->initialize_array( f_other.cbegin(), f_other.cend() );
        this->initialize_matrix_dims( f_other.stride(), f_other.n_cols() );
        assert_consistency();
    }


    template< typename U = elem_t,
              typename = typename std::enable_if< std::is_same<U,const_elem_t>::value >::type >
    inline val_matrix_view( const val_matrix_base<const_elem_t>& f_other ) SIMD_NOEXCEPT
    {
        this->initialize_array( f_other.cbegin(), f_other.cend() );
        this->initialize_matrix_dims( f_other.stride(), f_other.n_cols() );
        assert_consistency();
    }

    // ----------------------------------------------------------------------------------------------------

    inline this_t outside() SIMD_NOEXCEPT { return create_outside_view( *this ); }
    inline const_this_t outside() const SIMD_NOEXCEPT { return create_outside_view( *this ); }

    // ----------------------------------------------------------------------------------------------------

    inline void deep_copy_from( const const_this_t& f_other ) SIMD_NOEXCEPT;

    // ----------------------------------------------------------------------------------------------------


    /// for debugging
    inline void assert_consistency() const SIMD_NOEXCEPT;

protected:

    inline val_matrix_view( elem_t* f_mem_p, elem_t* f_mem_end_p, size_t f_nCols, size_t f_rowStride ) SIMD_NOEXCEPT;

    inline val_matrix_view( elem_t* f_mem_p, size_t f_nRows, size_t f_nCols, size_t f_rowStride ) SIMD_NOEXCEPT
        : val_matrix_view( f_mem_p, f_mem_p + (f_nRows * f_rowStride),  f_nCols, f_rowStride )
    {}

private:

    template <typename CNT_T>
    static inline CNT_T create_outside_view( CNT_T& f_matrix ) SIMD_NOEXCEPT;

};


} // namespace smd
