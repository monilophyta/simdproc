/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include <memory>
#include <cpp_tools.hpp>


namespace smd
{



template <typename T>
struct cnt_memory
{
private:


    typedef cnt_memory<T>                                       this_t;

    typedef T                                                   elem_t;
    typedef typename std::remove_const<T>::type         noconst_elem_t;

    
    /// memory alignment to cache size
    constexpr static size_t memory_alignemnt = std::max( sizeof(noconst_elem_t),
                                                         cppt::CACHE_LINE_SIZE );

    typedef
        typename std::conditional< is_simd<noconst_elem_t>::value,
                                   aligned_allocator<noconst_elem_t,memory_alignemnt>,
                                   simple_allocator<noconst_elem_t>
                                 >::type                   allocator_t;
    
    typedef deleter<allocator_t>                             deleter_t;


    typedef std::unique_ptr< noconst_elem_t[], deleter_t>    pointer_t;

    pointer_t m_memPointer;

public:

    inline cnt_memory() SIMD_NOEXCEPT;

    inline void initialize_memory( size_t f_nItems ) SIMD_NOEXCEPT;

    inline elem_t* getPtr() const SIMD_NOEXCEPT;

private:

    inline void free_memory() SIMD_NOEXCEPT;
};



}  // namespace smd
