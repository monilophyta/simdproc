/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_array.hpp"


namespace smd
{


template <typename ST>
inline vt_array<ST>::vt_array( size_t f_elemSize ) SIMD_NOEXCEPT
    : base_t()
    , m_memory()
{  
    const size_t l_simdItems   = simd_elem_t::num_items();
    const size_t l_simdSize    = (f_elemSize + l_simdItems - 1) / l_simdItems;
    const size_t l_overlapSize = (l_simdSize * l_simdItems) - f_elemSize;

    simd_assert( l_overlapSize < l_simdItems, "Not plausible overlap size" );
    
    this->initialize( l_simdSize, l_overlapSize );

    simd_assert( this->elem().size() == f_elemSize, "Wrong number of items" );
}


template <typename ST>
inline void
vt_array<ST>::initialize( size_t f_Size, size_t f_overlapSize ) SIMD_NOEXCEPT
{
    m_memory.initialize_memory( f_Size );
    this->initialize_array( m_memory.getPtr(), f_Size );
    this->initialize_overlap( f_overlapSize );

    this->assert_consistency();
}


} // namespace smd
