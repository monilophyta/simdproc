
/*  AVX implementation of natual logarithm

    Based on "avx_mathfun.h", by Giovanni Garberoglio
    http://software-lisc.fbk.eu/avx_mathfun/avx_mathfun.h

    which again is based on "sse_mathfun.h", by Julien Pommier
    http://gruntthepeon.free.fr/ssemath/
*/


#pragma once


namespace smd
{
namespace foreign
{


template <typename vf32_t, typename vi32_t>
struct CGarberoglioLog
{
    typedef CGarberoglioLog<vf32_t,vi32_t>   this_t;

    static inline constexpr vi32_t mant_mask();
    static inline constexpr vi32_t inv_mant_mask();
    static inline constexpr vi32_t sign_mask();
    static inline constexpr vi32_t inv_sign_mask();
    static inline constexpr vi32_t Ox7f();

    static inline constexpr vf32_t ONE();
    static inline constexpr vf32_t Op5();
    static inline constexpr vf32_t min_norm_pos();

    static inline constexpr vf32_t cephes_SQRTHF();
    static inline constexpr vf32_t cephes_log_p0();
    static inline constexpr vf32_t cephes_log_p1();
    static inline constexpr vf32_t cephes_log_p2();
    static inline constexpr vf32_t cephes_log_p3();
    static inline constexpr vf32_t cephes_log_p4();
    static inline constexpr vf32_t cephes_log_p5();
    static inline constexpr vf32_t cephes_log_p6();
    static inline constexpr vf32_t cephes_log_p7();
    static inline constexpr vf32_t cephes_log_p8();
    static inline constexpr vf32_t cephes_log_q1();
    static inline constexpr vf32_t cephes_log_q2();

    inline vf32_t operator()( const vf32_t &xvect );
};


} // namespace foreign
} // namespace smd


#include "simd_log.inl"

