
/*  AVX implementation of natual logarithm

    Based on "avx_mathfun.h", by Giovanni Garberoglio
    http://software-lisc.fbk.eu/avx_mathfun/avx_mathfun.h

    which again is based on "sse_mathfun.h", by Julien Pommier
    http://gruntthepeon.free.fr/ssemath/
*/


#pragma once


namespace smd
{
namespace foreign
{



#define I32_CONSTANT( NAME, VALUE ) \
template <typename vf32_t, typename vi32_t> \
inline constexpr vi32_t \
CGarberoglioLog<vf32_t,vi32_t>:: NAME () { return vi32_t( VALUE ); }

I32_CONSTANT( mant_mask,      0x7f800000 );
I32_CONSTANT( inv_mant_mask, ~0x7f800000 );
I32_CONSTANT( sign_mask,      0x80000000 );
I32_CONSTANT( inv_sign_mask, ~0x80000000 );
I32_CONSTANT( Ox7f,           0x7f );

#undef I32_CONSTANT


#define F32_CONSTANT( NAME, VALUE ) \
template <typename vf32_t, typename vi32_t> \
inline constexpr vf32_t \
CGarberoglioLog<vf32_t,vi32_t>:: NAME () { return vf32_t( VALUE ); }

F32_CONSTANT( ONE, 1.0f );
F32_CONSTANT( Op5, 0.5f );
F32_CONSTANT( min_norm_pos, std::numeric_limits<float32_t>::min() );

F32_CONSTANT( cephes_SQRTHF, 0.707106781186547524f )
F32_CONSTANT( cephes_log_p0,   7.0376836292E-2f );
F32_CONSTANT( cephes_log_p1,  -1.1514610310E-1f );
F32_CONSTANT( cephes_log_p2,   1.1676998740E-1f );
F32_CONSTANT( cephes_log_p3,  -1.2420140846E-1f );
F32_CONSTANT( cephes_log_p4,  +1.4249322787E-1f );
F32_CONSTANT( cephes_log_p5,  -1.6668057665E-1f );
F32_CONSTANT( cephes_log_p6,  +2.0000714765E-1f );
F32_CONSTANT( cephes_log_p7,  -2.4999993993E-1f );
F32_CONSTANT( cephes_log_p8,  +3.3333331174E-1f );
F32_CONSTANT( cephes_log_q1,  -2.12194440e-4f );
F32_CONSTANT( cephes_log_q2,   0.693359375f );

#undef F32_CONSTANT




template <typename vf32_t, typename vi32_t>
inline vf32_t
CGarberoglioLog<vf32_t,vi32_t>::operator()( const vf32_t &xvect )
{

    // invalidate what is smaller zero
    const vf32_t invalid_mask = (xvect <= 0.f).reinterpret_as_float32();

    // cut off denormalized stuff
    vf32_t x = smd::max( xvect, min_norm_pos() ); 

    // get exponent
    vf32_t emm0 = vf32_t( (x.reinterpret_as_int32() >> 23) + (1 - 0x7f) );

    // keep only the fractional part in x
    x &= inv_mant_mask().reinterpret_as_float32();
    x |= Op5();

    /* part2: 
        if( x < SQRTHF ) {
        e -= 1;
        x = x + x - 1.0;
        } else { x = x - 1.0; }
    */
    {
        //vf32 mask = instr.cmplt_ps(x, *(vf32*)_ps256_cephes_SQRTHF);
        const vf32_t mask = (x < cephes_SQRTHF()).reinterpret_as_float32();
        const vf32_t tmp = x & mask;
        
        x -= ONE();
        emm0 -= mask & ONE();
        x += tmp;
    }
    {
        const vf32_t z = x*x;

        vf32_t y = (cephes_log_p0() * x) + cephes_log_p1();

        y = (y*x) + cephes_log_p2();
        y = (y*x) + cephes_log_p3();
        y = (y*x) + cephes_log_p4();
        y = (y*x) + cephes_log_p5();
        y = (y*x) + cephes_log_p6();
        y = (y*x) + cephes_log_p7();
        y = (y*x) + cephes_log_p8();

        y *= x;
        y *= z;

        y += emm0 * cephes_log_q1();
        y -= z * 0.5f;

        x += y;
    }
    x += emm0 * cephes_log_q2();
    
    vf32_t lnx = x | invalid_mask; // negative arg will be NAN
    return lnx;
}


} // namespace foreign
} // namespace smd

