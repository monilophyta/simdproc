/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "base_tools/simd_support.hpp"
#include "base_tools/simd_debug.hpp"
#include "base_tools/simd_type_traits.hpp"

#include "base_tools/simd_memory.hpp"
#include "base_tools/simd_memory.inl"

