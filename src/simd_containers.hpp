/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

//-------------------------------------------------------------------------------------------------------------


#include "container/val_cnt_base.hpp"
#include "container/val_cnt_base.inl"

#include "container/val_cnt_memory_advise.hpp"

#include "container/val_array_base.hpp"
#include "container/val_array_base.inl"

#include "container/val_array_view.hpp"
#include "container/val_array_view.inl"

namespace smd
{
    typedef val_array_view<vb32_t::elem_t>  array_view_b32_t;
    typedef val_array_view<vi32_t::elem_t>  array_view_i32_t;
    typedef val_array_view<vf32_t::elem_t>  array_view_f32_t;

    typedef val_array_view<const vb32_t::elem_t>  array_view_cb32_t;
    typedef val_array_view<const vi32_t::elem_t>  array_view_ci32_t;
    typedef val_array_view<const vf32_t::elem_t>  array_view_cf32_t;

    static_assert( std::is_convertible< array_view_b32_t, array_view_cb32_t >::value );
    static_assert( std::is_convertible< array_view_i32_t, array_view_ci32_t >::value );
    static_assert( std::is_convertible< array_view_f32_t, array_view_cf32_t >::value );

    static_assert( false == std::is_convertible< array_view_cb32_t, array_view_b32_t >::value );
    static_assert( false == std::is_convertible< array_view_ci32_t, array_view_i32_t >::value );
    static_assert( false == std::is_convertible< array_view_cf32_t, array_view_f32_t >::value );
}



//-------------------------------------------------------------------------------------------------------------


#include "container/simd_array_view.hpp"
#include "container/simd_array_view.inl"

namespace smd
{
    typedef vt_array_view<vb32_t>  array_view_vb32_t;
    typedef vt_array_view<vi32_t>  array_view_vi32_t;
    typedef vt_array_view<vf32_t>  array_view_vf32_t;

    typedef vt_array_view<const vb32_t>  array_view_cvb32_t;
    typedef vt_array_view<const vi32_t>  array_view_cvi32_t;
    typedef vt_array_view<const vf32_t>  array_view_cvf32_t;

    static_assert( std::is_convertible< array_view_vb32_t, array_view_cvb32_t >::value );
    static_assert( std::is_convertible< array_view_vi32_t, array_view_cvi32_t >::value );
    static_assert( std::is_convertible< array_view_vf32_t, array_view_cvf32_t >::value );

    static_assert( false == std::is_convertible< array_view_cvb32_t, array_view_vb32_t >::value );
    static_assert( false == std::is_convertible< array_view_cvi32_t, array_view_vi32_t >::value );
    static_assert( false == std::is_convertible< array_view_cvf32_t, array_view_vf32_t >::value );
}


//-------------------------------------------------------------------------------------------------------------


#include "container/val_matrix_base.hpp"
#include "container/val_matrix_base.inl"

#include "container/val_matrix_view.hpp"
#include "container/val_matrix_view.inl"



namespace smd
{
    typedef val_matrix_view<vb32_t::elem_t>  matrix_view_b32_t;
    typedef val_matrix_view<vi32_t::elem_t>  matrix_view_i32_t;
    typedef val_matrix_view<vf32_t::elem_t>  matrix_view_f32_t;

    typedef val_matrix_view<const vb32_t::elem_t>  matrix_view_cb32_t;
    typedef val_matrix_view<const vi32_t::elem_t>  matrix_view_ci32_t;
    typedef val_matrix_view<const vf32_t::elem_t>  matrix_view_cf32_t;

    static_assert( std::is_convertible< matrix_view_b32_t, matrix_view_cb32_t >::value );
    static_assert( std::is_convertible< matrix_view_i32_t, matrix_view_ci32_t >::value );
    static_assert( std::is_convertible< matrix_view_f32_t, matrix_view_cf32_t >::value );

    static_assert( false == std::is_convertible< matrix_view_cb32_t, matrix_view_b32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_ci32_t, matrix_view_i32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cf32_t, matrix_view_f32_t >::value );
}

//-------------------------------------------------------------------------------------------------------------


#include "container/simd_matrix_view.hpp"
#include "container/simd_matrix_view.inl"


namespace smd
{
    typedef vt_matrix_view<vb32_t>  matrix_view_vb32_t;
    typedef vt_matrix_view<vi32_t>  matrix_view_vi32_t;
    typedef vt_matrix_view<vf32_t>  matrix_view_vf32_t;

    typedef vt_matrix_view<const vb32_t>  matrix_view_cvb32_t;
    typedef vt_matrix_view<const vi32_t>  matrix_view_cvi32_t;
    typedef vt_matrix_view<const vf32_t>  matrix_view_cvf32_t;

    static_assert( std::is_convertible< matrix_view_vb32_t, matrix_view_cvb32_t >::value );
    static_assert( std::is_convertible< matrix_view_vi32_t, matrix_view_cvi32_t >::value );
    static_assert( std::is_convertible< matrix_view_vf32_t, matrix_view_cvf32_t >::value );

    static_assert( false == std::is_convertible< matrix_view_cvb32_t, matrix_view_vb32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cvi32_t, matrix_view_vi32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cvf32_t, matrix_view_vf32_t >::value );
}


//-------------------------------------------------------------------------------------------------------------

#include "container/simd_cnt_memory.hpp"
#include "container/simd_cnt_memory.inl"


//-------------------------------------------------------------------------------------------------------------


#include "container/val_array.hpp"
#include "container/val_array.inl"


namespace smd
{
    typedef val_array<vb32_t::elem_t>     array_b32_t;
    typedef val_array<vi32_t::elem_t>     array_i32_t;
    typedef val_array<vf32_t::elem_t>     array_f32_t;

    typedef val_array<const vb32_t::elem_t>     array_cb32_t;
    typedef val_array<const vi32_t::elem_t>     array_ci32_t;
    typedef val_array<const vf32_t::elem_t>     array_cf32_t;

    static_assert( std::is_convertible< array_b32_t,  array_view_cb32_t >::value );
    static_assert( std::is_convertible< array_i32_t,  array_view_i32_t >::value );
    static_assert( std::is_convertible< array_cf32_t, array_view_cf32_t >::value );

    static_assert( false == std::is_convertible< array_view_b32_t,  array_b32_t >::value );
    static_assert( false == std::is_convertible< array_view_ci32_t, array_ci32_t >::value );
    static_assert( false == std::is_convertible< array_view_cf32_t, array_f32_t >::value );


} // namespace smd




//-------------------------------------------------------------------------------------------------------------


#include "container/simd_array.hpp"
#include "container/simd_array.inl"


namespace smd
{
    typedef vt_array<vb32_t>     array_vb32_t;
    typedef vt_array<vi32_t>     array_vi32_t;
    typedef vt_array<vf32_t>     array_vf32_t;

    typedef vt_array<const vb32_t>     array_cvb32_t;
    typedef vt_array<const vi32_t>     array_cvi32_t;
    typedef vt_array<const vf32_t>     array_cvf32_t;

    static_assert( std::is_convertible< array_vb32_t,  array_view_cvb32_t >::value );
    static_assert( std::is_convertible< array_vi32_t,  array_view_vi32_t >::value );
    static_assert( std::is_convertible< array_cvf32_t, array_view_cvf32_t >::value );

    static_assert( false == std::is_convertible< array_view_vb32_t,  array_vb32_t >::value );
    static_assert( false == std::is_convertible< array_view_cvi32_t, array_cvi32_t >::value );
    static_assert( false == std::is_convertible< array_view_cvf32_t, array_vf32_t >::value );




    template <typename T>
    struct select_array
        : public std::conditional< 
                    is_simd<T>::value,
                    typename select_simd_base_dependent< 
                        select_simd_generic<T, vb32_t, vi32_t, vf32_t>,
                        array_vb32_t,
                        array_vi32_t,
                        array_vf32_t >::type,
                    typename select_simd_base_dependent< 
                        select_simd_generic<T, bool32_t, int32_t, float32_t>,
                        array_b32_t,
                        array_i32_t,
                        array_f32_t >::type
                >
    {};

} // namespace smd


//-------------------------------------------------------------------------------------------------------------

#include "container/val_matrix.hpp"
#include "container/val_matrix.inl"


namespace smd
{
    typedef val_matrix<vb32_t::elem_t>     matrix_b32_t;
    typedef val_matrix<vi32_t::elem_t>     matrix_i32_t;
    typedef val_matrix<vf32_t::elem_t>     matrix_f32_t;

    typedef val_matrix<const vb32_t::elem_t>     matrix_cb32_t;
    typedef val_matrix<const vi32_t::elem_t>     matrix_ci32_t;
    typedef val_matrix<const vf32_t::elem_t>     matrix_cf32_t;

    static_assert( std::is_convertible< matrix_b32_t,  matrix_view_cb32_t >::value );
    static_assert( std::is_convertible< matrix_i32_t,  matrix_view_i32_t >::value );
    static_assert( std::is_convertible< matrix_cf32_t, matrix_view_cf32_t >::value );

    static_assert( false == std::is_convertible< matrix_view_b32_t,  matrix_b32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_ci32_t, matrix_ci32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cf32_t, matrix_f32_t >::value );


} // namespace smd


//-------------------------------------------------------------------------------------------------------------


#include "container/simd_matrix.hpp"
#include "container/simd_matrix.inl"


namespace smd
{
    typedef vt_matrix<vb32_t>     matrix_vb32_t;
    typedef vt_matrix<vi32_t>     matrix_vi32_t;
    typedef vt_matrix<vf32_t>     matrix_vf32_t;

    typedef vt_matrix<const vb32_t>     matrix_cvb32_t;
    typedef vt_matrix<const vi32_t>     matrix_cvi32_t;
    typedef vt_matrix<const vf32_t>     matrix_cvf32_t;

    static_assert( std::is_convertible< matrix_vb32_t,  matrix_view_cvb32_t >::value );
    static_assert( std::is_convertible< matrix_vi32_t,  matrix_view_vi32_t >::value );
    static_assert( std::is_convertible< matrix_cvf32_t, matrix_view_cvf32_t >::value );

    static_assert( false == std::is_convertible< matrix_view_vb32_t,  matrix_vb32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cvi32_t, matrix_cvi32_t >::value );
    static_assert( false == std::is_convertible< matrix_view_cvf32_t, matrix_vf32_t >::value );




    template <typename T>
    struct select_matrix
        : public std::conditional< 
                    is_simd<T>::value,
                    typename select_simd_base_dependent< 
                        select_simd_generic<T, vb32_t, vi32_t, vf32_t>,
                        matrix_vb32_t,
                        matrix_vi32_t,
                        matrix_vf32_t >::type,
                    typename select_simd_base_dependent< 
                        select_simd_generic<T, bool32_t, int32_t, float32_t>,
                        matrix_b32_t,
                        matrix_i32_t,
                        matrix_f32_t >::type
                >
    {};

} // namespace smd


//-------------------------------------------------------------------------------------------------------------

#include "container/simd_cnt_creation.hpp"
#include "container/simd_cnt_creation.inl"


//-------------------------------------------------------------------------------------------------------------

#include "container/simd_array_functions.hpp"
#include "container/simd_array_functions.inl"


#include "container/simd_matrix_functions.hpp"
#include "container/simd_matrix_functions.inl"

//-------------------------------------------------------------------------------------------------------------

#ifdef __SIMD_USE_AVX2__ 

    #ifdef __USE_SIMD_256__
        #include "simd_avx/simd_avx2_array.hpp"
    #elif defined(__USE_SIMD_128__)
        #include "simd_sse/simd_sse_avx2_array.hpp"
    #endif

#endif // __SIMD_USE_AVX2__


#if defined(__SIMD_USE_AVX2__)

    #if defined(__USE_SIMD_256__)
        #include "simd_avx/simd_avx2_matrix.hpp"
    #elif defined(__USE_SIMD_128__)
        #include "simd_sse/simd_sse_avx2_matrix.hpp"
    #endif

#endif // __SIMD_USE_AVX2__


#include "container/simd_array_numeric.hpp"
#include "container/simd_array_numeric.inl"

