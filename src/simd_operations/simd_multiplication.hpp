/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_binary_op.hpp"
#include "simd_binary_mem.hpp"



namespace smd
{

// ------------------------------------------------------------------------------------


template <typename VT, typename derived_t>
struct simd_multiply_base
    : public simd_binary_op<VT,derived_t>
{

public:

    // render multiplication
    inline VT render() const
    {
        typedef typename VT::base_t  vt_base_t;
        
        // when ariving here, use the base implementation as default
        return ( static_cast<const vt_base_t&>( this->vx() ) * 
                 static_cast<const vt_base_t&>( this->vy() ) );
    }
};


// ------------------------------------------------------------------------------------


template <typename VT, typename derived_t>
struct simd_multiply_rr_base
    : public simd_binary_rr<VT>                 // should be the first inheritance
    , public simd_multiply_base<VT,derived_t>
{
protected:

    typedef simd_binary_rr<VT>                   rr_base_t;
    //typedef simd_multiply_base<VT,derived_t>   mul_base_t;

public:

    // inherit constructor
    using rr_base_t::rr_base_t;
};

// ------------------------------------------------------------------------------------


template <typename VT, typename derived_t>
struct simd_multiply_vr_base
    : public simd_binary_vr<VT>                 // should be the first inheritance
    , public simd_multiply_base<VT,derived_t>
{
protected:

    typedef simd_binary_vr<VT>                   vr_base_t;
    //typedef simd_multiply_base<VT,derived_t>   mul_base_t;

public:

    // inherit constructor
    using vr_base_t::vr_base_t;
};

// ------------------------------------------------------------------------------------


} // namespace smd
