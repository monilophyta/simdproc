/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{


template <typename VT, typename derived_t>
struct simd_binary_op
{

protected:

    static_assert( is_simd<VT>::value );
    //static_assert( std::is_same<VT, typename cppt::remove_cvref<VT>::type >::value  );

    // cast to derived
    inline derived_t& derived() { return static_cast<derived_t&>(*this); }
    inline const derived_t& derived() const { return static_cast<const derived_t&>(*this); }


public:

    inline auto vx() const { return derived().get_vx(); }
    inline auto vy() const { return derived().get_vy(); }

    inline operator VT() const { return derived().render(); }
};





} // namespace smd

