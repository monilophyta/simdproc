/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <utility>


namespace smd
{



template <typename VT>
struct simd_binary_rr
{
protected:

    static_assert( is_simd<VT>::value );
    //static_assert( std::is_same<VT, typename cppt::remove_cvref<VT>::type>::value  );

    const VT& m_x;
    const VT& m_y;

public:

    inline explicit simd_binary_rr( const VT& f_x, const VT& f_y )
        : m_x(f_x)
        , m_y(f_y)
    {}

    inline const VT& get_vx() const { return m_x; }
    inline const VT& get_vy() const { return m_y; }
};



template <typename VT>
struct simd_binary_vr
{
protected:

    static_assert( is_simd<VT>::value );
    //static_assert( std::is_same<VT, typename cppt::remove_cvref<VT>::type>::value  );

    VT m_x;
    const VT& m_y;


public:

    inline explicit simd_binary_vr( VT&& f_x, const VT& f_y )
        : m_x(std::forward<VT>(f_x))
        , m_y(f_y)
    {}

    inline const VT& get_vx() const { return m_x; }
    inline const VT& get_vy() const { return m_y; }
};




} // namespace smd

