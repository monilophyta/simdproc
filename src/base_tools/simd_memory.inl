/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdlib>
#include <iostream>
#include <cpp_target_os.hpp>
#include <algorithm>
#include <limits>
#include <cstring>
#include "simd_debug.hpp"



namespace smd 
{

namespace
{

template <typename T>
inline void debug_initialization( T* f_begin, std::size_t f_size )
{
    memset( reinterpret_cast<void*>(f_begin), 0xFF, f_size * sizeof(T) );
}

inline void debug_initialization( float32_t* f_begin, std::size_t f_size )
{
    std::fill_n( f_begin, f_size, std::numeric_limits<float32_t>::signaling_NaN() );
}

inline void debug_initialization( float64_t* f_begin, std::size_t f_size )
{
    std::fill_n( f_begin, f_size, std::numeric_limits<float64_t>::signaling_NaN() );
}


} // anonymous namespace




template <typename T, uintptr_t alignment>
inline T* aligned_alloc( std::size_t size ) SIMD_NOEXCEPT
{
    void* l_ptr = NULL;
    
#if defined(TARGET_WINDOWS)

    l_ptr = _aligned_malloc( size, alignment );

#elif defined(TARGET_UNIX)

    #if defined(_GLIBCXX_HAVE_ALIGNED_ALLOC) || defined(_LIBCPP_HAS_C11_FEATURES)
        l_ptr = ::aligned_alloc( alignment, size );
    #else
    // somehow clang++10 on mobile device does not support aligned_alloc
    {
        int error = posix_memalign( &l_ptr, alignment, size );
        if (error != 0) {l_ptr = NULL;}
        //static_assert( false, "no aligned memory allocation possible");
    }
    #endif

#else
    static_assert( false, "no aligned memory allocation possible");
#endif

    simd_assert( ( true == is_memory_aligned<void,alignment>( l_ptr ) ), "aligned_alloc failed - no alignment" );


#ifndef NDEBUG
    simd_assert( 0 == (size % sizeof(T)), "invalid array size" );
    debug_initialization( reinterpret_cast<T*>(l_ptr), size  / sizeof(T) );
#endif

    return reinterpret_cast<T*>(l_ptr);
}



template <typename T>
inline void aligned_free( T* f_memptr ) SIMD_NOEXCEPT
{
#if defined(TARGET_WINDOWS)

    _aligned_free( f_memptr );

#elif defined(TARGET_UNIX)
    
    #if defined(_GLIBCXX_HAVE_ALIGNED_ALLOC) || defined(_LIBCPP_HAS_C11_FEATURES)
        std::free( f_memptr );
    #else
        free( f_memptr );
    #endif

#else
    static_assert( false, "no aligned memory allocation possible");
#endif
}


//--------------------------------------------------------------------------------------------------

template <typename T, uintptr_t alignment>
inline auto
aligned_allocator<T,alignment>::allocate( std::size_t n, const_pointer )  SIMD_NOEXCEPT -> pointer
{
    static_assert( 0 == ( (alignment % sizeof(value_type)) * (sizeof(value_type) % alignment) ),
                   "Inconsistency between alignment and type size" );
    
    value_type* l_mem_p = aligned_alloc<T,alignment>( n * sizeof(value_type) );

    if ( nullptr == l_mem_p )
    {
        throw std::runtime_error( "Failed Memory allocation" );
    }

    simd_check( nullptr != l_mem_p, "Failed Memory allocation" );
    
    return l_mem_p;
}


template <typename T>
inline auto
simple_allocator<T>::allocate( std::size_t n, const_pointer ) SIMD_NOEXCEPT -> pointer
{
    value_type* l_mem_p = reinterpret_cast<value_type*>( std::malloc( n * sizeof(value_type) ) );

    if ( nullptr == l_mem_p )
    {
        throw std::runtime_error( "Failed Memory allocation" );
    }

    simd_check( nullptr != l_mem_p, "Failed Memory allocation" );

#ifndef NDEBUG
    debug_initialization( l_mem_p, n );
#endif
    
    return l_mem_p;
}



//--------------------------------------------------------------------------------------------------



template <typename T, uintptr_t alignment>
inline constexpr bool is_memory_aligned( const T* f_mem_p )
{
    static_assert( 0 == (alignment % 2) );
    
    constexpr uintptr_t UNALIGNED_MASK = alignment - 1;
    return ( 0 == (reinterpret_cast<uintptr_t>( f_mem_p ) & UNALIGNED_MASK) );
}

//--------------------------------------------------------------------------------------------------

template <typename T, uintptr_t alignment>
inline T* equalize_alignemnt( const T* f_src_p, T* f_trg_p )
{
    static_assert( alignment > 0u );
    
    const uintptr_t l_srcShift_u = reinterpret_cast<uintptr_t>(f_src_p) % alignment;
    const uintptr_t l_trgShift_u = reinterpret_cast<uintptr_t>(f_trg_p) % alignment;

    const size_t l_deltaShift_u = (l_srcShift_u >= l_trgShift_u)
                                    ? (l_srcShift_u - l_trgShift_u)
                                    : (alignment - l_trgShift_u + l_srcShift_u);
    
    simd_assert( l_deltaShift_u < alignment, "invalid shift for memory alignment" );
    simd_assert( l_srcShift_u == ((l_trgShift_u + l_deltaShift_u) % alignment), "invalid shift for memory alignment" );
    
    f_trg_p = reinterpret_cast<T*>(reinterpret_cast<uintptr_t>(f_trg_p) + l_deltaShift_u);
    simd_assert( l_srcShift_u == (reinterpret_cast<uintptr_t>(f_trg_p) % alignment), "invalid shift for memory alignment" );
    
    return f_trg_p;
}

//--------------------------------------------------------------------------------------------------

template <typename T>
inline constexpr T* safe_aligned_ptr_cast( void* f_mem_p )
{
    if (false == is_memory_aligned<void,sizeof(T)>( f_mem_p ) )
        return nullptr;
    
    return reinterpret_cast<T*>( f_mem_p );
}


template <typename T>
inline constexpr const T* safe_aligned_ptr_cast( const void* f_mem_p )
{
    if (false == is_memory_aligned<void,sizeof(T)>( f_mem_p ) )
        return nullptr;
    
    return reinterpret_cast<const T*>( f_mem_p );
}


template <typename T>
inline constexpr int safe_aligned_ptr_cast( T*& f_trgmem_p, void* f_srcmem_p )
{
    if (false == is_memory_aligned<void,sizeof(T)>( f_srcmem_p ) )
    {
        f_trgmem_p = nullptr;
        return -1;
    }

    f_trgmem_p = reinterpret_cast<T*>( f_srcmem_p );
    return 0;
}


template <typename T>
inline constexpr int safe_aligned_ptr_cast( const T*& f_trgmem_p, const void* f_srcmem_p )
{
    if (false == is_memory_aligned<void,sizeof(T)>( f_srcmem_p ) )
    {
        f_trgmem_p = nullptr;
        return -1;
    }

    f_trgmem_p = reinterpret_cast<const T*>( f_srcmem_p );
    return 0;
}

} // namespace smd
