/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cpp_tools.hpp>


/**
 * Support for nontemporal store (stream)
 */
#if defined(X86_ARCH)
    #define NON_TEMPORAL_STORE_SUPPORT
#elif defined(ARM_ARCH)
    #undef NON_TEMPORAL_STORE_SUPPORT
#endif
