/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>
#include <cstddef>
#include <limits>
#include <cstdlib>
#include <memory>


namespace smd 
{


template <typename T, uintptr_t alignment = sizeof(typename smd::select_simd<T>::type)>
inline T* aligned_alloc( std::size_t size ) SIMD_NOEXCEPT;

template <typename T>
inline void aligned_free( T* f_memptr ) SIMD_NOEXCEPT;

//--------------------------------------------------------------------

template <typename T, uintptr_t alignment = sizeof(typename smd::select_simd<T>::type)>
struct aligned_allocator : public std::allocator<T>
{
private:

    typedef std::allocator<T> base_t;

public:

    using typename base_t::pointer;
    using typename base_t::const_pointer;
    using typename base_t::size_type;
    using typename base_t::value_type;

    inline pointer allocate( std::size_t n, const_pointer = 0 ) SIMD_NOEXCEPT;
    inline void deallocate( pointer p, size_type = 0 ) { aligned_free(p); }

    // Very helpefull rebind explanation:
    // https://stackoverflow.com/questions/15224988/custom-allocator-for-stdvectorchar-is-ignored
    template<typename U>
    struct rebind
    {
        typedef aligned_allocator<T,alignment>  other;
    };
};


//--------------------------------------------------------------------

template <typename T>
struct simple_allocator : public std::allocator<T>
{
private:
    typedef std::allocator<T> base_t;

public:

    using typename base_t::pointer;
    using typename base_t::const_pointer;
    using typename base_t::size_type;
    using typename base_t::value_type;

    inline pointer allocate( std::size_t n, const_pointer = 0 ) SIMD_NOEXCEPT;
    inline void deallocate( pointer p, size_type = 0 ) { std::free(p); }

    // Very helpefull rebind explanation:
    // https://stackoverflow.com/questions/15224988/custom-allocator-for-stdvectorchar-is-ignored
    template<typename U>
    struct rebind
    {
        typedef simple_allocator<U>  other;
    };
};


//--------------------------------------------------------------------

template <typename allocator_T>
struct deleter
{
    typedef typename allocator_T::value_type value_type;

    inline void operator()( value_type* ptr ) const
    {
        static allocator_T l_allocator;
        l_allocator.deallocate( ptr );
    }
};


//--------------------------------------------------------------------


template <typename T, uintptr_t alignment = sizeof(typename smd::select_simd<T>::type)>
inline constexpr bool is_memory_aligned( const T* f_mem_p );

//--------------------------------------------------------------------

template <typename T, uintptr_t alignment = sizeof(typename smd::select_simd<T>::type)>
inline T* equalize_alignemnt( const T* f_src_p, T* f_trg_p );

//--------------------------------------------------------------------

template <typename T>
inline constexpr T* safe_aligned_ptr_cast( void* f_mem_p );


template <typename T>
inline constexpr const T* safe_aligned_ptr_cast( const void* f_mem_p );


template <typename T>
inline constexpr int safe_aligned_ptr_cast( T*& f_trgmem_p, void* f_srcmem_p );

template <typename T>
inline constexpr int safe_aligned_ptr_cast( const T*& f_trgmem_p, const void* f_srcmem_p );


} // namespace smd
