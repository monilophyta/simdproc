/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cpp_debug.hpp>


//-------------------------------------------------------------------------------------------------------------------------


/// Assert that EXPRESSION evaluates to true, otherwise raise AssertionFailureException with associated MESSAGE 
/// (which may use C++ stream-style message formatting)
#define simd_assert(EXPRESSION, MESSAGE) cpp_assert( EXPRESSION, MESSAGE )


#ifndef NO_EXPENSIVE_DEBUG
    #define simd_expensive_assert(EXPRESSION, MESSAGE) simd_assert(EXPRESSION, MESSAGE)
#else
    #define simd_expensive_assert(EXPRESSION, MESSAGE)
#endif


// special kind of assert for array bounds
#define simd_bounds_check(EXPRESSION) simd_assert(EXPRESSION, "Out of bounds access")


// simd_check indicates a violated assumption
#define simd_check(EXPRESSION, MESSAGE) simd_assert(EXPRESSION, MESSAGE)

#ifdef CHECK_VECTOR_ALIGNMENT
    #define simd_alignment_check( PTR, MASK ) simd_assert( size_t(0) == (size_t(PTR) & size_t(MASK)), "Unaligned Pointer" )
#else
    #define simd_alignment_check( PTR, MASK )
#endif
//-------------------------------------------------------------------------------------------------------------------------

#define simd_debug_info(MESSAGE) 
//#pragma message( "Using simd binary comparison operator " "OP" )
//#pragma message( "Using simd binary comparison operator " "OP" )

//-------------------------------------------------------------------------------------------------------------------------

#define SIMD_STUB { simd_assert( false, "Implementation Missing" ); }
