/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>


namespace smd
{


//---------------------------------------------------------------------------------------------
/// Forward-Deklarations

template <typename T>
struct select_simd_elem;

template <typename T>
struct select_simd;


template <typename VT>
struct select_simd_array;


template <typename VT>
struct select_simd_matrix;


template <typename VT>
struct select_simd_raligned_matrix;

//---------------------------------------------------------------------------------------------



template <template<typename,typename> class Base_T,
          typename T>
struct lastScion : public Base_T< T, lastScion<Base_T,T> >
{
    // idea see https://stackoverflow.com/questions/213761/what-are-some-uses-of-template-template-parameters

    typedef Base_T< T, lastScion<Base_T,T> > nextToLastScion_t;
    
    using nextToLastScion_t::nextToLastScion_t;
};


//---------------------------------------------------------------------------------------------

template <typename Ts, typename Tt>
struct transfer_const 
    : public std::conditional< std::is_const<Ts>::value,
                               typename std::add_const<Tt>::type, // True
                               Tt>                                // False
{};

//---------------------------------------------------------------------------------------------

namespace __internal
{

template <typename T, 
          template<typename,typename> class TypeComparison,
          typename b32_T, typename i32_T, typename f32_T>
struct select_simd_generic_base
{
    static constexpr inline bool
    isBool() { return TypeComparison<b32_T,T>::value; }

    static constexpr inline bool
    isInt() { return TypeComparison<i32_T,T>::value; }

    static constexpr inline bool
    isFloat() { return TypeComparison<f32_T,T>::value; }

};

}  // namespace __internal


template <typename T, typename b32_T, typename i32_T, typename f32_T>
struct select_simd_generic
    : public std::conditional< 
                std::is_fundamental<T>::value,
                __internal::select_simd_generic_base<T, std::is_same,    b32_T,i32_T,f32_T>,
                __internal::select_simd_generic_base<T, std::is_base_of, b32_T,i32_T,f32_T> >::type
{
    typedef typename std::conditional< 
                std::is_fundamental<T>::value,
                __internal::select_simd_generic_base<T, std::is_same,    b32_T,i32_T,f32_T>,
                __internal::select_simd_generic_base<T, std::is_base_of, b32_T,i32_T,f32_T> >::type base_t;

    using base_t::isBool;
    using base_t::isInt;
    using base_t::isFloat;

    //static_assert( isBool() || isInt() || isFloat(), "Unhandable type" );
};



template <typename Base_T, typename bool_T, typename int32_T, typename float32_T>
struct select_simd_base_dependent : public Base_T
{
    typedef select_simd_base_dependent<Base_T,bool_T,int32_T,float32_T> this_t;
    
    typedef 
    typename std::conditional< this_t::isBool(), bool_T,
        typename std::conditional< this_t::isInt(), int32_T,
            typename std::conditional< this_t::isFloat(), float32_T, 
                std::enable_if<false>
             >::type
        >::type
    >::type  type;
};






} // namespace smd

