/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <functional>
#include "simd_functional.hpp"

namespace smd
{

    namespace __internal {

        template <typename T1, typename T2>
        struct generic_smd_comparison
            : public std::binary_function< T1,T2,
                                           typename
                                           std::conditional< is_simd<T1>::value || is_simd<T2>::value,
                                                             smd::vb32_t,
                                                             bool>::type
                                          >
        {};


    } // __internal


#define BINARY_BOOL_RES_FUNCTOR( NAME, OP ) \
template <typename T1, typename T2> \
struct NAME : public __internal::generic_smd_comparison<T1,T2> \
{ \
    typedef typename __internal::generic_smd_comparison<T1,T2>   base_t;  \
    using typename base_t::first_argument_type;  \
    using typename base_t::second_argument_type;  \
    using typename base_t::result_type; \
    inline result_type operator() ( const first_argument_type& a,  \
                                    const second_argument_type& b) const { return (a OP b); } \
};

BINARY_BOOL_RES_FUNCTOR( equal_to,      == )
BINARY_BOOL_RES_FUNCTOR( not_equal_to,  != )
BINARY_BOOL_RES_FUNCTOR( greater,        > )
BINARY_BOOL_RES_FUNCTOR( less,           < )
BINARY_BOOL_RES_FUNCTOR( greater_equal, >= )
BINARY_BOOL_RES_FUNCTOR( less_equal,    <= )
BINARY_BOOL_RES_FUNCTOR( logical_and,   && )
BINARY_BOOL_RES_FUNCTOR( logical_or,    || )

#undef BINARY_BOOL_RES_FUNCTOR


// ----------------------------------------------------------------------------------


template <typename T>
struct logical_not : public __internal::generic_smd_comparison<T,T>
{
    typedef typename __internal::generic_smd_comparison<T,T>   base_t;
     
    using typename base_t::first_argument_type;
    using typename base_t::result_type;
    
    inline
    result_type operator() ( const first_argument_type& a ) const {return (!a);}
};


// -------------------------------------------------------------------------------------------------------


namespace __internal {

    template <typename T1, typename T2>
    struct generic_type_keeping_operation
        : public std::binary_function< T1, T2, 
                typename std::conditional<false == is_simd<T2>::value, T1, T2 >::type >
    {};
} // __internal



#define UNARY_TYPE_KEEPING_FUNCTOR( NAME, FUNC ) \
template <typename T> \
struct NAME : public __internal::generic_type_keeping_operation<T,T> \
{ \
    typedef typename __internal::generic_type_keeping_operation<T,T>   base_t; \
    using typename base_t::first_argument_type; \
    using typename base_t::result_type; \
    inline result_type operator() ( const first_argument_type& a ) const { return (FUNC (a));} \
}; 

UNARY_TYPE_KEEPING_FUNCTOR( bit_invert, ~ )
UNARY_TYPE_KEEPING_FUNCTOR( absolute, std::abs )
UNARY_TYPE_KEEPING_FUNCTOR( logarithm, std::log )
UNARY_TYPE_KEEPING_FUNCTOR( reciprocal, smd::rcp )
UNARY_TYPE_KEEPING_FUNCTOR( square_root, std::sqrt )
UNARY_TYPE_KEEPING_FUNCTOR( reciprocal_square_root, smd::rsqrt )
UNARY_TYPE_KEEPING_FUNCTOR( rounding_up, std::ceil )
UNARY_TYPE_KEEPING_FUNCTOR( rounding_down, std::floor )
UNARY_TYPE_KEEPING_FUNCTOR( rounding, std::round )
UNARY_TYPE_KEEPING_FUNCTOR( truncation, std::trunc )

#undef UNARY_TYPE_KEEPING_FUNCTOR



template <typename T1, typename T2>
struct shift_left : public __internal::generic_type_keeping_operation<T1,T2>
{
    typedef typename __internal::generic_type_keeping_operation<T1,T2>   base_t;
     
    using typename base_t::first_argument_type;
    using typename base_t::second_argument_type;
    using typename base_t::result_type;
    
    inline
    result_type operator() ( const first_argument_type& a, 
                             const second_argument_type& b) const {return (a << b);}
};


template <typename T1, typename T2>
struct shift_right : public __internal::generic_type_keeping_operation<T1,T2>
{
    typedef typename __internal::generic_type_keeping_operation<T1,T2>   base_t;
     
    using typename base_t::first_argument_type;
    using typename base_t::second_argument_type;
    using typename base_t::result_type;
    
    inline
    result_type operator() ( const first_argument_type& a, 
                             const second_argument_type& b) const {return (a >> b);}
};


template <typename T1, typename T2>
struct maximum : public __internal::generic_type_keeping_operation<T1,T2>
{
    typedef typename __internal::generic_type_keeping_operation<T1,T2>   base_t;
     
    using typename base_t::first_argument_type;
    using typename base_t::second_argument_type;
    using typename base_t::result_type;
    
    inline
    result_type operator() ( const first_argument_type& a, 
                             const second_argument_type& b) const {return std::max(a, b);}
};


template <typename T1, typename T2>
struct minimum : public __internal::generic_type_keeping_operation<T1,T2>
{
    typedef typename __internal::generic_type_keeping_operation<T1,T2>   base_t;
     
    using typename base_t::first_argument_type;
    using typename base_t::second_argument_type;
    using typename base_t::result_type;
    
    inline
    result_type operator() ( const first_argument_type& a, 
                             const second_argument_type& b) const {return std::min(a, b);}
};



} // namespace smd


