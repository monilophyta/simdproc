/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>
#include <cfloat>


#include "simd_integer.hpp"
#include "simd_float.hpp"


namespace smd
{
namespace nhw
{

typedef int32_t    v4si __attribute__ ((vector_size (16)));
typedef float32_t  v4sf __attribute__ ((vector_size (16)));


union v128
{
    v4si    si;
    v4sf    sf;
    
    inline v128() = default;
    inline v128( const v128& ) = default;
    inline v128( v128&& ) = default;
    inline v128& operator=( const v128& ) = default;
    inline v128& operator=( v128&& ) = default;

    inline explicit v128( const v4si &init )
        : si( init )
    {}
    

    inline explicit v128( const v4sf &init )
        : sf( init )
    {}


    inline v128( int32_t si1, int32_t si2, int32_t si3, int32_t si4 )
        : si{ si1, si2, si3, si4 }
    {}


    inline v128( float32_t sf1, float32_t sf2, float32_t sf3, float32_t sf4 )
        : sf{ sf1, sf2, sf3, sf4 }
    {}
};
static_assert( sizeof(v128) == 16 );
static_assert( true == std::is_trivially_default_constructible<v128>::value );


struct nhw_b32_vt;
struct nhw_i32_vt;
struct nhw_f32_vt;


/*
 * No explicit hardware bool type
 */ 

struct nhw_b32_vt 
    : public vt_bool_base< v128, nhw_b32_vt >
{
    inline nhw_b32_vt() = default;
    inline nhw_b32_vt( const nhw_b32_vt& ) = default;
    inline nhw_b32_vt( nhw_b32_vt&& ) = default;
    inline nhw_b32_vt& operator=( const nhw_b32_vt& ) = default;
    inline nhw_b32_vt& operator=( nhw_b32_vt&& ) = default;


    explicit inline nhw_b32_vt( const v128 &f_initv )
        : vt_bool_base( f_initv )
    {}

    explicit inline nhw_b32_vt( bool f_initv_b )
        : vt_bool_base( v128( bool_to_i32( f_initv_b ), bool_to_i32( f_initv_b ), bool_to_i32( f_initv_b ), bool_to_i32( f_initv_b ) ) )
    {}
};


/*
 * No explicit hardware int32 type
 */ 


struct nhw_i32_vt
    : public vt_i32< v128,
                     nhw_b32_vt,
                     nhw_i32_vt >
{
    inline nhw_i32_vt() = default;
    inline nhw_i32_vt( const nhw_i32_vt& ) = default;
    inline nhw_i32_vt( nhw_i32_vt&& ) = default;
    inline nhw_i32_vt& operator=( const nhw_i32_vt& ) = default;
    inline nhw_i32_vt& operator=( nhw_i32_vt&& ) = default;


    explicit inline nhw_i32_vt( const v128 &f_initv )
        : vt_i32( f_initv )
    {}

    explicit inline nhw_i32_vt( int32_t f_initv_i32 )
        : vt_i32( v128( f_initv_i32, f_initv_i32, f_initv_i32, f_initv_i32 ) )
    {}


    explicit inline nhw_i32_vt( const nhw_f32_vt& other );
};


/*
 * No explicit hardware float32 type
 */ 



struct nhw_f32_vt 
    : public vt_f32< v128, 
                     nhw_b32_vt,
                     nhw_f32_vt >
{
    inline nhw_f32_vt() = default;
    inline nhw_f32_vt( const nhw_f32_vt& ) = default;
    inline nhw_f32_vt( nhw_f32_vt&& ) = default;
    inline nhw_f32_vt& operator=( const nhw_f32_vt& ) = default;
    inline nhw_f32_vt& operator=( nhw_f32_vt&& ) = default;


    explicit nhw_f32_vt( const v128 &f_initv )
        : vt_f32( f_initv )
    {}

    explicit nhw_f32_vt( float32_t f_initv_f32 )
        : vt_f32( v128( f_initv_f32, f_initv_f32, f_initv_f32, f_initv_f32 ) )
    {}

    inline nhw_f32_vt( const nhw_i32_vt& other );
};

}  // namespace nhw
}  // namespace smd


namespace std
{

inline smd::nhw::nhw_i32_vt max( const smd::nhw::nhw_i32_vt &x, const smd::nhw::nhw_i32_vt &y ) { return smd::max(x,y); }
inline smd::nhw::nhw_i32_vt min( const smd::nhw::nhw_i32_vt &x, const smd::nhw::nhw_i32_vt &y ) { return smd::min(x,y); }

inline smd::nhw::nhw_f32_vt max( const smd::nhw::nhw_f32_vt &x, const smd::nhw::nhw_f32_vt &y ) { return smd::max(x,y); }
inline smd::nhw::nhw_f32_vt min( const smd::nhw::nhw_f32_vt &x, const smd::nhw::nhw_f32_vt &y ) { return smd::min(x,y); }

} // namespace std

//----------------------------------------------------------------------------------


