/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_arithmetic.hpp"
#include <cmath>
#include <algorithm>


namespace smd
{

//----------------------------------------------------------------------------------



template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t sum( const vt_num_base<Ts...>& x )
{
    typedef typename vt_num_base<Ts...>::elem_t elem_t;
   
    return std::accumulate( x.cbegin(), x.cend(), elem_t(0), std::plus<elem_t>() );
}


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t prod( const vt_num_base<Ts...>& x )
{
    typedef typename vt_num_base<Ts...>::elem_t elem_t;
   
    return std::accumulate( x.cbegin(), x.cend(), elem_t(1), std::multiplies<elem_t>() );
}


//----------------------------------------------------------------------------------



template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
safe_sumXlogX( T x )
{
    return nuk::safe_XlogX( x );
}


template <class... Ts>
inline 
typename vt_f32<Ts...>::elem_t safe_sumXlogX( const vt_f32<Ts...>& x )
{
    typedef typename vt_f32<Ts...>::elem_t elem_t;
   
    return std::accumulate( x.cbegin(), x.cend(), elem_t(0), nuk::safe_sumXlogXkernel<elem_t>() );
}

//----------------------------------------------------------------------------------

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
rcp( T x )
{
    return (1.f / x);
}


template <class... Ts>
inline
typename smd::vt_f32<Ts...>::derived_t
rcp( const smd::vt_f32<Ts...> &x )
{
    typedef typename smd::vt_f32<Ts...>::derived_t derived_t;
    derived_t y;
    typedef typename derived_t::elem_t elem_t;

    std::transform( x.cbegin(), x.cend(), y.begin(), [](elem_t v) -> elem_t { return (1.f / v);} );
    return y;
}


//----------------------------------------------------------------------------------

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
rsqrt( T x )
{
    return (1.f / std::sqrt(x));
}


template <class... Ts>
inline
typename smd::vt_f32<Ts...>::derived_t
rsqrt( const smd::vt_f32<Ts...> &x )
{
    typedef typename smd::vt_f32<Ts...>::derived_t derived_t;
    derived_t y;
    typedef typename derived_t::elem_t elem_t;

    std::transform( x.cbegin(), x.cend(), y.begin(), [](elem_t v) -> elem_t { return (1.f / std::sqrt(v));} );
    return y;
}


//----------------------------------------------------------------------------------


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t min( const vt_num_base<Ts...>& x )
{
    return ( *std::min_element( x.cbegin(), x.cend() ) );
}


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t max( const vt_num_base<Ts...>& x )
{
    return ( *std::max_element( x.cbegin(), x.cend() ) );
}


//----------------------------------------------------------------------------------

template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t max( const smd::vt_num_base<Ts...> &x,
                                                 const smd::vt_num_base<Ts...> &y )
{
    typedef typename smd::vt_num_base<Ts...>::elem_t    elem_t;
    typedef typename smd::vt_num_base<Ts...>::derived_t derived_t;
    derived_t z;

    auto maxkernel = []( elem_t x, elem_t y ) -> elem_t { return std::max(x,y); };

    std::transform( x.cbegin(), x.cend(),
                    y.cbegin(),
                    z.begin(),
                    maxkernel );
    
    return z;
}


template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t min( const smd::vt_num_base<Ts...> &x,
                                                 const smd::vt_num_base<Ts...> &y )
{
    typedef typename smd::vt_num_base<Ts...>::elem_t    elem_t;
    typedef typename smd::vt_num_base<Ts...>::derived_t derived_t;
    derived_t z;

    auto minkernel = []( elem_t x, elem_t y ) -> elem_t { return std::min(x,y); };

    std::transform( x.cbegin(), x.cend(),
                    y.cbegin(),
                    z.begin(),
                    minkernel );
    
    return z;
}

//----------------------------------------------------------------------------------


template <typename T1, typename T2>
inline constexpr 
typename std::enable_if<std::is_signed<T1>::value,T1>::type 
copyminus( T1 f_farget, const T2& f_source )
{
    return ( ( ( f_source < T2(0) ) && ( f_farget > T1(0) ) )
               ? -f_farget
               :  f_farget );
}


template <class... T1s, class... T2s>
inline constexpr
typename vt_num_base<T1s...>::derived_t
copyminus( const vt_num_base<T1s...>& f_target, const vt_num_base<T2s...>& f_source )
{
    typedef typename vt_num_base<T1s...>::derived_t derivedt_t;
    typedef typename vt_num_base<T2s...>::derived_t deriveds_t;
    typedef typename vt_num_base<T1s...>::bool_vt   bool_vt;

    const bool_vt l_negative_mask =     (f_target >= derivedt_t(0)) 
                                     && ( f_source < deriveds_t(0) );
    
    return blend( l_negative_mask, -f_target, f_target );
}


//----------------------------------------------------------------------------------




} // namespace smd



namespace std
{



//----------------------------------------------------------------------------------

#if 0
inline constexpr
int32_t abs( int32_t f_value_i32 )
{
    return ( ( f_value_i32 < 0 )
              ? -f_value_i32
              : f_value_i32 );
}
#endif


template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t 
abs( const smd::vt_num_base<Ts...>& x )
{
    typedef typename smd::vt_num_base<Ts...>::derived_t  derived_t;
    typedef typename                  derived_t::elem_t  elem_t;
    
    derived_t res;

    std::transform( x.cbegin(), x.cend(),
                    res.begin(),
                    []( elem_t x ) { return std::abs(x); } );
    
    return res;
}


//----------------------------------------------------------------------------------


#define VT_UNARY_STD_FLOAT_FUNCTION( FUNC ) \
template <class... Ts> \
inline typename smd::vt_f32< Ts...>::derived_t \
FUNC ( const smd::vt_f32<Ts...> &x ) \
{ \
    typedef typename smd::vt_f32<Ts...>::derived_t derived_t; \
    derived_t y; \
    typedef typename derived_t::elem_t elem_t; \
    std::transform( x.cbegin(), x.cend(), y.begin(), [](elem_t v) -> elem_t { return std::  FUNC (v);}  ); \
    return y; \
}

VT_UNARY_STD_FLOAT_FUNCTION( log )
VT_UNARY_STD_FLOAT_FUNCTION( log10 )
VT_UNARY_STD_FLOAT_FUNCTION( log2 )
VT_UNARY_STD_FLOAT_FUNCTION( sqrt )
VT_UNARY_STD_FLOAT_FUNCTION( ceil )
VT_UNARY_STD_FLOAT_FUNCTION( floor )
VT_UNARY_STD_FLOAT_FUNCTION( round )
VT_UNARY_STD_FLOAT_FUNCTION( trunc )

#undef VT_UNARY_STD_FLOAT_FUNCTION


//----------------------------------------------------------------------------------


}  // namespace std

