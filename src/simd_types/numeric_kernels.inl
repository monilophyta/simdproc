/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "numeric_kernels.hpp"


#include "simd_numeric_limits.hpp"
#include "simd_arithmetic.hpp"
#include <cmath>



namespace nuk 
{


template <typename ST>
inline ST safe_XlogX( const ST& val )
{
    ST res = val * std::log( std::max( val, std::numeric_limits<ST>::min() ) );

    simd_assert( smd::all( std::isfinite(res) ), "Unexpected behaviour of safe_XlogX" );
    return res;
}



template <typename ST>
struct safe_sumXlogXkernel
{
    inline ST operator() ( const ST& lastv, const ST& newv ) const
    {
        return ( lastv + safe_XlogX( newv ) );
    }
};


} // namespace nuk

