/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "simd_base.hpp"
#include "simd_type_traits2.hpp"


namespace smd
{

// ------------------------------------------------------------------------------------------------

// generic type adapter for stream storage
template <class... Ts>
inline void nontemporal_store( const vt_base<Ts...> &val, vt_base<Ts...> *f_stream_p )
{ 
    typedef typename vt_base<Ts...>::derived_t derived_t;
    val.derived().nontemporal_store( static_cast<derived_t*>(f_stream_p) ); 
}


/// for compaitibility with other types
template <typename T, 
          typename = typename std::enable_if< !is_simd<T>::value >::type >
inline void nontemporal_store( const T &val, T *f_stream_p )
{
#if defined(__clang__)
    __builtin_nontemporal_store( val, f_stream_p );
#else
    (*f_stream_p) = val;
#endif
}

// ------------------------------------------------------------------------------------------------

template <typename T,
          typename = typename std::enable_if< !is_simd<T>::value >::type >
inline void set_zero( T& val )
{
    static_assert( !(is_simd<T>::value), "wrong function selected" );
    val = static_cast<T>(0);
}


template <class... Ts>
inline void set_zero( vt_base< vt_param<Ts...> >& val )
{
    typedef typename vt_base< vt_param<Ts...> >::derived_t derived_t;
    val = std::move(derived_t::zero());
}


} // namespace smd


