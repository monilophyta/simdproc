/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>



namespace smd
{

// ------------------------------------------------------------------------------------------------------------


template <typename T>
struct select_simd_elem
: public smd::transfer_const
    < 
        T,
        typename select_simd_base_dependent
        <
            select_simd_generic
            < 
                typename std::remove_const<T>::type,
                vb32_t,
                vi32_t,
                vf32_t
            >, 
            bool32_t,
            int32_t,
            float32_t
        >::type
    >
{};

static_assert( true == std::is_same< bool32_t, typename select_simd_elem<vb32_t>::type >::value );
static_assert( true == std::is_same< int32_t, typename select_simd_elem<vi32_t>::type >::value );
static_assert( true == std::is_same< float32_t, typename select_simd_elem<vf32_t>::type >::value );

static_assert( false == std::is_same< int32_t, typename select_simd_elem<vb32_t>::type >::value );
static_assert( false == std::is_same< int32_t, typename select_simd_elem<vf32_t>::type >::value );
static_assert( false == std::is_same< float32_t, typename select_simd_elem<vi32_t>::type >::value );
static_assert( false == std::is_same< float32_t, typename select_simd_elem<vb32_t>::type >::value );
static_assert( false == std::is_same< bool32_t, typename select_simd_elem<vi32_t>::type >::value );
static_assert( false == std::is_same< bool32_t, typename select_simd_elem<vf32_t>::type >::value );

// ------------------------------------------------------------------------------------------------------------


template <typename T>
struct select_simd 
:   public std::conditional
    <
        is_simd<T>::value,           // check IF T is already simd type
        T,                           // THEN keep the type
        typename smd::transfer_const // ELSE construct a new type
        < 
            T,
            typename select_simd_base_dependent
            <
                select_simd_generic
                < 
                    typename toBool32< 
                            typename std::remove_const<T>::type 
                        >::type,
                    bool32_t,
                    int32_t,
                    float32_t
                >, 
                vb32_t,
                vi32_t,
                vf32_t
            >::type
        >::type
    >
{};


static_assert( true == std::is_same< vb32_t, typename select_simd<bool>::type >::value );
static_assert( true == std::is_same< vb32_t, typename select_simd<bool32_t>::type >::value );
static_assert( true == std::is_same< vi32_t, typename select_simd<int32_t>::type >::value );
static_assert( true == std::is_same< vf32_t, typename select_simd<float32_t>::type >::value );

static_assert( true == std::is_same< const vb32_t, typename select_simd<const bool>::type >::value );
static_assert( true == std::is_same< const vb32_t, typename select_simd<const bool32_t>::type >::value );
static_assert( true == std::is_same< const vi32_t, typename select_simd<const int32_t>::type >::value );
static_assert( true == std::is_same< const vf32_t, typename select_simd<const float32_t>::type >::value );

static_assert( false == std::is_same< vi32_t, typename select_simd<bool32_t>::type >::value );

// ------------------------------------------------------------------------------------------------------------


template <typename T>
struct select_noconst_simd 
:   public std::conditional
    <
        is_simd<T>::value,                      // check IF T is already simd type
        typename std::remove_const<T>::type,    // THEN keep the type
        typename select_simd_base_dependent     // ELSE construct a new type
        <
            select_simd_generic
            < 
                typename std::remove_const<T>::type,
                bool32_t,
                int32_t,
                float32_t
            >, 
            vb32_t,
            vi32_t,
            vf32_t
        >::type
    >
{};

static_assert( true == std::is_same< vb32_t, typename select_noconst_simd<bool32_t>::type >::value );
static_assert( true == std::is_same< vi32_t, typename select_noconst_simd<int32_t>::type >::value );
static_assert( true == std::is_same< vf32_t, typename select_noconst_simd<float32_t>::type >::value );

static_assert( false == std::is_same< vi32_t, typename select_noconst_simd<bool32_t>::type >::value );

// ------------------------------------------------------------------------------------------------------------


template <typename T, typename FT>
struct select
    : public std::conditional< 
                    is_simd<T>::value,
                    typename select_simd<FT>::type,
                    FT>
{};


template <typename T>
struct comparison : public smd::select<T,bool32_t>
{};


template <typename T>
struct floating : public smd::select<T,float32_t>
{};


template <typename T>
struct integer : public smd::select<T,int32_t>
{};


// ------------------------------------------------------------------------------------------------------------

template <typename T>
struct is_simd_elem
    : public std::conditional<
        (std::is_arithmetic<T>::value || std::is_same< typename std::remove_cv<T>::type, bool32_t>::value) && 
            std::is_same< T, typename select_simd_elem< typename select_simd<T>::type >::type >::value,
        std::true_type,
        std::false_type
    >::type
{};

static_assert( true == is_simd_elem<bool32_t>::value );
static_assert( true == is_simd_elem<int32_t>::value );
static_assert( true == is_simd_elem<float32_t>::value );

static_assert( true == is_simd_elem<const bool32_t>::value );
static_assert( true == is_simd_elem<const int32_t>::value );
static_assert( true == is_simd_elem<const float32_t>::value );

static_assert( false == is_simd_elem<double>::value );
static_assert( false == is_simd_elem<vf32_t>::value );
static_assert( false == is_simd_elem<int32_t*>::value );

} // namespace smd
