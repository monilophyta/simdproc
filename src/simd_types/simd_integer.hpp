/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_numeric.hpp"
#include "simd_bool.hpp"


namespace smd
{



template <typename vect_t>
struct vt_i32_access_t
{
    typedef decltype( vect_t::si )  raw_vect_t;
    
    inline const raw_vect_t& operator()( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return val.si;
    }

    inline raw_vect_t& operator()( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return val.si;
    }

    inline const int32_t* elem_begin( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<const int32_t*>(&(val.si));
    }

    inline int32_t* elem_begin( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<int32_t*>(&(val.si));
    }
};



template <typename vect_t,
          typename bool_vt,
          typename derived_t>
struct vt_i32 : public vt_num_base< vt_param< vt_i32_access_t<vect_t>,
                                              vect_t, 
                                              bool_vt,
                                              derived_t > >
{
private:

    typedef vt_num_base< vt_param< vt_i32_access_t<vect_t>,
                                   vect_t, 
                                   bool_vt,
                                   derived_t > >  base_t;
    
    typedef vt_i32<vect_t,bool_vt,derived_t>      this_t;

public:

    // inherit constructors
    using base_t::base_t;

    using typename base_t::elem_t;
    using base_t::val;
    using base_t::derived;

    inline vt_i32() = default;
    
    template <class... Ts>
    explicit inline vt_i32( const vt_f32<vect_t,Ts...> &other );

    inline derived_t operator~() const;

    inline derived_t operator%( const this_t &other ) const;
    inline derived_t operator<<( const this_t &other ) const;
    inline derived_t operator>>( const this_t &other ) const;
    
    inline const derived_t& operator%=( const this_t &other );
    inline const derived_t& operator<<=( const this_t &other );
    inline const derived_t& operator>>=( const this_t &other );
    
    // @gcc: don't forget we have already some overloaded operators
    using base_t::operator&;
    using base_t::operator|;
    using base_t::operator^;
    using base_t::operator&=;
    using base_t::operator|=;
    using base_t::operator^=;

    inline derived_t operator%( elem_t other ) const;
    inline derived_t operator&( elem_t other ) const;
    inline derived_t operator|( elem_t other ) const;
    inline derived_t operator^( elem_t other ) const;
    inline derived_t operator<<( elem_t other ) const;
    inline derived_t operator>>( elem_t other ) const;

    inline const derived_t& operator%=( elem_t other );
    inline const derived_t& operator&=( elem_t other );
    inline const derived_t& operator|=( elem_t other );
    inline const derived_t& operator^=( elem_t other );
    inline const derived_t& operator<<=( elem_t other );
    inline const derived_t& operator>>=( elem_t other );

    
    inline void accumulate_boolbit( const bool_vt &bit );


    template <typename array_elem_t>
    inline typename select_simd<array_elem_t>::type 
    gather( const array_elem_t* f_from_a ) const;
};


#define VT_BINARY_NON_SYMMETRIC_ELEM_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t> \
inline derived_t \
operator OP ( typename derived_t::elem_t other, const vt_i32<vect_t,bool_vt,derived_t>& f_simdVal ) \
{ \
    return (derived_t(other) OP f_simdVal); \
}

VT_BINARY_NON_SYMMETRIC_ELEM_OP( % )
VT_BINARY_NON_SYMMETRIC_ELEM_OP( << )
VT_BINARY_NON_SYMMETRIC_ELEM_OP( >> )

#undef VT_BINARY_NON_SYMMETRIC_ELEM_OP



#define VT_BINARY_SYMMETRIC_ELEM_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t> \
inline derived_t \
operator OP ( typename derived_t::elem_t other, const vt_i32<vect_t,bool_vt,derived_t>& f_simdVal ) \
{ \
    return (f_simdVal OP other); \
}

VT_BINARY_SYMMETRIC_ELEM_OP( & )
VT_BINARY_SYMMETRIC_ELEM_OP( | )
VT_BINARY_SYMMETRIC_ELEM_OP( ^ )

#undef VT_BINARY_SYMMETRIC_ELEM_OP



}  // namespace smd

