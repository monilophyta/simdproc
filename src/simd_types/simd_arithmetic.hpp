/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include "simd_numeric.hpp"
#include "simd_float.hpp"
#include "simd_integer.hpp"



namespace smd
{

// for compatibility with integral types
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value,T>::type 
sum( T v ) { return v; }


// for compatibility with integral types
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value,T>::type 
prod( T v ) { return v; }


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t sum( const vt_num_base<Ts...>& x );


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t prod( const vt_num_base<Ts...>& x );

//----------------------------------------------------------------------------------

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
safe_sumXlogX( T x );


template <class... Ts>
inline 
typename vt_f32<Ts...>::elem_t 
safe_sumXlogX( const vt_f32<Ts...>& x );

//----------------------------------------------------------------------------------

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
rcp( T x );


template <class... Ts>
inline
typename smd::vt_f32<Ts...>::derived_t
rcp( const smd::vt_f32<Ts...> &x );


//----------------------------------------------------------------------------------

template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value,T>::type 
rsqrt( T x );


template <class... Ts>
inline
typename smd::vt_f32<Ts...>::derived_t
rsqrt( const smd::vt_f32<Ts...> &x );

//----------------------------------------------------------------------------------


// for compatibility with arithmetic types
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value,T>::type 
max( T v ) { return v; }

// for compatibility with arithmetic types
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value,T>::type 
min( T v ) { return v; }


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t min( const vt_num_base<Ts...>& x );


template <class... Ts>
inline 
typename vt_num_base<Ts...>::elem_t max( const vt_num_base<Ts...>& x );

//----------------------------------------------------------------------------------


template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t max( const smd::vt_num_base<Ts...> &x,
                                                 const smd::vt_num_base<Ts...> &y );


template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t min( const smd::vt_num_base<Ts...> &x,
                                                 const smd::vt_num_base<Ts...> &y );

//----------------------------------------------------------------------------------


template <typename T1, typename T2>
inline constexpr 
typename std::enable_if<std::is_signed<T1>::value,T1>::type 
copyminus( T1 f_farget, const T2& f_source );



template <class... T1s, class... T2s>
inline constexpr
typename vt_num_base<T1s...>::derived_t
copyminus( const vt_num_base<T1s...>& f_farget, const vt_num_base<T2s...>& f_source );


//----------------------------------------------------------------------------------


} // namespace smd




namespace std
{


//----------------------------------------------------------------------------------


//inline constexpr
//int32_t abs( int32_t f_value_i32 );


template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t 
abs( const smd::vt_num_base<Ts...>& x );



//----------------------------------------------------------------------------------


template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
log( const smd::vt_f32<Ts...> &x );


template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
log10( const smd::vt_f32<Ts...> &x );


template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
log2( const smd::vt_f32<Ts...> &x );


template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
sqrt( const smd::vt_f32<Ts...> &x );

//----------------------------------------------------------------------------------
// Unfortunatedly g++ does not select these generic max/min function but tries to use its own one
// This causes a failed compilation
// So every simd type should implement its own version of min/max functionas
template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t 
max( const smd::vt_num_base<Ts...> &x, const smd::vt_num_base<Ts...> &y ) { return smd::max( x, y ); }

template <class... Ts>
inline
typename smd::vt_num_base<Ts...>::derived_t
min( const smd::vt_num_base<Ts...> &x, const smd::vt_num_base<Ts...> &y ) { return smd::min( x, y ); }


template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
ceil( const smd::vt_f32<Ts...> &x );

template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
floor( const smd::vt_f32<Ts...> &x );

template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
round( const smd::vt_f32<Ts...> &x );

template <class... Ts>
inline typename smd::vt_f32<Ts...>::derived_t
trunc( const smd::vt_f32<Ts...> &x );


//----------------------------------------------------------------------------------

} //namespace std
