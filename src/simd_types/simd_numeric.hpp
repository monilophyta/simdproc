/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>


namespace smd
{


template <class... Ts>
struct vt_num_base : public vt_base< Ts... >
{
private:
    typedef vt_base< Ts... >            base_t;
    typedef vt_num_base< Ts... >        this_t;

public:
    // --------------------------
    using base_t::base_t;    // Inherit constructor
    using base_t::val;
    using typename base_t::derived_t;
    using typename base_t::elem_t;
    using typename base_t::op_access_t;
    using typename base_t::bool_vt;

    using base_t::begin;
    using base_t::end;
    using base_t::cbegin;
    using base_t::cend;
    using base_t::size;

    inline vt_num_base() = default;

    inline std::string to_string () const;

    inline void fill( elem_t f_val );

    // Common operators
    // see https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html#Vector-Extensions

    inline const derived_t& operator+() const;
    inline derived_t operator-() const;

    inline derived_t operator+( const this_t &other ) const;
    inline derived_t operator-( const this_t &other ) const;
    inline derived_t operator*( const this_t &other ) const;
    inline derived_t operator/( const this_t &other ) const;

    inline const derived_t& operator+=( const this_t &other );
    inline const derived_t& operator-=( const this_t &other );
    inline const derived_t& operator*=( const this_t &other );
    inline const derived_t& operator/=( const this_t &other );


    inline derived_t operator+( elem_t other ) const;
    inline derived_t operator-( elem_t other ) const;
    inline derived_t operator*( elem_t other ) const;
    inline derived_t operator/( elem_t other ) const;

    inline const derived_t& operator+=( elem_t other );
    inline const derived_t& operator-=( elem_t other );
    inline const derived_t& operator*=( elem_t other );
    inline const derived_t& operator/=( elem_t other );

    // comparisons
    inline bool_vt operator==( const this_t &other ) const;
    inline bool_vt operator!=( const this_t &other ) const;
    inline bool_vt operator<( const this_t &other ) const;
    inline bool_vt operator<=( const this_t &other ) const;
    inline bool_vt operator>( const this_t &other ) const;
    inline bool_vt operator>=( const this_t &other ) const;

    inline bool_vt operator==( elem_t other ) const;
    inline bool_vt operator!=( elem_t other ) const;
    inline bool_vt operator<( elem_t other ) const;
    inline bool_vt operator<=( elem_t other ) const;
    inline bool_vt operator>( elem_t other ) const;
    inline bool_vt operator>=( elem_t other ) const;
};


//-------------------------------------------------------------------------------------------------
// Symmetric Operations with elementary types

#define VT_BINARY_SYMMETRIC_ELEM_OP( OP ) \
template <class... Ts> \
inline auto \
operator OP ( typename vt_num_base<Ts...>::elem_t other, const vt_num_base<Ts...>& f_simdVal ) \
-> decltype(f_simdVal OP other) \
{ \
    return (f_simdVal OP other); \
}

VT_BINARY_SYMMETRIC_ELEM_OP( == )
VT_BINARY_SYMMETRIC_ELEM_OP( != )
VT_BINARY_SYMMETRIC_ELEM_OP( + )
VT_BINARY_SYMMETRIC_ELEM_OP( * )

#undef VT_BINARY_SYMMETRIC_ELEM_OP

// -----------------------------------------------------------------------------------------------

#define VT_BINARY_EXCHANGABLE_ELEM_OP( OP, RP ) \
template <class... Ts> \
inline auto \
operator OP ( typename vt_num_base<Ts...>::elem_t other, const vt_num_base<Ts...>& f_simdVal ) \
-> decltype(f_simdVal RP other) \
{ \
    return (f_simdVal RP other); \
}

VT_BINARY_EXCHANGABLE_ELEM_OP( <, > )
VT_BINARY_EXCHANGABLE_ELEM_OP( >, < )
VT_BINARY_EXCHANGABLE_ELEM_OP( <=, >= )
VT_BINARY_EXCHANGABLE_ELEM_OP( >=, <= )

#undef VT_BINARY_EXCHANGABLE_ELEM_OP

// -----------------------------------------------------------------------------------------------



template <class... Ts>
inline auto
operator- ( typename vt_num_base<Ts...>::elem_t other, const vt_num_base<Ts...>& f_simdVal )
-> decltype( f_simdVal - other )
{
    typedef typename vt_num_base<Ts...>::derived_t derived_t;
    return (derived_t(other) - f_simdVal);
}


template <class... Ts>
inline auto
operator/ ( typename vt_num_base<Ts...>::elem_t other, const vt_num_base<Ts...>& f_simdVal )
-> decltype( f_simdVal - other )
{
    typedef typename vt_num_base<Ts...>::derived_t derived_t;
    return (derived_t(other) / f_simdVal);
}


//-------------------------------------------------------------------------------------------------

} // namespace smd


