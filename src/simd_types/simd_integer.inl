/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_integer.hpp"


namespace smd
{

// -------------------------------------------------------------------------------------

template <typename vect_t,typename bool_vt,typename derived_t>
template <class... Ts>
inline vt_i32<vect_t,bool_vt,derived_t>::vt_i32( const vt_f32<vect_t,Ts...> &other )
    : base_t()
{
    std::transform( other.cbegin(), other.cend(), 
                    this->begin(),
                    []( float32_t x ) -> int32_t { return static_cast<int32_t>(x); });
}

// -------------------------------------------------------------------------------------

template <typename vect_t,typename bool_vt,typename derived_t>
inline derived_t vt_i32<vect_t,bool_vt,derived_t>::operator~ () const
{
    derived_t res;
    res.val.si = ~val.si;
    return res;
}


#define VT_BINARY_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t>  \
inline derived_t vt_i32<vect_t,bool_vt,derived_t>::operator OP ( const this_t &other ) const \
{ \
    derived_t res; \
    res.val.si = val.si OP other.val.si; \
    return res; \
}

VT_BINARY_OP( % )
VT_BINARY_OP( << )
VT_BINARY_OP( >> )

#undef VT_BINARY_OP




#define VT_BINARY_ASSIGNMENT_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t>  \
inline const derived_t& vt_i32<vect_t,bool_vt,derived_t>::operator OP##= ( const this_t &other ) \
{ \
    val.si OP##= other.val.si; \
    return this->derived(); \
}

VT_BINARY_ASSIGNMENT_OP( % )
VT_BINARY_ASSIGNMENT_OP( >> )
VT_BINARY_ASSIGNMENT_OP( << )

#undef VT_BINARY_ASSIGNMENT_OP



#define VT_BINARY_SINGLE_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t>  \
inline derived_t vt_i32<vect_t,bool_vt,derived_t>::operator OP ( elem_t other ) const \
{ \
    derived_t res; \
    res.val.si = val.si OP other; \
    return res; \
}

VT_BINARY_SINGLE_OP( % )
VT_BINARY_SINGLE_OP( & )
VT_BINARY_SINGLE_OP( | )
VT_BINARY_SINGLE_OP( ^ )
VT_BINARY_SINGLE_OP( >> )
VT_BINARY_SINGLE_OP( << )

#undef VT_BINARY_SINGLE_OP




#define VT_BINARY_ASSIGNMENT_SINGLE_OP( OP ) \
template <typename vect_t,typename bool_vt,typename derived_t>  \
inline const derived_t& vt_i32<vect_t,bool_vt,derived_t>::operator OP##= ( elem_t other ) \
{ \
    val.si OP##= other; \
    return static_cast<derived_t&>(*this); \
}

VT_BINARY_ASSIGNMENT_SINGLE_OP( % )
VT_BINARY_ASSIGNMENT_SINGLE_OP( & )
VT_BINARY_ASSIGNMENT_SINGLE_OP( | )
VT_BINARY_ASSIGNMENT_SINGLE_OP( ^ )
VT_BINARY_ASSIGNMENT_SINGLE_OP( >> )
VT_BINARY_ASSIGNMENT_SINGLE_OP( << )

#undef VT_BINARY_ASSIGNMENT_SINGLE_OP





template <typename vect_t,
          typename bool_vt,
          typename derived_t>
inline void vt_i32<vect_t,bool_vt,derived_t>::accumulate_boolbit( const bool_vt &bit )
{
    *this <<= derived_t(1);
    val.si |= (bit.val.si) ? derived_t(1).val.si : derived_t(0).val.si;
    //*this |= ( (bit.val.si) ? derived_t(1) : derived_t(0) );
}


template <typename vect_t,
          typename bool_vt,
          typename derived_t>
template <typename array_elem_t>
inline typename select_simd<array_elem_t>::type 
vt_i32<vect_t,bool_vt,derived_t>::gather( const array_elem_t* f_from_a ) const
{
    typedef typename select_simd<array_elem_t>::type  simd_elem_t;
    simd_elem_t res;

    auto kernel = [f_from_a]( elem_t idx ) -> array_elem_t {
        return f_from_a[idx]; };

    std::transform( derived().cbegin(), derived().cend(),
                    res.begin(),
                    kernel );
    
    return res;
}



}  // namespace smd

