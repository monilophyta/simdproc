/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_numeric.hpp"
#include <sstream>
#include <algorithm>
#include <numeric>
#include <functional>
#include <cstring>



namespace smd
{


//----------------------------------------------------------------------------------


template <class... Ts>
inline std::string vt_num_base<Ts...>::to_string () const
{
    std::ostringstream out;
    
    auto it = begin();

    out << typeid(derived_t).name() << "( " << *it;
    ++it;

    for ( ; end() != it; ++it )
    {
        out << ", " << *it;
    }

    out << " )";

    return out.str();
}


template <class... Ts>
inline void vt_num_base<Ts...>::fill( elem_t f_val )
{
    if (elem_t(0) == f_val)
    {
        std::memset( begin(), 0, size() );
    }
    else
    {
        for (auto it = begin(); it != end(); ++it)
        {
            *it = f_val;
        }
    }
}


//----------------------------------------------------------------------------------

template <class... Ts>
inline auto vt_num_base<Ts...>::operator-() const -> derived_t
{
    op_access_t o;
    derived_t res;
    o(res.val) = -o(val);
    return res;
}

template <class... Ts>
inline auto vt_num_base<Ts...>::operator+() const -> const derived_t&
{
    return *this;
}

//----------------------------------------------------------------------------------


#define VT_BINARY_OP( OP ) \
template <class... Ts>  \
inline auto vt_num_base<Ts...>::operator OP ( const this_t &other ) const -> derived_t \
{ \
    op_access_t o; \
    derived_t res; \
    o(res.val) = o(val) OP o(other.val); \
    return res; \
}

VT_BINARY_OP( + )
VT_BINARY_OP( - )
VT_BINARY_OP( * )
VT_BINARY_OP( / )

#undef VT_BINARY_OP


//----------------------------------------------------------------------------------


#define VT_BINARY_ASSIGNMENT_OP( OP ) \
template <class... Ts> \
inline auto vt_num_base<Ts...>::operator OP##= ( const this_t &other ) -> const derived_t& \
{ \
    op_access_t o; \
    o(val) OP##= o(other.val); \
    return this->derived(); \
}

VT_BINARY_ASSIGNMENT_OP( + )
VT_BINARY_ASSIGNMENT_OP( - )
VT_BINARY_ASSIGNMENT_OP( * )
VT_BINARY_ASSIGNMENT_OP( / )

#undef VT_BINARY_ASSIGNMENT_OP


//----------------------------------------------------------------------------------


#define VT_BINARY_SINGLE_OP( OP ) \
template <class... Ts>  \
inline auto vt_num_base<Ts...>::operator OP ( elem_t other ) const -> derived_t \
{ \
    op_access_t o; \
    derived_t res; \
    o(res.val) = o(val) OP other; \
    return res; \
}

VT_BINARY_SINGLE_OP( + )
VT_BINARY_SINGLE_OP( - )
VT_BINARY_SINGLE_OP( * )
VT_BINARY_SINGLE_OP( / )

#undef VT_BINARY_SINGLE_OP


//----------------------------------------------------------------------------------


#define VT_BINARY_ASSIGNMENT_SINGLE_OP( OP ) \
template <class... Ts> \
inline auto vt_num_base<Ts...>::operator OP##= ( elem_t other ) -> const derived_t& \
{ \
    op_access_t o; \
    o(val) OP##= other; \
    return static_cast<derived_t&>(*this); \
}

VT_BINARY_ASSIGNMENT_SINGLE_OP( + )
VT_BINARY_ASSIGNMENT_SINGLE_OP( - )
VT_BINARY_ASSIGNMENT_SINGLE_OP( * )
VT_BINARY_ASSIGNMENT_SINGLE_OP( / )

#undef VT_BINARY_ASSIGNMENT_SINGLE_OP


//----------------------------------------------------------------------------------


#define VT_BINARY_COMPARISON_OP( OP ) \
template <class... Ts> \
inline auto vt_num_base<Ts...>::operator OP ( const this_t &other ) const -> bool_vt \
{ \
    simd_debug_info( "Using simd binary comparison operator " "OP" ); \
    op_access_t o; \
    bool_vt res; \
    res.val.si = o(val) OP o(other.val); \
    return res; \
}

VT_BINARY_COMPARISON_OP( == )
VT_BINARY_COMPARISON_OP( != )
VT_BINARY_COMPARISON_OP( < )
VT_BINARY_COMPARISON_OP( <= )
VT_BINARY_COMPARISON_OP( > )
VT_BINARY_COMPARISON_OP( >= )

#undef VT_BINARY_COMPARISON_OP

//----------------------------------------------------------------------------------


#define VT_BINARY_ELEM_COMPARISON_OP( OP ) \
template <class... Ts> \
inline auto vt_num_base<Ts...>::operator OP ( elem_t other ) const -> bool_vt \
{ \
    simd_debug_info( "Using simd binary comparison operator " "OP" ); \
    op_access_t o; \
    bool_vt res; \
    res.val.si = o(val) OP other; \
    return res; \
}

VT_BINARY_ELEM_COMPARISON_OP( == )
VT_BINARY_ELEM_COMPARISON_OP( != )
VT_BINARY_ELEM_COMPARISON_OP( < )
VT_BINARY_ELEM_COMPARISON_OP( <= )
VT_BINARY_ELEM_COMPARISON_OP( > )
VT_BINARY_ELEM_COMPARISON_OP( >= )

#undef VT_BINARY_ELEM_COMPARISON_OP

//----------------------------------------------------------------------------------


} // namespace smd


