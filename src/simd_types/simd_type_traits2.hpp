/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_base.hpp"
#include <type_traits>


namespace smd
{
//--------------------------------------------------------------------------------------------


namespace __internal
{

    template <class... Ts>
    typename vt_base<Ts...>::derived_t final_simd_type( const vt_base<Ts...>& );

} // namespace __internal


template <typename T>
struct final_simd;

// ----------------------------------------------------------------------------------

namespace __internal
{

    template <class... Ts>
    std::true_type  is_simd_based_impl( const vt_base<Ts...>& );
    std::false_type is_simd_based_impl( ... );


    template <class... Ts>
    std::true_type  is_simd_vb32_based_impl( const vt_bool_base<Ts...>& );
    std::false_type is_simd_vb32_based_impl( ... );

    template <class... Ts>
    std::true_type  is_simd_vi32_based_impl( const vt_i32<Ts...>& );
    std::false_type is_simd_vi32_based_impl( ... );


    template <class... Ts>
    std::true_type  is_simd_vf32_based_impl( const vt_f32<Ts...>& );
    std::false_type is_simd_vf32_based_impl( ... );


} // namespace __internal




template <typename T>
struct is_simd : public decltype( __internal::is_simd_based_impl( T() ) ) {};


template <typename T>
struct is_simd_vb32 : public decltype( __internal::is_simd_vb32_based_impl( T() ) ) {};

template <typename T>
struct is_simd_vi32 : public decltype( __internal::is_simd_vi32_based_impl( T() ) ) {};

template <typename T>
struct is_simd_vf32 : public decltype( __internal::is_simd_vf32_based_impl( T() ) ) {};


// --------------------------------------------------------------------------------------

template <typename T,
          typename = typename std::enable_if< std::is_fundamental<T>::value >::type >
constexpr size_t num_items(T) { return 1; }

template <class... Ts>
constexpr size_t num_items(const vt_base<Ts...>&) { return vt_base<Ts...>::num_items(); }

template <typename T>
constexpr size_t num_items() { return num_items(T()); }


}  // namespace smd
