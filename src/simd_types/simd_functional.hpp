/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{


template <typename T1, typename T2=T1>
struct equal_to;

template <typename T1, typename T2=T1>
struct not_equal_to;

template <typename T1, typename T2=T1>
struct greater;

template <typename T1, typename T2=T1>
struct less;

template <typename T1, typename T2=T1>
struct greater_equal;

template <typename T1, typename T2=T1>
struct less_equal;

// ----------------------------------------------------------------------------------

template <typename T1, typename T2=T1>
struct logical_and;

template <typename T1, typename T2=T1>
struct logical_or;

template <typename T>
struct logical_not;

// ----------------------------------------------------------------------------------

template <typename T1, typename T2=T1>
struct shift_right;

template <typename T1, typename T2=T1>
struct shift_left;

template <typename T>
struct bit_invert;

// ----------------------------------------------------------------------------------

template <typename T1, typename T2=T1>
struct maximum;

template <typename T1, typename T2=T1>
struct minimum;

template <typename T>
struct absolute;

template <typename T>
struct logarithm;


template <typename T>
struct reciprocal;

template <typename T>
struct square_root;

template <typename T>
struct reciprocal_square_root;

template <typename T>
struct rounding_up;

template <typename T>
struct rounding_down;

template <typename T>
struct rounding;

template <typename T>
struct truncation;

} // namespace smd


