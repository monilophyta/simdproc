/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_numeric.hpp"
#include "simd_integer.hpp"
#include <algorithm>


namespace smd
{

template <typename vect_t>
struct vt_f32_access_t
{
    typedef decltype( vect_t::sf )  raw_vect_t;
    
    inline const raw_vect_t& operator()( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return val.sf;
    }

    inline raw_vect_t& operator()( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return val.sf;
    }

    inline const float32_t* elem_begin( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<const float32_t*>(&(val.sf));
    }

    inline float32_t* elem_begin( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<float32_t*>(&(val.sf));
    }
};



template <typename vect_t,
          class... Ts>
struct vt_f32 : public vt_num_base< vt_param< vt_f32_access_t<vect_t>,
                                              vect_t, 
                                              Ts... > >
{
private:

    typedef vt_f32<vect_t, Ts...> this_t;

    typedef vt_num_base< vt_param< vt_f32_access_t<vect_t>,
                                   vect_t, 
                                   Ts... > >  base_t;
  
public:
    
    using typename base_t::derived_t;
    
    // Inherit constructor
    using base_t::base_t;

    inline vt_f32() = default;

    // conversion from integer
    template <class... iTs>
    inline vt_f32( const vt_i32<iTs...>& other );
};


} // namespace smd


