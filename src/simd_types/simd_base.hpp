/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include <iostream>


namespace smd
{

// Forward Declarations


// --------------------------------------------------------------------------------


template <typename vect_t,
          typename bool_vt,
          typename derived_t>
struct vt_i32;

template <typename vect_t,
          class... Ts>
struct vt_f32;

template <class... Ts>
struct vt_num_base;

template <typename vect_t,
          typename derived_t>
struct vt_bool_base;

// --------------------------------------------------------------------------------


template <typename OP_ACCESS,
          typename VECT_T,
          typename BOOL_VT,
          typename DERIVED_T>
struct vt_param
{
    typedef VECT_T    vect_t;
    typedef OP_ACCESS op_access_t;
    typedef BOOL_VT   bool_vt;
    typedef DERIVED_T derived_t;

    // type for array access
    typedef typename std::remove_const< 
                typename std::remove_reference< 
                    decltype( op_access_t()( vect_t() ) )
                >::type
            >::type                                                        raw_vect_t;

    // element type
    typedef typename std::remove_const< 
                typename std::remove_reference< 
                    decltype( op_access_t()( vect_t() )[0] )
                >::type
            >::type                                                            elem_t;

    // const element type
    typedef typename std::add_const<elem_t>::type                        const_elem_t;


    static_assert( false == std::is_const<elem_t>::value );
};


// --------------------------------------------------------------------------------

template <typename T>
struct vt_base {};


template <class... Ts>
struct vt_base< vt_param<Ts...> >;


// --------------------------------------------------------------------------------


template <class... Ts>
struct vt_base< vt_param<Ts...> >
{
private:

    typedef vt_param<Ts...>                   vt_param_t;
    typedef vt_base<vt_param<Ts...>>          this_t;
    
public:

    typedef typename vt_param_t::derived_t    derived_t;
    typedef typename vt_param_t::vect_t       vect_t;
    typedef typename vt_param_t::raw_vect_t   raw_vect_t;
    typedef typename vt_param_t::bool_vt      bool_vt;
    typedef typename vt_param_t::elem_t       elem_t;
    typedef typename vt_param_t::const_elem_t const_elem_t;
    typedef typename vt_param_t::op_access_t  op_access_t;

    // --------------------------------------------------------------------------------

    vect_t val;

    
    inline vt_base() = default;
    explicit inline vt_base( const vect_t &init );

    /// factory function from memory pointer
    static inline derived_t from_ptr( const_elem_t* f_elemPtr );

    // ------------------------------------------------------------------------------------

    inline derived_t& derived() { return static_cast<derived_t&>(*this); }
    inline const derived_t& derived() const { return static_cast<const derived_t&>(*this); }


    // ------------------------------------------------------------------------------------
    // number of items
    static constexpr size_t size() { return (sizeof(raw_vect_t) / sizeof(elem_t)); }
    static constexpr size_t num_items() { return size(); }

    // ------------------------------------------------------------------------------------
    // item access
    inline elem_t& operator[]( size_t f_idx ) { 
        simd_bounds_check( f_idx < size() );
        return ( op_access_t().elem_begin(val)[ f_idx ] ); }
    
    inline elem_t  operator[]( size_t f_idx ) const {
        simd_bounds_check( f_idx < size() );
        return ( op_access_t().elem_begin(val)[ f_idx ] ); }

    // ------------------------------------------------------------------------------------
    // bitwise operations

    inline derived_t operator&( const this_t &other ) const;
    inline derived_t operator|( const this_t &other ) const;
    inline derived_t operator^( const this_t &other ) const;

    inline const derived_t& operator&=( const this_t &other );
    inline const derived_t& operator|=( const this_t &other );
    inline const derived_t& operator^=( const this_t &other );

    // ------------------------------------------------------------------------------------
    // Iterator functions

    inline elem_t* begin() {
        return op_access_t().elem_begin(val); }
    
    inline elem_t* end() {
        return ( this->begin()+size() ); }

    inline const_elem_t* cbegin() const {
        return op_access_t().elem_begin(val); }
    
    inline const_elem_t* cend() const {
        return ( this->cbegin()+size() ); }


    /*inline const elem_t* begin() const 
    { return cbegin(); }
    
    inline const elem_t* end() const
    { return cend(); }*/

    // ------------------------------------------------------------------------------------

    inline explicit operator elem_t* () { return begin(); }
    inline explicit operator const_elem_t* () const { return cbegin(); }

    // ------------------------------------------------------------------------------------

    static constexpr derived_t* cast_from( elem_t* f_elem_p ) { return reinterpret_cast<derived_t*>(f_elem_p); }
    static constexpr const derived_t* cast_from( const_elem_t* f_elem_p ) { return reinterpret_cast<const derived_t*>(f_elem_p); }

    static constexpr auto safe_cast_from( elem_t* f_elem_p ) { return safe_aligned_ptr_cast<derived_t>(f_elem_p); }
    static constexpr auto safe_cast_from( const_elem_t* f_elem_p ) { return safe_aligned_ptr_cast<derived_t>(f_elem_p); }

    // ------------------------------------------------------------------------------------

    /// store content of vector in memory
    inline void store( elem_t* f_stream_p ) const;

    // base function for overloading with non-temporal memory hinting storing routines
    inline void nontemporal_store( vt_base* f_stream_p ) const;
};

// --------------------------------------------------------------------------------

template <class... Ts>
inline std::ostream & operator<< ( std::ostream &out, const vt_base<Ts...> &c);

template <class... Ts>
inline std::istream & operator>> ( std::istream &in, vt_base<Ts...> &c);

// --------------------------------------------------------------------------------


} // namespace smd

