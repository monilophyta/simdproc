/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_nohw.hpp"
#include <algorithm>


namespace smd
{
namespace nhw
{


inline nhw_i32_vt::nhw_i32_vt( const nhw_f32_vt& other )
    : vt_i32( other )
{}


// ---------------------------------------------------------------------------------------

inline nhw_f32_vt::nhw_f32_vt( const nhw_i32_vt& other )
    : vt_f32( other )
{}



}  // namespace nhw
}  // namespace smd

