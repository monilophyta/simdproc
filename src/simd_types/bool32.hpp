/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>


namespace smd
{


struct bool32
{
private:
    
    int32_t m_val;

public:

    inline bool32() {}

    inline bool32( bool f_val ) SIMD_NOEXCEPT: m_val( static_cast<int32_t>(f_val) ) {}
    inline bool32& operator=( bool f_val ) SIMD_NOEXCEPT { m_val = static_cast<int32_t>(f_val); return *this; }
    inline operator bool() const SIMD_NOEXCEPT { return ( m_val != static_cast<int32_t>(0) ); }
    
    inline explicit bool32( int32_t f_val ) SIMD_NOEXCEPT: m_val( f_val ) {}
    inline bool32& operator=( int32_t f_val ) SIMD_NOEXCEPT { m_val = f_val; return *this; }
    inline explicit operator int32_t() const SIMD_NOEXCEPT { return static_cast<int32_t>( bool(*this) ); }

    // logical operators
    inline bool operator!() const SIMD_NOEXCEPT { return (!bool(*this)); }
    inline bool operator&&( const bool f_other ) const { return (bool(*this) && f_other ); }
    inline bool operator||( const bool f_other ) const { return (bool(*this) || f_other ); }

    // comparisons
    inline bool operator==( const bool f_other ) const { return (bool(*this) == f_other ); }
    inline bool operator!=( const bool f_other ) const { return (bool(*this) != f_other ); }
};


static_assert( sizeof( bool32 ) == sizeof( int32_t ) );


// converts bool to bool32, keeps other types untouched
template <typename T>
struct toBool32
    : public std::conditional< std::is_same<T,bool>::value || std::is_same<T,bool32>::value,
                               bool32,
                               T>
{};
static_assert( true == std::is_same< typename toBool32<bool>::type, bool32 >::value );
static_assert( true == std::is_same< typename toBool32<int32_t>::type, int32_t >::value );


} // namespace smd
