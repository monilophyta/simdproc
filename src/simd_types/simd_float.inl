/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cmath>
#include "simd_float.hpp"
#include <algorithm>


namespace smd
{

template <typename vect_t, class... Ts>
template <class... iTs>
inline vt_f32<vect_t, Ts... >::vt_f32( const vt_i32<iTs...>& other )
{
    std::transform( other.cbegin(), other.cend(), 
                    this->begin(),
                    []( int32_t x ) -> float32_t { return static_cast<float32_t>(x); });
}

} // namespace smd

//-----------------------------------------------------------------------------------------------------

