/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_base.hpp"


namespace smd
{


template <typename vect_t>
struct vt_b32_access_t
{
    typedef bool32_t  raw_vect_t[ sizeof(vect_t::si) / sizeof(int32_t) ];

    static_assert( sizeof( raw_vect_t ) == sizeof( vect_t::si ) );
    
    inline const raw_vect_t& operator()( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<const raw_vect_t&>(val.si);
    }

    inline raw_vect_t& operator()( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<raw_vect_t&>(val.si);
    }

    inline const bool32_t* elem_begin( const vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<const bool32_t*>(&(val.si));
    }

    inline bool32_t* elem_begin( vect_t& val ) const
    {
        simd_alignment_check( &val, sizeof(vect_t)-1 );
        return reinterpret_cast<bool32_t*>(&(val.si));
    }
};



template <typename vect_t,
          typename derived_t>
struct vt_bool_base : public vt_base< vt_param< vt_b32_access_t< vect_t >,
                                                vect_t, 
                                                derived_t,
                                                derived_t> >
{
private:

    typedef vt_base< vt_param< vt_b32_access_t< vect_t >,
                               vect_t, 
                               derived_t,
                               derived_t> >  base_t;

    typedef vt_bool_base<vect_t,derived_t>   this_t;

    // --------------------------

public:

    using base_t::base_t;
    using base_t::val;
    using typename base_t::elem_t;

    using base_t::derived;
    using base_t::begin;
    using base_t::end;
    using base_t::cbegin;
    using base_t::cend;

    inline vt_bool_base() = default;

    inline std::string to_string () const;

    // logical operators
    inline derived_t operator!() const;
    inline derived_t operator&&( const this_t &other ) const;
    inline derived_t operator||( const this_t &other ) const;

    inline derived_t operator&&( bool32_t other ) const;
    inline derived_t operator||( bool32_t other ) const;

    // comparisons
    inline derived_t operator==( const this_t &other ) const;
    inline derived_t operator!=( const this_t &other ) const;

    inline derived_t operator==( bool32_t other ) const;
    inline derived_t operator!=( bool32_t other ) const;

    // conversion to bool if all element as bool (!=0)  - disabled due to very bad side effects with min/max
    //explicit inline operator bool() const;


    static constexpr int32_t bool_to_i32( bool f_b ) { return ((f_b) ? int32_t(-1) : int32_t(0)); }
};

//----------------------------------------------------------------------------------

#define VT_BINARY_SYMMETRIC_ELEM_OP( OP ) \
template <typename vect_t, typename derived_t> \
inline derived_t operator OP ( bool32_t other, const vt_bool_base<vect_t,derived_t>& f_simdVal ) \
{ \
    return (f_simdVal OP other); \
}

VT_BINARY_SYMMETRIC_ELEM_OP( && )
VT_BINARY_SYMMETRIC_ELEM_OP( || )
VT_BINARY_SYMMETRIC_ELEM_OP( == )
VT_BINARY_SYMMETRIC_ELEM_OP( != )

#undef VT_BINARY_SYMMETRIC_ELEM_OP



//----------------------------------------------------------------------------------


inline bool all( bool f_val ) { return f_val; }


template <class... bTs>
inline bool all( const vt_bool_base<bTs...>& f_mask );


inline bool any( bool f_val ) { return f_val; }

template <class... bTs>
inline bool any( const vt_bool_base<bTs...>& f_mask );


template <class... bTs, class... oTs>
inline typename vt_base<oTs...>::derived_t
blend( const vt_bool_base<bTs...>& f_mask,
       const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right );



//----------------------------------------------------------------------------------


} // namespace smd


