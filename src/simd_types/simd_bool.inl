/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_bool.hpp"
#include <sstream>


namespace smd
{


template <typename vect_t,typename derived_t>
inline std::string vt_bool_base<vect_t,derived_t>::to_string () const
{
    std::ostringstream out;
    
    auto it = begin();

    static const std::string True = "true";
    static const std::string False = "False";

    out << typeid(derived_t).name() << "( ";
    out << ( (0 != *it) ? True : False ); 
    ++it;

    for ( ; end() != it; ++it )
    {
        out << ", " << ( (0 != (*it)) ? True : False );
    }

    out << " )";

    return out.str();
}


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator! () const
{
    derived_t res;

    #if defined(__clang__)
        // CLANG does not support this unary operator
        res.val.si = (val.si == 0);
    #else
        // GCC supports ternary operator
        res.val.si = !val.si;
    #endif

    return res;
}

// -----------------------------------------------------------------------------------------


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator== ( const this_t &other ) const
{
    derived_t res;
    
    res.val.si = ((0 == val.si) == (0 == other.val.si));
    return res;
}


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator!= ( const this_t &other ) const
{
    derived_t res;
    
    res.val.si = ((0 == val.si) != (0 == other.val.si));
    return res;
}

// -----------------------------------------------------------------------------------------


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator== ( bool32_t other ) const
{
    return ((*this) == derived_t(other));
}


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator!= ( bool32_t other ) const
{
    return ((*this) != derived_t(other));
}

// -----------------------------------------------------------------------------------------

template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator&& ( const this_t &other ) const
{
    derived_t res;
    
    res.val.si = ( val.si && other.val.si );
    return res;
}


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator|| ( const this_t &other ) const
{
    derived_t res;
    
    res.val.si = ( val.si || other.val.si );
    return res;
}

// -----------------------------------------------------------------------------------------

template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator&& ( bool32_t other ) const
{
    return ((*this) && derived_t(other));
}


template <typename vect_t, typename derived_t>
inline derived_t vt_bool_base<vect_t,derived_t>::operator|| ( bool32_t other ) const
{
    return ((*this) || derived_t(other));
}


// -----------------------------------------------------------------------------------------

//  disabled due to very bad side effects with min/max
#if 0
template <typename vect_t, typename derived_t>
inline vt_bool_base<vect_t,derived_t>::operator bool() const
{
    return all(derived());
}
#endif


//----------------------------------------------------------------------------------




template <class... bTs>
inline bool all( const vt_bool_base<bTs...>& f_mask )
{
    bool l_res_b = true;

    for (auto it = f_mask.cbegin(); f_mask.cend() != it; ++it)
    {
        l_res_b = l_res_b && ( 0 != *it );
    }
    
    return l_res_b;
}


template <class... bTs>
inline bool any( const vt_bool_base<bTs...>& f_mask )
{
    for (auto it = f_mask.cbegin(); f_mask.cend() != it; ++it)
    {
        if (0 != *it)
            return true;
    }
    
    return false;
}



template <class... bTs, class... oTs>
inline typename vt_base<oTs...>::derived_t
blend( const vt_bool_base<bTs...>& f_mask,
       const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right )
{
    typename vt_base<oTs...>::derived_t   res;
    
    #if defined(__clang__)
        // CLANG does not support ternary operator
        typename vt_base<oTs...>::derived_t selector;
        selector.val.si = f_mask.val.si != 0;
        res.val.si = (f_left.val.si & selector.val.si) | (f_right.val.si & (~selector.val.si));
    #else
        // GCC supports ternary operator
        typename vt_base<oTs...>::op_access_t o;
        o(res.val) = (f_mask.val.si) ? o(f_left.val) : o(f_right.val);
    #endif

    return res;
}


} // namespace smd
