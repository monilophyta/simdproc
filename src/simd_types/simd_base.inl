/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <algorithm>
#include "simd_base.hpp"


namespace smd
{

template <class... Ts>
inline vt_base< vt_param<Ts...> >::vt_base( const vect_t &init )
    : val( init )
{
    static_assert( (sizeof(this_t) % sizeof(elem_t)) == 0 );
    static_assert( sizeof(this_t) == sizeof(raw_vect_t) );
    static_assert( sizeof(this_t) == sizeof(derived_t) );
}


template <class... Ts>
inline auto vt_base< vt_param<Ts...> >::from_ptr( const_elem_t* f_elemPtr ) -> derived_t
{
    derived_t val;
    std::copy_n( f_elemPtr, derived_t::num_items(), val.begin() );
    return val;
}


// ----------------------------------------------------------------------------------------------------

#define VT_BINARY_OP( OP ) \
template <class... Ts>  \
inline auto vt_base< vt_param<Ts...> >::operator OP ( const this_t &other ) const -> derived_t \
{ \
    derived_t res; \
    res.val.si = val.si OP other.val.si; \
    return res; \
}

VT_BINARY_OP( & )
VT_BINARY_OP( | )
VT_BINARY_OP( ^ )

#undef VT_BINARY_OP

// ----------------------------------------------------------------------------------------------------


#define VT_BINARY_ASSIGNMENT_OP( OP ) \
template <class... Ts>  \
inline auto vt_base< vt_param<Ts...> >::operator OP##= ( const this_t &other ) \
-> const derived_t& \
{ \
    val.si OP##= other.val.si; \
    return this->derived(); \
}

VT_BINARY_ASSIGNMENT_OP( & )
VT_BINARY_ASSIGNMENT_OP( | )
VT_BINARY_ASSIGNMENT_OP( ^ )

#undef VT_BINARY_ASSIGNMENT_OP


/*
 * input output operators
 *
 */


template <class... Ts>
inline std::ostream & operator<< ( std::ostream &out, const vt_base<Ts...> &c )
{
    for (auto it = c.cbegin(); it != c.cend(); ++it)
    {
        out << *it;
    }

    return out;
}

template <class... Ts>
inline std::istream & operator>> ( std::istream &in, vt_base<Ts...> &c )
{
    for (auto it = c.begin(); it != c.end(); ++it)
    {
        in >> *it;
    }

    return in;
}



template <class... Ts>
inline void vt_base< vt_param<Ts...> >::store( elem_t* f_stream_p ) const
{
    std::copy( this->cbegin(), this->cend(), f_stream_p );
}


template <class... Ts>
inline void vt_base< vt_param<Ts...> >::nontemporal_store( vt_base* f_stream_p ) const
{
#if defined(__clang__)
    op_access_t o;
    __builtin_nontemporal_store( o(val), reinterpret_cast<raw_vect_t*>(f_stream_p) );
#else
    // fallback is storing without hint
    (*f_stream_p) = (*this);
#endif
}


}  // namespace smd
