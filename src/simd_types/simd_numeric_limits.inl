/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_numeric_limits.hpp"
#include <cmath>


namespace smd
{

namespace __internal
{

template <class... Ts, typename FUNC_T>
inline static
vb32_t generic_is( const vt_f32<Ts...>& f_val, FUNC_T efunc )
{
    vb32_t ret;

    auto retEnd = std::transform( f_val.cbegin(), f_val.cend(),
                                  ret.begin(),
                                  efunc );

    simd_assert( retEnd == ret.end(), "Non fitting simd types" );

    return ret;
}

} // namespace __internal
} // namespace smd

namespace std
{


template <class... Ts>
inline static
smd::vb32_t isnan( const smd::vt_f32<Ts...>& f_val )
{
    return smd::__internal::generic_is( f_val, []( smd::float32_t x ) { return std::isnan(x); } );
}


template <class... Ts>
inline static
smd::vb32_t isinf( const smd::vt_f32<Ts...>& f_val )
{
    
    return smd::__internal::generic_is( f_val, []( smd::float32_t x ) { return std::isinf(x); } );
}


template <class... Ts>
inline static
smd::vb32_t isfinite( const smd::vt_f32<Ts...>& f_val )
{
    return smd::__internal::generic_is( f_val, []( smd::float32_t x ) { return std::isfinite(x); } );
}


} // namespace std
