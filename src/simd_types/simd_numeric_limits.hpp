/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <limits>
#include "simd_type_traits2.hpp"


//-------------------------------------------------------------------------------------------


namespace smd
{

namespace limits
{

template <typename T>
struct numeric_limits_vb32
{
    // TODO: implement
};


template <typename T>
struct numeric_limits_vi32
{
public:
    static_assert( is_simd<T>::value );
    
    typedef typename T::derived_t  type;

protected:

    static const type min_val;
    static const type lowest_val;
    static const type max_val;

public:

    static inline const type& min() { return min_val; }
    static inline const type& lowest() { return lowest_val; }
    static inline const type& max() { return max_val; }
};


template <typename T>
const typename numeric_limits_vi32<T>::type 
numeric_limits_vi32<T>::min_val( std::numeric_limits<typename T::elem_t>::min() );

template <typename T>
const typename numeric_limits_vi32<T>::type 
numeric_limits_vi32<T>::lowest_val( std::numeric_limits<typename T::elem_t>::lowest() );

template <typename T>
const typename numeric_limits_vi32<T>::type 
numeric_limits_vi32<T>::max_val( std::numeric_limits<typename T::elem_t>::max() );


//-------------------------------------------------------------------------------------------


template <typename T>
struct numeric_limits_vf32 : public numeric_limits_vi32<T>
{
    using typename numeric_limits_vi32<T>::type;

protected:

    static const type epsilon_val;
    static const type infinity_val;
    static const type signaling_NaN_val;
    static const type quiet_NaN_val;


public:

    static inline const type& epsilon() { return epsilon_val; }
    static inline const type& infinity() { return infinity_val; }
    static inline const type& signaling_NaN() { return signaling_NaN_val; }
    static inline const type& quiet_NaN() { return quiet_NaN_val; }
};

template <typename T>
const typename numeric_limits_vf32<T>::type 
numeric_limits_vf32<T>::epsilon_val( std::numeric_limits<typename T::elem_t>::epsilon() );

template <typename T>
const typename numeric_limits_vf32<T>::type 
numeric_limits_vf32<T>::infinity_val( std::numeric_limits<typename T::elem_t>::infinity() );

template <typename T>
const typename numeric_limits_vf32<T>::type 
numeric_limits_vf32<T>::signaling_NaN_val( std::numeric_limits<typename T::elem_t>::signaling_NaN() );

template <typename T>
const typename numeric_limits_vf32<T>::type 
numeric_limits_vf32<T>::quiet_NaN_val( std::numeric_limits<typename T::elem_t>::quiet_NaN() );


} // namespace limits

//-------------------------------------------------------------------------------------------

} // namespace smd



//-------------------------------------------------------------------------------------------


namespace std
{

template <>
struct  numeric_limits< smd::vb32_t > 
    : public smd::limits::numeric_limits_vb32< smd::vb32_t >
{};


template <>
struct  numeric_limits< smd::vi32_t > 
    : public smd::limits::numeric_limits_vi32< smd::vi32_t >
{};


template <>
struct  numeric_limits< smd::vf32_t > 
    : public smd::limits::numeric_limits_vf32< smd::vf32_t >
{};


//---------------------------------------------------------------------------------


template <class... Ts>
inline static
smd::vb32_t isnan( const smd::vt_f32<Ts...>& f_val );


template <class... Ts>
inline static
smd::vb32_t isinf( const smd::vt_f32<Ts...>& f_val );


template <class... Ts>
inline static
smd::vb32_t isfinite( const smd::vt_f32<Ts...>& f_val );

//---------------------------------------------------------------------------------

} // namespace ::std

