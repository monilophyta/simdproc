/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_sse_avx2_helper.hpp"
#include <limits>



namespace smd
{


namespace __sse_internal
{


template <typename T, typename I32_GATHER_FUNC, typename I64_GATHER_FUNC, typename COL_IDX_T, bool USE_I32>
inline typename select_simd<T>::type
gather( const val_matrix_view<const T>& f_from_a, const  vi32_t& f_rowidx_vi32, COL_IDX_T f_colidx )
{
    typedef typename select_simd<T>::type  simd_elem_t;
    
    simd_debug_info( "Using avx matrix gather(vint32,vint32) implementation" );

    simd_bounds_check( std::all_of( f_rowidx_vi32.cbegin(), f_rowidx_vi32.cend(),
                                    [&f_from_a]( size_t idx ) -> bool { 
                                        return (idx < f_from_a.n_rows()); } ) );

    simd_elem_t res;

    if (USE_I32)
    {
        simd_check( f_from_a.mem_size() <= static_cast<size_t>( std::numeric_limits<int32_t>::max() ), 
                    "Matrix too large for indexing with int32" );
        
        I32_GATHER_FUNC gfunc32;

        const vi32_t l_flatIdx_vi32 = ( f_rowidx_vi32 * f_from_a.stride() ) + f_colidx;

        simd_bounds_check( std::all_of( l_flatIdx_vi32.cbegin(), l_flatIdx_vi32.cend(),
                                        [&f_from_a]( size_t idx ) -> bool { 
                                            return (idx < f_from_a.mem_size()); } ) );
        
        res.val = v128( gfunc32( f_from_a.cbegin(), l_flatIdx_vi32.val.m128i ) );
    }
    else
    {
        I64_GATHER_FUNC gfunc64;

        const vi32_t l_colIdx_vi32( f_colidx );
        simd_bounds_check( std::all_of( l_colIdx_vi32.cbegin(), l_colIdx_vi32.cend(),
                                        [&f_from_a]( size_t idx ) -> bool { 
                                            return (idx < f_from_a.n_cols()); } ) );
        
        const v128 l_stride_vi64( _mm_set1_epi64x( f_from_a.stride() ) );

        for (int hidx = 0; hidx < 2; ++hidx)
        {
            v128 l_rowIdx_vi64;
            v128 l_colIdx_vi64;

            l_rowIdx_vi64.di[0] = f_rowidx_vi32.val.si[(2*hidx)];
            l_rowIdx_vi64.di[1] = f_rowidx_vi32.val.si[(2*hidx)+1];

            l_colIdx_vi64.di[0] = l_colIdx_vi32.val.si[(2*hidx)];
            l_colIdx_vi64.di[1] = l_colIdx_vi32.val.si[(2*hidx)+1];

            const v128 l_flatIdx_vi64( (l_rowIdx_vi64.di * l_stride_vi64.di) + l_colIdx_vi64.di );

            // only lower 64bit part contains 2 gathered int32 
            const v128 l_gathered_v64( gfunc64( f_from_a.cbegin(), l_flatIdx_vi64.m128i ) );
            simd_assert( 0 == l_gathered_v64.di[1], "invalid result in gathered items" );

            res.val.di[hidx] = l_gathered_v64.di[0];
        }
    }

    return res;
}


template <typename T, typename I32_GATHER_FUNC>
inline typename select_simd<T>::type
gather( const val_matrix_view<const T>& f_from_a, int32_t f_rowidx_i32, const vi32_t& f_colidx_vi32 )
{
    typedef typename select_simd<T>::type  simd_elem_t;
    
    simd_debug_info( "Using avx matrix gather(int32,vint32) implementation" );

    simd_check( f_from_a.n_cols() <= static_cast<size_t>( std::numeric_limits<int32_t>::max() ), 
                "Matrix Row too large for indexing with int32" );
    

    simd_bounds_check( static_cast<size_t>( f_rowidx_i32 ) < f_from_a.n_rows() );

    simd_bounds_check( std::all_of( f_colidx_vi32.cbegin(), f_colidx_vi32.cend(),
                                    [&f_from_a]( size_t idx ) -> bool { 
                                        return (idx < f_from_a.n_cols()); } ) );

    I32_GATHER_FUNC gfunc;

    return simd_elem_t( v128( gfunc( f_from_a.cbegin( f_rowidx_i32 ),
                                     f_colidx_vi32.val.m128i ) ) );
}

}  // namespace __sse_internal




//-------------------------------------------------------------------------------------------------

inline vb32_t gather( const matrix_view_cb32_t& f_matrix_b32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32, sse_i64gather_b32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_b32, f_rowIdx_vi32, f_colIdx_i32 );
}


inline vb32_t gather( const matrix_view_cvb32_t& f_matrix_vb32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32, sse_i64gather_b32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vb32.elem(), f_rowIdx_vi32, f_colIdx_i32 );
}


//-----------------------------------------------------------------------------


inline vi32_t gather( const matrix_view_ci32_t& f_matrix_i32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32, sse_i64gather_i32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_i32, f_rowIdx_vi32, f_colIdx_i32 );
}


inline vi32_t gather( const matrix_view_cvi32_t& f_matrix_vi32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32, sse_i64gather_i32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vi32.elem(), f_rowIdx_vi32, f_colIdx_i32 );
}


//-----------------------------------------------------------------------------


inline vf32_t gather( const matrix_view_cf32_t& f_matrix_f32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32, sse_i64gather_f32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_f32, f_rowIdx_vi32, f_colIdx_i32 );
}


inline vf32_t gather( const matrix_view_cvf32_t& f_matrix_vf32, const vi32_t& f_rowIdx_vi32, int32_t f_colIdx_i32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32, sse_i64gather_f32, int32_t, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vf32.elem(), f_rowIdx_vi32, f_colIdx_i32 );
}

//-------------------------------------------------------------------------------------------------





//-------------------------------------------------------------------------------------------------

inline vb32_t gather( const matrix_view_cb32_t& f_matrix_b32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32>( 
                        f_matrix_b32, f_rowIdx_i32, f_colIdx_vi32 );
}


inline vb32_t gather( const matrix_view_cvb32_t& f_matrix_vb32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32>( 
                        f_matrix_vb32.elem(), f_rowIdx_i32, f_colIdx_vi32 );
}


//-----------------------------------------------------------------------------


inline vi32_t gather( const matrix_view_ci32_t& f_matrix_i32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32>( 
                        f_matrix_i32, f_rowIdx_i32, f_colIdx_vi32 );
}


inline vi32_t gather( const matrix_view_cvi32_t& f_matrix_vi32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32>( 
                        f_matrix_vi32.elem(), f_rowIdx_i32, f_colIdx_vi32 );
}


//-----------------------------------------------------------------------------


inline vf32_t gather( const matrix_view_cf32_t& f_matrix_f32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32>( 
                        f_matrix_f32, f_rowIdx_i32, f_colIdx_vi32 );
}


inline vf32_t gather( const matrix_view_cvf32_t& f_matrix_vf32, int32_t f_rowIdx_i32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32>( 
                        f_matrix_vf32.elem(), f_rowIdx_i32, f_colIdx_vi32 );
}

//-------------------------------------------------------------------------------------------------




//-------------------------------------------------------------------------------------------------

inline vb32_t gather( const matrix_view_cb32_t& f_matrix_b32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32, sse_i64gather_b32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_b32, f_rowIdx_vi32, f_colIdx_vi32 );
}


inline vb32_t gather( const matrix_view_cvb32_t& f_matrix_vb32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32, sse_i64gather_b32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vb32.elem(), f_rowIdx_vi32, f_colIdx_vi32 );
}


//-----------------------------------------------------------------------------


inline vi32_t gather( const matrix_view_ci32_t& f_matrix_i32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32, sse_i64gather_i32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_i32, f_rowIdx_vi32, f_colIdx_vi32 );
}


inline vi32_t gather( const matrix_view_cvi32_t& f_matrix_vi32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32, sse_i64gather_i32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vi32.elem(), f_rowIdx_vi32, f_colIdx_vi32 );
}


//-----------------------------------------------------------------------------


inline vf32_t gather( const matrix_view_cf32_t& f_matrix_f32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32, sse_i64gather_f32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_f32, f_rowIdx_vi32, f_colIdx_vi32 );
}


inline vf32_t gather( const matrix_view_cvf32_t& f_matrix_vf32, const vi32_t& f_rowIdx_vi32, const vi32_t& f_colIdx_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32, sse_i64gather_f32, const vi32_t&, USE_I32_MATRIX_INDICES>( 
                        f_matrix_vf32.elem(), f_rowIdx_vi32, f_colIdx_vi32 );
}

//-------------------------------------------------------------------------------------------------



} // namespace smd
