/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{

inline smd::sse_f32_vt rcp( const smd::sse_f32_vt &x );
inline smd::sse_f32_vt rsqrt( const smd::sse_f32_vt &x );


#ifdef __SIMD_USE_SSE41__

inline smd::sse_f32_vt ceil( const smd::sse_f32_vt &x );
inline smd::sse_f32_vt floor( const smd::sse_f32_vt &x );
inline smd::sse_f32_vt round( const smd::sse_f32_vt &x );
inline smd::sse_f32_vt trunc( const smd::sse_f32_vt &x );

#endif // __SIMD_USE_SSE41__

} // namespace smd



namespace std
{

#ifdef __SIMD_USE_SSE41__
    inline smd::sse_i32_vt max( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y );
    inline smd::sse_i32_vt min( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y );
#else
    inline smd::sse_i32_vt max( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y ) { return smd::max(x,y); }
    inline smd::sse_i32_vt min( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y ) { return smd::min(x,y); }
#endif // __SIMD_USE_SSE41__

inline smd::sse_f32_vt max( const smd::sse_f32_vt &x, const smd::sse_f32_vt &y );
inline smd::sse_f32_vt min( const smd::sse_f32_vt &x, const smd::sse_f32_vt &y );

inline smd::sse_f32_vt log( const smd::sse_f32_vt &x );
inline smd::sse_f32_vt sqrt( const smd::sse_f32_vt &x );

inline smd::sse_i32_vt abs( const smd::sse_i32_vt &x );
inline smd::sse_f32_vt abs( const smd::sse_f32_vt &x );

}  // namespace std

//----------------------------------------------------------------------------------

