/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_sse.hpp"



namespace smd
{

namespace __sse_helper
{

#define SSE_GATHER_FUNC( RETVAL, FNAME, SSEFNAME, PTR_T, PARAM_T ) \
struct FNAME { \
    inline RETVAL operator()( const PTR_T* base_addr, const m128i_t& vindex ) const \
    { \
        return _ ## SSEFNAME ( reinterpret_cast<const PARAM_T*>(base_addr), vindex, sizeof(PARAM_T) ); \
    } \
};

SSE_GATHER_FUNC( m128i_t, sse_i32gather_b32, mm_i32gather_epi32, bool32_t,   int32_t )
SSE_GATHER_FUNC( m128i_t, sse_i32gather_i32, mm_i32gather_epi32, int32_t,    int32_t )
SSE_GATHER_FUNC( m128f_t, sse_i32gather_f32, mm_i32gather_ps,    float32_t,  float32_t )

SSE_GATHER_FUNC( m128i_t, sse_i64gather_b32, mm_i64gather_epi32, bool32_t,   int32_t   )
SSE_GATHER_FUNC( m128i_t, sse_i64gather_i32, mm_i64gather_epi32, int32_t,    int32_t   )
SSE_GATHER_FUNC( m128f_t, sse_i64gather_f32, mm_i64gather_ps,    float32_t,  float32_t )


static_assert( sizeof(bool32_t) == sizeof(int32_t) );

} // namespace __sse_helper

} // namespace smd
