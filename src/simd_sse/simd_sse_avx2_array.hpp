/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_sse_avx2_helper.hpp"
#include <algorithm>



namespace smd
{


namespace __sse_internal
{

template <typename T, typename I32_GATHER_FUNC>
inline typename select_simd<T>::type
gather( const val_array_view<const T>& f_from_a, const vi32_t& f_idx_vi32 )
{
    typedef typename select_simd<T>::type  simd_elem_t;
    
    simd_debug_info( "Using avx2 array gather implementation" );

    simd_check( f_from_a.size() <= static_cast<size_t>( std::numeric_limits<int32_t>::max() ), 
                "Array too large for indexing with int32" );
    
    simd_bounds_check( std::all_of( f_idx_vi32.cbegin(), f_idx_vi32.cend(),
                                    [&f_from_a]( size_t idx ) -> bool { return (idx < f_from_a.size()); } ) );

    I32_GATHER_FUNC gfunc;

    return simd_elem_t( v128( gfunc( f_from_a.cbegin(),
                                     f_idx_vi32.val.m128i ) ) );
}

}  // namespace __sse_internal



inline vb32_t gather( const array_view_cb32_t& f_array_b32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32>( f_array_b32, f_indices_vi32 );
}

inline vb32_t gather( const array_view_cvb32_t& f_array_vb32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<bool32_t, sse_i32gather_b32>( f_array_vb32.elem(), f_indices_vi32 );
}



inline vi32_t gather( const array_view_ci32_t& f_array_i32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32>( f_array_i32, f_indices_vi32 );
}

inline vi32_t gather( const array_view_cvi32_t& f_array_vi32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<int32_t, sse_i32gather_i32>( f_array_vi32.elem(), f_indices_vi32 );
}



inline vf32_t gather( const array_view_cf32_t& f_array_f32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32>( f_array_f32, f_indices_vi32 );
}

inline vf32_t gather( const array_view_cvf32_t& f_array_vf32, const vi32_t& f_indices_vi32 )
{
    using namespace __sse_helper;
    return __sse_internal::gather<float32_t, sse_i32gather_f32>( f_array_vf32.elem(), f_indices_vi32 );
}

} // namespace smd
