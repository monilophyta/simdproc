/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <xmmintrin.h>  // SSE
#include <emmintrin.h>  // SSE2
//#include <pmmintrin.h>  // SSE3
//#include <tmmintrin.h>  // SSSE3
//#include <smmintrin.h>  // SSE4.1
//#include <nmmintrin.h>  // SSE4.2
//#include <immintrin.h>  // AVX, AVX2


namespace smd
{


struct mm_instructions
{
    typedef smd::v4sf    vf32;
    typedef smd::m128i_t vi32;


    #define SSE128_INSTR_P( INFIX, POSTFIX ) \
    template<class ... Types> \
    inline static constexpr auto INFIX(Types ... args) { return _mm_ ## INFIX ## POSTFIX ( args...); }

    #define SSE128_INSTR( INFIX ) \
    template<class ... Types> \
    inline static constexpr auto INFIX(Types ... args) { return _mm_ ## INFIX ( args...); }

    SSE128_INSTR( add_ps )            // SSE
    SSE128_INSTR( and_ps )            // SSE
    SSE128_INSTR_P( castps, _si128 )  // SSE2
    SSE128_INSTR( cvtepi32_ps )       // SSE2
    SSE128_INSTR( max_ps )            // SSE
    SSE128_INSTR( mul_ps )            // SSE
    SSE128_INSTR( or_ps )             // SSE
    SSE128_INSTR( set1_epi32 )        // SSE2
    SSE128_INSTR( set1_ps )           // SSE
    SSE128_INSTR( setzero_ps )        // SSE
    SSE128_INSTR( srli_epi32 )        // SSE2
    SSE128_INSTR( sub_epi32 )         // SSE2
    SSE128_INSTR( sub_ps )            // SSE

    // comparison operators
    SSE128_INSTR( cmplt_ps )          // SSE
    SSE128_INSTR( cmple_ps )          // SSE

    #undef SSE128_INSTR
    #undef SSE128_INSTR_P
};


} // namespace smd
