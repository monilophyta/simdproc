/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "simd_sse.hpp"
#include <smmintrin.h>   // SSE 4.1
#include <immintrin.h>   // AVX2

#ifdef __SIMD_USE_AVX2__
    #include "simd_sse_avx2_helper.hpp"
#endif // __SIMD_USE_AVX2__


namespace smd
{

namespace
{

constexpr bool is_sse_aligned( const void* f_ptr )
{
    return ( 0u == (reinterpret_cast<size_t>(f_ptr) & (sizeof(v128)-1u)) );
}

} // anonymous namespace



inline sse_b32_vt sse_b32_vt::from_ptr( const bool32_t* f_ptr )
{
    sse_b32_vt v;
    v.val.m128i = is_sse_aligned(f_ptr) ? _mm_load_si128( reinterpret_cast<const m128i_t*>(f_ptr) )
                                        : _mm_loadu_si128( reinterpret_cast<const m128i_t*>(f_ptr) );
    return v;
}


// --------------------------------------------------------------------------------

#ifdef __SIMD_USE_SSE41__


    #ifdef ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

        template <class... oTs>
        inline typename vt_base<oTs...>::derived_t 
        blend( const sse_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right )
        {
            typedef typename vt_base<oTs...>::derived_t arg_derived_t;

            sse_b32_vt fullbit_mask;
            fullbit_mask.val.m128i = _mm_cmpeq_epi32( f_mask.val.m128i, _mm_setzero_si128() );  // mask == 0

            arg_derived_t res;
            res.val.m128f = _mm_blendv_ps( f_left.val.m128f,         // else        - then
                                           f_right.val.m128f,        // then        - else
                                           fullbit_mask.val.m128f ); // if (mask)   - (not mask)
            return res;
        }

    #endif // ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

    // --------------------------------------------------------------------------------


    inline bool
    all( const sse_b32_vt &x )
    {
        sse_i32_vt xint;

        #if defined(__clang__)
            // CLANG does not support this unary operator
            xint.val.si = (x.val.si == 0);
        #else
            // GCC supports ternary operator
            xint.val.si = !(x.val.si);
        #endif

        const int c = _mm_testz_si128( xint.val.m128i, xint.val.m128i );
        simd_assert( (c == 0) || (c == 1), "Wrong understanding of _mm_testz_si128" );

        return (c != 0);
        //return ( 0 == sum(xint) );
    }


    inline bool
    any( const sse_b32_vt &x )
    {
        const int c = _mm_testz_si128( x.val.m128i, x.val.m128i );
        simd_assert( (c == 0) || (c == 1), "Wrong understanding of _mm_testz_si128" );

        return (c == 0);
        //return ( 0 < sum( sse_i32_vt( x.val ) ) );
    }

#endif // __SIMD_USE_SSE41__


// --------------------------------------------------------------------------------

/// store content of vector in memory
inline void sse_b32_vt::store( bool32_t* f_stream_p ) const
{
    if ( is_sse_aligned(f_stream_p) )
    {
        _mm_store_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
    }
    else
    {
        _mm_storeu_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
    }
}

// non-temporal memory hinting storing routine
inline void sse_b32_vt::nontemporal_store( sse_b32_vt* f_stream_p ) const
{
    _mm_stream_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
}


// --------------------------------------------------------------------------------

inline sse_i32_vt::sse_i32_vt( const sse_f32_vt &other )
    : base_t()
{
    this->val.m128i = _mm_cvtps_epi32( other.val.m128f );
}


inline sse_i32_vt sse_i32_vt::from_ptr( const int32_t* f_ptr )
{
    sse_i32_vt v;
    v.val.m128i = is_sse_aligned(f_ptr) ? _mm_load_si128( reinterpret_cast<const m128i_t*>(f_ptr) )
                                        : _mm_loadu_si128( reinterpret_cast<const m128i_t*>(f_ptr) );
    return v;
}


inline sse_i32_vt sse_i32_vt::zero()
{
    return sse_i32_vt( v128( _mm_setzero_si128() ) );
}


/// store content of vector in memory
inline void sse_i32_vt::store( int32_t* f_stream_p ) const
{
    if ( is_sse_aligned(f_stream_p) )
    {
        _mm_store_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
    }
    else
    {
        _mm_storeu_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
    }
}

// non-temporal memory hinting storing routine
inline void sse_i32_vt::nontemporal_store( sse_i32_vt* f_stream_p ) const
{
    _mm_stream_si128( reinterpret_cast<m128i_t*>(f_stream_p), this->val.m128i );
}

// --------------------------------------------------------------------------------



#ifdef __SIMD_USE_AVX2__


    inline sse_i32_vt sse_i32_vt::gather( const int32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx int32 pointer gather implementation" );
        
        __sse_helper::sse_i32gather_i32 gfunc;

        return sse_i32_vt( v128( gfunc( f_from_a,
                                        derived().val.m128i ) ) );
    }


    inline sse_f32_vt sse_i32_vt::gather( const float32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx float32 pointer gather implementation" );
        
        __sse_helper::sse_i32gather_f32 gfunc;

        return sse_f32_vt( v128( gfunc( f_from_a,
                                        derived().val.m128i ) ) );
    }

#endif // __SIMD_USE_AVX2__

// --------------------------------------------------------------------------------

inline sse_f32_vt::sse_f32_vt( const sse_i32_vt& other )
    : base_t()
{
    this->val.m128f = _mm_cvtepi32_ps( other.val.m128i );
}


inline sse_f32_vt sse_f32_vt::from_ptr( const float32_t* f_ptr )
{
    sse_f32_vt v;
    v.val.m128f = is_sse_aligned(f_ptr) ? _mm_load_ps( f_ptr )
                                        : _mm_loadu_ps( f_ptr );
    return v;
}


inline sse_f32_vt sse_f32_vt::zero()
{
    return sse_f32_vt( v128( _mm_setzero_ps() ) );
}

// --------------------------------------------------------------------------------


/// store content of vector in memory
inline void sse_f32_vt::store( float32_t* f_stream_p ) const
{
    if ( is_sse_aligned(f_stream_p) )
    {
        _mm_store_ps( f_stream_p, this->val.m128f );
    }
    else
    {
        _mm_storeu_ps( f_stream_p, this->val.m128f );
    }
}


// non-temporal memory hinting storing routine
inline void sse_f32_vt::nontemporal_store( sse_f32_vt* f_stream_p ) const
{
    _mm_stream_ps( reinterpret_cast<float32_t*>(f_stream_p), this->val.m128f );
}


// --------------------------------------------------------------------------------

#ifdef __SIMD_USE_SSE3__

    inline int32_t sum( const sse_i32_vt &x )
    {
        v128 hs = _mm_movehdup_ps( x.val.m128f );  // [ 1 1 3 3 ]
        v128 ls = _mm_moveldup_ps( x.val.m128f );  // [ 0 0 2 2 ]

        v128 y = hs.si + ls.si;  // [ 1+0 1+0 3+2 3+2 ] two values left

        return (y.si[0] + y.si[2]);
    }



    inline int32_t prod( const sse_i32_vt &x )
    {
        v128 hs = _mm_movehdup_ps( x.val.m128f );  // [ 1 1 3 3 ]
        v128 ls = _mm_moveldup_ps( x.val.m128f );  // [ 0 0 2 2 ]

        v128 y = hs.si * ls.si;  // [ 1*0 1*0 3*2 3*2 ] two values left

        return (y.si[0] * y.si[2]);
    }

#endif

// --------------------------------------------------------------------------------

#ifdef __SIMD_USE_SSE3__

    inline float32_t sum( const sse_f32_vt &x )
    {
        v128 hs = _mm_movehdup_ps( x.val.m128f );  // [ 1 1 3 3 ]
        v128 ls = _mm_moveldup_ps( x.val.m128f );  // [ 0 0 2 2 ]

        v128 y = hs.sf + ls.sf;  // [ 1+0 1+0 3+2 3+2 ] two values left

        return (y.sf[0] + y.sf[2]);
    }


    inline float32_t prod( const sse_f32_vt &x )
    {
        v128 hs = _mm_movehdup_ps( x.val.m128f );  // [ 1 1 3 3 ]
        v128 ls = _mm_moveldup_ps( x.val.m128f );  // [ 0 0 2 2 ]

        v128 y = hs.sf * ls.sf;  // [ 1*0 1*0 3*2 3*2 ] two values left

        return (y.sf[0] * y.sf[2]);
    }

#endif

// --------------------------------------------------------------------------------


} // namespace smd


