/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <xmmintrin.h>  // SSE
#include <emmintrin.h>  // SSE2
#include <cfloat>
#include <type_traits>


// Minimum requirement:
#ifndef __SIMD_USE_SSE2__
    #error Needs minimum SSE2 instructions 
#endif


namespace smd
{

//  According to:
// https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
// https://godbolt.org/z/rccvq9


typedef int32_t    v4si __attribute__ ((vector_size (16)));
typedef float32_t  v4sf __attribute__ ((vector_size (16)));

typedef int64_t    v2di __attribute__ ((vector_size (16)));


typedef __m128i   m128i_t;
typedef __m128    m128f_t;


#define sse_alignment_check( PTR ) simd_alignment_check( PTR, 0x0f )


//----------------------------------------------------------------------------------


union alignas(sizeof(v4si)) v128
{
    
    v4si    si;
    v4sf    sf;
    
    m128i_t m128i;
    m128f_t m128f;
    
    v2di    di;
    

    inline v128() = default;
    inline v128( const v128& ) = default;
    inline v128( v128&& ) = default;
    inline v128& operator=( const v128& ) = default;
    inline v128& operator=( v128&& ) = default;


    inline v128( const v4si &init )
        : si( init )
    {
        sse_alignment_check( this );
    }

    inline v128( const v4sf &init )
        : sf( init )
    {
        sse_alignment_check( this );
    }

    inline v128( const v2di &init )
        : di( init )
    {
        sse_alignment_check( this );
    }

    inline v128( int32_t si1, int32_t si2, int32_t si3, int32_t si4 )
        : m128i( _mm_set_epi32( si1, si2, si3, si4 ) )
    {
        sse_alignment_check( this );
    }

    inline v128( int32_t f_initv_i32 )
        : m128i( _mm_set1_epi32( f_initv_i32 ) )
    {
        sse_alignment_check( this );
    }

    inline v128( float32_t sf1, float32_t sf2, float32_t sf3, float32_t sf4 )
        : m128f( _mm_set_ps( sf1, sf2, sf3, sf4 ) )
    {
        sse_alignment_check( this );
    }

    inline v128( float32_t f_initv_f32 )
        : m128f( _mm_set1_ps( f_initv_f32 ) )
    {
        sse_alignment_check( this );
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"

    // for some g++ m128i_t and v4si are the same types
    template < typename U = m128i_t,
               typename = typename std::enable_if< false == std::is_same<U,v4si>::value >::type>
    inline v128( const m128i_t &init )
        : m128i( init )
    {
        sse_alignment_check( this );
    }

    //probably not required since __m128 and v4sf seem to be the same types
    template < typename U = m128f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline v128( const m128f_t &init )
        : m128f( init )
    {
        sse_alignment_check( this );
    }
#pragma GCC diagnostic pop

};

static_assert( sizeof(v128) == 16 );
static_assert( true == std::is_trivially_default_constructible<v128>::value );

//----------------------------------------------------------------------------------


struct sse_b32_vt;
struct sse_i32_vt;
struct sse_f32_vt;


//----------------------------------------------------------------------------------


/*
 * SSE bool type
 */ 

struct alignas(sizeof(v128)) sse_b32_vt 
    : public vt_bool_base< v128, sse_b32_vt >
{
private:

    typedef vt_bool_base< v128, sse_b32_vt > base_t;

public:

    using typename base_t::elem_t;
    
    //using base_t::vt_bool_base;

    constexpr int32_t bool_to_i32( bool f_b ) const { return ((f_b) ? int32_t(-1) : int32_t(0)); }

    inline sse_b32_vt() = default;
    inline sse_b32_vt( const sse_b32_vt& ) = default;
    inline sse_b32_vt( sse_b32_vt&& ) = default;
    inline sse_b32_vt& operator=( const sse_b32_vt& ) = default;
    inline sse_b32_vt& operator=( sse_b32_vt&& ) = default;


    explicit inline sse_b32_vt( const v128 &f_initv )
        : vt_bool_base( f_initv )
    {}

    explicit inline sse_b32_vt( bool f_initv_b )
        : vt_bool_base( v128( bool_to_i32( f_initv_b ) ) )
    {}

    /// factory function from memory pointer
    static inline sse_b32_vt from_ptr( const bool32_t* f_elemPtr );

    // Type reinterpretation
    inline       sse_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      sse_i32_vt&>(*this); }
    inline const sse_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const sse_i32_vt&>(*this); }

    inline       sse_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      sse_f32_vt&>(*this); }
    inline const sse_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const sse_f32_vt&>(*this); }


    /// store content of vector in memory
    inline void store( bool32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( sse_b32_vt* f_stream_p ) const;
};

#ifdef __SIMD_USE_SSE41__

    #ifdef ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

        template <class... oTs>
        inline typename vt_base<oTs...>::derived_t 
        blend( const sse_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right );

    #endif // ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION


    inline bool all( const sse_b32_vt &x );
    inline bool any( const sse_b32_vt &x );

#endif // __SIMD_USE_SSE41__

//----------------------------------------------------------------------------------


/*
 * SSE int32 type
 */ 


struct alignas(sizeof(v128)) sse_i32_vt
    : public vt_i32< v128,
                     sse_b32_vt,
                     sse_i32_vt >
{

private:

    typedef vt_i32< v128,sse_b32_vt,sse_i32_vt > base_t;

public:

    using typename base_t::elem_t;
    

    inline sse_i32_vt() = default;
    inline sse_i32_vt( const sse_i32_vt& ) = default;
    inline sse_i32_vt( sse_i32_vt&& ) = default;
    inline sse_i32_vt& operator=( const sse_i32_vt& ) = default;
    inline sse_i32_vt& operator=( sse_i32_vt&& ) = default;


    explicit inline sse_i32_vt( const v128 &f_initv )
        : vt_i32( f_initv )
    {}

    explicit inline sse_i32_vt( int32_t f_initv_i32 )
        : vt_i32( v128( f_initv_i32 ) )
    {}

    explicit inline sse_i32_vt( const sse_f32_vt &other );

    /// factory function from memory pointer
    static inline sse_i32_vt from_ptr( const int32_t* f_elemPtr );

    inline explicit operator const m128i_t() const { return val.m128i; }
    inline explicit operator const v4si() const { return val.si; }

    
    static inline sse_i32_vt zero();

    // type reinterpretation
    inline       sse_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      sse_f32_vt&>(*this); }
    inline const sse_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const sse_f32_vt&>(*this); }


#ifdef __SIMD_USE_AVX2__

    inline sse_i32_vt gather( const int32_t* f_from_a ) const;
    inline sse_f32_vt gather( const float32_t* f_from_a ) const;

#endif // __SIMD_USE_AVX2__

    /// store content of vector in memory
    inline void store( int32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( sse_i32_vt* f_stream_p ) const;
};

#ifdef __SIMD_USE_SSE3__

    inline int32_t sum( const sse_i32_vt &x );
    inline int32_t prod( const sse_i32_vt &x );

#endif

//----------------------------------------------------------------------------------


/*
 * SSE float32 type
 */ 

struct alignas(sizeof(v128)) sse_f32_vt
    : public vt_f32< v128, 
                     sse_b32_vt,
                     sse_f32_vt >
{

public:

    typedef vt_f32< v128,sse_b32_vt,sse_f32_vt > base_t;

    using typename base_t::elem_t;
    
    
    inline sse_f32_vt() = default;
    inline sse_f32_vt( const sse_f32_vt& ) = default;
    inline sse_f32_vt( sse_f32_vt&& ) = default;
    inline sse_f32_vt& operator=( const sse_f32_vt& ) = default;
    inline sse_f32_vt& operator=( sse_f32_vt&& ) = default;


    explicit inline sse_f32_vt( const v128 &f_initv )
        : vt_f32( f_initv )
    {}

    explicit inline sse_f32_vt( float32_t f_initv_f32 )
        : vt_f32( v128( f_initv_f32 ) )
    {}

    inline sse_f32_vt( const sse_i32_vt& other );

    /// factory function from memory pointer
    static inline sse_f32_vt from_ptr( const float32_t* f_elemPtr );

    inline explicit operator const v4sf() const { return val.sf; }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    template < typename U = m128f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline operator const m128f_t() const { return val.m128f; }
#pragma GCC diagnostic pop

    
    static inline sse_f32_vt zero();


    // type reinterpretation
    inline       sse_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      sse_i32_vt&>(*this); }
    inline const sse_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const sse_i32_vt&>(*this); }


    // FMA instruction based operators
    #ifdef __SIMD_USE_FMA__

        // Don't forget that we have already some operator we like to continue using
        using base_t::operator+=;
        using base_t::operator-=;

        template <typename MT>
        inline const sse_f32_vt& operator+=( const simd_multiply_base<sse_f32_vt,MT> &other );

        template <typename MT>
        inline const sse_f32_vt& operator-=( const simd_multiply_base<sse_f32_vt,MT> &other );

    #endif

    /// store content of vector in memory
    inline void store( float32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( sse_f32_vt* f_stream_p ) const;
};

#ifdef __SIMD_USE_SSE3__

    inline float32_t sum( const sse_f32_vt &x );
    inline float32_t prod( const sse_f32_vt &x );

#endif

}  // namespace smd


//----------------------------------------------------------------------------------
