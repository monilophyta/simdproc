/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_sse_arithmetic.hpp"
#include <xmmintrin.h>  // SSE
#include <emmintrin.h>  // SSE2
#include <smmintrin.h>   // SSE 4.1


namespace smd
{

inline smd::sse_f32_vt rcp( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_rcp_ps( x.val.m128f );
    return res;
}


inline smd::sse_f32_vt rsqrt( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_rsqrt_ps( x.val.m128f );
    return res;
}


#ifdef __SIMD_USE_SSE41__

inline smd::sse_f32_vt ceil( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_ceil_ps( x.val.m128f );
    return res;
}

inline smd::sse_f32_vt floor( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_floor_ps( x.val.m128f );
    return res;
}


inline smd::sse_f32_vt round( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_round_ps( x.val.m128f, (_MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC) );
    return res;
}

inline smd::sse_f32_vt trunc( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_round_ps( x.val.m128f, (_MM_FROUND_TO_ZERO |_MM_FROUND_NO_EXC) );
    return res;
}

#endif // __SIMD_USE_SSE41__


} // namespace smd



// --------------------------------------------------------------------------------------------
// ---STD NAMESPACE----------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------


namespace std
{

#ifdef __SIMD_USE_SSE41__

    inline smd::sse_i32_vt max( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y )
    {
        smd::sse_i32_vt z;

        z.val.m128i = _mm_max_epi32( x.val.m128i, y.val.m128i );

        return z;
    }


    inline smd::sse_i32_vt min( const smd::sse_i32_vt &x, const smd::sse_i32_vt &y )
    {
        smd::sse_i32_vt z;

        z.val.m128i = _mm_min_epi32( x.val.m128i, y.val.m128i );

        return z;
    }

#endif // __SIMD_USE_SSE41__

// --------------------------------------------------------------------------------



inline smd::sse_f32_vt max( const smd::sse_f32_vt &x, const smd::sse_f32_vt &y )
{
    smd::sse_f32_vt z;
    z.val.m128f = _mm_max_ps( x.val.m128f, y.val.m128f );

    return z;
}

inline smd::sse_f32_vt min( const smd::sse_f32_vt &x, const smd::sse_f32_vt &y )
{
    smd::sse_f32_vt z;
    z.val.m128f = _mm_min_ps( x.val.m128f, y.val.m128f );

    return z;
}

// --------------------------------------------------------------------------------

inline smd::sse_f32_vt log( const smd::sse_f32_vt &x )
{
    smd::foreign::CGarberoglioLog<smd::sse_f32_vt,smd::sse_i32_vt> logfunctor;
    
    smd::sse_f32_vt res = logfunctor(x);
    return res;
}


inline smd::sse_f32_vt sqrt( const smd::sse_f32_vt &x )
{
    smd::sse_f32_vt res;
    res.val.m128f = _mm_sqrt_ps( x.val.m128f );
    return res;
}




inline smd::sse_i32_vt abs( const smd::sse_i32_vt &x )
{
    #ifdef __SIMD_USE_SSSE3__
        return smd::sse_i32_vt( smd::v128( _mm_abs_epi32( x.val.m128i ) ) );
    #else
        return smd::blend( x >= smd::sse_i32_vt::zero(), x, -x );
    #endif
}



inline smd::sse_f32_vt abs( const smd::sse_f32_vt &x )
{
    return smd::blend( x >= smd::sse_f32_vt::zero(), x, -x );
}


}  // namespace std



