/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "iterator/simd_memory_alignment.hpp"

#include "iterator/simd_iterator.hpp"
#include "iterator/simd_iterator.inl"

namespace smd
{
    typedef ualigned_iterator<vb32_t>              ua_vb32_iter_t;
    typedef ualigned_iterator<vi32_t>              ua_vi32_iter_t;
    typedef ualigned_iterator<vf32_t>              ua_vf32_iter_t;

    typedef const_ualigned_iterator<vb32_t>        const_ua_vb32_iter_t;
    typedef const_ualigned_iterator<vi32_t>        const_ua_vi32_iter_t;
    typedef const_ualigned_iterator<vf32_t>        const_ua_vf32_iter_t;
}


#include "iterator/simd_iterator_bundle.hpp"
#include "iterator/simd_iterator_bundle.inl"

namespace smd
{

    typedef CIteratorBundleValueIterator<vb32_t::elem_t>  column_iterator_b32_t;
    typedef CIteratorBundleValueIterator<vi32_t::elem_t>  column_iterator_i32_t;
    typedef CIteratorBundleValueIterator<vf32_t::elem_t>  column_iterator_f32_t;

    typedef CIteratorBundleValueIterator<const vb32_t::elem_t>  column_iterator_cb32_t;
    typedef CIteratorBundleValueIterator<const vi32_t::elem_t>  column_iterator_ci32_t;
    typedef CIteratorBundleValueIterator<const vf32_t::elem_t>  column_iterator_cf32_t;

    // TODO: implement the following properties
    //static_assert( std::is_convertible< column_iterator_b32_t, column_iterator_cb32_t >::value );
    //static_assert( std::is_convertible< column_iterator_i32_t, column_iterator_ci32_t >::value );
    //static_assert( std::is_convertible< column_iterator_f32_t, column_iterator_cf32_t >::value );

    static_assert( false == std::is_convertible< column_iterator_cb32_t, column_iterator_b32_t >::value );
    static_assert( false == std::is_convertible< column_iterator_ci32_t, column_iterator_i32_t >::value );
    static_assert( false == std::is_convertible< column_iterator_cf32_t, column_iterator_f32_t >::value );



    typedef CIteratorBundleValueIterator<vb32_t>  column_iterator_vb32_t;
    typedef CIteratorBundleValueIterator<vi32_t>  column_iterator_vi32_t;
    typedef CIteratorBundleValueIterator<vf32_t>  column_iterator_vf32_t;

    typedef CIteratorBundleValueIterator<const vb32_t>  column_iterator_cvb32_t;
    typedef CIteratorBundleValueIterator<const vi32_t>  column_iterator_cvi32_t;
    typedef CIteratorBundleValueIterator<const vf32_t>  column_iterator_cvf32_t;

    // TODO: implement the following properties
    //static_assert( std::is_convertible< column_iterator_vb32_t, column_iterator_cvb32_t >::value );
    //static_assert( std::is_convertible< column_iterator_vi32_t, column_iterator_cvi32_t >::value );
    //static_assert( std::is_convertible< column_iterator_vf32_t, column_iterator_cvf32_t >::value );

    static_assert( false == std::is_convertible< column_iterator_cvb32_t, column_iterator_vb32_t >::value );
    static_assert( false == std::is_convertible< column_iterator_cvi32_t, column_iterator_vi32_t >::value );
    static_assert( false == std::is_convertible< column_iterator_cvf32_t, column_iterator_vf32_t >::value );



    typedef CIteratorBundleIterator<vb32_t::elem_t>  bundle_iterator_b32_t;
    typedef CIteratorBundleIterator<vi32_t::elem_t>  bundle_iterator_i32_t;
    typedef CIteratorBundleIterator<vf32_t::elem_t>  bundle_iterator_f32_t;

    typedef CIteratorBundleIterator<const vb32_t::elem_t>  bundle_iterator_cb32_t;
    typedef CIteratorBundleIterator<const vi32_t::elem_t>  bundle_iterator_ci32_t;
    typedef CIteratorBundleIterator<const vf32_t::elem_t>  bundle_iterator_cf32_t;

    // TODO: implement the following properties
    //static_assert( std::is_convertible< bundle_iterator_b32_t, bundle_iterator_cb32_t >::value );
    //static_assert( std::is_convertible< bundle_iterator_i32_t, bundle_iterator_ci32_t >::value );
    //static_assert( std::is_convertible< bundle_iterator_f32_t, bundle_iterator_cf32_t >::value );

    static_assert( false == std::is_convertible< bundle_iterator_cb32_t, bundle_iterator_b32_t >::value );
    static_assert( false == std::is_convertible< bundle_iterator_ci32_t, bundle_iterator_i32_t >::value );
    static_assert( false == std::is_convertible< bundle_iterator_cf32_t, bundle_iterator_f32_t >::value );



    typedef CIteratorBundleIterator<vb32_t>  bundle_iterator_vb32_t;
    typedef CIteratorBundleIterator<vi32_t>  bundle_iterator_vi32_t;
    typedef CIteratorBundleIterator<vf32_t>  bundle_iterator_vf32_t;

    typedef CIteratorBundleIterator<const vb32_t>  bundle_iterator_cvb32_t;
    typedef CIteratorBundleIterator<const vi32_t>  bundle_iterator_cvi32_t;
    typedef CIteratorBundleIterator<const vf32_t>  bundle_iterator_cvf32_t;

    // TODO: implement the following properties
    //static_assert( std::is_convertible< bundle_iterator_vb32_t, bundle_iterator_cvb32_t >::value );
    //static_assert( std::is_convertible< bundle_iterator_vi32_t, bundle_iterator_cvi32_t >::value );
    //static_assert( std::is_convertible< bundle_iterator_vf32_t, bundle_iterator_cvf32_t >::value );

    static_assert( false == std::is_convertible< bundle_iterator_cvb32_t, bundle_iterator_vb32_t >::value );
    static_assert( false == std::is_convertible< bundle_iterator_cvi32_t, bundle_iterator_vi32_t >::value );
    static_assert( false == std::is_convertible< bundle_iterator_cvf32_t, bundle_iterator_vf32_t >::value );


} // namespace smd


