/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_avx_fma.hpp"
#include <immintrin.h>


namespace smd
{

// -------------------------------------------------------------------------------------------------------------
// Inside class operators
// -------------------------------------------------------------------------------------------------------------


template <typename MT>
inline const avx_f32_vt& avx_f32_vt::operator+=( const simd_multiply_base<avx_f32_vt,MT> &other )
{
    // _mm256_fmadd_ps(a,b,c) => a * b + c
    this->val.m256f = _mm256_fmadd_ps( other.vx().val.m256f, other.vy().val.m256f, this->val.m256f );
    return *this;
}


template <typename MT>
inline const avx_f32_vt& avx_f32_vt::operator-=( const simd_multiply_base<avx_f32_vt,MT> &other )
{
    // _mm256_fnmadd_ps(a,b,c) => -(a * b) + c
    this->val.m256f = _mm256_fnmadd_ps( other.vx().val.m256f, other.vy().val.m256f, this->val.m256f );
    return *this;
}

// -------------------------------------------------------------------------------------------------------------
// Outside class operators
// -------------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------
// operations of type fmadd: "(a*b) + c"


template <typename MT>
inline avx_f32_vt
operator+( const simd_multiply_base<avx_f32_vt,MT>& f_a, const avx_f32_vt& f_b )
{ 
    avx_f32_vt res;
    // _mm256_fmadd_ps(a,b,c) => a * b + c
    res.val.m256f = _mm256_fmadd_ps( f_a.vx().val.m256f, f_a.vy().val.m256f, f_b.val.m256f );
    return res;
}


template <typename MT>
inline avx_f32_vt
operator+( const simd_multiply_base<avx_f32_vt,MT>& f_a, float32_t f_b )
{ 
    avx_f32_vt res;
    // _mm256_fmadd_ps(a,b,c) => a * b + c
    res.val.m256f = _mm256_fmadd_ps( f_a.vx().val.m256f, f_a.vy().val.m256f, avx_f32_vt( f_b ).val.m256f );
    return res;
}

// -------------------------------------------------------------------------------------------------
// operations of type fmsub: "(a*b) - c"

template <typename MT>
inline avx_f32_vt
operator-( const simd_multiply_base<avx_f32_vt,MT>& f_a, const avx_f32_vt& f_b )
{
    avx_f32_vt res;
    // _mm256_fmsub_ps(a,b,c) => (a * b) - c
    res.val.m256f = _mm256_fmsub_ps( f_a.vx().val.m256f, f_a.vy().val.m256f, f_b.val.m256f );
    return res;
}


template <typename MT>
inline avx_f32_vt
operator-( const simd_multiply_base<avx_f32_vt,MT>& f_a, float32_t f_b )
{
    avx_f32_vt res;
    // _mm256_fmsub_ps(a,b,c) => (a * b) - c
    res.val.m256f = _mm256_fmsub_ps( f_a.vx().val.m256f, f_a.vy().val.m256f, avx_f32_vt( f_b ).val.m256f );
    return res;
}

// -------------------------------------------------------------------------------------------------
// operations of type fnmadd: "c - (a*b)"

template <typename MT>
inline avx_f32_vt
operator-( const avx_f32_vt& f_a, const simd_multiply_base<avx_f32_vt,MT>& f_b )
{
    avx_f32_vt res;
    // _mm256_fnmadd_ps(a,b,c) => -(a * b) + c
    res.val.m256f = _mm256_fnmadd_ps( f_b.vx().val.m256f, f_b.vy().val.m256f, f_a.val.m256f );
    return res;
}


template <typename MT>
inline avx_f32_vt
operator-( float32_t f_a, const simd_multiply_base<avx_f32_vt,MT>& f_b )
{
    avx_f32_vt res;
    // _mm256_fnmadd_ps(a,b,c) => -(a * b) + c
    res.val.m256f = _mm256_fnmadd_ps( f_b.vx().val.m256f, f_b.vy().val.m256f, avx_f32_vt( f_a ).val.m256f );
    return res;
}

// -------------------------------------------------------------------------------------------------------------

} // namespace smd
