/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


//#include "simd_cfg.hpp"
//#include "simd_avx.hpp"
#include "../simd_operations/simd_multiplication.hpp"


namespace smd
{


struct avx_multiply_rr
    : public simd_multiply_rr_base<avx_f32_vt,avx_multiply_rr>
{
private:
    typedef simd_multiply_rr_base<avx_f32_vt,avx_multiply_rr>  base_t;

public:

    // inherit constructor
    using base_t::base_t;
};


inline avx_multiply_rr
operator*( const avx_f32_vt& f_x, const avx_f32_vt& f_y ) { return avx_multiply_rr( f_x, f_y ); }



// --------------------------------------------------------------------------------------------------


struct avx_multiply_vr
    : public simd_multiply_vr_base<avx_f32_vt,avx_multiply_vr>
{
private:
    typedef simd_multiply_vr_base<avx_f32_vt,avx_multiply_vr>  base_t;

public:

    // inherit constructor
    using base_t::base_t;
};


inline avx_multiply_vr
operator*( const avx_f32_vt& f_x, float32_t f_y ) { return avx_multiply_vr( avx_f32_vt(f_y), f_x ); }

inline avx_multiply_vr
operator*( float32_t f_x, const avx_f32_vt& f_y ) { return avx_multiply_vr( avx_f32_vt(f_x), f_y ); }


// --------------------------------------------------------------------------------------------------


} // namespace smd


