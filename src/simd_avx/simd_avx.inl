/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_avx.hpp"

#ifdef __SIMD_USE_AVX2__
    #include "simd_avx2_helper.hpp"
#endif // __SIMD_USE_AVX2__



namespace smd
{

namespace
{

constexpr bool is_avx_aligned( const void* f_ptr )
{
    return ( 0u == (reinterpret_cast<size_t>(f_ptr) & (sizeof(v256)-1u)) );
}

} // anonymous namespace


// --------------------------------------------------------------------------------

inline avx_b32_vt avx_b32_vt::from_ptr( const bool32_t* f_ptr )
{
    avx_b32_vt v;
    v.val.m256i = is_avx_aligned(f_ptr) ? _mm256_load_si256( reinterpret_cast<const m256i_t*>(f_ptr) )
                                        : _mm256_loadu_si256( reinterpret_cast<const m256i_t*>(f_ptr) );
    return v;
}

// --------------------------------------------------------------------------------

#ifdef ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

template <class... oTs>
inline typename vt_base<oTs...>::derived_t 
blend( const avx_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right )
{
    typedef typename vt_base<oTs...>::derived_t arg_derived_t;

    avx_b32_vt fullbit_mask;
    fullbit_mask.val.m256i = _mm256_cmpeq_epi32( f_mask.val.m256i, _mm256_setzero_si256() );  // mask == 0

    arg_derived_t res;
    res.val.m256f = _mm256_blendv_ps( f_left.val.m256f,         // else        - then
                                      f_right.val.m256f,        // then        - else
                                      fullbit_mask.val.m256f ); // if (mask)   - (not mask)
    return res;
}

#endif // ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

// --------------------------------------------------------------------------------

/// store content of vector in memory
inline void avx_b32_vt::store( bool32_t* f_stream_p ) const
{
    if ( is_avx_aligned(f_stream_p) )
    {
        _mm256_store_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
    }
    else
    {
        _mm256_storeu_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
    }
}

// non-temporal memory hinting storing routine
inline void avx_b32_vt::nontemporal_store( avx_b32_vt* f_stream_p ) const
{
    _mm256_stream_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
}


// --------------------------------------------------------------------------------


inline bool
all( const avx_b32_vt &x )
{
    avx_i32_vt xint;

    #if defined(__clang__)
        // CLANG does not support this unary operator
        xint.val.si = (x.val.si == 0);
    #else
        // GCC supports ternary operator
        xint.val.si = !(x.val.si);
    #endif

    const int c = _mm256_testz_si256( xint.val.m256i, xint.val.m256i );
    simd_assert( (c == 0) || (c == 1), "Wrong understanding of _mm256_testz_si256" );

    return (c != 0);
    //return ( 0 == sum(xint) );
}


inline bool
any( const avx_b32_vt &x )
{
    const int c = _mm256_testz_si256( x.val.m256i, x.val.m256i );
    simd_assert( (c == 0) || (c == 1), "Wrong understanding of _mm256_testz_si256" );

    return (c == 0);
    //return ( 0 < sum( avx_i32_vt( x.val ) ) );
}


// --------------------------------------------------------------------------------


inline avx_i32_vt::avx_i32_vt( const avx_f32_vt &other )
    : base_t()
{
    this->val.m256i = _mm256_cvtps_epi32( other.val.m256f );
}


inline avx_i32_vt avx_i32_vt::from_ptr( const int32_t* f_ptr )
{
    avx_i32_vt v;
    v.val.m256i = is_avx_aligned(f_ptr) ? _mm256_load_si256( reinterpret_cast<const m256i_t*>(f_ptr) )
                                        : _mm256_loadu_si256( reinterpret_cast<const m256i_t*>(f_ptr) );
    return v;
}


inline avx_i32_vt avx_i32_vt::zero()
{
    return avx_i32_vt( v256( _mm256_setzero_si256() ) );
}

// --------------------------------------------------------------------------------


#ifdef __SIMD_USE_AVX2__

    inline avx_i32_vt avx_i32_vt::gather( const int32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx int32 pointer gather implementation" );
        
        __avx2_helper::avx_i32gather_i32 gfunc;

        return avx_i32_vt( v256( gfunc( f_from_a,
                                        derived().val.m256i ) ) );
    }


    inline avx_f32_vt avx_i32_vt::gather( const float32_t* f_from_a ) const
    {
        simd_debug_info( "Using avx float32 pointer gather implementation" );
        
        __avx2_helper::avx_i32gather_f32 gfunc;

        return avx_f32_vt( v256( gfunc( f_from_a,
                                        derived().val.m256i ) ) );
    }

#endif //__SIMD_USE_AVX2__

// --------------------------------------------------------------------------------


/// store content of vector in memory
inline void avx_i32_vt::store( int32_t* f_stream_p ) const
{
    if ( is_avx_aligned(f_stream_p) )
    {
        _mm256_store_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
    }
    else
    {
        _mm256_storeu_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
    }
}

// non-temporal memory hinting storing routine
inline void avx_i32_vt::nontemporal_store( avx_i32_vt* f_stream_p ) const
{
    _mm256_stream_si256( reinterpret_cast<m256i_t*>(f_stream_p), this->val.m256i );
}


// --------------------------------------------------------------------------------

inline avx_f32_vt::avx_f32_vt( const avx_i32_vt& other )
    : base_t()
{
    this->val.m256f = _mm256_cvtepi32_ps( other.val.m256i );
}


inline avx_f32_vt avx_f32_vt::from_ptr( const float32_t* f_ptr )
{
    avx_f32_vt v;
    v.val.m256f = is_avx_aligned(f_ptr) ? _mm256_load_ps( f_ptr )
                                        : _mm256_loadu_ps( f_ptr );
    return v;
}


inline avx_f32_vt avx_f32_vt::zero()
{
    return avx_f32_vt( v256( _mm256_setzero_ps() ) );
}


// --------------------------------------------------------------------------------


/// store content of vector in memory
inline void avx_f32_vt::store( float32_t* f_stream_p ) const
{
    if ( is_avx_aligned(f_stream_p) )
    {
        _mm256_store_ps( f_stream_p, this->val.m256f );
    }
    else
    {
        _mm256_storeu_ps( f_stream_p, this->val.m256f );
    }
}

// non-temporal memory hinting storing routine
inline void avx_f32_vt::nontemporal_store( avx_f32_vt* f_stream_p ) const
{
    _mm256_stream_ps( reinterpret_cast<float32_t*>(f_stream_p), this->val.m256f );
}


// --------------------------------------------------------------------------------


inline int32_t sum( const avx_i32_vt &x )
{
    v128 xs = x.val.hlf[0].si + x.val.hlf[1].si; // 4 values left

    v128 hs = _mm_movehdup_ps( xs.m128f );  // [ 1 1 3 3 ]
    v128 ls = _mm_moveldup_ps( xs.m128f );  // [ 0 0 2 2 ]

    v128 y = hs.si + ls.si;  // [ 1+0 1+0 3+2 3+2 ] two values left

    return (y.si[0] + y.si[2]);
}



inline int32_t prod( const avx_i32_vt &x )
{
    v128 xs = x.val.hlf[0].si * x.val.hlf[1].si; // 4 values left

    v128 hs = _mm_movehdup_ps( xs.m128f );  // [ 1 1 3 3 ]
    v128 ls = _mm_moveldup_ps( xs.m128f );  // [ 0 0 2 2 ]

    v128 y = hs.si * ls.si;  // [ 1*0 1*0 3*2 3*2 ] two values left

    return (y.si[0] * y.si[2]);
}


// --------------------------------------------------------------------------------


inline float32_t sum( const avx_f32_vt &x )
{
    v128 xs = x.val.hlf[0].sf + x.val.hlf[1].sf; // 4 values left

    v128 hs = _mm_movehdup_ps( xs.m128f );  // [ 1 1 3 3 ]
    v128 ls = _mm_moveldup_ps( xs.m128f );  // [ 0 0 2 2 ]

    v128 y = hs.sf + ls.sf;  // [ 1+0 1+0 3+2 3+2 ] two values left

    return (y.sf[0] + y.sf[2]);
}


inline float32_t prod( const avx_f32_vt &x )
{
    v128 xs = x.val.hlf[0].sf * x.val.hlf[1].sf; // 4 values left

    v128 hs = _mm_movehdup_ps( xs.m128f );  // [ 1 1 3 3 ]
    v128 ls = _mm_moveldup_ps( xs.m128f );  // [ 0 0 2 2 ]

    v128 y = hs.sf * ls.sf;  // [ 1*0 1*0 3*2 3*2 ] two values left

    return (y.sf[0] * y.sf[2]);
}

} // namespace smd

