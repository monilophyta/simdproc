/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "simd_avx_arithmetic.hpp"
#include <immintrin.h> // AVX

namespace smd
{


inline smd::avx_f32_vt rcp( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_rcp_ps( x.val.m256f );
    return res;
}


inline smd::avx_f32_vt rsqrt( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_rsqrt_ps( x.val.m256f );
    return res;
}


inline smd::avx_f32_vt ceil( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_ceil_ps( x.val.m256f );
    return res;
}

inline smd::avx_f32_vt floor( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_floor_ps( x.val.m256f );
    return res;
}


inline smd::avx_f32_vt round( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_round_ps( x.val.m256f, (_MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC) );
    return res;
}

inline smd::avx_f32_vt trunc( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_round_ps( x.val.m256f, (_MM_FROUND_TO_ZERO |_MM_FROUND_NO_EXC) );
    return res;
}


// --------------------------------------------------------------------------------


inline smd::avx_i32_vt max( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y )
{
    smd::avx_i32_vt z;

    #ifdef __SIMD_USE_AVX2__
        z.val.m256i = _mm256_max_epi32( x.val.m256i, y.val.m256i );
    #else
        // SSE fallback
        z.val.m128i[0] = _mm_max_epi32( x.val.m128i[0], y.val.m128i[0] );
        z.val.m128i[1] = _mm_max_epi32( x.val.m128i[1], y.val.m128i[1] );
    #endif // __SIMD_USE_AVX2__

    return z;
}


inline smd::avx_i32_vt min( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y )
{
    smd::avx_i32_vt z;

    #ifdef __SIMD_USE_AVX2__
        z.val.m256i = _mm256_min_epi32( x.val.m256i, y.val.m256i );
    #else
        // SSE fallback
        z.val.m128i[0] = _mm_min_epi32( x.val.m128i[0], y.val.m128i[0] );
        z.val.m128i[1] = _mm_min_epi32( x.val.m128i[1], y.val.m128i[1] );
    #endif  // __SIMD_USE_AVX2__

    return z;
}


// --------------------------------------------------------------------------------

inline smd::avx_f32_vt max( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y )
{
    smd::avx_f32_vt z;
    z.val.m256f = _mm256_max_ps( x.val.m256f, y.val.m256f );

    return z;
}

inline smd::avx_f32_vt min( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y )
{
    smd::avx_f32_vt z;
    z.val.m256f = _mm256_min_ps( x.val.m256f, y.val.m256f );

    return z;
}


} // namespace smd


// --------------------------------------------------------------------------------------------
// ---STD NAMESPACE----------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------



namespace std
{

// --------------------------------------------------------------------------------

inline smd::avx_f32_vt log( const smd::avx_f32_vt &x )
{
    smd::foreign::CGarberoglioLog<smd::avx_f32_vt,smd::avx_i32_vt> logfunctor;
    
    smd::avx_f32_vt res = logfunctor(x);
    return res;
}


inline smd::avx_f32_vt sqrt( const smd::avx_f32_vt &x )
{
    smd::avx_f32_vt res;
    res.val.m256f = _mm256_sqrt_ps( x.val.m256f );
    return res;
}


inline smd::avx_i32_vt abs( const smd::avx_i32_vt &x )
{
    #ifdef __SIMD_USE_AVX2__

        return smd::avx_i32_vt( smd::v256( _mm256_abs_epi32( x.val.m256i ) ) );

    #else  // __SIMD_USE_AVX2__

        return smd::blend( x >= smd::avx_i32_vt::zero(), x, -x );

    #endif // __SIMD_USE_AVX2__
}


inline smd::avx_f32_vt abs( const smd::avx_f32_vt &x )
{
    return smd::blend( x >= smd::avx_f32_vt::zero(), x, -x );
}

}  // namespace std


