/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <immintrin.h>
#include <cfloat>
#include <type_traits>



namespace smd
{



typedef int32_t    v8si __attribute__ ((vector_size (32)));
typedef float32_t  v8sf __attribute__ ((vector_size (32)));

typedef int32_t    v4si __attribute__ ((vector_size (16)));
typedef float32_t  v4sf __attribute__ ((vector_size (16)));

typedef int64_t    v4di __attribute__ ((vector_size (32)));


typedef __m256i   m256i_t;
typedef __m256    m256f_t;

typedef __m128i   m128i_t;
typedef __m128    m128f_t;


#define avx_alignment_check( PTR ) simd_alignment_check( PTR, 0x1f )

static_assert( sizeof(m256i_t) == sizeof(v8si) );
static_assert( sizeof(m256f_t) == sizeof(v8si) );
static_assert( sizeof(v8sf) == sizeof(v8si) );

//----------------------------------------------------------------------------------


union alignas(sizeof(v4si)) v128
{
    v4si    si;
    v4sf    sf;
    
    m128i_t m128i;
    m128f_t m128f;


    inline v128() = default;
    inline v128( const v128& ) = default;
    inline v128( v128&& ) = default;
    inline v128& operator=( const v128& ) = default;
    inline v128& operator=( v128&& ) = default;


    inline v128( const m128i_t &init )
        : m128i( init )
    {}

    inline v128( const v4si &init )
        : si( init )
    {}

    inline v128( const v4sf &init )
        : sf( init )
    {}


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    //probably not required since __m128 and v4sf seem to be the same types
    template < typename U = m128f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline explicit v128( const m128f_t &init )
        : m128f( init )
    {}
#pragma GCC diagnostic pop

};

static_assert( sizeof(v128) == 16 );
static_assert( true == std::is_trivially_default_constructible<v128>::value );




union alignas(sizeof(v8si)) v256
{
    
    v8si    si;
    v8sf    sf;
    
    m256i_t m256i;
    m256f_t m256f;
    
    v128    hlf[2];

    v4di    di;
    

    inline v256() = default;
    inline v256( const v256& ) = default;
    inline v256( v256&& ) = default;
    inline v256& operator=( const v256& ) = default;
    inline v256& operator=( v256&& ) = default;


    inline v256( const v8si &init )
        : si( init )
    {
        avx_alignment_check( this );
    }

    inline v256( const v8sf &init )
        : sf( init )
    {
        avx_alignment_check( this );
    }

    inline v256( const v4di &init )
        : di( init )
    {
        avx_alignment_check( this );
    }

    inline v256( int32_t si1, int32_t si2, int32_t si3, int32_t si4,
                 int32_t si5, int32_t si6, int32_t si7, int32_t si8 )
        : m256i( _mm256_set_epi32( si1, si2, si3, si4, si5, si6, si7, si8 ) )
    {
        avx_alignment_check( this );
    }


    inline v256( float32_t sf1, float32_t sf2, float32_t sf3, float32_t sf4,
                 float32_t sf5, float32_t sf6, float32_t sf7, float32_t sf8 )
        : m256f( _mm256_set_ps( sf1, sf2, sf3, sf4, sf5, sf6, sf7, sf8 ) )
    {
        avx_alignment_check( this );
    }


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"

    // for some g++ m256i_t and v8si are the same types
    template < typename U = m256i_t,
               typename = typename std::enable_if< false == std::is_same<U,v8si>::value >::type>
    inline v256( const m256i_t &init )
        : m256i( init )
    {
        avx_alignment_check( this );
    }

    //probably not required since __m256 and v8sf seem to be the same types
    template < typename U = m256f_t,
               typename = typename std::enable_if< false == std::is_same<U,v8sf>::value >::type>
    inline v256( const m256f_t &init )
        : m256f( init )
    {
        avx_alignment_check( this );
    }
#pragma GCC diagnostic pop

};

static_assert( sizeof(v256) == 32 );
static_assert( true == std::is_trivially_default_constructible<v256>::value );

//----------------------------------------------------------------------------------


struct avx_b32_vt;
struct avx_i32_vt;
struct avx_f32_vt;


//----------------------------------------------------------------------------------


/*
 * AVX bool type
 */ 

struct alignas(sizeof(v256)) avx_b32_vt 
    : public vt_bool_base< v256, avx_b32_vt >
{
private:

    typedef vt_bool_base< v256, avx_b32_vt > base_t;

public:

    using typename base_t::elem_t;
    

    inline avx_b32_vt() = default;
    inline avx_b32_vt( const avx_b32_vt& ) = default;
    inline avx_b32_vt( avx_b32_vt&& ) = default;
    inline avx_b32_vt& operator=( const avx_b32_vt& ) = default;
    inline avx_b32_vt& operator=( avx_b32_vt&& ) = default;


    explicit inline avx_b32_vt( const v256 &f_initv )
        : vt_bool_base( f_initv )
    {}

    explicit inline avx_b32_vt( bool f_initv_b )
        : vt_bool_base( v256( _mm256_set1_epi32( bool_to_i32( f_initv_b ) ) ) )
    {}

    /// factory function from memory pointer
    static inline avx_b32_vt from_ptr( const bool32_t* f_elemPtr );


    constexpr int32_t bool_to_i32( bool f_b ) const { return ((f_b) ? int32_t(-1) : int32_t(0)); }

    // Type reinterpretation
    inline       avx_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      avx_i32_vt&>(*this); }
    inline const avx_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const avx_i32_vt&>(*this); }

    inline       avx_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      avx_f32_vt&>(*this); }
    inline const avx_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const avx_f32_vt&>(*this); }


    /// store content of vector in memory
    inline void store( bool32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( avx_b32_vt* f_stream_p ) const;
};


#ifdef ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

    // probably already implemented via gcc vector extension

    template <class... oTs>
    inline typename vt_base<oTs...>::derived_t 
    blend( const avx_b32_vt& f_mask, const vt_base<oTs...> &f_left, const vt_base<oTs...> &f_right );

#endif // ENFORCE_SIMD_HW_SPECIFIC_IMPLEMENTATION

inline bool all( const avx_b32_vt &x );
inline bool any( const avx_b32_vt &x );


//----------------------------------------------------------------------------------


/*
 * AVX int32 type
 */ 


struct alignas(sizeof(v256)) avx_i32_vt
    : public vt_i32< v256,
                     avx_b32_vt,
                     avx_i32_vt >
{

private:

    typedef vt_i32< v256,avx_b32_vt,avx_i32_vt > base_t;

public:

    using typename base_t::elem_t;
    

    inline avx_i32_vt() = default;
    inline avx_i32_vt( const avx_i32_vt& ) = default;
    inline avx_i32_vt( avx_i32_vt&& ) = default;
    inline avx_i32_vt& operator=( const avx_i32_vt& ) = default;
    inline avx_i32_vt& operator=( avx_i32_vt&& ) = default;
    

    explicit inline avx_i32_vt( const v256 &f_initv )
        : vt_i32( f_initv )
    {}

    explicit inline avx_i32_vt( int32_t f_initv_i32 )
        : vt_i32( v256( _mm256_set1_epi32( f_initv_i32 ) ) )
    {}

    // conversion from  float to inter must be explicit
    explicit inline avx_i32_vt( const avx_f32_vt &other );

    /// factory function from memory pointer
    static inline avx_i32_vt from_ptr( const int32_t* f_elemPtr );

    inline explicit operator const m256i_t() const { return val.m256i; }
    inline explicit operator const v8si() const { return val.si; }

    
    static inline avx_i32_vt zero();

    // type reinterpretation
    inline       avx_f32_vt& reinterpret_as_float32()       { return reinterpret_cast<      avx_f32_vt&>(*this); }
    inline const avx_f32_vt& reinterpret_as_float32() const { return reinterpret_cast<const avx_f32_vt&>(*this); }

#ifdef __SIMD_USE_AVX2__

    inline avx_i32_vt gather( const int32_t* f_from_a ) const;
    inline avx_f32_vt gather( const float32_t* f_from_a ) const;

#endif // __SIMD_USE_AVX2__

    /// store content of vector in memory
    inline void store( int32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( avx_i32_vt* f_stream_p ) const;
};


inline int32_t sum( const avx_i32_vt &x );
inline int32_t prod( const avx_i32_vt &x );


//----------------------------------------------------------------------------------


/*
 * AVX float32 type
 */ 

struct alignas(sizeof(v256)) avx_f32_vt
    : public vt_f32< v256, 
                     avx_b32_vt,
                     avx_f32_vt >
{

public:

    typedef vt_f32< v256,avx_b32_vt,avx_f32_vt > base_t;

    using typename base_t::elem_t;
    
    
    inline avx_f32_vt() = default;
    inline avx_f32_vt( const avx_f32_vt& ) = default;
    inline avx_f32_vt( avx_f32_vt&& ) = default;
    inline avx_f32_vt& operator=( const avx_f32_vt& ) = default;
    inline avx_f32_vt& operator=( avx_f32_vt&& ) = default;


    explicit inline avx_f32_vt( const v256 &f_initv )
        : vt_f32( f_initv )
    {}

    explicit inline avx_f32_vt( float32_t f_initv_f32 )
        : vt_f32( v256( _mm256_set1_ps( f_initv_f32 ) ) )
    {}

    // conversion from integer to float
    inline avx_f32_vt( const avx_i32_vt& other );

    /// factory function from memory pointer
    static inline avx_f32_vt from_ptr( const float32_t* f_elemPtr );

    inline explicit operator const v8sf() const { return val.sf; }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    template < typename U = m256f_t,
               typename = typename std::enable_if< false == std::is_same<U,v4sf>::value >::type>
    inline operator const m256f_t() const { return val.m256f; }
#pragma GCC diagnostic pop

    
    static inline avx_f32_vt zero();

    // type reinterpretation
    inline       avx_i32_vt& reinterpret_as_int32()       { return reinterpret_cast<      avx_i32_vt&>(*this); }
    inline const avx_i32_vt& reinterpret_as_int32() const { return reinterpret_cast<const avx_i32_vt&>(*this); }

    // FMA instruction based operators
    #ifdef __SIMD_USE_FMA__

        // Don't forget that we have already some operator we like to continue using
        using base_t::operator+=;
        using base_t::operator-=;

        template <typename MT>
        inline const avx_f32_vt& operator+=( const simd_multiply_base<avx_f32_vt,MT> &other );

        template <typename MT>
        inline const avx_f32_vt& operator-=( const simd_multiply_base<avx_f32_vt,MT> &other );

    #endif // __SIMD_USE_FMA__

    /// store content of vector in memory
    inline void store( float32_t* f_stream_p ) const;

    // non-temporal memory hinting storing routine
    inline void nontemporal_store( avx_f32_vt* f_stream_p ) const;
};


inline float32_t sum( const avx_f32_vt &x );
inline float32_t prod( const avx_f32_vt &x );


}  // namespace smd


