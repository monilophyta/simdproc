/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <immintrin.h>
#include "simd_avx.hpp"



namespace smd
{

namespace __avx2_helper
{


#define AVX_GATHER_FUNC( RETVAL, FNAME, AVXFNAME, PTR_T, PARAM_T ) \
struct FNAME { \
    inline RETVAL operator()( const PTR_T* base_addr, const m256i_t& vindex ) const \
    { \
        return _ ## AVXFNAME ( reinterpret_cast<const PARAM_T*>(base_addr), vindex, sizeof(PARAM_T) ); \
    } \
};

AVX_GATHER_FUNC( m256i_t, avx_i32gather_b32, mm256_i32gather_epi32, bool32_t,   int32_t )
AVX_GATHER_FUNC( m256i_t, avx_i32gather_i32, mm256_i32gather_epi32, int32_t,    int32_t )
AVX_GATHER_FUNC( m256f_t, avx_i32gather_f32, mm256_i32gather_ps,    float32_t,  float32_t )

AVX_GATHER_FUNC( m128i_t, avx_i64gather_b32, mm256_i64gather_epi32, bool32_t,   int32_t   )
AVX_GATHER_FUNC( m128i_t, avx_i64gather_i32, mm256_i64gather_epi32, int32_t,    int32_t   )
AVX_GATHER_FUNC( m128f_t, avx_i64gather_f32, mm256_i64gather_ps,    float32_t,  float32_t )



static_assert( sizeof(bool32_t) == sizeof(int32_t) );

} // namespace __avx2_helper

} // namespace smd
