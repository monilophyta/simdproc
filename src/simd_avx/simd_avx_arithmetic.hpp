/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace smd
{

inline smd::avx_f32_vt rcp( const smd::avx_f32_vt &x );
inline smd::avx_f32_vt rsqrt( const smd::avx_f32_vt &x );


inline smd::avx_f32_vt ceil( const smd::avx_f32_vt &x );
inline smd::avx_f32_vt floor( const smd::avx_f32_vt &x );
inline smd::avx_f32_vt round( const smd::avx_f32_vt &x );
inline smd::avx_f32_vt trunc( const smd::avx_f32_vt &x );

inline smd::avx_i32_vt max( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y );
inline smd::avx_i32_vt min( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y );

inline smd::avx_f32_vt max( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y );
inline smd::avx_f32_vt min( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y );

} // namespace smd





namespace std
{

inline smd::avx_i32_vt max( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y ) { return smd::max(x,y); }
inline smd::avx_i32_vt min( const smd::avx_i32_vt &x, const smd::avx_i32_vt &y ) { return smd::min(x,y); }

inline smd::avx_f32_vt max( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y ) { return smd::max(x,y); }
inline smd::avx_f32_vt min( const smd::avx_f32_vt &x, const smd::avx_f32_vt &y ) { return smd::min(x,y); }

inline smd::avx_f32_vt log( const smd::avx_f32_vt &x );
inline smd::avx_f32_vt sqrt( const smd::avx_f32_vt &x );

inline smd::avx_i32_vt abs( const smd::avx_i32_vt &x );
inline smd::avx_f32_vt abs( const smd::avx_f32_vt &x );

} // namespace std
