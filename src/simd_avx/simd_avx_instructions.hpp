/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <immintrin.h>


namespace smd
{


struct mm256_instructions
{
    typedef mm256_instructions this_t;
    
    //typedef smd::v8sf    vf32;
    typedef smd::m256f_t vf32;
    typedef smd::m256i_t vi32;

    // --------------------------------------------------------------------------------------------------
    // directly forwarded instructions

    #define AVX256_INSTR_P( INFIX, POSTFIX ) \
    template<class ... Types> \
    inline static constexpr auto INFIX(Types ... args) { return _mm256_ ## INFIX ## POSTFIX ( args...); }

    #define AVX256_INSTR( INFIX ) \
    template<class ... Types> \
    inline static constexpr auto INFIX(Types ... args) { return _mm256_ ## INFIX ( args...); }


    AVX256_INSTR( add_ps )            // AVX
    AVX256_INSTR( and_ps )            // AVX
    AVX256_INSTR_P( castps, _si256 )  // AVX
    //AVX256_INSTR( cmp_ps )            // AVX  - gives error with -O0
    AVX256_INSTR( cvtepi32_ps )       // AVX
    AVX256_INSTR( max_ps )            // AVX
    AVX256_INSTR( mul_ps )            // AVX
    AVX256_INSTR( or_ps )             // AVX
    AVX256_INSTR( set1_epi32 )        // AVX
    AVX256_INSTR( set1_ps )           // AVX
    AVX256_INSTR( setzero_ps )        // AVX
    AVX256_INSTR( srli_epi32 )        // AVX2
    AVX256_INSTR( sub_epi32 )         // AVX2
    AVX256_INSTR( sub_ps )            // AVX

    #undef AVX256_INSTR

    template <int CMP_PARAM>
    inline static constexpr auto cmp_ps(const vf32& x, const vf32& y )
    {
        return _mm256_cmp_ps( x, y, CMP_PARAM );
    }

    // --------------------------------------------------------------------------------------------------
    // mapping comparison operations to SSE style

    #define CMP_OP_INSTR( NAME, CMP_PARAM ) \
    inline static constexpr auto NAME ## _ps ( const vf32& x, const vf32& y ) { return this_t::cmp_ps<CMP_PARAM>(x, y ); }

    CMP_OP_INSTR( cmplt, _CMP_LT_OS )
    CMP_OP_INSTR( cmple, _CMP_LE_OS )

    #undef CMP_OP_INSTR

};


} // namespace smd
